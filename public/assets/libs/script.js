Dropzone.autoDiscover = false;

var myDropzone = new Dropzone(document.getElementById('product-image-drpzone'), {
  uploadMultiple: false,
  acceptedFiles: '.jpg,.png,.jpeg,.gif',
  parallelUploads: 5,
  maxFiles: 5,
  url: 'yourUrl',
  previewsContainer: '#previews',
  thumbnailWidth: 100,
  thumbnailHeight: 100,

  init: function() {

    // This adds the bootstrap class col-sm-6 to the dropzone preview
    var classIndex = this.options.previewTemplate.indexOf('dz-preview');
    this.options.previewTemplate = this.options.previewTemplate.slice(0, classIndex) + 'col-sm-6 ' + this.options.previewTemplate.slice(classIndex);

    this.on('addedfile', function(file) {
      $('.drp-image').first().remove();
    });
  }
});