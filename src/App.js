import React, { Component, Fragment } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import {
  MyHeader,
  Home,
  Listing,
  Promotions,
  Financing,
  Shop,
  Dealers,
  Advancesearch,
  FAQ,
  PrivacyPolicy,
  TermsOfUse,
  ContactUs,
  Advertise,
  Careers,
  OurPartners,
  PlaceAnAd,
  PostAd,
  Newsone,
  Newstwo,
  Newsthree,
  LoginSignup,
  Forgotpassword,
  Changepassword,
  Adpostdetail,
  EditProfile,
  MyAds,
  EditPostAd,
  DraftListing,
  EditDraftAd,
  MySavedAds,
  MyPromoOffers,
  CreatePromotions,
  EditPromotions,
  AdPostNewCar,
  AdPostNewBike,
  DealerDetail,
  MyPackages,
  DealerRegistration,
  RegistrationSuccess,
  CreateUser,
  EditUser,
  ViewUser,
  MyUsers,
  MyBranches,
  CompareVehicle,
  VehicleCategory,
  AdPostUsedBike,
  ErrorFile,
  ResetPassword,
  MyFooter,
  FeedbackLeave,
  Adpostdetailbike,
  BackToTop,
  Popover,
  CreateBranch,
  VehicleType,
  ModalSuccess
} from "./pages";

import { Provider } from "./Context";
class App extends Component {
  constructor(props) {
    super(props);
    this.timer = null;
    this.state = {
      //advsearch 27params
      ssellertype: 0,
      vehicleTypeID: 2,
      vehicleCategoryID: 1,
      scity: 0,
      sowner: 0,
      minVal: "NA",
      maxVal: "NA",
      smake: 0,
      smodel: 0,
      sversion: 0,
      versionyearfrom: "NA",
      versionyearto: "NA",
      sassembly: "NA",
      bodytypeid: 0,
      transmissionid: 0,
      mileagemin: "NA",
      mileagemax: "NA",
      extcolorid: 0,
      sdoormin: "NA",
      sdoormax: "NA",
      sdriveside: "NA",
      smetermin: "NA",
      smetermax: "NA",
      sengsizemin: "NA",
      sengsizemax: "NA",
      senginetype: "NA",
      spic: true,

      //promo listing
      searchkeyword: "",
      //dealer listing
      searchname: "",

      //advsearchpromo 3params
      cityString: "0",
      dealerString: "0",
      selpic: "true",

      openfeedbackmodal: false,
      openpopover: false,
      openmodal: false,
      modalres: '',
    };
  }

  componentDidMount() {
    this.timer = setTimeout(
      function () {
        this.setState({ openfeedbackmodal: true });
      }.bind(this),
      1800000
    );
  }
  componentWillUnmount() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
  }

  handleParentListingClick = (vehiclecategoryid, vehicletypeid, props) => {
    this.setState({
      vehicleCategoryID: vehiclecategoryid,
      vehicleTypeID: vehicletypeid
    });
    props.history.push("/vehicle-listing");
  };

  handleParentVehicletypeId = selVehicleTypeID => {
    this.setState({
      vehicleTypeID: selVehicleTypeID
    });
  };

  handleParentQuickSrchAdPost = (
    ssellertype,
    vehicleTypeID,
    scity,
    sowner,
    minVal,
    maxVal,
    smake,
    smodel,
    sversion,
    versionyearfrom,
    versionyearto,
    sassembly,
    bodytypeid,
    transmissionid,
    mileagemin,
    mileagemax,
    extcolorid,
    sdoormin,
    sdoormax,
    sdriveside,
    smetermin,
    smetermax,
    sengsizemin,
    sengsizemax,
    senginetype,
    spic,
    vehicleCategoryID,
    reset
  ) => {
    console.log(
      ssellertype,
      vehicleTypeID,
      scity,
      sowner,
      minVal,
      maxVal,
      smake,
      smodel,
      sversion,
      versionyearfrom,
      versionyearto,
      sassembly,
      bodytypeid,
      transmissionid,
      mileagemin,
      mileagemax,
      extcolorid,
      sdoormin,
      sdoormax,
      sdriveside,
      smetermin,
      smetermax,
      sengsizemin,
      sengsizemax,
      senginetype,
      spic,
      vehicleCategoryID
    );
    this.setState({
      ssellertype: ssellertype,
      vehicleTypeID: reset === 'reset' ? vehicleTypeID : this.state.vehicleTypeID,
      scity: scity,
      sowner: sowner,
      minVal: minVal,
      maxVal: maxVal,
      smake: smake,
      smodel: smodel,
      sversion: sversion,
      versionyearfrom: versionyearfrom,
      versionyearto: versionyearto,
      sassembly: sassembly,
      bodytypeid: bodytypeid,
      transmissionid: transmissionid,
      mileagemin: mileagemin,
      mileagemax: mileagemax,
      extcolorid: extcolorid,
      sdoormin: sdoormin,
      sdoormax: sdoormax,
      sdriveside: sdriveside,
      smetermin: smetermin,
      smetermax: smetermax,
      sengsizemin: sengsizemin,
      sengsizemax: sengsizemax,
      senginetype: senginetype,
      spic: spic,
      vehicleCategoryID: vehicleCategoryID,
    });
  };

  //promolisting, vehiclelisting
  handleParentSearchKeyword = searchkeyword => {
    console.log(searchkeyword);
    this.setState({
      searchkeyword: searchkeyword
    });
  };

  handleParentAdvSearchPromo = (cityString, dealerString, selpic) => {
    console.log(cityString, dealerString, selpic);

    this.setState({
      cityString: cityString,
      dealerString: dealerString,
      selpic: selpic
    });
  };

  //dealerlisting
  handleParentSearchname = searchname => {
    console.log(searchname);
    this.setState({
      searchname: searchname,
      searchbyname: "searchbyname",
      reset: "",
      advsearch: ""
    });
  };

  handleParentAdvSearchDealer = (cityid, makeString, selPicDealer) => {
    console.log(cityid, makeString, selPicDealer);
    this.setState({
      cityid: cityid,
      makeString: makeString,
      selPicDealer: selPicDealer,
      advsearch: "advsearch",
      reset: "",
      searchbyname: ""
    });
  };

  handleParentResetSettings = (
    cityid,
    makeString,
    selPicDealer,
    searchname
  ) => {
    console.log(
      cityid,
      makeString,
      selPicDealer,
      searchname,
      "cityid, makeString, selPicDealer, searchname"
    );
    if (cityid === "" && makeString === "" && selPicDealer === "") {
      return;
    }
    this.setState({
      cityid: "0",
      makeString: "0",
      selPicDealer: true,
      searchname: "",
      reset: "reset",
      searchbyname: "",
      advsearch: ""
    });
  };

  //feedback
  handleParentFeedbackModal = boolean => {
    this.setState({
      openfeedbackmodal: boolean
    });
  };
  //popover
  handleParentOpenPopover = boolean => {
    this.setState({
      openpopover: boolean
    });
  };
  //modalresponse
  handleParentOpenModal = (bool, res) => {
    this.setState({
      openmodal: bool,
      modalres: res
    });
  };
  render() {
    const contextValue = {
      data: this.state,
      //usedcars listing
      handleChildVehicletypeId: this.handleParentVehicletypeId,
      handleChildQuickSrchAdPost: this.handleParentQuickSrchAdPost,
      //promo listing
      handleChildSearchKeyword: this.handleParentSearchKeyword,
      handleChildAdvSearchPromo: this.handleParentAdvSearchPromo,
      //dealer listing
      handleChildSearchName: this.handleParentSearchname,
      handleChildAdvSearchDealer: this.handleParentAdvSearchDealer,
      handleChildResetSettings: this.handleParentResetSettings,
      //signin popup
      handleChildOpenPopover: this.handleParentOpenPopover,
      //modal response
      handleChildOpenModal: this.handleParentOpenModal
    };
    return (
      <Provider value={contextValue}>
        <Router>
          <Fragment>
            <BackToTop scrollStepInPx="50" delayInMs="16.66" />
            <Popover />
            <ModalSuccess />
            <Route render={props => <PlaceAnAd {...props} />} />

            <Route
              render={props => (
                <FeedbackLeave
                  openfeedbackmodal={this.state.openfeedbackmodal}
                  handleParentFeedbackModal={this.handleParentFeedbackModal}
                  {...props}
                />
              )}
            />
            <Route
              render={props => (
                <MyHeader
                  {...props}
                  handleParentListingClick={this.handleParentListingClick}
                />
              )}
            />

            <div className="appFrame">
              <Switch>
                <Route path="/" exact render={props => <Home {...props} />} />
                <Route
                  path="/vehicle-listing"
                  exact
                  render={props => <Listing {...props} />}
                />
                <Route
                  path="/compare-vehicle"
                  exact
                  render={props => <CompareVehicle {...props} />}
                />
                <Route
                  path="/promotions"
                  exact
                  render={props => <Promotions {...props} />}
                />
                <Route
                  path="/financing"
                  exact
                  render={props => <Financing {...props} />}
                />
                <Route
                  path="/shop"
                  exact
                  render={props => <Shop {...props} />}
                />
                <Route
                  path="/dealers"
                  exact
                  render={props => <Dealers {...props} />}
                />
                <Route
                  path="/dealer-detail"
                  exact
                  render={props => <DealerDetail {...props} />}
                />
                <Route
                  path="/edit-profile"
                  exact
                  render={props => <EditProfile {...props} />}
                />
                <Route
                  path="/editpost-ad"
                  exact
                  render={props => <EditPostAd {...props} />}
                />
                <Route
                  path="/my-ads"
                  exact
                  render={props => <MyAds {...props} />}
                />
                <Route
                  path="/draft-listing"
                  exact
                  render={props => <DraftListing {...props} />}
                />
                <Route
                  path="/editdraft-ad"
                  exact
                  render={props => <EditDraftAd {...props} />}
                />
                <Route
                  path="/mysaved-ads"
                  exact
                  render={props => <MySavedAds {...props} />}
                />
                <Route
                  path="/my-packages"
                  exact
                  render={props => <MyPackages {...props} />}
                />
                <Route
                  path="/dealerofferpromotions"
                  exact
                  render={props => <MyPromoOffers {...props} />}
                />
                <Route
                  path="/create-promotion"
                  exact
                  render={props => <CreatePromotions {...props} />}
                />
                <Route
                  path="/edit-promotion"
                  exact
                  render={props => <EditPromotions {...props} />}
                />
                <Route
                  path="/my-users"
                  exact
                  render={props => <MyUsers {...props} />}
                />
                <Route
                  path="/create-user"
                  exact
                  render={props => <CreateUser {...props} />}
                />
                <Route
                  path="/edit-user"
                  exact
                  render={props => <EditUser {...props} />}
                />
                <Route
                  path="/view-user"
                  exact
                  render={props => <ViewUser {...props} />}
                />
                <Route
                  path="/my-branches"
                  exact
                  render={props => <MyBranches {...props} />}
                />
                <Route
                  path="/create-branch"
                  exact
                  render={props => <CreateBranch {...props} />}
                />
                <Route
                  path="/dealer-registration"
                  exact
                  render={props => <DealerRegistration {...props} />}
                />
                <Route
                  path="/advance-search"
                  exact
                  render={props => <Advancesearch {...props} />}
                />
                <Route path="/faq" exact render={props => <FAQ {...props} />} />
                <Route
                  path="/privacy-policy"
                  exact
                  render={props => <PrivacyPolicy {...props} />}
                />
                <Route
                  path="/terms-conditions"
                  exact
                  render={props => <TermsOfUse {...props} />}
                />
                <Route
                  path="/contact-us"
                  exact
                  render={props => <ContactUs {...props} />}
                />
                <Route
                  path="/advertise"
                  exact
                  render={props => <Advertise {...props} />}
                />
                <Route
                  path="/careers"
                  exact
                  render={props => <Careers {...props} />}
                />
                <Route
                  path="/our-partners"
                  exact
                  render={props => <OurPartners {...props} />}
                />
                <Route
                  path="/adpost-usedbike"
                  exact
                  render={props => <AdPostUsedBike {...props} />}
                />
                <Route
                  path="/adpost-usedcar"
                  exact
                  render={props => <PostAd {...props} />}
                />
                <Route
                  path="/adpost-newcar"
                  exact
                  render={props => <AdPostNewCar {...props} />}
                />
                <Route
                  path="/adpost-newbike"
                  exact
                  render={props => <AdPostNewBike {...props} />}
                />
                <Route
                  path="/news-one"
                  exact
                  render={props => <Newsone {...props} />}
                />
                <Route
                  path="/news-two"
                  exact
                  render={props => <Newstwo {...props} />}
                />
                <Route
                  path="/news-three"
                  exact
                  render={props => <Newsthree {...props} />}
                />
                <Route
                  path="/postad-detail"
                  render={props => <Adpostdetail {...props} />}
                />
                <Route
                  path="/postad-detailbike"
                  render={props => <Adpostdetailbike {...props} />}
                />
                <Route
                  path="/login-signup"
                  render={props => <LoginSignup {...props} />}
                />
                <Route
                  path="/registration-success"
                  render={props => <RegistrationSuccess {...props} />}
                />
                <Route
                  path="/forgot-password"
                  exact
                  render={props => <Forgotpassword {...props} />}
                />
                <Route
                  path="/reset-password"
                  exact
                  render={props => <ResetPassword {...props} />}
                />
                <Route
                  path="/change-password"
                  render={props => <Changepassword {...props} />}
                />
                <Route
                  path="/vehicle-category"
                  exact
                  render={props => <VehicleCategory {...props} />}
                />
                <Route
                  path="/vehicle-type"
                  exact
                  render={props => <VehicleType {...props} />}
                />
                <Route component={ErrorFile} />
              </Switch>
            </div>
            <Route
              render={props => (
                <MyFooter
                  {...props}
                  handleParentFeedbackModal={this.handleParentFeedbackModal}
                  handleParentListingClick={this.handleParentListingClick}
                />
              )}
            />
          </Fragment>
        </Router>
      </Provider>
    );
  }
}
export default App;
// w.irfan@techarabiya.com
// 12345678