const apiBaseURL = "https://cararabiya.ahalfa.com/api";
const apiUrlcity = "https://cararabiya.ahalfa.com/api/city?countryID=184";
const apiUrlmake = "https://cararabiya.ahalfa.com/api/make";
const apiUrlfueltype = "https://cararabiya.ahalfa.com/api/FuelType";
const apiUrlvehicletype = "https://cararabiya.ahalfa.com/api/VehicleType";
const apiUrltransmission = "https://cararabiya.ahalfa.com/api/vehicletransmission";
const apiUrlAttribute = "https://cararabiya.ahalfa.com/api/Attribute?VehicleCategoryID=1";
const apiUrlAttributeBike = "https://cararabiya.ahalfa.com/api/Attribute?VehicleCategoryID=4";

const apiUrlBodytype = "https://cararabiya.ahalfa.com/api/bodytype?VehicleCategoryID=1";
const apiUrlBodytypeUsedBike = "https://cararabiya.ahalfa.com/api/bodytype?VehicleCategoryID=4";
const apiUrlUserPackageAdPost = "https://cararabiya.ahalfa.com/api/UserPackageAdPost/GetPackageAdPostbyUserID?UserID=98";
//const apiLogin = "http://localhost:53428/api/UserLogin";
const apiLogin = "https://cararabiya.ahalfa.com/api/UserLogin/NewLogin";
const apiSocialLogin = "https://localhost:53428/api/RegisterUser";
const apiUrldealer = "https://cararabiya.ahalfa.com/api/Dealer";
const apiGetUserByEmail =  "https://cararabiya.ahalfa.com/api/UserLogin?email=";
const apiForgetPassword =  "https://cararabiya.ahalfa.com/api/UserResetPassword/forgetpassword?";
const apiSetForgetPassword =  "https://cararabiya.ahalfa.com/api/UserResetPassword?";
const apiChangePassword =  "https://cararabiya.ahalfa.com/api/User?";
const apiUrlenginetype =  "https://cararabiya.ahalfa.com/api/enginetype";
const apiUrlsellertype = "https://cararabiya.ahalfa.com/api/usertype";
const apiUrlmodel = "https://cararabiya.ahalfa.com/api/model";
const apiUrlversion = "https://cararabiya.ahalfa.com/api/vehicleversion";
export {
    apiBaseURL, apiUrlcity, apiUrlmake, apiUrlfueltype, apiUrlvehicletype, 
    apiUrltransmission, apiUrlenginetype, apiUrlAttribute, apiUrlBodytype, apiUrlUserPackageAdPost, 
    apiLogin, apiSocialLogin, apiUrldealer, apiGetUserByEmail, apiForgetPassword, apiSetForgetPassword,  
    apiChangePassword, apiUrlsellertype, apiUrlmodel, apiUrlversion,apiUrlAttributeBike,apiUrlBodytypeUsedBike
} 