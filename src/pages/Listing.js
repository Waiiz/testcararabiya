import React, { Component, Fragment } from "react";
import {
  Listingsearch,
  Listingadvertismenttop,
  Listinglatestadsleft,
  Listingadvertismentleft,
  ListingSearchkeyword,
  Listinglist
} from "../components/listing";
import Dealeradvertisement from "./Dealeradvertisement";

class Listing extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <Fragment>
        <div className="container mt-5">
          <div className="row">
            <div className="col-12">
              <Listingadvertismenttop />
            </div>
          </div>

          <div className="row">
            <div className="col-lg-3">
              <aside className="l-sidebar">
                <ListingSearchkeyword />
                <div className="accordion" id="accordion-2">
                  <Listingsearch />

                  <div className="widget section-sidebar bg-light">
                    <div className="widget-content">
                      {/* quick search widget inner close */}
                    </div>{" "}
                    {/* advance search widget inner close */}
                    <div className="container-fluid advertise-advance">
                      <Listingadvertismentleft />
                    </div>
                    <div className="container-fluid advertise-advance">
                      <div
                        className="container py-3"
                        style={{
                          backgroundColor: "#e9e9e9",
                          borderTop: "1px solid #cacaca"
                        }}
                      >
                        <div className="row mb-3 mt-2">
                          <span
                            style={{
                              fontSize: "14px",
                              fontWeight: 600,
                              color: "#000"
                            }}
                          >
                            Related Ads
                          </span>{" "}
                        </div>
                        <Listinglatestadsleft />
                        <Dealeradvertisement />
                      </div>
                    </div>
                  </div>
                </div>
                {/* end .b-filter*/}
                {/* end .b-brands*/}
              </aside>
            </div>
            <div className="col-lg-9">
              <Listinglist />
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Listing;