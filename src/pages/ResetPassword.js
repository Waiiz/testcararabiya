import React, { useState, useEffect } from "react";
import "./Forgotpassword.css";
import axios from "axios";
import { apiBaseURL } from "../ApiBaseURL";
import { Modal } from "react-bootstrap";
import md5 from "md5";

const ResetPassword = (props) => {
  const [isShowPass, setIsShowPass] = useState("password");
  const [password, setPassword] = useState("");
  const [status, setStatus] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [showMessage, setShowMessage] = useState("");
  const [confirmpassword, setConfirmPassword] = useState("");
  const [isShowConfPass, setIsShowConfPass] = useState("password");

  useEffect(() => {
    window.scrollTo(0, 0);
    handleLogout();
  }, []);

  const handleLogout = () => {
    let userData = localStorage.getItem("user");
    if (userData === null) {
      console.log('LS null');
    } else {
      localStorage.clear();
    }
  }

  const handleResetPassword = (event) => {
    event.preventDefault();

    if (password !== confirmpassword) {
      setShowModal(true);
      setShowMessage("Passwords do not match.");
      return;
    }
    const params = new URLSearchParams(window.location.search);
    var id = params.get('id');
    var token = params.get('token');
    if (id === null || token === null) {
      setShowModal(true);
      setShowMessage("Reset password link is incorrect.");
      return;
    }
    const url = `${apiBaseURL}/UserResetPassword/forgetpasswordset`;
    var encpassword = md5(password + "s0mRIdlKvI");
    let ApiParamForResetPass = {
      userid: id,
      code: token,
      Password: encpassword
    };
    axios
      .post(url, null, {params: ApiParamForResetPass} )
      .then((res) => {
        if (res.status === 200) {
          setStatus(res.status);
          setShowModal(true);
          setShowMessage("Awesome. you've successfully updated your password.");
        } else {
          console.log(res.status, "statuscode from Reset Pass");
        }
      })
      .catch((error) => {
        setShowModal(true);
        setShowMessage("Something went wrong.");
        console.log(error, "from Reset Pass");
      });
  };

  const handleCloseModal = () => {
    if (status === 200) {
      setShowModal(false);
      props.history.push("/login-signup");
    } else {
      setShowModal(false);
    }
  };

  return (
    <>
      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Cararabiya</Modal.Title>
        </Modal.Header>
        <Modal.Body>{showMessage}</Modal.Body>
      </Modal>
      <div className="logmod__wrapper">
        <div className="logmod__container">
          <div className="logmod__heading">
            <span className="logmod__heading-password mt-4 mb-4">
              <strong>Reset Password</strong>
            </span>
          </div>
          <form onSubmit={handleResetPassword}>
          <ul className="logmod__tabs">
            <li data-tabtar="lgm-4">
              <div className="sminputs">
                <div className="input full">
                  <label className="string optional" htmlFor="user-pw">
                    Password *
                  </label>
                  <input
                    required
                    className="string optional"
                    maxLength={255}
                    id="user-pw"
                    placeholder="Create Password"
                    type={isShowPass}
                    size={50}
                    value={password}
                    onChange={(e) => {
                      setPassword(e.target.value);
                    }}
                  />
                  <span
                    onClick={() => {
                      isShowPass == "password"
                        ? setIsShowPass("text")
                        : setIsShowPass("password");
                    }}
                    className="hide-password"
                  >
                    {isShowPass === "password" ? (
                      <i class="fas fa-eye" />
                    ) : (
                      <i className="fas fa-eye-slash" />
                    )}
                  </span>
                </div>
              </div>

              <div className="sminputs">
                <div className="input full">
                  <label className="string optional" htmlFor="user-pw">
                    CONFIRM PASSWORD *
                  </label>
                  <input
                    required
                    className="string optional"
                    maxLength={255}
                    id="user-pw"
                    placeholder="Confirm Password"
                    type={isShowConfPass}
                    size={50}
                    value={confirmpassword}
                    onChange={(e) => {
                      setConfirmPassword(e.target.value);
                    }}
                  />

                  <span
                    onClick={() => {
                      isShowConfPass === "password"
                        ? setIsShowConfPass("text")
                        : setIsShowConfPass("password");
                    }}
                    className="hide-password"
                  >
                    {isShowConfPass === "password" ? (
                      <i class="fas fa-eye" />
                    ) : (
                      <i className="fas fa-eye-slash" />
                    )}
                  </span>
                </div>
              </div>

              <div className="d-flex justify-content-center mb-5 mt-5">
                <button
                  style={{
                    border: "none",
                    backgroundColor: "#D3E428",
                    color: "white",
                  }}
                  type="submit"
                  role="button"
                  className="btn btn-block"
                >
                  RESET PASSWORD
                </button>
              </div>
            </li>
          </ul>
          </form>
        </div>
      </div>
    </>
  );
};

export default ResetPassword;