import React, { Fragment, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "./LoginSignup.css";
import { apiBaseURL, apiLogin, apiSocialLogin } from "../ApiBaseURL";
import axios from "axios";
import md5 from "md5";
import GoogleLogin from "react-google-login";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import { Redirect } from "react-router-dom";
import ForgotPassword from "./Forgotpassword";
import { Tabs, Tab } from "react-bootstrap";
import "./OTP.css";
import OtpInput from "react-otp-input";
import { Modal } from "react-bootstrap";

const LoginSignUp = (props) => {
  const [key, setKey] = useState("signin");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loginSuccess, setLoginSuccess] = useState(false);
  const [isShowPass, setIsShowPass] = useState("password");
  const [isForgetPassword, SetIsForgetPassword] = useState(false);
  const [gender, setGender] = useState("Male");
  const [firstname, setFirstName] = useState("");
  const [lastname, setLastName] = useState("");
  const [signinuserid, setSigninUserId] = useState("");
  const [signinuseremail, setSigninUserEmail] = useState("");
  const [useremail, setUserEmail] = useState("");
  const [mobile, setMobile] = useState("");
  const [cnic, setCnic] = useState("");
  const [userpassword, setUserPassword] = useState("");
  const [isShowConfPass, setIsShowConfPass] = useState("password");
  const [confirmpassword, setConfirmPassword] = useState("");
  const [isHover, SetIsHover] = useState(false);
  const [isUserRegister, SetIsUserRegister] = useState(false);
  const [registerid, setRegisterId] = useState("");
  const [otp, setOTP] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [showMessage, setShowMessage] = useState("");

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const handleLogout = () => {
    let userData = localStorage.getItem("user");
    if (userData === null) {
      console.log('LS null');
    } else {
      localStorage.clear();
      props.history.push("/login-signup");
    }
  }

  const handleCloseModal = () => setShowModal(false);
  const handleUserRegistration = (event) => {
    event.preventDefault();
    console.log(
      gender,
      firstname,
      lastname,
      useremail,
      mobile,
      userpassword,
      confirmpassword
    );

    if (userpassword !== confirmpassword) {
      setShowModal(true);
      setShowMessage("Passwords do not match.");
      return;
    }

    const url = `${apiBaseURL}/user`;
    let ApiParamForUser = {
      FirstName: firstname,
      MiddleName: "NA",
      LastName: lastname,
      Email: useremail,
      Mobile: mobile,
      aqamaNo_CNIC: cnic,
      Phone: "NA",
      Password: md5(userpassword + "s0mRIdlKvI"),
      Address1: "NA",
      Address2: "NA",
      UserTypeID: 1,
      CountryID: 184,
      CityID: -1,
      Area: 0,
      IsActive: "false",
      CreatedBy: 0,
      CreatedDate: "1900-01-01T00:00:00",
      DOB: "1900-01-01T00:00:00",
      Registeredat: "ca",
      ImageUrl: "NA",
      Gender: gender,
      BranchID: 0,
      CARegistrationID: 0,
      RoleID: 5,
    };
    axios
      .post(url, ApiParamForUser)
      .then((res) => {
        console.log(res.data, "userid from user api");

        if (res.status === 200) {
          setRegisterId(res.data);
          handleUserResetPasswordApi(res.data, useremail);
        } else {
          console.log(res.status, "statuscode from user api");
        }
      })
      .catch((error) => {
        setShowModal(true);
        setShowMessage("User already exist");
        console.log(error, "from user api");
      });
  };

  const handleUserResetPasswordApi = (userid, useremail) => {
    const url = `${apiBaseURL}/UserResetPassword?UserID=${userid}&Email=${useremail}`;
    axios
      .post(url)
      .then((res) => {
        console.log(userid, res.data, "UserResetPassword");
        setRegisterId(userid);
        if (res.status === 200) {
          setShowModal(true);
          setShowMessage(
            "A verification code has been sent to your Email Address."
          );
          setKey("userotp");
        } else {
          console.log(res.status, "statuscode from UserResetPassword api");
        }
      })
      .catch((error) => {
        console.log(error, "from UserResetPassword api");
      });
  };

  const handleUserLogin = () => {
    console.log(registerid, otp, "handleUserLogin");
    const url = `${apiBaseURL}/userlogin?UserID=${registerid}&Code=${otp}`;
    axios
      .post(url)
      .then((res) => {
        console.log(res, 'handleUserLogin');
        if (res.status === 200) {
          handleFetchOrder();
          props.history.push("/registration-success");
        } else {
          setShowModal(true);
          setShowMessage("Incorrect Verification Code");
        }
      })
      .catch((error) => {
        console.log(error, "from userlogin api");
      });
  };

  const handleFetchOrder = () => {
    const url = `${apiBaseURL}/Order/DefaultOrder?UserID=${registerid}`;
    axios
      .post(url)
      .then((res) => {
        if (res.data === "Success") {
          props.history.push("/registration-success");
        } else {
          setShowModal(true);
          setShowMessage("Something went wrong in order");
        }
      })
      .catch((error) => {
        console.log(error, "from Order api");
      });
  };

  const handleChangeGender = (event) => {
    setGender(event.target.value);
  };

  const handleLogin = () => {
    axios({
      method: "POST",
      url: apiLogin,
      data: {
        Email: email,
        Password: md5(password + "s0mRIdlKvI"),
      },
    }).then(
      (res) => {
        if (res && res.status == 200) {
          console.log("------------>", res, signinuserid, signinuseremail);
          if (Array.isArray(res["data"])) {

            if (res["data"][0]["IsActive"]) {
              localStorage.setItem("user", JSON.stringify(res["data"]));
              setLoginSuccess(true);
            } else {
              handleUserResetPasswordApi(signinuserid, signinuseremail);
            }
          } else {
            console.log(res.status, "user login");
          }
        }
      },
      (error) => {
        setShowModal(true);
        setShowMessage("Sign-in unsuccessful, Please try again.");
        console.log(error);
      }
    );
  };
  const responseGoogle = (response) => {
    console.log(response);
    if (response && response["googleId"]) {
      axios({
        method: "POST",
        url: apiSocialLogin,
        data: {
          DealerID: 0,
          registeredAt: "gl",
          Email: response["profileObj"]["email"],
          FirstName: response["profileObj"]["givenName"],
          LastName: response["profileObj"]["familyName"],
          ImageUrl: response["profileObj"]["imageUrl"],
          CreatedBy: 0,
          UserTypeID: 1,
          IsActive: true,
        },
      }).then(
        (res) => {
          if (res && res.status == 200) {
            console.log("------------>", res);
            if (Array.isArray(res["data"])) {
              if (res["data"][0]["IsActive"]) {
                localStorage.setItem("user", JSON.stringify(res["data"]));
                setLoginSuccess(true);
              }
            } else {
              if (res["data"]["IsActive"]) {
                localStorage.setItem("user", JSON.stringify(res["data"]));
                setLoginSuccess(true);
              }
            }
          }
        },
        (error) => {
          alert(error);
        }
      );
    }
  };

  const responseFacebook = (response) => {
    console.log(response, 'fb response');
    //check fb response
    if (response && response["userID"]) {
      //check user email if not exist register else login
      checkUserEmailExist(response["email"], response);

    } else {
      setShowModal(true);
      setShowMessage("Something Went Wrong...");
    }
  }

  const checkUserEmailExist = (checkemail, response) => {
    const url = `${apiBaseURL}/userlogin?Email=${checkemail}`;
    axios
      .get(url)
      .then((res) => {
        console.log(res, "check fbuser register");
        if (res.status === 200) {
          if (res["data"][0]["IsActive"]) {
            if (res["data"][0]["registeredAt"] === "ca") {
              setShowModal(true);
              setShowMessage(
                "You are registered With Cararabiya, Please Login With Cararabiya"
              );
            } else if (res["data"][0]["registeredAt"] === "fb") {
              //login
              let FbUserResponse = [];
              let fbparams = {
                "BranchID": 0,
                "Email": checkemail,
                "IsActive": true,
                "Password": null,
                "UserID": res.data[0].UserID,
                "UserTypeID": 1,
                "registeredAt": null,
                "social_id": null,
              };

              FbUserResponse.push(fbparams);

              localStorage.setItem("user", JSON.stringify(FbUserResponse));
              setLoginSuccess(true);
            } else if (res["data"][0]["registeredAt"] === "gl") {
              setShowModal(true);
              setShowMessage(
                "You are registered With Google, Please Login With Google"
              );
            } else {
              setShowModal(true);
              setShowMessage(
                "You are registered With Twitter, Please Login With Twitter"
              );
            }
          }
          else {
            console.log(res.status, "statuscode from userlogin api");
          }

        } else {
          //register
          handleFBUserRegister(response);
          console.log(res.status, "statuscode from userlogin api");
        }
      })
      .catch((error) => {
        console.log(error, "from userlogin api");
      });
  };

  const handleFBUserRegister = (response) => {
    const url = `${apiBaseURL}/user`;
    let ApiParamForFBUser = {
      FirstName: response["name"].substr(0, response["name"].indexOf(" ")),
      MiddleName: "NA",
      LastName: response["name"].substr(response["name"].indexOf(" ") + 1),
      Email: response["email"],
      Mobile: "NA",
      aqamaNo_CNIC: "NA",
      Phone: "NA",
      Password: "NA",
      Address1: "NA",
      Address2: "NA",
      UserTypeID: 1,
      CountryID: 184,
      CityID: -1,
      Area: 0,
      IsActive: true,
      CreatedBy: 0,
      CreatedDate: "1900-01-01T00:00:00",
      DOB: "1900-01-01T00:00:00",
      registeredAt: "fb",
      ImageUrl: "NA",
      Gender: gender,
      BranchID: 0,
      CARegistrationID: 0,
      RoleID: 5,
    };
    axios
      .post(url, ApiParamForFBUser)
      .then((res) => {
        console.log(res.data, "userid from user api");
        if (res && res.status == 200) {
          setRegisterId(res.data);
          handleFetchOrder();
          props.history.push("/registration-success");
        } else {
          console.log(res.status, "statuscode from user api");
        }
      })
      .catch((error) => {
        setShowModal(true);
        setShowMessage("User already exist");
        console.log(error, "from user api");
      });
  }

  const showLogin = () => {
    return (
      <Fragment>
        <div className="logmod__tab lgm-4 show">
          <div className="logmod__heading mt-3">
            <span className="logmod__heading-subtitle">
              Sign up with your favourite social network
            </span>
          </div>
          <div className="logmod__alter-container">
            <div className="social-login">
              {/* Facebook Login */}
              <FacebookLogin
                //appId="1178238482542284"
                appId="345833969948128"
                fields="name,email,picture"
                callback={responseFacebook}
                render={(renderProps) => (
                  <button
                    className="btn facebook-btn social-btn social-shadow"
                    type="button"
                    onClick={renderProps.onClick}
                    disabled={renderProps.isDisabled}
                  >
                    <span className="d-flex justify-content-center">
                      <i className="fab fa-facebook-f" />{" "}
                    </span>{" "}
                  </button>
                )}
              />

              {/* google login */}
              <GoogleLogin
                clientId="170144247770-vsshmnmsfjusa5rqagnlnpin6p6f8nuc.apps.googleusercontent.com"
                render={(renderProps) => (
                  <button
                    className="btn btn-primary social-btn social-shadow"
                    style={{ background: "#dd4b39" }}
                    onClick={renderProps.onClick}
                    disabled={renderProps.disabled}
                  >
                    <span className="d-flex justify-content-center">
                      <i className="fab fa-google-plus-g" />
                    </span>{" "}
                  </button>
                )}
                buttonText="Login"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                cookiePolicy={"single_host_origin"}
              />
            </div>
          </div>
          <div className="logmod__heading">
            <div className="OR">
              {" "}
              <span>OR</span>
            </div>
            <hr style={{ margin: "0px" }} />
          </div>
          <div>
            <span className="logmod__heading-subtitlee">
              Sign in with Email
            </span>
          </div>
          <div className="logmod__form">
            <div
              acceptCharset="utf-8"
              onSubmit={handleLogin}
              className="simform"
            >
              <div className="sminputs">
                <div className="input full">
                  <label className="string optional" htmlFor="user-name">
                    Email*
                  </label>
                  <input
                    className="string optional"
                    maxLength={255}
                    id="user-email"
                    placeholder="Email"
                    type="email"
                    size={50}
                    value={email}
                    onChange={(e) => {
                      setEmail(e.target.value);
                    }}
                    onBlur={(e) => {
                      handleBlurEmail(e, email);
                    }}
                  />
                </div>
              </div>
              <div className="sminputs">
                <div className="input full">
                  <label className="string optional" htmlFor="user-pw">
                    Password *
                  </label>
                  <input
                    className="string optional"
                    maxLength={255}
                    id="user-pw"
                    placeholder="Password"
                    type={isShowPass}
                    size={50}
                    value={password}
                    onChange={(e) => {
                      setPassword(e.target.value);
                    }}
                  />
                  <span
                    onClick={() => {
                      isShowPass == "password"
                        ? setIsShowPass("text")
                        : setIsShowPass("password");
                    }}
                    className="hide-password"
                  >
                    <i className="fas fa-eye" />
                  </span>
                </div>
              </div>
              <div>
                <button
                  className="btn btn-block btn-dark-blue mt-3"
                  style={{ marginLeft: "0px" }}
                  onClick={handleLogin}
                >
                  Sign in
                </button>
              </div>
            </div>
          </div>
          <div className="logmod__alter">
            <button
              className="special"
              onClick={() => SetIsForgetPassword(true)}
            >
              Forgot Password
            </button>
          </div>
        </div>
      </Fragment>
    );
  };

  const goToUserReg = () => {
    setKey("userregistration");
  };
  const goToDealerReg = () => {
    props.history.push("/dealer-registration");
  };

  const showRegisterTabs = () => {
    return (
      <Fragment>
        <div className="logmod__tab-wrapper">
          <div className="logmod__tab lgm-1 show">
            <div className="mt-5">
              <div className="row justify-content-center">
                <ul className="logmod__tabs">
                  <li data-tabtar="lgm-4">
                    <div className="col-6">
                      <div
                        className="bg-shadow py-5 indv-box"
                        onClick={goToUserReg}
                      >
                        <img
                          src="./assets/media/individual.png"
                          width="130px"
                          height="auto"
                          alt="individual"
                          style={{ cursor: "pointer" }}
                        />
                        <span
                          className="mt-3"
                          style={{
                            display: "block",
                            font: "600 18px/1 Montserrat",
                            cursor: "pointer",
                          }}
                        >
                          As Individual
                        </span>
                      </div>
                    </div>
                  </li>
                </ul>
                <ul style={{ paddingInlineStart: 0 }}>
                  <li>
                    <div className="col-6">
                      <div
                        className="bg-shadow py-5 dealer-box"
                        onClick={goToDealerReg}
                      >
                        <img
                          src="./assets/media/dealer.png"
                          width="130px"
                          height="auto"
                          alt="dealer"
                          style={{ cursor: "pointer" }}
                        />
                        <span
                          className="mt-3"
                          style={{
                            display: "block",
                            font: "600 18px/1 Montserrat",
                            cursor: "pointer",
                          }}
                        >
                          As Dealer
                        </span>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  };

  const handleChangeotp = (otp) => {
    setOTP(otp);
  };
  const showUserOTP = () => {
    return (
      <Fragment>
        <div className="logmod__tab lgm-3 show">
          <div className="logmod__alter">
            <div className="logmod__alter-container">
              <div className="logmod__heading mt-5">
                <span className="logmod__heading-subtitle">
                  Verify Email Address
                </span>
              </div>
            </div>
          </div>
          <span className="logmod__heading-otp text-center">
            {" "}
            OTP has been sent to your e-mail,
            <br />
            please enter below
          </span>
          <div className="d-flex justify-content-center mb-1 mt-3">
            <form className="otp mb-3">
              <OtpInput value={otp} onChange={handleChangeotp} numInputs={6} />
            </form>
          </div>
          <div className="d-flex justify-content-center mb-2 mt-4">
            <button
              type="button"
              className="btn"
              style={{
                border: "none",
                padding: "10px 80px",
                backgroundColor: "#D3E428",
                color: "white",
              }}
              onClick={handleUserLogin}
            >
              Submit
            </button>
          </div>
        </div>
      </Fragment>
    );
  };

  const handleMouseIn = () => {
    SetIsHover(true);
  };
  const handleMouseOut = () => {
    SetIsHover(false);
  };
  const handleBlurRegEmail = (e, checkemail) => {
    const url = `${apiBaseURL}/userlogin?Email=${checkemail}`;
    axios
      .get(url)
      .then((res) => {
        if (res.status === 200) {
          console.log(res, "check user register");
          setShowMessage("User Already Register");
          SetIsUserRegister(true);
        } else {
          setShowMessage("");
          SetIsUserRegister(false);
          console.log(res.status, "statuscode from userlogin api");
        }
      })
      .catch((error) => {
        setShowMessage("");
        SetIsUserRegister(false);
        console.log(error, "from userlogin api");
      });
  };

  const handleBlurEmail = (e, checkemail) => {
    const url = `${apiBaseURL}/userlogin?Email=${checkemail}`;
    axios
      .get(url)
      .then((res) => {
        if (res.status === 200) {
          console.log(res.data[0].UserID, "check user register platform");
          setSigninUserId(res.data[0].UserID);
          setSigninUserEmail(checkemail);
          if (res["data"][0]["IsActive"]) {
            if (res["data"][0]["registeredAt"] === "ca") {

            } else if (res["data"][0]["registeredAt"] === "fb") {
              setShowModal(true);
              setShowMessage(
                "You are registered With Facebook, Please Login With Facebook"
              );
            } else if (res["data"][0]["registeredAt"] === "gl") {
              setShowModal(true);
              setShowMessage(
                "You are registered With Google, Please Login With Google"
              );
            } else {
              setShowModal(true);
              setShowMessage(
                "You are registered With Twitter, Please Login With Twitter"
              );
            }
          }
        } else {
          console.log(res.status, "statuscode from userlogin api");
        }
      })
      .catch((error) => {
        console.log(error, "from userlogin api");
      });
  };
  const showUserRegisteration = () => {

    return (
      <Fragment>
        <div className="logmod__tab lgm-4 show">
          <div className="logmod__heading mt-3">
            <span className="logmod__heading-subtitle">
              Sign up with your favourite social network
            </span>
          </div>
          <div className="logmod__alter-container">
            <div className="social-login">
              {/* Facebook Login */}
              <FacebookLogin
                appId="1178238482542284"
                fields="name,email,picture"
                callback={responseFacebook}
                render={(renderProps) => (
                  <button
                    className="btn facebook-btn social-btn social-shadow"
                    type="button"
                    onClick={renderProps.onClick}
                    disabled={renderProps.isDisabled}
                  >
                    <span className="d-flex justify-content-center">
                      <i className="fab fa-facebook-f" />{" "}
                    </span>{" "}
                  </button>
                )}
              />

              <button
                className="btn twitter-btn social-btn social-shadow"
                type="button"
              >
                <span className="d-flex justify-content-center">
                  <i className="fab fa-twitter" />
                </span>{" "}
              </button>
              {/* google login */}
              <GoogleLogin
                clientId="170144247770-vsshmnmsfjusa5rqagnlnpin6p6f8nuc.apps.googleusercontent.com"
                render={(renderProps) => (
                  <button
                    className="btn btn-primary social-btn social-shadow"
                    style={{ background: "#dd4b39" }}
                    onClick={renderProps.onClick}
                    disabled={renderProps.disabled}
                  >
                    <span className="d-flex justify-content-center">
                      <i className="fab fa-google-plus-g" />
                    </span>{" "}
                  </button>
                )}
                buttonText="Login"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                cookiePolicy={"single_host_origin"}
              />
            </div>
          </div>
          <div className="logmod__heading">
            <div className="OR">
              {" "}
              <span>OR</span>
            </div>
            <hr style={{ margin: "0px" }} />
          </div>
          <div>
            <span className="logmod__heading-subtitlee">
              Register with Email
            </span>
          </div>
          <div className="logmod__form">
            <div className="container mt-3 border-bottom">
              <label
                className="string optional"
                style={{ fontWeight: 700, fontSize: "12px" }}
              >
                Select Gender *
              </label>

              <form id="radios" onChange={handleChangeGender}>
                <label className="material-icons">
                  <input
                    checked={"Male" === gender}
                    required
                    type="radio"
                    value="Male"
                  />
                  <span className="outline-orange">
                    <i className="fas fa-male fa-2x icon-pos-y" /> &nbsp;Male
                  </span>
                </label>
                <label className="material-icons">
                  <input
                    checked={"Female" === gender}
                    required
                    type="radio"
                    value="Female"
                  />
                  <span className="outline-orange">
                    <i className="fas fa-female fa-2x icon-pos-y" />{" "}
                    &nbsp;Female
                  </span>
                </label>
              </form>
            </div>
            <form className="simform" onSubmit={handleUserRegistration}>
              <div className="sminputs">
                <div className="input string optional">
                  <label className="string optional" htmlFor="user-name">
                    First Name *
                  </label>
                  <input
                    required
                    className="string optional"
                    maxLength={255}
                    id="user-firstname"
                    placeholder="First Name"
                    type="text"
                    size={50}
                    value={firstname}
                    onChange={(e) => {
                      setFirstName(e.target.value);
                    }}
                  />
                </div>
                <div className="input string optional">
                  <label className="string optional" htmlFor="user-name">
                    Last Name *
                  </label>
                  <input
                    required
                    className="string optional"
                    maxLength={255}
                    id="user-lastname"
                    placeholder="Last Name"
                    type="text"
                    size={50}
                    value={lastname}
                    onChange={(e) => {
                      setLastName(e.target.value);
                    }}
                  />
                  <label htmlFor className="text-danger" />
                </div>
              </div>
              <div className="sminputs">
                <div className="input full">
                  <label className="string optional" htmlFor="user-name">
                    Email*
                  </label>
                  <input
                    required
                    className="string optional"
                    maxLength={255}
                    id="user-email"
                    placeholder="Email"
                    type="email"
                    size={50}
                    value={useremail}
                    onChange={(e) => {
                      setUserEmail(e.target.value);
                    }}
                    onBlur={(e) => {
                      handleBlurRegEmail(e, useremail);
                    }}
                  />
                </div>
              </div>
              <div
                style={{ display: isUserRegister ? "block" : "none" }}
                className="email-verify-status"
                id="email-verify-status"
              >
                {showMessage}
              </div>
              <div className="sminputs">
                <div className="input full">
                  <label className="string optional " htmlFor="user-name">
                    Mobile Phone Number
                  </label>
                  <div className="row">
                    <input
                      required
                      className="string optional mobile pl-2"
                      maxLength={255}
                      id="user-mobile"
                      placeholder="Enter your Mobile Number"
                      type="text"
                      size={15}
                      value={mobile}
                      onChange={(e) => {
                        setMobile(e.target.value);
                      }}
                    />
                  </div>
                  <br />
                </div>
              </div>

              <div className="sminputs">
                <div className="input full">
                  <label className="string optional " htmlFor="user-name">
                    CNIC Number *
                  </label>
                  <div className="row">
                    <input
                      required
                      className="string optional mobile pl-2"
                      maxLength={255}
                      id="user-mobile"
                      placeholder="Enter your CNIC Number"
                      type="text"
                      size={15}
                      value={cnic}
                      onChange={(e) => {
                        setCnic(e.target.value);
                      }}
                    />
                  </div>
                  <br />
                </div>
              </div>

              <div className="sminputs password">
                <div className="input string optional">
                  <label className="string optional" htmlFor="user-pw">
                    Password *
                  </label>
                  <input
                    required
                    className="string optional"
                    maxLength={255}
                    id="user-pw"
                    placeholder="Password"
                    type={isShowPass}
                    size={50}
                    value={userpassword}
                    onChange={(e) => {
                      setUserPassword(e.target.value);
                    }}
                    onMouseOver={handleMouseIn}
                    onMouseOut={handleMouseOut}
                  />
                  <span
                    onClick={() => {
                      isShowPass == "password"
                        ? setIsShowPass("text")
                        : setIsShowPass("password");
                    }}
                    className="hide-password"
                  >
                    <i className="fas fa-eye" />
                  </span>
                </div>

                <Fragment>
                  <div
                    style={{ display: isHover ? "block" : "none" }}
                    id="password-help"
                    className="helper-text-box"
                  >
                    <ul
                      style={{ paddingInlineStart: 0 }}
                      className="helper-text"
                    >
                      <li className="helper-text-item">
                        <i
                          className="fas fa-circle pr-1"
                          style={{ fontSize: "0.4em", color: "#D3E428" }}
                        />
                        Must include at least 8 characters
                      </li>
                      <li className="helper-text-item">
                        <i
                          className="fas fa-circle pr-1"
                          style={{ fontSize: "0.4em", color: "#D3E428" }}
                        />
                        Must include at least 1 uppercase letter (A-Z)
                      </li>
                      <li className="helper-text-item">
                        <i
                          className="fas fa-circle pr-1"
                          style={{ fontSize: "0.4em", color: "#D3E428" }}
                        />
                        Must include at least 1 lowercase letter (a-z)
                      </li>
                      <li className="helper-text-item">
                        <i
                          className="fas fa-circle pr-1"
                          style={{ fontSize: "0.4em", color: "#D3E428" }}
                        />
                        Must include at least 1 numerical digit (0-9)
                      </li>
                      <li className="helper-text-item">
                        <i
                          className="fas fa-circle pr-1"
                          style={{ fontSize: "0.4em", color: "#D3E428" }}
                        />
                        Must include at least 1 special character (!@#$%^*)
                      </li>
                    </ul>
                  </div>
                </Fragment>

                <div className="input string optional">
                  <label className="string optional" htmlFor="user-pw-repeat">
                    Confirm password *
                  </label>
                  <input
                    required
                    className="string optional"
                    maxLength={255}
                    id="user-pw-repeat"
                    placeholder="Confirm password"
                    type={isShowConfPass}
                    size={50}
                    value={confirmpassword}
                    onChange={(e) => {
                      setConfirmPassword(e.target.value);
                    }}
                  />
                  <span
                    onClick={() => {
                      isShowConfPass == "password"
                        ? setIsShowConfPass("text")
                        : setIsShowConfPass("password");
                    }}
                    className="hide-password"
                  >
                    <i className="fas fa-eye" />
                  </span>
                </div>
              </div>

              <div>
                <input
                  className="btn btn-block btn-dark-blue mt-3"
                  style={{ marginLeft: "0px" }}
                  type="submit"
                  value="SIGN UP"
                />
              </div>
            </form>
          </div>
          <div className="logmod__alter mt-5">
            We value your privacy.
            <br />
            Cararabiya.com{" "}
            <Link className="special" to="/privacy-policy">
              Privacy Statement.
            </Link>
          </div>
        </div>
      </Fragment>
    );
  };

  return (
    <div>
      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Cararabiya</Modal.Title>
        </Modal.Header>
        <Modal.Body>{showMessage}</Modal.Body>
      </Modal>
      <div>{loginSuccess ? <Redirect to="/" /> : ""}</div>
      {isForgetPassword === true ? (
        <ForgotPassword {...props} />
      ) : (
          <div className="logmod__wrapper" style={{ marginTop: "72px" }}>
            <div className="logmod__container" id="test2">
              <Tabs
                style={{ justifyContent: "center" }}
                id="controlled-tab-example"
                activeKey={key}
                onSelect={(k) => setKey(k)}
              >
                <Tab eventKey="signin" title="Sign In">
                  {showLogin()}
                </Tab>
                <Tab eventKey="register" title="Register">
                  {showRegisterTabs()}
                </Tab>
                <Tab eventKey="userregistration">{showUserRegisteration()}</Tab>
                <Tab eventKey="userotp">{showUserOTP()}</Tab>
              </Tabs>
            </div>
          </div>
        )}
    </div>
  );
};

export default LoginSignUp;