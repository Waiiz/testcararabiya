import React, { Component } from "react";
import { Modal } from "react-bootstrap";
import StarRatings from "react-star-ratings";
import { apiBaseURL } from "../ApiBaseURL";
import axios from "axios";
import _ from "lodash";

class FeedbackLeave extends Component {
  constructor(props) {
    super(props);
    this.SuggestionsArr = [];
    this.previousUserData = "";
    this.state = {
      //modals
      showModalSuccess: false,

      name: "",
      email: "",
      rating: 0,
      Suggestions: [
        { id: 0, value: "Good Car Deals" },
        { id: 1, value: "Awesome Features" },
        { id: 2, value: "Unresponsive app/website" },
        { id: 3, value: "CarArabiya is best" },
        { id: 4, value: "Listing not updated" }
      ],
      suggestedtext: "",
      review: "",
      userId: "",
      AppTestimonialSuccess: "",
      userData: []
    };
  }

  componentDidMount() {
    this.loadUserRecord();
  }
  componentDidUpdate() {
    if (!_.isEqual(this.previousUserData, localStorage.getItem("user"))) {
      this.loadUserRecord();
    }
  }

  loadUserRecord = () => {
    let userData = localStorage.getItem("user");
    this.previousUserData = userData;
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      this.setState({
        userId: parseUserData[0].UserID
      });

      const url = `${apiBaseURL}/user?UserID=${parseUserData[0].UserID}`;
      axios
        .get(url)
        .then(res => {
          console.log(res);
          this.setState({
            userData: res.data,
            name: res.data[0].FirstName + " " + res.data[0].LastName,
            email: res.data[0].Email
          });
        })
        .catch(error => {
          console.log(error, "userdata api");
        });
    } else {
      this.setState({
        name: "",
        email: ""
      });
      console.log("localstorage null");
    }
  };

  handleCloseModal = () => {
    this.props.handleParentFeedbackModal(false);
  };

  handleCloseSuccess = () => {
    this.setState({
      showModalSuccess: false
    });
  };

  modalSuccess = () => {
    return (
      <Modal show={this.state.showModalSuccess}>
        <Modal.Body>
          <div className="my-3 px-3">
            <div className="d-flex justify-content-center my-3">
              <h4>
                <strong>Thank you!</strong>
              </h4>
            </div>

            <div style={{ textAlign: "center" }}>
              Thank you for submitting your feedback.
            </div>

            <div className="d-flex justify-content-center mb-2 mt-3">
              <button
                onClick={this.handleCloseSuccess}
                className="btn"
                style={{
                  border: "none",
                  padding: "10px 80px",
                  backgroundColor: "#D3E428",
                  color: "white"
                }}
              >
                OK
              </button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    );
  };
  //testimonial
  changeRating = newRating => {
    this.setState({
      rating: newRating
    });
  };

  handleNameChange = event => {
    this.setState({ name: event.target.value });
  };
  handleEmailChange = event => {
    this.setState({ email: event.target.value });
  };
  handleReviewChange = event => {
    this.setState({ review: event.target.value });
  };
  handleSuggestionChange = (suggestion, index) => {
    this.SuggestionsArr.push(" " + suggestion);
    this.setState({
      review: this.SuggestionsArr
    });
    this.state.Suggestions.splice(index, 1);
  };

  handleUserFeedback = event => {
    event.preventDefault();
    const { userId, rating, name, email, review } = this.state;
    if (rating === 0) {
      alert("Please select star to rate us");
      return;
    }

    const url = `${apiBaseURL}/AppTestimonial`;
    let ApiParamForTestimonial = {
      UserID: userId,
      UserName: name,
      EmailAdd: email,
      StarsCount: rating,
      ReviewText: review.toString()
    };
    axios
      .post(url, ApiParamForTestimonial)
      .then(res => {
        console.log(res, "AppTestimonial api");

        if (res.status === 200) {
          this.props.handleParentFeedbackModal(false);
          this.setState({
            showModalSuccess: true
          });
        } else {
          console.log(res.status, "statuscode from AppTestimonial api");
        }
      })
      .catch(error => {
        console.log(error, "AppTestimonial api");
      });
  };

  showModal = () => {
    return (
      <Modal
        show={this.props.openfeedbackmodal}
        centered
        onHide={this.handleCloseModal}
      >
        <div className="modal-body">
          <div className="container">
            <form onSubmit={this.handleUserFeedback}>
              <div className="col-12">
                <div className="d-flex justify-content-center align-items-center">
                  <div className="container mt-2 mb-3">
                    <div className="row justify-content-center align-items-center">
                      <div className="mr-2" style={{ marginBottom: "2%" }}>
                        <span className="heading-primary">
                          Enjoying the app? <br></br> Please rate us!
                        </span>
                      </div>

                      <div className="rate">
                        <StarRatings
                          rating={this.state.rating}
                          starRatedColor="#deb217"
                          changeRating={this.changeRating}
                          numberOfStars={5}
                          name="rating"
                          starDimension="40px"
                          starHoverColor="#D3E428"
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <input
                  required
                  onChange={this.handleNameChange}
                  value={this.state.name}
                  type="text"
                  className="form-control mb-2"
                  placeholder="Enter your Name"
                  style={{ border: "1px solid #D3E42899", fontSize: "12px" }}
                />
                <input
                  required
                  onChange={this.handleEmailChange}
                  value={this.state.email}
                  type="text"
                  className="form-control mb-2"
                  placeholder="Enter your Email"
                  style={{ border: "1px solid #D3E42899", fontSize: "12px" }}
                />
                <textarea
                  required
                  onChange={this.handleReviewChange}
                  value={this.state.review}
                  className="form-control"
                  type="text"
                  style={{ color: "black", transform: "translateY(20px)" }}
                  cols="30"
                  rows="6"
                  placeholder="Enter your review here"
                ></textarea>

                <div className="d-block mt-5">
                  <div className="mb-2">
                    <span className="heading-secondary">
                      You can also use these suggestions
                    </span>
                  </div>

                  <div id="keypad">
                    {this.state.Suggestions.map((suggestion, index) => (
                      <span
                        type="button"
                        key={index}
                        onClick={() =>
                          this.handleSuggestionChange(suggestion.value, index)
                        }
                      >
                        <span className="btn btn-primary btn-sm sugg-text">
                          {suggestion.value}
                        </span>
                      </span>
                    ))}
                  </div>
                </div>
              </div>

              <div className="col-12 d-flex justify-content-center mb-2">
                <input
                  type="submit"
                  className="btn btn-primary color-arabiya btn-block"
                  style={{
                    backgroundColor: "#D3E428",
                    border: "none",
                    marginTop: "30px"
                  }}
                  value="LEAVE A REVIEW"
                />
              </div>
            </form>
          </div>
        </div>
      </Modal>
    );
  };

  render() {
    return (
      <>
        {this.modalSuccess()}
        {this.showModal()}
      </>
    );
  }
}

export default FeedbackLeave;
