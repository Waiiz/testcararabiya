import React, { Component, Fragment } from "react";
import {
  Dealerlatestads,
  DealerListing,
  Dealersearch
} from "../components/dealers";
import Dealeradvertisement from './Dealeradvertisement';

class Dealers extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <Fragment>
        <div className="container mt-5">
          <div className="row">
            <div className="col-lg-3">
              <Dealersearch {...this.props} />
              <Dealeradvertisement {...this.props} />
            </div>
            <div className="col-lg-9">
              <Dealerlatestads {...this.props} />
              <DealerListing {...this.props} />
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Dealers;