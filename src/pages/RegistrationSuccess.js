import React, { Fragment, Component } from "react";
class RegistrationSuccess extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  goToLogin = (e) => {
    this.props.history.push('/login-signup');
  }

  render() {
    return (
      <div className="container-fluid register-success">
        <div className="container">
          <div className="col-12" style={{marginTop: '60px', padding: '80px 0'}}>
            <div style={{textAlign: 'center'}}>
              <div className="d-flex justify-content-center">
                <div className="success-black-box">
                  <img
                    width="200px"
                    height="auto"
                    src="assets/media/goal.png"
                    alt=""
                  />
                  <div className="mt-3">
                    <span className="congrats-text">Congratulations!</span>
                  </div>
                  <div className="mt-2">
                    <span style={{fontSize: '16px', color: 'white', textTransform: 'uppercase', fontWeight: '500'}}>
                      You are registered successfully
                    </span>
                  </div>
                </div>
              </div>

              <div className="mt-5">
                <button
                  className="btn"
                  style={{border: '1px solid white', padding: '10px 80px', backgroundColor: 'transparent', color: 'white'}}
                  onClick={this.goToLogin}
                >
                  Go to Login Screen
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RegistrationSuccess;