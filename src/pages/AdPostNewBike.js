import React, { Component, Fragment } from "react";
import { Tabs, Tab, Button, Alert, Image, Modal } from "react-bootstrap";
import axios from "axios";
import ImageUploading from "react-images-uploading";
import Gallery from "react-photo-gallery";
import Adpostdetailsimilar from "../components/adpostdetail/Adpostdetailsimilar";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

import {
  apiBaseURL,
  apiUrlmake,
  apiUrlBodytype,
  apiUrlAttribute,
} from "../ApiBaseURL";

//Trim
const Trim = [
  { id: 1, value: "R6 AT" },
  { id: 2, value: "R6 MT" },
];
//Features
const Features = [
  { id: 1, value: "ABS" },
  { id: 2, value: "Kill Switch" },
  { id: 3, value: "Digital Fuel Guage" },
  { id: 4, value: "Pillion Backrest" },
  { id: 5, value: "Pillion Footrest" },
  { id: 6, value: "Pillion Grab Rail" },
  { id: 7, value: "Kill Switch" },
];
//prev icon
const PreviewIcons = [
  { id: 1, url: "./assets/media/calculator.png" },
  { id: 2, url: "./assets/media/heart-solid.png" },
  { id: 3, url: "./assets/media/compare-deselected.png" },
  { id: 4, url: "./assets/media/Share.png" },
  { id: 5, url: "./assets/media/Report.png" },
];
//prev list
const PreviewLists = [
  { id: 1, url: "./assets/media/year.svg", name: "year", value: "2020" },
  {
    id: 2,
    url: "./assets/media/transmission.svg",
    name: "transmission",
    value: "Constant Mesh",
  },
  {
    id: 3,
    url: "./assets/media/assembly.svg",
    name: "Assembly",
    value: "Imported",
  },
];
//Image Colors
const ImageColors = [
  {
    id: 1,
    url: "./assets/media/newbike/ybrblue.png",
    code: "#0d37d0",
    name: "Icon Blue",
  },
  {
    id: 2,
    url: "./assets/media/newbike/ybrblack.png",
    code: "#000000",
    name: "Midnight Black",
  },
];

const photos = [
  {
    src: "./assets/media/thumb/6.png",
    width: 4,
    height: 3,
  },
  {
    src: "./assets/media/thumb/6.png",
    width: 1,
    height: 1,
  },
  {
    src: "./assets/media/thumb/6.png",
    width: 3,
    height: 4,
  },
  {
    src: "./assets/media/thumb/6.png",
    width: 3,
    height: 4,
  },
  {
    src: "./assets/media/thumb/6.png",
    width: 3,
    height: 4,
  },
  {
    src: "./assets/media/thumb/6.png",
    width: 4,
    height: 3,
  },
];

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 6,
    slidesToSlide: 3, // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    slidesToSlide: 2, // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1, // optional, default to 1.
  },
};

const maxNumber = 5;
class AdPostNewBike extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //basic
      //make
      smake: "",
      lmake: [],

      //model
      smodel: "",
      lmodel: [],

      //version
      lversion: [],
      lvinfo: [],
      sversion: "",

      //year
      syear: "",

      //exterior color
      secolor: "",
      lecolor: [],

      //interior color
      licolor: [],
      sicolor: "",
      //preview tab
      lbodytype: [],
      sbodytype: "",
      smakename: "",
      smodelname: "",
      sbodytypename: "",
      secolorname: "",
      sicolorname: "",

      imageurl: "",
      //additinal
      //attribute
      lattribute: [],
      sattribute: [],
      sattributename: [],
      svattribute: [],

      overalllength: "",
      overallwidth: "",
      overallheight: "",
      wheelbase: "",
      widthplusdoors: "",
      widthminusdoors: "",
      //upload tab
      imageList: [],

      UserPackageAdPost: [],
      NumOfImages: "",
      imageUpload: 0,
      toggleAlertMsg: false,
      //Guidlines
      Guidlines: [
        {
          id: 1,
          value: "Front Side",
        },
        {
          id: 2,
          value: "Back Side",
        },
        {
          id: 3,
          value: "Left Side",
        },
        {
          id: 4,
          value: "Right Side",
        },
        {
          id: 5,
          value: "Engine",
        },
      ],
      //tab key
      key: 4,
    };
  }

  componentDidMount() {
    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      var userid = parseUserData[0].UserID;
      this.setState({
        userId: parseUserData[0].UserID,
      });
    } else {
      this.props.history.push({
        pathname: "/login-signup",
      });
    }

    //basic info
    fetch(apiUrlmake)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lmake: result,
            });
          } else {
            console.log("make api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    fetch(apiUrlBodytype)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lbodytype: result,
            });
          } else {
            console.log("bodytype api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    const apiUrlecolor =
      "https://cararabiya.ahalfa.com/api/color?VehicleInfoID=" +
      1 +
      "&&IEType=e";

    fetch(apiUrlecolor)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lecolor: result,
            });
          } else {
            console.log("ecolor api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    const apiUrlicolor =
      "https://cararabiya.ahalfa.com/api/color?VehicleInfoID=" +
      1 +
      "&&IEType=i";

    fetch(apiUrlicolor)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            let test = result.map((obj) => ({ ...obj, Active: "true" }));

            this.setState({
              licolor: test,
            });
          } else {
            console.log("icolor api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    //additional
    var listatt = [];
    var newlist = [];
    var ouy = [];

    fetch(apiUrlAttribute)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            listatt = result;

            for (var a = 0; a < listatt.length; a++) {
              ouy = {
                AttributeID: listatt[a].AttributeID,
                AttributeName: "" + listatt[a].AttributeName + "",
                CreatedBy: listatt[a].CreatedBy,
                CreatedDate: "" + listatt[a].CreatedDate + "",
                IsActive: false,
              };
              newlist.push(ouy);
            }
            this.setState({
              lattribute: newlist,
            });
          } else {
            console.log("attribute api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    //preview
    this.setState({
      imageurl: ImageColors[0].url,
    });
  }

  handleMakeChange = (event) => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ smake: event.target.value });

    let filteredArray = this.state.lmake.filter(
      (item) => event.target.value == item.MakeID
    );
    this.setState({ smakename: filteredArray[0].MakeName });

    const apiUrlmodel =
      "https://cararabiya.ahalfa.com/api/model?MakeID=" +
      event.target.value +
      "&&VehicleCategoryID=1";

    fetch(apiUrlmodel)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lmodel: result,
            });
          } else {
            console.log("model api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );
  };

  handleModelChange = (event) => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ smodel: event.target.value });

    let filteredArray = this.state.lmodel.filter(
      (item) => event.target.value == item.ModelID
    );
    this.setState({ smodelname: filteredArray[0].ModelName });

    const apiUrlversion =
      "https://cararabiya.ahalfa.com/api/vehicleversion?ModelID=" +
      event.target.value +
      "";

    fetch(apiUrlversion)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lversion: result,
            });
          } else {
            console.log("version api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );
  };

  handleVersionChange = (event) => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ sversion: event.target.value });

    let filteredArray = this.state.lversion.filter(
      (item) => event.target.value == item.VersionID
    );
    this.setState({ sversionname: filteredArray[0].VersionName });

    const apiUrlvinfo =
      "https://cararabiya.ahalfa.com/api/vehicleinfo?MakeID=" +
      this.state.smake +
      "&&ModelID=" +
      this.state.smodel +
      "&&VersionID=" +
      event.target.value +
      "";

    fetch(apiUrlvinfo)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lvinfo: result,
            });
          } else {
            console.log("vinfo api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );
  };

  handleYearChange = (event) => {
    if (event.target.value === "") {
      return;
    }
    let idv = "";

    this.setState({ syear: event.target.value });
    this.state.lvinfo.map((vinfo, index) => {
      this.setState({
        svinfoid: vinfo.VehicleInfoID,
        sbodytype: vinfo.BodyTypeID,
      });
      idv = vinfo.VehicleInfoID;

      let filteredArray3 = this.state.lbodytype.filter(
        (item) => vinfo.BodyTypeID == item.BodyTypeID
      );

      this.setState({ sbodytypename: filteredArray3[0].BodyTypeName });

      const apiUrlvattribute =
        "https://cararabiya.ahalfa.com/api/vehicleattribute?vehicleinfoid=" +
        idv;

      fetch(apiUrlvattribute)
        .then((res) => res.json())
        .then(
          (result) => {
            if (result.length > 0) {
              this.setState({
                svattribute: result,
              });
            } else {
              console.log("vattribute api");
            }
          },
          (error) => {
            this.setState({ error });
          }
        );
    });

    const apiUrlecolor =
      "https://cararabiya.ahalfa.com/api/color?VehicleInfoID=" +
      idv +
      "&&IEType=e";

    fetch(apiUrlecolor)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lecolor: result,
            });
          } else {
            console.log("ecolor api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    const apiUrlicolor =
      "https://cararabiya.ahalfa.com/api/color?VehicleInfoID=" +
      idv +
      "&&IEType=i";

    fetch(apiUrlicolor)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              licolor: result,
            });
          } else {
            console.log("icolor api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    const apiUrlvattribute =
      "https://cararabiya.ahalfa.com/api/vehicleattribute?vehicleinfoid=" + idv;

    fetch(apiUrlvattribute)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              svattribute: result,
            });
          } else {
            console.log("vattr api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );
  };

  handleBodyTypeChange = (event) => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ sbodytype: event.target.value });
  };

  handleEcolorChange = (ecolor, index, event) => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ secolor: event.target.value });

    let filteredArray = this.state.lecolor.filter(
      (item) => event.target.value == item.ColorID
    );
    this.setState({ secolorname: filteredArray[0].ColorName });

    var listatt = this.state.lattribute;
    var listvatt = this.state.svattribute;
    var newlistatt = [];
    var satt = this.state.sattribute;
    var sattname = this.state.sattributename;
    var flag = false;

    for (var a = 0; a < listatt.length; a++) {
      for (var b = 0; b < listvatt.length; b++) {
        if (listatt[a].AttributeID == listvatt[b].AttributeID) {
          flag = true;
        }
      }
      if (flag == true) {
        flag = false;
        var ouy = {
          AttributeID: listatt[a].AttributeID,
          AttributeName: "" + listatt[a].AttributeName + "",
          CreatedBy: listatt[a].CreatedBy,
          CreatedDate: "" + listatt[a].CreatedDate + "",
          IsActive: true,
        };
        newlistatt.push(ouy);
        satt.push(listatt[a].AttributeID);
        sattname.push(listatt[a].AttributeName);
      } else {
        var ouy = {
          AttributeID: listatt[a].AttributeID,
          AttributeName: "" + listatt[a].AttributeName + "",
          CreatedBy: listatt[a].CreatedBy,
          CreatedDate: "" + listatt[a].CreatedDate + "",
          IsActive: false,
        };
        newlistatt.push(ouy);
      }
    }

    this.setState({
      lattribute: newlistatt,
      sattribute: satt,
      sattributename: sattname,
    });
  };

  handleIcolorChange = (icolor, index, event) => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ sicolor: event.target.value });

    let filteredArray = this.state.licolor.filter(
      (item) => event.target.value == item.ColorID
    );
    this.setState({ sicolorname: filteredArray[0].ColorName });
  };

  handleTabSelect = (key) => {
    this.setState({
      key,
    });
  };

  //basic
  renderBasicInfoTab = () => {
    return (
      <Fragment>
        <form className="form-horizontal" onSubmit={this.gotoAdditionalTab}>
          <div className="row mt-5">
            <div className="col-lg-6">
              <div className="form-group">
                <div className="row">
                  <div className="col-3">
                    <label>Make *</label>
                  </div>
                  <div className="col-9">
                    <select
                      required
                      onChange={this.handleMakeChange}
                      className="custom-select-adpost"
                      value={this.state.smake}
                    >
                      <option value="" selected>
                        Select Make
                      </option>
                      {this.state.lmake.map((make, index) => (
                        <option key={make.MakeID} value={make.MakeID}>
                          {make.MakeName}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>

              <div className="form-group">
                <div className="row">
                  <div className="col-3">
                    <label>Model *</label>
                  </div>
                  <div className="col-9">
                    <select
                      required
                      onChange={this.handleModelChange}
                      className="custom-select-adpost"
                      value={this.state.smodel}
                    >
                      <option value="" selected>
                        Select Model
                      </option>
                      {this.state.lmodel.map((model, index) => (
                        <option key={model.ModelID} value={model.ModelID}>
                          {model.ModelName}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>

              <div className="form-group">
                <div className="row">
                  <div className="col-3">
                    <label>Version *</label>
                  </div>
                  <div className="col-9">
                    <select
                      required
                      onChange={this.handleVersionChange}
                      className="custom-select-adpost"
                      value={this.state.sversion}
                    >
                      <option value="" selected>
                        Select Version
                      </option>
                      {this.state.lversion.map((version, index) => (
                        <option
                          key={version.VersionID}
                          value={version.VersionID}
                        >
                          {version.VersionName}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>

              <div className="form-group">
                <div className="row">
                  <div className="col-3">
                    <label>Year *</label>
                  </div>
                  <div className="col-9">
                    <select
                      required
                      onChange={this.handleYearChange}
                      className="custom-select-adpost"
                      value={this.state.syear}
                    >
                      <option value="">Select Year</option>
                      {this.state.lvinfo.map((vinfo, index) => (
                        <option
                          key={vinfo.VersionYear}
                          value={vinfo.VersionYear}
                        >
                          {vinfo.VersionYear}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>
            </div>
            {/* second colomn */}
            <div className="col-lg-6">
              <div className="form-group">
                <div className="row">
                  <div className="col-3">
                    <label>Body Type *</label>
                  </div>
                  <div className="col-9">
                    <select
                      required
                      onChange={this.handleBodyTypeChange}
                      className="custom-select-adpost"
                      value={this.state.sbodytype}
                    >
                      <option value="">Select Body Type</option>
                      {this.state.lbodytype.map((bodytype, index) => (
                        <option
                          key={bodytype.BodyTypeID}
                          value={bodytype.BodyTypeID}
                        >
                          {bodytype.BodyTypeName}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>

              <div className="form-group" style={{ marginTop: "6%" }}>
                <div className="row">
                  <div className="col-3">
                    <label>Trim *</label>
                  </div>
                  <div className="col-9">
                    {Trim.map((trim, index) => (
                      <div key={index} style={{ marginBottom: "6%" }}>
                        <span
                          style={{
                            paddingRight: "10px",
                            borderRight: "1px solid #ccc",
                          }}
                        >
                          <i class="fas fa-eye"></i>
                        </span>

                        <span
                          style={{
                            paddingLeft: "10px",
                            paddingRight: "10px",
                            borderRight: "1px solid #ccc",
                          }}
                        >
                          <input
                            type="checkbox"
                            checked={this.state.trimchecked}
                            onChange={this.handletrimChecked}
                          />
                        </span>

                        <span
                          style={{
                            paddingLeft: "10px",
                            paddingRight: "10px",
                            borderRight: "1px solid #ccc",
                          }}
                        >
                          {trim.value}
                        </span>

                        <span style={{ paddingLeft: "10px" }}>
                          <input
                            style={{ borderBottom: "1px solid #ccc" }}
                            required
                            onChange={this.handleTrimChange}
                            value={this.state.strim}
                            type="text"
                            placeholder="Enter Price!"
                          />
                        </span>
                      </div>
                    ))}
                  </div>
                </div>
              </div>

              <div className="form-group">
                <div className="row">
                  <div className="col-3">
                    <label>Brochure</label>
                  </div>
                  <div className="col-9">
                    <div
                      style={{
                        color: "#6c757d",
                        border: "1px solid #D3E428",
                        padding: "10px 20px",
                        borderRadius: "5px",
                      }}
                    >
                      Toyota Prius Brouchure 2017 (IMC).pdf
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="mt-4 col-12">
              <h4
                style={{
                  fontFamily: '"Montserrat"',
                  fontSize: "18px",
                  fontWeight: 600,
                }}
              >
                Select Interior Color
              </h4>
              <hr />
            </div>

            {this.state.licolor.map((icolor, index) => (
              <div className="col-4" key={index}>
                <div className="form-check">
                  <input
                    onChange={(event) =>
                      this.handleIcolorChange(icolor, index, event)
                    }
                    type="checkbox"
                    id={"attp-" + icolor.ColorID}
                    value={icolor.ColorName}
                    checked={icolor.Active}
                  />
                  <label
                    className="form-check-label"
                    style={{ padding: "0 0 10px 10px" }}
                  >
                    {icolor.ColorName}
                  </label>
                </div>
              </div>
            ))}
          </div>

          <div className="row">
            <div className="mt-4 col-12">
              <h4
                style={{
                  fontFamily: '"Montserrat"',
                  fontSize: "18px",
                  fontWeight: 600,
                }}
              >
                Select Exterior Color
              </h4>
              <hr />
            </div>

            {this.state.lecolor.map((ecolor, index) => (
              <div className="col-4" key={index}>
                <div className="form-check">
                  <input
                    onChange={(event) =>
                      this.handleEcolorChange(ecolor, index, event)
                    }
                    type="checkbox"
                    id={"attp-" + ecolor.ColorID}
                    value={ecolor.ColorName}
                    checked={false}
                  />
                  <label
                    className="form-check-label"
                    style={{ padding: "0 0 10px 10px" }}
                  >
                    {ecolor.ColorName}
                  </label>
                </div>
              </div>
            ))}
          </div>

          <div className="mt-3">
            <input
              className="btn next-step"
              type="submit"
              value="Continue"
              style={{
                position: "absolute",
                zIndex: 151,
                right: 0,
                padding: "10px 80px",
                backgroundColor: "#D3E428",
                color: "white",
              }}
            />
          </div>
        </form>
      </Fragment>
    );
  };

  gotoAdditionalTab = (event) => {
    event.preventDefault();
    this.setState({
      key: 2,
    });
  };

  //additional

  handleOveralllengthChange = (event) => {
    this.setState({ overalllength: event.target.value });
  };

  handleOverallwidthChange = (event) => {
    this.setState({ overallwidth: event.target.value });
  };

  handleOverallheightChange = (event) => {
    this.setState({ overallheight: event.target.value });
  };

  handleWheelbaseChange = (event) => {
    this.setState({ wheelbase: event.target.value });
  };

  handleWidthplusdoorsChange = (event) => {
    this.setState({ widthplusdoors: event.target.value });
  };

  handleWidthminusdoorsChange = (event) => {
    this.setState({ widthminusdoors: event.target.value });
  };

  renderAdditionalInfoTab = () => {
    return (
      <Fragment>
        <div className="row mt-5">
          <div className="col-lg-6">
            <div className="form-group">
              <div className="row">
                <div className="col-3">
                  <label>Overall Length</label>
                </div>
                <div className="col-9">
                  <input
                    onChange={this.handleOveralllengthChange}
                    value={this.state.overalllength}
                    type="text"
                    className="form-control"
                    placeholder="Enter!"
                  />
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="col-3">
                  <label>Overall Width</label>
                </div>
                <div className="col-9">
                  <input
                    onChange={this.handleOverallwidthChange}
                    value={this.state.overallwidth}
                    type="text"
                    className="form-control"
                    placeholder="Enter!"
                  />
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="col-3">
                  <label>Overall Height</label>
                </div>
                <div className="col-9">
                  <input
                    onChange={this.handleOverallheightChange}
                    value={this.state.overallheight}
                    type="text"
                    className="form-control"
                    placeholder="Enter!"
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-6">
            <div className="form-group">
              <div className="row">
                <div className="col-3">
                  <label>Wheelbase</label>
                </div>
                <div className="col-9">
                  <input
                    onChange={this.handleWheelbaseChange}
                    value={this.state.wheelbase}
                    type="text"
                    className="form-control"
                    placeholder="Enter!"
                  />
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="col-3">
                  <label>Width + Doors</label>
                </div>
                <div className="col-9">
                  <input
                    onChange={this.handleWidthplusdoorsChange}
                    value={this.state.widthplusdoors}
                    type="text"
                    className="form-control"
                    placeholder="Enter!"
                  />
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="col-3">
                  <label>Width - Doors</label>
                </div>
                <div className="col-9">
                  <input
                    onChange={this.handleWidthminusdoorsChange}
                    value={this.state.widthminusdoors}
                    type="text"
                    className="form-control"
                    placeholder="Enter!"
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="form-group">
            <div className="row">
              <div className="mt-4 col-12">
                <h4
                  style={{
                    fontFamily: '"Montserrat"',
                    fontSize: "18px",
                    fontWeight: 600,
                  }}
                >
                  Features
                </h4>
                <hr />
              </div>

              {this.state.lattribute.map((attribute, index) => (
                <div className="col-4" key={index}>
                  <div className="form-check">
                    <input
                      onChange={(event) =>
                        this.handleAttributeChange(attribute, index, event)
                      }
                      type="checkbox"
                      id={"attp-" + attribute.AttributeID}
                      value={attribute.AttributeName}
                      checked={attribute.IsActive}
                    />
                    <label
                      className="form-check-label"
                      style={{ padding: "0 0 10px 10px" }}
                    >
                      {attribute.AttributeName}
                    </label>
                  </div>
                </div>
              ))}
            </div>

            <div className="mt-5">
              <div>
                <Button
                  className="btn previous-step"
                  onClick={this.gotoBasicTab}
                >
                  Previous
                </Button>

                <Button className="btn next-step" onClick={this.gotoUploadTab}>
                  Continue
                </Button>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  };

  //upload
  gotoBasicTab = () => {
    this.setState({
      key: 1,
    });
  };

  gotoUploadTab = () => {
    this.setState({
      key: 3,
    });
  };

  //upload tab
  onChange = (imageList) => {
    // data for submit
    this.setState({
      NumOfImages: this.photosRemaining - imageList.length,
      imageUpload: imageList.length,
      imageList: imageList,
    });
  };

  onError = (errors, files) => {
    console.log(errors, files);
  };

  onRemoveImage = (imageList, e) => {
    imageList[e.target.value].onRemove();
  };

  onUploadImage = (imageList, onImageUpload) => {
    onImageUpload();
  };

  showUploadImages = (imageList) => {
    var imagesData = [];
    for (var i = 0; i < 5; i++) {
      if (i < imageList.length) {
        imagesData.push(
          <div key={i} className="image-container">
            <div className="col-2">
              <div className="ImgContainerStyle">
                <Button
                  className="close"
                  value={i}
                  onClick={(event) => this.onRemoveImage(imageList, event)}
                >
                  &times;
                </Button>
                <img src={imageList[i].dataURL} className="img-style" />
              </div>
            </div>
          </div>
        );
      } else {
        imagesData.push(
          <div key={i} className="image-container">
            <div className="col-2">
              <div className="ContainerStyle">
                <div className="child">{this.state.Guidlines[i].value}</div>
              </div>
            </div>
          </div>
        );
      }
    }
    return imagesData;
  };

  renderUploadTab = () => {
    return (
      <Fragment>
        <div className="tab-pane" id="content-images">
          <div className="mt-5">
            <div class="row">
              <div class="col-6 image-upload">
                <p>Images Upload: {this.state.imageUpload}</p>
              </div>
              <div class="col-6 photos-remaining">
                <p>Photos Remaining: {this.state.NumOfImages}</p>
              </div>
            </div>
            <div className="col-12" style={{ padding: 0 }}>
              <form className="uploadContainer">
                <div className="mt-3 d-flex justify-content-center">
                  <ImageUploading
                    onChange={this.onChange}
                    maxNumber={maxNumber}
                    multiple
                    acceptType={["jpg", "png"]}
                    onError={this.onError}
                  >
                    {({ imageList, onImageUpload, errors }) => (
                      // write your building UI
                      <div>
                        <div>
                          {errors.maxNumber && (
                            <Alert variant="primary">
                              <p style={{ color: "#721c24", fontWeight: 600 }}>
                                Image upload limit exceed...
                              </p>
                            </Alert>
                          )}
                          {errors.acceptType && (
                            <Alert variant="primary">
                              <p style={{ color: "#721c24", fontWeight: 600 }}>
                                Your selected file type is not allow...
                              </p>
                            </Alert>
                          )}
                        </div>
                        <div class="row upload-box">
                          <div class="col-2">
                            <img
                              src="./assets/media/upload.png"
                              alt="upload-image"
                            />
                          </div>

                          <div class="col-6 upload-col">
                            <p>
                              For best results and fast selling follow the below
                              guidelines
                            </p>
                          </div>

                          <div class="col-4 upload-col">
                            <Button
                              style={{ backgroundColor: "#D3E428" }}
                              onClick={() =>
                                this.onUploadImage(imageList, onImageUpload)
                              }
                            >
                              Upload images
                            </Button>
                          </div>
                        </div>
                        <div>{this.showUploadImages(imageList)}</div>
                      </div>
                    )}
                  </ImageUploading>
                </div>
              </form>
            </div>
          </div>
          <div className="mt-5">
            <Button
              className="btn previous-step"
              onClick={this.gotoAdditionalTab}
            >
              Previous
            </Button>

            <Button className="btn next-step" onClick={this.gotoPreviewTab}>
              Continue
            </Button>
          </div>
        </div>
      </Fragment>
    );
  };

  gotoPreviewTab = () => {
    this.setState({
      key: 4,
    });
  };

  //preview
  GridNine = () => {
    return (
      <Fragment>
        <div className="row" style={{ borderBottom: "1px solid #D3E428" }}>
          <div className="col-3 dimensiondiv" style={{ padding: 0 }}>
            <div>
              <img
                src="./assets/media/newbike/dimension-bike.svg"
                alt="360-degree-gallery"
              />
              <p className="dimensionp">DIMENSION</p>
            </div>
          </div>

          <div className="col-9 overalldiv">
            <div>
              <div className="overalldiv1">
                <span className="overallspan">OVERALL LENGTH</span>
                <span className="overallspan1">4750 cm</span>
              </div>
              <div className="overalldiv1">
                <span className="overallspan">OVERALL WIDTH</span>
                <span className="overallspan1">1960 cm</span>
              </div>
              <div className="overalldiv1">
                <span className="overallspan">OVERALL HEIGHT</span>
                <span className="overallspan1">2054 cm</span>
              </div>
              <div className="overalldiv1" style={{ borderBottom: "none" }}>
                <span className="overallspan">WHEELBASE</span>
                <span className="overallspan1">6521 cm</span>
              </div>
            </div>
          </div>
        </div>

        <div className="row" style={{ borderBottom: "1px solid #D3E428" }}>
          <div className="col-3 dimensiondiv" style={{ padding: 0 }}>
            <div>
              <img
                src="./assets/media/newbike/performance.svg"
                alt="360-degree-gallery"
              />
              <p className="dimensionp">PERFORMANCE</p>
            </div>
          </div>

          <div className="col-9 overalldiv">
            <div>
              <div className="overalldiv1">
                <span className="overallspan">TRANSMISSION</span>
                <span className="overallspan1">Automatic</span>
              </div>
              <div className="overalldiv1">
                <span className="overallspan">SUSPENSION</span>
                <span className="overallspan1">Double Wishbone</span>
              </div>
              <div className="overalldiv1">
                <span className="overallspan">BRAKES</span>
                <span className="overallspan1">solid Disc</span>
              </div>
              <div className="overalldiv1" style={{ borderBottom: "none" }}>
                <span className="overallspan">55 ltr</span>
                <span className="overallspan1">6521 cm</span>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-3 dimensiondiv" style={{ padding: 0 }}>
            <div>
              <img
                src="./assets/media/newbike/engine.svg"
                alt="360-degree-gallery"
              />
              <p className="dimensionp">ENGINE</p>
            </div>
          </div>

          <div className="col-9 overalldiv">
            <div>
              <div className="overalldiv1">
                <span className="overallspan">ENGINE SIZE</span>
                <span className="overallspan1">4750 cc</span>
              </div>
              <div className="overalldiv1">
                <span className="overallspan">FUEL TYPE</span>
                <span className="overallspan1">Petrol</span>
              </div>
              <div className="overalldiv1">
                <span className="overallspan">NO. OF CYLINDERS</span>
                <span className="overallspan1">16</span>
              </div>
              <div className="overalldiv1" style={{ borderBottom: "none" }}>
                <span className="overallspan">MAX OUTPUE (KW/RPM)</span>
                <span className="overallspan1">72/5200</span>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  };

  GridThree = () => {
    return (
      <Fragment>
        <div className="row">
          <div className="col-12 dealerdiv">
            <div style={{ display: "flex" }}>
              <img
                src="./assets/media/dealer.svg"
                alt="dealer"
                style={{ width: "45px" }}
              />

              <span className="dealerspan">
                Abdullah Motors <br />
                <span style={{ fontWeight: "normal" }}>
                  Member since Jan 2019
                </span>
              </span>
            </div>

            <div className="dealericondiv">
              <img
                className="dealericonimg"
                src="./assets/media/msg.svg"
                alt="msg"
              />
              <img
                className="dealericonimg"
                src="./assets/media/email.svg"
                alt="email"
              />
              <img
                src="./assets/media/phone.svg"
                alt="phone"
                style={{ width: "60px" }}
              />
            </div>

            <Button
              className="btn btnfinance"
              onClick={this.handleApplyFinance}
            >
              <span style={{ fontSize: "14px" }}>Apply For Finance</span>
            </Button>
          </div>
        </div>

        <div className="row">
          <div className="col-12 dealerdiv2">
            <div style={{ display: "flex", alignItems: "center" }}>
              <span className="dealerspan1">VARIANT</span>
              <span className="dealerspan2">1.6L MT</span>
              <span className="dealerspan3">1.6L AT</span>
            </div>

            <div className="dealerpricediv">
              <span className="dealerpricespan1">PRICE</span>
              <span className="dealerpricespan2">SAR 200,000</span>
            </div>

            <Button
              className="btn btnfinance"
              onClick={this.handleApplyFinance}
            >
              <img
                src="./assets/media/brochure.svg"
                alt="phone"
                style={{ width: "20px", marginRight: "8px" }}
              />
              <span style={{ fontSize: "14px" }}>Download Brochure</span>
            </Button>
          </div>
        </div>
      </Fragment>
    );
  };

  handleImageColor = (url, event) => {
    this.setState({
      imageurl: url,
    });
  };

  renderPreviewTab = () => {
    return (
      <Fragment>
        <div className="row mt-3">
          <div className="col-12" style={{ padding: 0 }}>
            {/* carbg */}
            <div className="newcar-slide">
              <div className="container" style={{ paddingTop: 25 }}>
                <div className="newcat_wrap">
                  {/* icons */}
                  <div className="row">
                    <div className="col-12" style={{ textAlign: "right" }}>
                      {PreviewIcons.map((icon, index) => (
                        <img
                          onClick={this.handleClick}
                          key={index}
                          src={icon.url}
                          alt={index}
                          style={{ paddingRight: "10px", width: "50px" }}
                        />
                      ))}
                    </div>
                  </div>
                  {/* carcolors */}
                  <div className="row">
                    <div className="col-4" style={{ paddingLeft: "50px" }}>
                      <div>
                        <p className="newcar-title1">Yamaha </p>
                        <p className="newcar-title2">ybr r6</p>
                        <p className="newcar-title3">sar 20,000</p>
                      </div>

                      <div style={{ marginTop: "25px" }}>
                        <img
                          onClick={this.handleClick}
                          src="./assets/media/360-degree-gallery.png"
                          alt="360-degree-gallery"
                          style={{ marginRight: "15px" }}
                        />
                        <img
                          onClick={this.handleClick}
                          src="./assets/media/Video-player-gallery.png"
                          alt="Video-player-gallery"
                        />
                      </div>

                      <div style={{ marginTop: "35px" }}>
                        {PreviewLists.map((list, index) => (
                          <div key={index} className="newcar-lists">
                            <span style={{ marginRight: "8px" }}>
                              {" "}
                              <img
                                src={list.url}
                                alt={list.id}
                                style={{ width: "25px" }}
                              />{" "}
                            </span>
                            <span
                              style={{ flex: 1, textTransform: "uppercase" }}
                            >
                              {" "}
                              {list.name}{" "}
                            </span>
                            <span style={{ fontWeight: 600 }}>
                              {" "}
                              {list.value}{" "}
                            </span>
                          </div>
                        ))}
                      </div>
                    </div>

                    <div className="col-8">
                      <div className="newbike-colors">
                        <img
                          src={this.state.imageurl}
                          alt="red"
                          style={{ width: "400px", zIndex: 2 }}
                        />
                        <img
                          src="./assets/media/PriusColors/shadowbg.svg"
                          alt="shadow"
                          className="newbike-shadow"
                        />
                      </div>

                      <div
                        className="newcar-colors"
                        style={{ marginTop: "5%" }}
                      >
                        {ImageColors.map((color, index) => (
                          <Fragment key={index}>
                            <span
                              style={{ backgroundColor: color.code }}
                              onClick={(event) =>
                                this.handleImageColor(color.url, event)
                              }
                              className="dot"
                            >
                              <span className="center">{color.name}</span>
                            </span>
                          </Fragment>
                        ))}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* 2nd col */}
            <div className="row">
              {/* dimention */}
              <div className="col-9" style={{ padding: 0 }}>
                {this.GridNine()}
              </div>
              {/* dealer */}
              <div className="col-3" style={{ padding: 0 }}>
                {this.GridThree()}
              </div>
            </div>

            {/* features */}
            <div className="row">
              <div className="featurecol-2" style={{ padding: 0 }}>
                <div className="featurediv">Features</div>
              </div>
              <div className="featurecol-10" style={{ padding: 0 }}>
                <div className="featurecarousel" style={{ cursor: "pointer" }}>
                  <Carousel
                    additionalTransfrom={0}
                    arrows={false}
                    swipeable={false}
                    draggable={true}
                    showDots={false}
                    centerMode={false}
                    responsive={responsive}
                    customTransition="all 1s linear"
                    ssr={true} // means to render carousel on server-side.
                    infinite={false}
                    autoPlay
                    autoPlaySpeed={5000}
                    keyBoardControl={true}
                    transitionDuration={500}
                    containerClass="carousel-container"
                    removeArrowOnDeviceType={["tablet", "mobile"]}
                    deviceType={this.props.deviceType}
                    dotListClass="custom-dot-list-style"
                    itemClass="carousel-item-padding-40-px"
                  >
                    {Features.map((feature, index) => (
                      <Fragment key={index}>
                        <img
                          src="./assets/media/bullet.svg"
                          alt="feature"
                          className="featurebullet"
                        />
                        <span style={{ fontSize: "13px" }}>
                          {feature.value}
                        </span>
                      </Fragment>
                    ))}
                  </Carousel>
                </div>
              </div>
            </div>

            {/* gallerytitle */}
            <div className="row">
              <div className="col-12" style={{ padding: 0 }}>
                <p className="gallery">GALLERY</p>
              </div>
            </div>
            {/* galleryphotos */}
            <div className="row">
              <div className="col-12" style={{ padding: 0 }}>
                <Gallery photos={photos} />
              </div>
            </div>
            {/* similarads */}
            <Adpostdetailsimilar adpostid={310} />
          </div>
        </div>
      </Fragment>
    );
  };

  render() {
    return (
      <Fragment>
        <div className="l-main-content-inner" style={{ paddingBottom: "50px" }}>
          <div className="container">
            <div className="row">
              <div className="col-12">
                <div className="b-find">
                  <Tabs
                    onSelect={this.handleTabSelect}
                    className="b-find-nav nav nav-tabs justify-content-between"
                    activeKey={this.state.key}
                    id="uncontrolled-tab-example"
                  >
                    <Tab eventKey={1} title="BASIC INFO" className="tab">
                      {this.renderBasicInfoTab()}
                    </Tab>

                    <Tab eventKey={2} title="ADDITIONAL INFO">
                      {this.renderAdditionalInfoTab()}
                    </Tab>

                    <Tab eventKey={3} title="UPLOAD IMAGES">
                      {this.renderUploadTab()}
                    </Tab>

                    <Tab eventKey={4} title="PREVIEW">
                      {this.renderPreviewTab()}
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default AdPostNewBike;