import React, { Component, Fragment } from "react";
import HashMap from "hashmap";
import axios from "axios";
import { apiBaseURL } from "../ApiBaseURL";
import Loader from "./Loader";
import ErrorAlert from "./ErrorAlert";

var AdPostAttributeData = new HashMap();
var AdPostDetailData = new HashMap();
var AccordionData = new HashMap();
AccordionData.set(1, true);
AccordionData.set(2, true);
AccordionData.set(3, true);
var AttributeData = [];
class CompareVehicle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingForAdPostDetail: false,
      isLoadingForFeatures: false,
      toggleAlertMsg: false,
    };
  }

  componentDidMount() {
    try {
      window.scrollTo(0, 0);
      this.setState({ isLoadingForAdPostDetail: true });
      var vcid = this.props.history.location.state.vehiclecategoryid;
      this.setState({ vcid: vcid })
      const urlAttribute = `${apiBaseURL}/Attribute?VehicleCategoryID=${vcid}`;
      axios
        .get(urlAttribute)
        .then((res) => {
          if (res.data.length > 0) {
            AttributeData = res.data;
            //console.log(res.data, "Attribute");
          } else {
            console.log(res.data, "Attribute");
          }
        })
        .catch((error) => {
          console.log(error, "from Attribute api");
        });

      var CompareMap = this.props.history.location.state.comparedata;
      for (let pair of CompareMap) {
        const urlAdPostDetail = `${apiBaseURL}/AdPostDetail/AdPostDetail?AdPostID=${pair.key}`;
        axios
          .get(urlAdPostDetail)
          .then((res) => {
            if (res.data.length > 0) {
              AdPostDetailData.set(pair.key, res.data);
              this.setState({
                isLoadingForAdPostDetail: false,
                toggleAlertMsg: false,
              });
              //console.log(AdPostDetailData, "AdPostDetailData");
            } else {
              this.setState({
                isLoadingForAdPostDetail: false,
                toggleAlertMsg: true,
              });
              console.log(res.data, "urlAdPostDetail");
            }
          })
          .catch((error) => {
            this.setState({
              isLoadingForAdPostDetail: false,
              toggleAlertMsg: true,
            });
            console.log(error, "from urlAdPostDetail api");
          });

        this.setState({ isLoadingForFeatures: true });
        const urlAdPostAttribute = `${apiBaseURL}/AdPostAttribute?AdPostID=${pair.key}`;
        axios
          .get(urlAdPostAttribute)
          .then((res) => {
            if (res.data.length > 0) {
              var newlistatt = [];
              var flag = false;

              for (var a = 0; a < AttributeData.length; a++) {
                for (var b = 0; b < res.data.length; b++) {
                  if (AttributeData[a].AttributeID == res.data[b].AttributeID) {
                    flag = true;
                  }
                }
                if (flag == true) {
                  flag = false;
                  var ouy = {
                    AttributeID: AttributeData[a].AttributeID,
                    AttributeName: "" + AttributeData[a].AttributeName + "",
                    CreatedBy: AttributeData[a].CreatedBy,
                    CreatedDate: "" + AttributeData[a].CreatedDate + "",
                    IsActive: true,
                  };
                  newlistatt.push(ouy);
                } else {
                  var ouy = {
                    AttributeID: AttributeData[a].AttributeID,
                    AttributeName: "" + AttributeData[a].AttributeName + "",
                    CreatedBy: AttributeData[a].CreatedBy,
                    CreatedDate: "" + AttributeData[a].CreatedDate + "",
                    IsActive: false,
                  };
                  newlistatt.push(ouy);
                }
              }

              AdPostAttributeData.set(pair.key, newlistatt);
              //console.log(AdPostAttributeData, "urlAdPostAttribute");
              this.setState({ isLoadingForFeatures: false });
            } else {
              this.setState({ isLoadingForFeatures: false });
              console.log(res.data, "from urlAdPostAttribute api");
            }
          })
          .catch((error) => {
            this.setState({ isLoadingForFeatures: false });
            console.log(error, "from urlAdPostAttribute api");
          });
      }
    } catch {
      this.props.history.push("./vehicle-listing");
    }
  }

  componentWillUnmount() {
    AdPostDetailData.clear();
    AdPostAttributeData.clear();
  }

  handleAccordionToggle = (adpostid) => {
    if (AccordionData.get(adpostid) === true) {
      AccordionData.set(adpostid, false);
      this.setState({
        refresh: this.state.refresh + 1,
      });
    } else if (AccordionData.get(adpostid) === false) {
      AccordionData.set(adpostid, true);
      this.setState({
        refresh: this.state.refresh + 1,
      });
    }
  };

  handleRemoveCompareItem = (adpostid) => {
    AdPostDetailData.delete(adpostid);
    AdPostAttributeData.delete(adpostid);
    this.props.history.location.state.comparedata.delete(adpostid);
    this.setState({
      refresh: this.state.refresh + 1,
    });
    if (AdPostDetailData.size === 0) {
      this.setState({
        toggleAlertMsg: true,
      });
    }
  };

  numberWithCommas = (price) => {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  carFeatures = () => {
    return (
      <Fragment>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>ABS</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[0].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Air Conditioning</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[1].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Alloy Rims</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[2].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>AM/FM Radio</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[3].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Blind Spot Detection Mirror</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[4].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Bluetooth System</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[5].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>CD player</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[6].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Climate Control</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[7].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Cool Box</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[8].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Cruise Control</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[9].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>DVD Player</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[10].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Fog Lights</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[11].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>{" "}
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Front Camera</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[12].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>{" "}
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Keyless Entry</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[13].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>{" "}
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Lane Departure Alert</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[14].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>{" "}
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Navigation System</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[15].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>{" "}
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Paddle Shifters</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[16].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>{" "}
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Panoramic Roof</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[17].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>{" "}
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Parking Sensors</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[18].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>{" "}
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Power Mirror</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[19].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>{" "}
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Power Seats with Memory</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[20].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>{" "}
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Power Steering</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[21].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Power Windows</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[22].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Push Button Start</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[23].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Rear AC Vents</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[24].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Rear Camera</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[25].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Sun Roof</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[26].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Traction Control</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[27].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>{" "}
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Tyre Pressure Warning System</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[28].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>{" "}
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Xeon Headlights </td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[29].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
      </Fragment>


    );
  }

  bikeFeatures = () => {
    return (
      <Fragment>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>ABS</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[0].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Digital Fuel Guage</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[1].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Kill Switch</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[2].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Pass Light</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[3].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Pillion Backrest</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[4].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Pillion Footrest</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[5].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Pillion Grab Rail</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[6].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Stepped Seat</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[7].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
        <tr
          style={{
            display:
              AccordionData.get(3) === true ? "" : "none",
          }}
        >
          <td>Turn Signal</td>
          {[...AdPostAttributeData.values()].map(
            (attributedata) => (
              <td>
                <span className="tickgreen ">
                  {attributedata[8].IsActive ? "✔" : ""}
                </span>
              </td>
            )
          )}
        </tr>
      </Fragment>
    );
  }
  render() {
    return (
      <Fragment>
        <div style={{ paddingBottom: "30px" }}>
          <div className="comparison">
            <div className="comparison-table">
              {this.state.toggleAlertMsg === true ? (
                <ErrorAlert show={this.state.toggleAlertMsg} />
              ) : (
                  <Fragment>

                    <table style={{ tableLayout: "auto" }}>
                      {this.state.isLoadingForAdPostDetail ? (
                        <Loader />
                      ) : (
                          <>
                            <thead>
                              <tr>
                                <th className="tl tl2"></th>
                                {[...AdPostDetailData.values()].map(
                                  (adpostdetail) => (
                                    <th className="qbse">
                                      <button
                                        style={{ color: "white", fontSize: "15px" }}
                                        onClick={() =>
                                          this.handleRemoveCompareItem(
                                            adpostdetail[0].AdPostID
                                          )
                                        }
                                      >
                                        Remove <i className="fas fa-times"></i>
                                      </button>
                                    </th>
                                  )
                                )}
                              </tr>
                              <tr>
                                <th className="tl"></th>
                                {[...AdPostDetailData.values()].map(
                                  (adpostdetail) => (
                                    <th className="compare-heading">
                                      {adpostdetail[0].VehicleName}
                                    </th>
                                  )
                                )}
                              </tr>
                              <tr>
                                <th></th>
                                {[...AdPostDetailData.values()].map(
                                  (adpostdetail) => (
                                    <th className="price-info">
                                      <span class="justify-content-center row">
                                        {this.numberWithCommas(
                                          adpostdetail[0].Price
                                        ) === "0.00"
                                          ? "Call For Price"
                                          : adpostdetail[0].CurCode +
                                          " " +
                                          this.numberWithCommas(
                                            adpostdetail[0].Price
                                          )}
                                      </span>

                                      <img
                                        className="b-goods-f__img mt-1"
                                        width="150px"
                                        height="100px"
                                        src={this.props.history.location.state.comparedata.get(
                                          adpostdetail[0].AdPostID
                                        )}
                                        alt=""
                                      />
                                    </th>
                                  )
                                )}
                              </tr>
                            </thead>
                            <tbody>
                              <tr onClick={() => this.handleAccordionToggle(1)}>
                                <td colSpan="5" className="compare-divider">
                                  <div className="d-flex justify-content-between">
                                    Basic Info <i className="fas fa-chevron-down"></i>
                                  </div>
                                </td>
                              </tr>
                              <Fragment>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(1) === true ? "" : "none",
                                  }}
                                >
                                  <td>Area</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].Area}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(1) === true ? "" : "none",
                                  }}
                                >
                                  <td>Body Name</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].BodyTypeName}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(1) === true ? "" : "none",
                                  }}
                                >
                                  <td>Vehicle Type</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].VehicleTypeName}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(1) === true ? "" : "none",
                                  }}
                                >
                                  <td>Engine</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].EngineType}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(1) === true ? "" : "none",
                                  }}
                                >
                                  <td>Exterior Color</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].ExteriorColor}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(1) === true ? "" : "none",
                                  }}
                                >
                                  <td>Interior Color</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].InteriorColor}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                              </Fragment>
                            </tbody>
                            <tbody>
                              <tr onClick={() => this.handleAccordionToggle(2)}>
                                <td colSpan="5" className="compare-divider">
                                  <div className="d-flex justify-content-between">
                                    Additional Info
                              <i className="fas fa-chevron-down"></i>
                                  </div>
                                </td>
                              </tr>
                              <Fragment>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(2) === true ? "" : "none",
                                  }}
                                >
                                  <td>Engine Capacity</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].EngineSize_Capacity_CC}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(2) === true ? "" : "none",
                                  }}
                                >
                                  <td>Trim</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].Trim}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(2) === true ? "" : "none",
                                  }}
                                >
                                  <td>Engin No</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].EngineNumber}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(2) === true ? "" : "none",
                                  }}
                                >
                                  <td>Cylinder</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].Cylinders}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(2) === true ? "" : "none",
                                  }}
                                >
                                  <td>Drive Side</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].DriveSide}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(2) === true ? "" : "none",
                                  }}
                                >
                                  <td>MeterReading</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].MeterReading}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(2) === true ? "" : "none",
                                  }}
                                >
                                  <td>Mileage</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].Mileage}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(2) === true ? "" : "none",
                                  }}
                                >
                                  <td>Assembly</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].Assembly}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(2) === true ? "" : "none",
                                  }}
                                >
                                  <td>Transmission</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].TransmissionName}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(2) === true ? "" : "none",
                                  }}
                                >
                                  <td>Gears</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].NumberofGears}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                                <tr
                                  style={{
                                    display:
                                      AccordionData.get(2) === true ? "" : "none",
                                  }}
                                >
                                  <td>Seats</td>
                                  {[...AdPostDetailData.values()].map(
                                    (adpostdetail) => (
                                      <td>
                                        <span className="tickgreen ">
                                          {adpostdetail[0].NumberofSeats}
                                        </span>
                                      </td>
                                    )
                                  )}
                                </tr>
                              </Fragment>
                            </tbody>
                          </>
                        )}

                      {this.state.isLoadingForFeatures ? (
                        <Loader />
                      ) : (
                          <tbody>
                            <tr onClick={() => this.handleAccordionToggle(3)}>
                              <td colSpan="5" className="compare-divider">
                                <div className="d-flex justify-content-between">
                                  Features
                              <i className="fas fa-chevron-down"></i>
                                </div>
                              </td>
                            </tr>

                            {this.state.vcid === 1 ? this.carFeatures() : this.bikeFeatures()}

                          </tbody>
                        )}
                    </table>

                  </Fragment>
                )}
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default CompareVehicle;