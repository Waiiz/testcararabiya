import React, { Component } from "react";
import Dealeradvertisement from './Dealeradvertisement';

export default class Newstwo extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <div>
        <div className="l-main-content" style={{paddingTop: '50px'}}>
          <div className="container">
            <div className="row">
              <div className="col-lg-9">
                <main className="b-post-full-wrap">
                  <article className="b-post b-post-full clearfix">
                    <div className="entry-media">
                      <a
                        className="js-zoom-images"
                        href="./assets/media/content/b-posts/830x450/porsche/1.jpg"
                      >
                        <img
                          className="img-fluid"
                          src="./assets/media/content/b-posts/830x450/porsche/1.jpg"
                          alt="Foto"
                        />
                      </a>
                    </div>
                    <div className="entry-main">
                      <div className="entry-meta">
                        <div className="entry-date bg-primary">
                          <span className="entry-date__number">20</span>
                          <span className="entry-date__month">dec</span>
                        </div>
                        <span className="entry-meta__item">
                          <a className="entry-meta__link" href="#">
                            <i className="ic icon-user" />
                            by ANGUS MACKENZIE
                          </a>
                        </span>
                      </div>
                      <div className="entry-header">
                        <h1 className="entry-title">
                          Inside the Porsche Taycan Factory: How Porsche Will
                          Build Its Electric Sports Car
                        </h1>
                      </div>
                      <br />
                      <div className="entry-content">
                        <p>
                          Porsche is betting big on the Taycan. Make no mistake,
                          Taycan is more than just greenwash, more than just a
                          hastily improvised mea culpa for the Volkswagen
                          Group's dieselgate shenanigans. Porsche is spending
                          more than a billion dollars to bring its first
                          electric sports car to market, almost $800 million of
                          that on a dedicated new factory.
                        </p>
                        <br />
                        <p>
                          The Porsche Taycan factory has been wedged in and
                          among various existing buildings at Porsche's main
                          plant in Zuffenhausen, Germany. Clearing the site
                          alone cost more than $50 million, making it a more
                          expensive option than simply expanding the newer
                          production facility at Leipzig, in the eastern part of
                          the country, where the Macan and Panamera are built.
                        </p>
                        <blockquote className="entry-blockquote">
                          <p>
                            The QR code system means the layout of the Taycan
                            factory's large final assembly plant—it could house
                            the entire Porsche Museum building three times
                            over—is very flexible. Assembly lines can be moved
                            or changed simply by laying a new line of tape. And
                            without the need to support bulky conveyors, the
                            building's floors didn't have to be as robustly
                            engineered and expensive to build as in a
                            conventional factory.
                          </p>
                        </blockquote>
                        <div className="entry-media">
                          <img
                            className="img-fluid"
                            src="./assets/media/content/b-posts/830x450/porsche/2.jpg"
                            alt="Foto"
                          />
                        </div>
                        <br />
                        <p>
                          Spending the money at Zuffenhausen was a deliberate
                          decision, says Taycan production specialist David Thor
                          Tryggvason, who points out the site is the symbolic
                          heart of Porsche, where the company's iconic 911 has
                          long been made. "We want Taycan to be part of
                          Porsche," he says, "and to send a signal that the
                          future of Porsche is at Zuffenhausen." <br />
                          <br />
                          Constructed in less than 48 months, the Porsche Taycan
                          factory includes a facility that assembles e-motors,
                          transmissions, and axles, along with a dedicated body
                          assembly line, a new paint shop, and a 2,900-foot-long
                          conveyor that transports bodies, e-motors, battery
                          packs, and axles to the final assembly hall. <br />
                          <br />
                          The innovative factory makes heavy use of automated
                          guided vehicles (AGVs) developed in house by Porsche.
                          Small AGVs in the powertrain and axle assembly areas
                          navigate between stations using sensors and wireless
                          communication. Larger AGVs that rely on optical
                          sensors to follow QR codes on lines taped to the floor
                          are used instead of traditional conveyor belts in the
                          body and final assembly areas.
                        </p>
                        <br />
                        <p>
                          The QR code system means the layout of the Taycan
                          factory's large final assembly plant—it could house
                          the entire Porsche Museum building three times over—is
                          very flexible. Assembly lines can be moved or changed
                          simply by laying a new line of tape. And without the
                          need to support bulky conveyors, the building's floors
                          didn't have to be as robustly engineered and expensive
                          to build as in a conventional factory.
                        </p>
                        <br />
                        <div className="entry-media">
                          <img
                            className="img-fluid"
                            src="./assets/media/content/b-posts/830x450/porsche/3.jpg"
                            alt="Foto"
                          />
                        </div>{" "}
                        <br />
                        <p>
                          As mentioned in our First Look story, Taycan will
                          launch in two-motor form, with e-motors in two levels
                          of tune. Core e-motor parts—rotor, stator, etc.—are
                          from third-party suppliers. The casings and
                          transmission, however, are Porsche's own design.
                          Because the e-motors will be built in two power
                          variants, Porsche says this requires a high-precision
                          assembly process. But although the e-motor assembly is
                          largely automated, Porsche workers hand-select the
                          shims that ensure there is just 0.3mm clearance
                          between the rotor/stator and the housing. Porsche
                          wants its workers actively involved in building Taycan
                          motors.
                        </p>
                        <br />
                        <p>
                          The Taycan factory is slowly being brought up to
                          speed. The first e-motor, transmission, and axle
                          assemblies were built in the middle of 2018, and the
                          first bodies a few months later. The first completed
                          Taycan came off the line in the unfinished final
                          assembly hall in December 2018, just before Christmas.
                        </p>
                        <br />
                        <p>
                          Taycan will become Porsche's sixth model line, joining
                          911, 718, Macan, Cayenne, and Panamera. Porsche says
                          the factory can produce 20,000 Taycans annually, which
                          would equate to a healthy 7.5 percent bump in total
                          production for the company over 2018. But that's a
                          conservative number: Once production processes have
                          been bedded in and Porsche is confident cars can be
                          produced in higher numbers without impacting quality,
                          the factory's innovative and flexible layout means
                          output could readily be doubled to 40,000 Taycans a
                          year. <br />
                          <br />
                          What's more, Porsche claims it is already holding
                          30,000 orders for the Taycan, before a single customer
                          has even driven one. Based on the company's 2018
                          production numbers, that would make this electric
                          Porsche more popular than the 718 Cayman/Boxster and
                          within striking distance of both the 911 and the
                          Panamera. And it's roughly 60 percent of total Tesla
                          Model S production last year.
                        </p>
                        <br />
                        <div className="url-style">
                          <span className="pr-3">
                            <b>Source :</b>
                          </span>{" "}
                          <span>
                            {" "}
                            <a
                              className="special"
                              href="https://www.motortrend.com/news/inside-porsche-taycan-factory/"
                            >
                              https://www.motortrend.com/news/inside-porsche-taycan-factory/
                            </a>
                          </span>
                        </div>
                      </div>
                    </div>
                  </article>
                  {/* end .section-reply-form*/}
                </main>
              </div>
              <div className="col-lg-3">
                <aside className="l-sidebar l-sidebar_last">
                  <section className="widget section-sidebar bg-light">
                    <h3 className="widget-title bg-cararabiya">recent posts</h3>
                    <div className="widget-content">
                      <div className="widget-inner">
                        <section className="post-widget clearfix">
                          <div className="post-widget__media">
                            <a href="News-1.html">
                              <img
                                className="img-fluid"
                                src="./assets/media/content/b-posts/80x80/1.jpg"
                                alt="foto"
                              />
                            </a>
                          </div>
                          <div className="post-widget__inner">
                            <h2 className="post-widget__title">
                              <a href="News-1.html">
                                2020 Mazda CX-30 First Drive: Recipe for Success
                              </a>
                            </h2>
                            <div className="post-widget__date">
                              <time dateTime="2019-10-27 15:20">
                                Dec 20, 2019
                              </time>
                            </div>
                          </div>
                          {/* end .widget-post*/}
                        </section>
                        <section className="post-widget clearfix">
                          <div className="post-widget__media">
                            <a href="News-3.html">
                              <img
                                className="img-fluid"
                                src="./assets/media/content/b-posts/80x80/3.jpg"
                                alt="foto"
                              />
                            </a>
                          </div>
                          <div className="post-widget__inner">
                            <h2 className="post-widget__title">
                              <a href="News-3.html">
                                2019 Subaru WRX STI S209 First Test: What Makes
                                You So Special?
                              </a>
                            </h2>
                            <div className="post-widget__date">
                              <time dateTime="2019-10-27 15:20">
                                Dec 20, 2019
                              </time>
                            </div>
                          </div>
                          {/* end .widget-post*/}
                        </section>
                      </div>
                    </div>
                  </section>
                  <Dealeradvertisement />
                  {/* end .widget-post*/}
                </aside>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
