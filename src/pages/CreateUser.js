import React, { Component, Fragment } from "react";
import { CreateUserDetails } from "../components/myusers";
import UserProfile from './UserProfile';

class CreateUser extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <Fragment>
        <div className="container mt-5 mb-5">
          <div className="row">
            <div className="col-12">
              <UserProfile />
              <hr />

              <div>
                <CreateUserDetails {...this.props} />
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default CreateUser;
