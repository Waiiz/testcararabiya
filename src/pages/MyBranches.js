import React, { Component, Fragment } from "react";
import { BranchDetails } from "../components/mybranches";
import UserProfile from './UserProfile';

class MyBranches extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <Fragment>
        <div className="container mt-5 mb-5">
          <div className="row">
            <div className="col-12">
              <UserProfile />
              <hr />

              <div>
                <BranchDetails {...this.props} />
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default MyBranches;
