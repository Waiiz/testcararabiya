import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { Dropdown } from "react-bootstrap";
import axios from "axios";
import { apiBaseURL } from "../ApiBaseURL";

class MyHeader extends Component {
  constructor(props) {
    super(props);
    this.isUserDataLoading = false;
    this.state = {
      UserData: [],
    };
  }

  getUserDataById = (userdata) => {
    let parseUserData = JSON.parse(userdata);
    //console.log(this.props.history.location.state, 'this.state.userData');

    if (this.props.history.location.state !== undefined) {
      if (this.props.history.location.state.editprofile === "editprofile") {
        this.setState({
          UserData: [],
        });
        this.props.history.location.state.editprofile = "";
      }
    }

    if (parseUserData !== null && this.state.UserData.length === 0) {
      let userid = parseUserData[0].UserID;

      if (this.isUserDataLoading) {
        return;
      }
      this.isUserDataLoading = true;

      const url = `${apiBaseURL}/user?UserID=${userid}`;
      axios
        .get(url)
        .then((res) => {
          this.isUserDataLoading = false;
          //console.log(res.data, 'res.data');
          let name = this.truncateName(res.data[0].FirstName)
          this.setState({
            UserData: res.data,
            UserID: res.data[0].UserID,
            FirstName: name,
            titleName: res.data[0].FirstName,
            ImageUrl: res.data[0].ImageUrl,
          });
        })
        .catch((error) => {
          this.isUserDataLoading = false;
          console.log(error, "userdata api");
        });
    }
  };

  //cars,bikes listing navigation
  handleChildListingClick = (vehiclecategoryid, vehicletypeid, props) => {
    this.props.handleParentListingClick(
      vehiclecategoryid,
      vehicletypeid,
      props
    );
  };

  //logout
  handleLogOutClick = () => {
    localStorage.clear();
    console.log(window.FB);
    // if (window.FB.logout !== undefined) {
    //   window.FB.logout();
    // }
    this.setState({
      UserData: [],
    });
    this.props.history.push("/login-signup");
  };

  truncateName = (name) => {
    if (name.length > 6) {
      return name.substring(0, 8).concat("...");
    } else {
      return name;
    }
  }

  render() {
    let userdata = localStorage.getItem("user");
    this.getUserDataById(userdata);
    return (
      <Fragment>
        <header className="header">
          <div className="header-main" style={{ padding: "10px 1%" }}>
            <div className="container-fluid">
              <div className="row nav-margin-b">
                <div className="logo col-lg-2 col-auto">
                  <Link className="navbar-brand scroll " to="/">
                    <img
                      className="normal-logo"
                      src="./assets/media/logo-white.png"
                      alt="logo"
                    />
                  </Link>
                </div>
                <div className="d-flex align-self-center">
                  <button className="menu-mobile-button js-toggle-mobile-slidebar toggle-menu-button">
                    <i className="toggle-menu-button-icon">
                      <span />
                      <span />
                      <span />
                      <span />
                      <span />
                      <span />
                    </i>
                  </button>
                </div>
                <div className="container-fluid">
                  <div id="nav-main" className="row nav">
                    <div className="col-lg-2 d-flex align-items-center justify-content-start">
                      <Link className="navbar-brand scroll" to="/">
                        <img
                          className="normal-logo"
                          src="./assets/media/logo-white.png"
                          alt="logo"
                        />
                      </Link>
                    </div>
                    <div className="col-lg-6">
                      <nav
                        className="navbar navbar-dark navbar-expand-lg justify-content-left"
                        id="nav"
                      >
                        <ul className="yamm main-menu navbar-nav">
                          <Dropdown>
                            <Dropdown.Toggle
                              variant="link"
                              id="dropdown-basic"
                              className="nav-link ddheader"
                            >
                              Cars
                            </Dropdown.Toggle>

                            <Dropdown.Menu>
                              <Dropdown.Item
                                onClick={() =>
                                  this.handleChildListingClick(1, 1, this.props)
                                }
                                style={{ color: "#000" }}
                              >
                                New Cars
                              </Dropdown.Item>
                              <Dropdown.Item
                                onClick={() =>
                                  this.handleChildListingClick(1, 2, this.props)
                                }
                                style={{ color: "#000" }}
                              >
                                Used Cars
                              </Dropdown.Item>
                              <Dropdown.Item
                                onClick={() =>
                                  this.handleChildListingClick(1, 3, this.props)
                                }
                                style={{ color: "#000" }}
                              >
                                Certified Cars
                              </Dropdown.Item>
                            </Dropdown.Menu>
                          </Dropdown>

                          <Dropdown>
                            <Dropdown.Toggle
                              variant="link"
                              id="dropdown-basic"
                              className="nav-link ddheader"
                            >
                              Bikes
                            </Dropdown.Toggle>

                            <Dropdown.Menu>
                              <Dropdown.Item
                                onClick={() =>
                                  this.handleChildListingClick(4, 1, this.props)
                                }
                                style={{ color: "#000" }}
                              >
                                New Bikes
                              </Dropdown.Item>
                              <Dropdown.Item
                                onClick={() =>
                                  this.handleChildListingClick(4, 2, this.props)
                                }
                                style={{ color: "#000" }}
                              >
                                Used Bikes
                              </Dropdown.Item>
                              <Dropdown.Item
                                onClick={() =>
                                  this.handleChildListingClick(4, 3, this.props)
                                }
                                style={{ color: "#000" }}
                              >
                                Certified Bikes
                              </Dropdown.Item>
                            </Dropdown.Menu>
                          </Dropdown>

                          <li className="nav-item">
                            <Link className="nav-link" to="/promotions">
                              Offers &amp; Promotions
                            </Link>
                          </li>
                          <li className="nav-item">
                            <Link className="nav-link" to="/financing">
                              Financing
                            </Link>
                          </li>
                          <li className="nav-item">
                            <Link className="nav-link csoon" to="/shop">
                              Shop
                            </Link>
                          </li>
                          <li className="nav-item">
                            <Link className="nav-link" to="/dealers">
                              Dealers
                            </Link>
                          </li>
                        </ul>
                      </nav>
                    </div>
                    <div className="col-lg-4">
                      <div className="row justify-content-end align-items-center">
                        <Dropdown>
                          <Dropdown.Toggle
                            variant="link"
                            style={{
                              border: "none",
                              color: "white",
                              fontWeight: 500,
                              fontSize: "1em",
                            }}
                            id="dropdown-basic"
                          >
                            English
                          </Dropdown.Toggle>

                          <Dropdown.Menu>
                            <Dropdown.Item>
                              <Link to="/" style={{ color: "#000" }}>
                                عربي
                              </Link>
                            </Dropdown.Item>
                          </Dropdown.Menu>
                        </Dropdown>

                        <div className="border1 mr-4">&nbsp;</div>

                        <div>
                          <li className="dropdown country-select open">
                            <Link to="/">
                              <span className="flag-sa f-sa" />
                              <i className="mr-5" aria-hidden="true">
                                <span
                                  className="pl-1 mt-3"
                                  style={{
                                    fontSize: "13px",
                                    fontFamily: '"Montserrat"',
                                    fontWeight: 700,
                                    fontStyle: "normal",
                                  }}
                                >
                                  KSA
                                </span>
                              </i>
                            </Link>
                          </li>
                          <li className="dropdown country-select open d-none">
                            <Link
                              to="/"
                              data-toggle="dropdown"
                              aria-label="Countries"
                              aria-expanded="true"
                            >
                              <span className="flag-uae f-uae" />
                              <i className="mr-5" aria-hidden="true">
                                <span
                                  className="pl-1 mt-3"
                                  style={{
                                    fontSize: "13px",
                                    fontFamily: '"Montserrat"',
                                    fontWeight: 700,
                                    fontStyle: "normal",
                                  }}
                                >
                                  UAE
                                </span>
                              </i>
                            </Link>
                          </li>
                        </div>

                        {userdata === null || userdata === undefined ? (
                          <div className>
                            <Link
                              role="button"
                              className="btn btn-outline-light btn-sm btn-s"
                              to="/login-signup"
                            >
                              Sign In / Register
                            </Link>
                          </div>
                        ) : (
                            <Dropdown>
                              <Dropdown.Toggle
                                variant="link"
                                style={{
                                  border: "none",
                                  color: "white",
                                  fontWeight: 500,
                                  fontSize: "1em",
                                }}
                                id="dropdown-basic"
                              >
                                <img
                                  style={{
                                    paddingLeft: "0.25rem",
                                    float: "right",
                                  }}
                                  width="32px"
                                  heigth="32px"
                                  src={this.state.ImageUrl}
                                  alt={this.state.UserID}
                                  title={this.state.titleName}
                                />
                                <span title={this.state.titleName}>{this.state.FirstName}</span>
                              </Dropdown.Toggle>

                              <Dropdown.Menu>
                                <Dropdown.Item>
                                  <Link to="/" style={{ color: "#000" }}>
                                    Inbox
                                </Link>
                                </Dropdown.Item>
                                <Dropdown.Item>
                                  <Link
                                    to="/edit-profile"
                                    style={{ color: "#000" }}
                                  >
                                    Edit Profile
                                </Link>
                                </Dropdown.Item>
                                <Dropdown.Item>
                                  <Link
                                    to="/change-password"
                                    style={{ color: "#000" }}
                                  >
                                    Change Password
                                </Link>
                                </Dropdown.Item>
                                <Dropdown.Item>
                                  <Link to="/my-ads" style={{ color: "#000" }}>
                                    My Ads
                                </Link>
                                </Dropdown.Item>
                                <Dropdown.Item>
                                  <Link
                                    to="/mysaved-ads"
                                    style={{ color: "#000" }}
                                  >
                                    My SavedAds
                                </Link>
                                </Dropdown.Item>
                                <Dropdown.Item>
                                  <Link
                                    onClick={this.handleLogOutClick}
                                    style={{ color: "#000" }}
                                  >
                                    Logout
                                </Link>
                                </Dropdown.Item>
                              </Dropdown.Menu>
                            </Dropdown>
                          )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
      </Fragment>
    );
  }
}
export default MyHeader;