import React, { Component, Fragment } from "react";
import { apiBaseURL } from "../ApiBaseURL";
import axios from "axios";
import moment from "moment";
import { Link } from "react-router-dom";

class UserProfile extends Component {
  constructor(props) {
    super(props);
    this.isUserDataLoading = false;
    this.state = {
      userData: [],
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);

    let userData = localStorage.getItem("user");
    this.getUserDataById(userData);
  }

  getUserDataById = (userData) => {
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null && this.state.userData.length === 0) {
      let userid = parseUserData[0].UserID;
      if (this.isUserDataLoading) {
        return;
      }
      this.isUserDataLoading = true;

      const url = `${apiBaseURL}/user?UserID=${userid}`;
      axios
        .get(url)
        .then((res) => {
          //console.log(res.data, 'res');

          this.isUserDataLoading = false;
          const formatteddate = moment(res.data[0].CreatedDate).format(
            "MMMM YYYY"
          );
          this.setState({
            userData: res.data,
            UserID: res.data[0].UserID,
            RoleName: res.data[0].RoleName,
            Firstname: res.data[0].FirstName,
            Lastname: res.data[0].LastName,
            CreatedDate: formatteddate,
            ImageUrl: res.data[0].ImageUrl,
          });
        })
        .catch((error) => {
          this.isUserDataLoading = false;
          console.log(error, "userdata api");
        });
    }
  };

  individualUser = () => {
    return (
      <div className="profile-user">
        <div className="b-seller__detail_2 pt-3">
          <div className="row">
            <Link style={{ color: "black" }} to="/my-ads">
              <div className="mx-4">
                <i style={{ color: "#D3E428" }} className="fas fa-car"></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> My Ads</strong>
                </span>
              </div>
            </Link>
            <Link style={{ color: "black" }} to="/mysaved-ads">
              <div className="mx-4">
                <i style={{ color: "#D3E428" }} className="fas fa-heart"></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> My Saved Ads</strong>
                </span>
              </div>
            </Link>
            <Link
              style={{ color: "black" }}
              to="javascript:alert('Coming Soon!')"
            >
              <div className="mx-4">
                <i style={{ color: "#D3E428" }} className="fas fa-envelope"></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> My Messages</strong>
                </span>
              </div>
            </Link>
            <Link style={{ color: "black" }} to="/my-packages">
              <div className="mx-4">
                <i
                  style={{ color: "#D3E428" }}
                  className="far fa-credit-card"
                ></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> My Packages</strong>
                </span>
              </div>
            </Link>
          </div>
        </div>
      </div>
    );
  };

  BranchAdmin = () => {
    return (
      <div className="profile-user">
        <div className="b-seller__detail_2 pt-3">
          <div className="row">
            <Link style={{ color: "black" }} to="/my-ads">
              <div className="mx-4">
                <i style={{ color: "#D3E428" }} className="fas fa-car"></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> My Ads</strong>
                </span>
              </div>
            </Link>
            <Link style={{ color: "black" }} to="/mysaved-ads">
              <div className="mx-4">
                <i style={{ color: "#D3E428" }} className="fas fa-heart"></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> My Saved Ads</strong>
                </span>
              </div>
            </Link>
            <Link
              style={{ color: "black" }}
              to="javascript:alert('Coming Soon!')"
            >
              <div className="mx-4">
                <i style={{ color: "#D3E428" }} className="fas fa-envelope"></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> My Messages</strong>
                </span>
              </div>
            </Link>
            <Link style={{ color: "black" }} to="/my-users">
              <div className="mx-4">
                <i
                  style={{ color: "#D3E428" }}
                  className="fas fa-users"
                ></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> My Users</strong>
                </span>
              </div>
            </Link>
            <Link style={{ color: "black" }} to="/dealerofferpromotions">
              <div className="mx-4">
                <i
                  style={{ color: "#D3E428" }}
                  className="fas fa-ad"
                ></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> Promotions & Offers</strong>
                </span>
              </div>
            </Link>
          </div>
        </div>
      </div>
    );
  };

  BranchAdminUser = () => {
    return (
      <div className="profile-user">
        <div className="b-seller__detail_2 pt-3">
          <div className="row">
            <Link style={{ color: "black" }} to="/my-ads">
              <div className="mx-4">
                <i style={{ color: "#D3E428" }} className="fas fa-car"></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> My Ads</strong>
                </span>
              </div>
            </Link>
            <Link style={{ color: "black" }} to="/mysaved-ads">
              <div className="mx-4">
                <i style={{ color: "#D3E428" }} className="fas fa-heart"></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> My Saved Ads</strong>
                </span>
              </div>
            </Link>
            <Link
              style={{ color: "black" }}
              to="javascript:alert('Coming Soon!')"
            >
              <div className="mx-4">
                <i style={{ color: "#D3E428" }} className="fas fa-envelope"></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> My Messages</strong>
                </span>
              </div>
            </Link>
            <Link style={{ color: "black" }} to="/dealerofferpromotions">
              <div className="mx-4">
                <i
                  style={{ color: "#D3E428" }}
                  className="fas fa-ad"
                ></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> Promotions & Offers</strong>
                </span>
              </div>
            </Link>
          </div>
        </div>
      </div>
    );
  };

  OwnerDealer = () => {
    return (
      <div className="profile-user">
        <div className="b-seller__detail_2 pt-3">
          <div className="row">
            <Link style={{ color: "black" }} to="/my-ads">
              <div className="mx-4">
                <i style={{ color: "#D3E428" }} className="fas fa-car"></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> My Ads</strong>
                </span>
              </div>
            </Link>
            <Link style={{ color: "black" }} to="/mysaved-ads">
              <div className="mx-4">
                <i style={{ color: "#D3E428" }} className="fas fa-heart"></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> My Saved Ads</strong>
                </span>
              </div>
            </Link>
            <Link
              style={{ color: "black" }}
              to="/my-branches"
            >
              <div className="mx-4">
                <i style={{ color: "#D3E428" }} className="fas fa-envelope"></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> My Branches</strong>
                </span>
              </div>
            </Link>
            <Link style={{ color: "black" }} to="/my-users">
              <div className="mx-4">
                <i
                  style={{ color: "#D3E428" }}
                  className="fas fa-users"
                ></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> My Users</strong>
                </span>
              </div>
            </Link>
            <Link style={{ color: "black" }} to="/my-packages">
              <div className="mx-4">
                <i
                  style={{ color: "#D3E428" }}
                  className="far fa-credit-card"
                ></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> My Packages</strong>
                </span>
              </div>
            </Link>
            <Link style={{ color: "black" }} to="/dealerofferpromotions">
              <div className="mx-4">
                <i
                  style={{ color: "#D3E428" }}
                  className="fas fa-ad"
                ></i>
                <span style={{ fontSize: "12px" }}>
                  {" "}
                  <strong> Promotions & Offers</strong>
                </span>
              </div>
            </Link>
          </div>
        </div>
      </div>
    );
  };

  render() {
    const { UserID, RoleName, Firstname, Lastname, CreatedDate, ImageUrl } = this.state;
    return (
      <Fragment>
        <div className="profile-user">
          <div className="b-seller__detail pt-3">
            <div className="b-seller__img">
              <img className="img-scale" src={ImageUrl} alt={UserID} />
            </div>
            <div className="b-seller__title">
              <div className="b-seller__name">
                {Firstname} {Lastname}
              </div>
              <div className="b-seller__category">
                Member Since {CreatedDate}
              </div>
              <br />
              <div className="savesearch">
                <Link to="/edit-profile">Edit Profile</Link>
              </div>
            </div>
          </div>
        </div>

        {RoleName === "Individual Seller" ? this.individualUser() : RoleName === "Branch Admin" ? this.BranchAdmin() : RoleName === "Data Entry Operator" ? this.BranchAdminUser() : RoleName === "Owner" ? this.OwnerDealer() : ''}
      </Fragment>
    );
  }
}

export default UserProfile;