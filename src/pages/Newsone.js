import React, { Component } from "react";
import { Link } from "react-router-dom";
import Dealeradvertisement from './Dealeradvertisement';

export default class Newsone extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <div>
        <div>
          <div className="l-main-content" style={{paddingTop: '50px'}}>
            <div className="container">
              <div className="row">
                <div className="col-lg-9">
                  <main className="b-post-full-wrap">
                    <article className="b-post b-post-full clearfix">
                      <div className="entry-media">
                        <a
                          className="js-zoom-images"
                          href="./assets/media/content/b-posts/830x450/mazda/1.jpg"
                        >
                          <img
                            className="img-fluid"
                            src="./assets/media/content/b-posts/830x450/mazda/1.jpg"
                            alt="Foto"
                          />
                        </a>
                      </div>
                      <div className="entry-main">
                        <div className="entry-meta">
                          <div className="entry-date bg-primary">
                            <span className="entry-date__number">20</span>
                            <span className="entry-date__month">dec</span>
                          </div>
                          <span className="entry-meta__item">
                            <a className="entry-meta__link" href="#">
                              <i className="ic icon-user" />
                              by Erick Ayapana
                            </a>
                          </span>
                        </div>
                        <div className="entry-header">
                          <h1 className="entry-title">
                            2020 Mazda CX-30 First Drive: Recipe for Success
                          </h1>
                        </div>
                        <br />
                        <div className="entry-content">
                          <p>
                            The 2020 Mazda CX-30 hopes to make lemonade out of
                            lemons. In this case, the lemons are the growing
                            number of American customers who continue to look
                            away from handsome sedans and hatchbacks such as the
                            Mazda3 in favor of crossovers. And with that in
                            mind, Mazda came up with a new recipe, one that uses
                            the Mazda3's chassis to create the new CX-30, set to
                            arrive in showrooms just in time for Christmas.
                          </p>
                          <br />
                          <p>
                            Slotting in between the CX-3 and CX-5, the 2020
                            CX-30 also borrows styling cues from the Mazda3,
                            showcasing the automaker's latest iteration of Kodo
                            design with clean lines and smooth curves. The thick
                            plastic cladding bordering the wheel wells may be
                            controversial, but necessary to reduce the
                            sheetmetal's visual mass. The round LED rear turn
                            signals that fade out are a cool and unique touch.
                          </p>
                          <blockquote className="entry-blockquote">
                            <p>
                              Under the hood is the same 2.5-liter four-cylinder
                              engine found in the Mazda3. Rated at 186 hp and
                              186 lb-ft of torque, it's mated to a six-speed
                              automatic transmission driving the front wheels.
                            </p>
                          </blockquote>
                          <div className="entry-media">
                            <img
                              className="img-fluid"
                              src="./assets/media/content/b-posts/830x450/mazda/2.jpg"
                              alt="Foto"
                            />
                          </div>
                          <br />
                          <p>
                            Under the hood is the same 2.5-liter four-cylinder
                            engine found in the Mazda3. Rated at 186 hp and 186
                            lb-ft of torque, it's mated to a six-speed automatic
                            transmission driving the front wheels. Our tester
                            had the optional all-wheel-drive system. The CX-30
                            puts down power in a noticeably more peppy and
                            responsive manner compared to many of its
                            competitors saddled with laggy turbos and
                            continuously variable transmissions. The CX-30 isn't
                            much heavier than the Mazda3 from which it's based,
                            so power feels adequate, though more oomph at
                            highway passing speeds would be welcome. We're
                            crossing our fingers that Mazda's 2.5-liter turbo
                            eventually finds its way to the CX-30. With EPA
                            estimates ranging from 24-25 mpg city and 31-33 mpg
                            highway (depending on trim), the CX-30 won't be the
                            most miserly among competitors, but it's a small
                            sacrifice to pay for those who value performance.
                          </p>
                          <br />
                          <p>
                            With a ground clearance of 7.9 inches, the CX-30
                            sits 2.4 inches higher than the Mazda3. Thankfully
                            its taller stance doesn't have a negative effect on
                            handling, as it seemingly tackles sweeping corners
                            with the control and balance of the fun-to-drive
                            Mazda3. Brakes are excellent, befitting of a Miata,
                            with firm feel and short travel. Suspension tuning
                            is on the firm side, but overall ride quality is
                            relatively smooth, especially considering our
                            tester's 18-inch wheels (16s are standard). The rear
                            end occasionally dances a bit at highway speeds
                            above 70 mph, but not often enough to be a nuisance.
                            Road noise could be quieter, but seems to be on par
                            with the segment.
                          </p>
                          <br />
                          <p>
                            Where the CX-30 excels, however, is interior design
                            and quality. Like the Mazda3, the soft touch
                            surfaces on the dash and door panels wouldn't be out
                            of place in an Audi, and the same goes for the
                            knurled HVAC knobs and the instrument panel's
                            digital screen. The infotainment screen placed high
                            on the dashboard also looks premium and is operated
                            by a fairly intuitive mix of buttons and a rotary
                            knob that sit just below the gear shifter.
                            Highlights from our fully loaded test car include a
                            head-up display, powerful and clear Bose sound
                            system, and a power rear liftgate.
                          </p>
                          <br />
                          <div className="entry-media">
                            <img
                              className="img-fluid"
                              src="./assets/media/content/b-posts/830x450/mazda/3.jpg"
                              alt="Foto"
                            />
                          </div>{" "}
                          <br />
                          <p>
                            Price of entry for a base model, front-drive CX-30
                            is $22,945 and the list of standard features is
                            impressive: LED headlights, rain-sensing wipers,
                            lane-keep assist, emergency brake assist, and
                            adaptive cruise control that remains active in
                            stop-and-go traffic. Apple CarPlay and Android Auto
                            will require a step up to the Select Package
                            ($24,945), which also includes 18-inch wheels,
                            dual-zone climate control, rear HVAC vents, privacy
                            glass, and leatherette seats. Preferred
                            Package-equipped CX-30s cost $27,245 and add heated
                            front seats, power-adjustable driver's seat, Bose
                            sound system, and satellite radio. Finally, the
                            range-topping Premium Package ($29,245) throws in
                            more tech and luxury including leather seats, the
                            head-up display, paddle shifters, sunroof, and power
                            liftgate. All-wheel drive is optional on all trims
                            for $1,400.
                          </p>
                          <br />
                          <p>
                            So why should you spend the extra coin for a CX-30
                            over a Mazda3 aside from its higher seating position
                            and pseudo off-roader looks? For starters, there's
                            more room for rear passengers. Its 36.3 inches of
                            rear legroom provides 1.2 inches more than the 3,
                            which doesn't sound like a lot, but it's definitely
                            noticeable. Cargo room behind the rear seats is on
                            par with the Mazda3 hatchback, but interestingly
                            enough, the CX-30 provides 1.9 cubic feet less space
                            than the Mazda3 with the rear seats folded down. It
                            can also be optioned with roof rails for your
                            outdoor gear, and the CX-30 debuts Mazda's new
                            Connected Services system (standard across the
                            lineup), which includes Wi-Fi hotspot and
                            over-the-air updates for the infotainment system.
                            Mazda Connected Services also interacts with the
                            CX-30 via your personal device, allowing you to
                            start the engine remotely, lock/unlock doors, send
                            destination details to the navigation system, and
                            monitor vitals like oil levels, tire pressures, and
                            more.
                          </p>
                          <br />
                          <div className="url-style">
                            <span className="pr-3">
                              <b>Source :</b>
                            </span>{" "}
                            <span>
                              {" "}
                              <a
                                className="special"
                                href="https://www.motortrend.com/cars/mazda/cx-30/2020/2020-mazda-cx-30-first-drive-review/"
                              >
                                https://www.motortrend.com/cars/mazda/cx-30/2020/2020-mazda-cx-30-first-drive-review/
                              </a>
                            </span>
                          </div>
                        </div>
                      </div>
                    </article>
                    {/* end .section-reply-form*/}
                  </main>
                </div>
                <div className="col-lg-3">
                  <aside className="l-sidebar l-sidebar_last">
                    <section className="widget section-sidebar bg-light">
                      <h3 className="widget-title bg-cararabiya">
                        recent posts
                      </h3>
                      <div className="widget-content">
                        <div className="widget-inner">
                          <section className="post-widget clearfix">
                            <div className="post-widget__media">
                              <a href="News-2.html">
                                <img
                                  className="img-fluid"
                                  src="./assets/media/content/b-posts/80x80/2.jpg"
                                  alt="foto"
                                />
                              </a>
                            </div>
                            <div className="post-widget__inner">
                              <h2 className="post-widget__title">
                                <a href="News-2.html">
                                  Inside the Porsche Taycan Factory: How Porsche
                                  Will Build Its Electric Sports Car
                                </a>
                              </h2>
                              <div className="post-widget__date">
                                <time dateTime="2019-10-27 15:20">
                                  Dec 20, 2019
                                </time>
                              </div>
                            </div>
                            {/* end .widget-post*/}
                          </section>
                          <section className="post-widget clearfix">
                            <div className="post-widget__media">
                              <a href="News-3.html">
                                <img
                                  className="img-fluid"
                                  src="./assets/media/content/b-posts/80x80/3.jpg"
                                  alt="foto"
                                />
                              </a>
                            </div>
                            <div className="post-widget__inner">
                              <h2 className="post-widget__title">
                                <a href="News-3.html">
                                  2019 Subaru WRX STI S209 First Test: What
                                  Makes You So Special?
                                </a>
                              </h2>
                              <div className="post-widget__date">
                                <time dateTime="2019-10-27 15:20">
                                  Dec 20, 2019
                                </time>
                              </div>
                            </div>
                            {/* end .widget-post*/}
                          </section>
                        </div>
                      </div>
                    </section>
                    <Dealeradvertisement />
                    {/* end .widget-post*/}
                  </aside>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}