import React, { Component } from "react";
import { Accordion, Card } from "react-bootstrap";

export default class FAQ extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <div>
        <div style={{ minHeight: "780px" }}>
          <div
            className="container-fluid"
            style={{
              backgroundColor: "#646464",
              padding: "60px 50px 40px 50px"
            }}
          >
            <div className="col-12 d-flex justify-content-center">
              <span
                style={{ fontSize: "30px", fontWeight: 800, color: "white" }}
              >
                Frequently Asked Questions
              </span>
            </div>
          </div>

          <div className="container">
            <div className="col-12 mt-5">
              <section>
                {/*Accordion wrapper*/}
                <Accordion className="accordion md-accordion">
                  {/* Accordion card */}
                  <Card
                    className="card"
                    style={{ borderBottom: "1px solid #99999978" }}
                  >
                    {/* Card header */}
                    <Accordion.Toggle
                      as={Card.Header}
                      eventKey="0"
                      className="card-header"
                      style={{ padding: "1rem 1.5rem", cursor: "pointer" }}
                    >
                      <span className="collapsed">
                        <h5 className="mb-0">
                          Q.1: What is Cararabiya?{" "}
                          <i className="fas fa-angle-down rotate-icon" />
                        </h5>
                      </span>
                    </Accordion.Toggle>
                    {/* Card body */}
                    <Accordion.Collapse eventKey="0">
                      <Card.Body>
                        <div
                          className="card-body faq"
                          style={{
                            fontSize: ".9rem",
                            fontWeight: 400,
                            lineHeight: "1.7",
                            color: "#464646",
                            padding: "0px 55px 16px 66px"
                          }}
                        >
                          <p>
                            Cararabiya is an online virtual marketplace for car
                            shoppers and sellers. Cararabiya offers complete and
                            seamless online car buying experience.
                          </p>
                        </div>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                  {/* Accordion card */}

                  {/* Accordion card */}
                  <Card
                    className="card"
                    style={{ borderBottom: "1px solid #99999978" }}
                  >
                    {/* Card header */}
                    <Accordion.Toggle
                      as={Card.Header}
                      eventKey="1"
                      className="card-header"
                      style={{ padding: "1rem 1.5rem", cursor: "pointer" }}
                    >
                      <span className="collapsed">
                        <h5 className="mb-0">
                          Q.2: How do I register for Cararabiya?
                          <i className="fas fa-angle-down rotate-icon" />
                        </h5>
                      </span>
                    </Accordion.Toggle>
                    {/* Card body */}
                    <Accordion.Collapse eventKey="1">
                      <Card.Body>
                        <div
                          className="card-body faq"
                          style={{
                            fontSize: ".9rem",
                            fontWeight: 400,
                            lineHeight: "1.7",
                            color: "#464646",
                            padding: "0px 55px 16px 28px"
                          }}
                        >
                          <ul>
                            <p>Registration is a quick and simple process :</p>
                            <li>
                              This can be done by clicking on the sign
                              in/register icon.
                            </li>
                            <li>
                              You can register via your social media profiles or
                              by using your valid email address and phone
                              number. You will receive a notification confirming
                              your registration.
                            </li>
                          </ul>
                        </div>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                  {/* Accordion card */}

                  {/* Accordion card */}
                  <Card
                    className="card"
                    style={{ borderBottom: "1px solid #99999978" }}
                  >
                    {/* Card header */}
                    <Accordion.Toggle
                      as={Card.Header}
                      eventKey="2"
                      className="card-header"
                      style={{ padding: "1rem 1.5rem", cursor: "pointer" }}
                    >
                      <span className="collapsed">
                        <h5 className="mb-0">
                          Q.3: How do I sign in?
                          <i className="fas fa-angle-down rotate-icon" />
                        </h5>
                      </span>
                    </Accordion.Toggle>
                    {/* Card body */}
                    <Accordion.Collapse eventKey="2">
                      <Card.Body>
                        <div
                          className="card-body faq"
                          style={{
                            fontSize: ".9rem",
                            fontWeight: 400,
                            lineHeight: "1.7",
                            color: "#464646",
                            padding: "0px 55px 16px 28px"
                          }}
                        >
                          <ul>
                            <li>
                              Sign in can be done by clicking on the Sign
                              in/register icon.
                            </li>
                            <li>
                              If you have registered via your social media
                              profiles then please use the relevant icons to
                              sign in.
                            </li>
                            <li>
                              Or if you have used your email address to register
                              you will have to use your email address to sign
                              in.
                            </li>
                          </ul>
                        </div>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                  {/* Accordion card */}

                  {/* Accordion card */}
                  <Card
                    className="card"
                    style={{ borderBottom: "1px solid #99999978" }}
                  >
                    {/* Card header */}
                    <Accordion.Toggle
                      as={Card.Header}
                      eventKey="3"
                      className="card-header"
                      style={{ padding: "1rem 1.5rem", cursor: "pointer" }}
                    >
                      <span className="collapsed">
                        <h5 className="mb-0">
                          Q.4: Why am I unable to sign in?
                          <i className="fas fa-angle-down rotate-icon" />
                        </h5>
                      </span>
                    </Accordion.Toggle>
                    {/* Card body */}
                    <Accordion.Collapse eventKey="3">
                      <Card.Body>
                        <div
                          className="card-body faq"
                          style={{
                            fontSize: ".9rem",
                            fontWeight: 400,
                            lineHeight: "1.7",
                            color: "#464646",
                            padding: "0px 55px 16px 28px"
                          }}
                        >
                          <ul>
                            <li>
                              Please check your sign in details such as email
                              address, password and make sure that the correct
                              information is being inserted. Kindly save your
                              login details in a safe and secure place.
                            </li>
                            <li>
                              Please check if you have completed the
                              registration process by entering OTP received via
                              email. If you haven’t please complete the
                              registration process. You can also contact our
                              support team for further assistance.
                            </li>
                          </ul>
                        </div>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                  {/* Accordion card */}

                  {/* Accordion card */}
                  <Card
                    className="card"
                    style={{ borderBottom: "1px solid #99999978" }}
                  >
                    {/* Card header */}
                    <Accordion.Toggle
                      as={Card.Header}
                      eventKey="4"
                      className="card-header"
                      style={{ padding: "1rem 1.5rem", cursor: "pointer" }}
                    >
                      <span className="collapsed">
                        <h5 className="mb-0">
                          Q.5: What if I forgot my password?
                          <i className="fas fa-angle-down rotate-icon" />
                        </h5>
                      </span>
                    </Accordion.Toggle>
                    {/* Card body */}
                    <Accordion.Collapse eventKey="4">
                      <Card.Body>
                        <div
                          className="card-body faq"
                          style={{
                            fontSize: ".9rem",
                            fontWeight: 400,
                            lineHeight: "1.7",
                            color: "#464646",
                            padding: "0px 55px 16px 28px"
                          }}
                        >
                          <ul>
                            <li>
                              To retrieve forgotten password please click the
                              link forgot password on sign in page. If you have
                              used your email address to register you will be
                              asked to enter the same email address.
                            </li>
                            <li>
                              A link to reset password will be sent on the
                              registered email address. Or if you have
                              registered via social media profiles please check
                              if you still have access to these social media
                              profiles and try signing in again.
                            </li>
                          </ul>
                        </div>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                  {/* Accordion card */}

                  {/* Accordion card */}
                  <Card
                    className="card"
                    style={{ borderBottom: "1px solid #99999978" }}
                  >
                    {/* Card header */}
                    <Accordion.Toggle
                      as={Card.Header}
                      eventKey="5"
                      className="card-header"
                      style={{ padding: "1rem 1.5rem", cursor: "pointer" }}
                    >
                      <span className="collapsed">
                        <h5 className="mb-0">
                          Q.6: what you do....Why do you do?{" "}
                          <i className="fas fa-angle-down rotate-icon" />
                        </h5>
                      </span>
                    </Accordion.Toggle>
                    {/* Card body */}
                    <Accordion.Collapse eventKey="5">
                      <Card.Body>
                        <div
                          className="card-body faq"
                          style={{
                            fontSize: ".9rem",
                            fontWeight: 400,
                            lineHeight: "1.7",
                            color: "#464646",
                            padding: "0px 55px 16px 66px"
                          }}
                        >
                          <p>
                            Cararabiya.com consists of four sub-categories --New
                            Car Details, New Car Search By Make, Vehicle Type
                            and Price Range, Dealer Details and Latest Car
                            Details. Cararabiya.com features latest car news,
                            car photos, all car model detailed specification ,
                            photo galleries, classics, videos and more.
                            Cararabiya is an online service community that
                            offers auto enthusiasts a friendly home where they
                            can find their dream car.
                          </p>
                        </div>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                  {/* Accordion card */}

                  {/* Accordion card */}
                  <Card
                    className="card"
                    style={{ borderBottom: "1px solid #99999978" }}
                  >
                    {/* Card header */}
                    <Accordion.Toggle
                      as={Card.Header}
                      eventKey="6"
                      className="card-header"
                      style={{ padding: "1rem 1.5rem", cursor: "pointer" }}
                    >
                      <span className="collapsed">
                        <h5 className="mb-0">
                          Q.7: How do I access Cararabiya?
                          <i className="fas fa-angle-down rotate-icon" />
                        </h5>
                      </span>
                    </Accordion.Toggle>
                    {/* Card body */}
                    <Accordion.Collapse eventKey="6">
                      <Card.Body>
                        <div
                          className="card-body faq"
                          style={{
                            fontSize: ".9rem",
                            fontWeight: 400,
                            lineHeight: "1.7",
                            color: "#464646",
                            padding: "0px 55px 16px 28px"
                          }}
                        >
                          <ul>
                            <li style={{ listStyleType: "square" }}>
                              Cararabiya.com consists of four sub-categories
                              --New Car Details, New Car Search By Make, Vehicle
                              Type and Price Range, Dealer Details and Latest
                              Car Details.
                            </li>
                            <li style={{ listStyleType: "square" }}>
                              Cararabiya.com features latest car news, car
                              photos, all car model detailed specification ,
                              photo galleries, classics, videos and more.
                            </li>
                            <li style={{ listStyleType: "square" }}>
                              Cararabiya is an online service community that
                              offers auto enthusiasts a friendly home where they
                              can find their dream car.
                            </li>
                          </ul>
                        </div>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                  {/* Accordion card */}
                </Accordion>
                {/* Accordion wrapper */}
              </section>
            </div>
          </div>
        </div>
      </div>
    );
  }
}