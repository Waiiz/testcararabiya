import React, { Component } from "react";
import { Range } from "rc-slider";
import "rc-slider/assets/index.css";
import { Consumer } from "../Context";
import { Link } from "react-router-dom";
import { Accordion, Card, Button, Modal } from "react-bootstrap";
import { getModalData, getVersionData } from '../util/request';
import {
  apiUrlcity,
  apiUrlmake,
  apiUrlvehicletype,
  apiUrltransmission,
  apiUrlBodytype,
  apiUrlsellertype,
} from "../ApiBaseURL";

class Advancesearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //make
      lmake: [],
      //model
      lmodel: [],
      //version
      lversion: [],
      //price
      min: 15000,
      max: 15000000,

      //door
      doormin: 2,
      doormax: 5,

      //engine size
      engsizemin: 1000,
      engsizemax: 4000,

      //versionyear
      versionyearmin: 1990,
      versionyearmax: 2020,

      //meterreading
      meterreadingmin: 0,
      meterreadingmax: 20000000,

      //mileage
      mileageminimum: 10,
      mileagemaximum: 40,

      //fueltype
      fuelTypes: [
        { id: 1, value: "Diesel" },
        { id: 2, value: "Petrol" }
      ],

      //owners
      owners: [
        { id: 1, value: "1" },
        { id: 2, value: "2" },
        { id: 3, value: "3+" }
      ],

      //driveside
      driveSide: [
        { id: "LHD", value: "Left Hand Drive" },
        { id: "RHD", value: "Right Hand Drive" }
      ],

      //assembly
      assembly: [
        { id: "imp", value: "Imported" },
        { id: "loc", value: "local" }
      ],

      //images
      images: [
        { id: true, value: "Ads with images" },
        { id: false, value: "Ads without images" }
      ],

      //sellertype
      lsellertype: [],
      //city
      lcity: [],
      scity: [],
      //vehicletype
      lvtype: [],
      //bodytype
      sbodytype: [],
      lbodytype: [],

      //transmission
      ltrans: [],

      strans: [],

      smake: [],
      makematch: "",
      smodel: [],
      sversion: [],
      modelmatch: "",
      error: null,

      //params
      ssellertype: 0,
      svtype: 2,
      cityid: 0,
      sowner: 0,
      minVal: "NA",
      maxVal: "NA",
      snewmake: 0,
      snewmodel: 0,
      versionid: 0,
      versionyearfrom: "NA",
      versionyearto: "NA",
      sassembly: "NA",
      bodytypeid: 0,
      transmissionid: 0,
      mileagemin: "NA",
      mileagemax: "NA",
      extcolorid: 0,
      sdoormin: "NA",
      sdoormax: "NA",
      sdriveside: "NA",
      smetermin: "NA",
      smetermax: "NA",
      sengsizemin: "NA",
      sengsizemax: "NA",
      senginetype: "NA",
      spic: true,
      vehiclecategoryid: 1,
      //modals
      showModalBodyType: false,
      showModalTransmission: false,
      showModalCity: false,
      showModalMake: false,
      showModalModel: false,
      showModalVersion: false,

      //accordion text
      accordionToggle: false,
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);

    fetch(apiUrlmake)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
          this.setState({
            lmake: result
          });
        } else {
          console.log("make api");
        }
        },
        error => {
          this.setState({ error });
        }
      );

    fetch(apiUrlcity)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
          this.setState({
            lcity: result
          });
        } else {
          console.log("city api");
        }
        },
        error => {
          this.setState({ error });
        }
      );

    fetch(apiUrlBodytype)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
          this.setState({
            lbodytype: result
          });
        } else {
          console.log("bodytype api");
        }
        },
        error => {
          this.setState({ error });
        }
      );

    fetch(apiUrltransmission)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
          this.setState({
            ltrans: result
          });
        } else {
          console.log("trans api");
        }
        },
        error => {
          this.setState({ error });
        }
      );

    fetch(apiUrlvehicletype)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
          this.setState({
            lvtype: result
          });
        } else {
          console.log("vtype api");
        }
        },
        error => {
          this.setState({ error });
        }
      );

    fetch(apiUrlsellertype)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
          this.setState({
            lsellertype: result
          });
        } else {
          console.log("sellertype api");
        }
        },
        error => {
          this.setState({ error });
        }
      );
  }

  handleMakeChange = event => {
    const searchkeyid2 = event.target.id;
    var pieces = searchkeyid2.split("-");

    const searchkeyid = pieces[1];
    const searchkeyvalue = event.target.value;
    const cheked = event.target.checked;

    if (cheked == true) {
      var joined = this.state.smake;
      joined.push(searchkeyid);
      let joinedString = joined.toString();
      this.setState({ snewmake: joinedString });
    } else {
      let filteredArray = this.state.smake.filter(item => item !== searchkeyid);
      let filteredArrayString = filteredArray.toString();
      this.setState({ snewmake: filteredArrayString });
    }
  };

  handleModelChange = event => {
    const searchkeyid2 = event.target.id;
    var pieces = searchkeyid2.split("-");

    const searchkeyid = pieces[1];
    const searchkeyvalue = event.target.value;
    const cheked = event.target.checked;

    if (cheked == true) {
      var joined = this.state.smodel;
      joined.push(searchkeyid);
      let joinedString = joined.toString();
      this.setState({ snewmodel: joinedString });
    } else {
      let filteredArray = this.state.smodel.filter(
        item => item !== searchkeyid
      );
      let filteredArrayString = filteredArray.toString();
      this.setState({ snewmodel: filteredArrayString });
    }
  };

  handleVersionChange = event => {
    const searchkeyid2 = event.target.id;
    var pieces = searchkeyid2.split("-");

    const searchkeyid = pieces[1];
    const searchkeyvalue = event.target.value;
    const cheked = event.target.checked;

    if (cheked == true) {
      var joined = this.state.sversion;
      joined.push(searchkeyid);
      let joinedString = joined.toString();
      this.setState({ versionid: joinedString });
    } else {
      let filteredArray = this.state.sversion.filter(
        item => item !== searchkeyid
      );
      let filteredArrayString = filteredArray.toString();
      this.setState({ versionid: filteredArrayString });
    }
  };

  handleCityChange = event => {
    const searchkeyid2 = event.target.id;
    var pieces = searchkeyid2.split("-");

    const searchkeyid = pieces[1];

    //   const searchkeyid = event.target.id;
    const searchkeyvalue = event.target.value;
    const cheked = event.target.checked;

    if (cheked == true) {
      var joined = this.state.scity;
      joined.push(searchkeyid);
      let joinedString = joined.toString();
      this.setState({ cityid: joinedString });
    } else {
      let filteredArray = this.state.scity.filter(item => item !== searchkeyid);
      let filteredArrayString = filteredArray.toString();
      this.setState({ cityid: filteredArrayString });
    }
  };

  handleBodytypeChange = event => {
    const searchkeyid2 = event.target.id;
    var pieces = searchkeyid2.split("-");

    const searchkeyid = pieces[1];
    const searchkeyvalue = event.target.value;
    const cheked = event.target.checked;

    if (cheked == true) {
      var joined = this.state.sbodytype;
      joined.push(searchkeyid);
      let joinedString = joined.toString();
      this.setState({ bodytypeid: joinedString });
    } else {
      let filteredArray = this.state.sbodytype.filter(
        item => item !== searchkeyid
      );
      let filteredArrayString = filteredArray.toString();
      this.setState({ bodytypeid: filteredArrayString });
    }
  };

  handleTransChange = event => {
    const searchkeyid2 = event.target.id;
    var pieces = searchkeyid2.split("-");

    const searchkeyid = pieces[1];
    const searchkeyvalue = event.target.value;
    const cheked = event.target.checked;

    if (cheked == true) {
      var joined = this.state.strans;
      joined.push(searchkeyid);
      let joinedString = joined.toString();
      this.setState({ transmissionid: joinedString });
    } else {
      let filteredArray = this.state.strans.filter(
        item => item !== searchkeyid
      );
      let filteredArrayString = filteredArray.toString();
      this.setState({ transmissionid: filteredArrayString });
    }
  };

  handleSellertypeChange = ssellertypeid => {
    this.setState({ ssellertype: ssellertypeid });
  };

  handleEnginetypeChange = fueltype => {
    this.setState({
      senginetype: fueltype
    });
  };

  handleOwnerNoChange = ownerid => {
    this.setState({
      sowner: ownerid
    });
  };

  handleDriveSideChange = driveside => {
    this.setState({
      sdriveside: driveside
    });
  };

  handleAssemblyChange = assembly => {
    this.setState({
      sassembly: assembly
    });
  };

  handleImageChange = image => {
    this.setState({
      spic: image
    });
  };

  handleMakeClick = async event => {
    let loadedModels = await getModalData(event.target.id);
    this.setState({
      showModalModel: true,
      lmodel: loadedModels
    });
  };

  handleModelClick = async event => {
    let loadedVersions = await getVersionData(event.target.id);
    this.setState({
      showModalVersion: true,
      lversion: loadedVersions
    });
  };

  //door
  onDoorSliderChange = value => {
    this.setState({
      sdoormin: value[0],
      sdoormax: value[1]
    });
  };

  //enginesize
  onEngineSizeSliderChange = value => {
    this.setState({
      sengsizemin: value[0],
      sengsizemax: value[1]
    });
  };

  //price
  onSliderChange = value => {
    this.setState({
      minVal: value[0],
      maxVal: value[1]
    });
  };

  onVersionYearSliderChange = value => {
    this.setState({
      versionyearfrom: value[0],
      versionyearto: value[1]
    });
  };

  onMeterReadingSliderChange = value => {
    this.setState({
      smetermin: value[0],
      smetermax: value[1]
    });
  };

  onMileageSliderChange = value => {
    this.setState({
      mileagemin: value[0],
      mileagemax: value[1]
    });
  };

  //modals
  //bodytype
  handleCloseBodyType = () => {
    this.setState({
      showModalBodyType: false
    });
  };
  handleShowBodyType = () => {
    this.setState({
      showModalBodyType: true
    });
  };
  //trans
  handleCloseTransmission = () => {
    this.setState({
      showModalTransmission: false
    });
  };
  handleShowTransmission = () => {
    this.setState({
      showModalTransmission: true
    });
  };
  //city
  handleCloseCity = () => {
    this.setState({
      showModalCity: false
    });
  };
  handleShowCity = () => {
    this.setState({
      showModalCity: true
    });
  };
  //make
  handleCloseMake = () => {
    this.setState({
      showModalMake: false
    });
  };
  handleShowMake = () => {
    this.setState({
      showModalMake: true
    });
  };
  //model
  handleCloseModel = () => {
    this.setState({
      showModalModel: false
    });
  };
  //version
  handleCloseVersion = () => {
    this.setState({
      showModalVersion: false
    });
  };

  modalBodyType = () => {
    return (
      <Modal
        size="lg" 
        show={this.state.showModalBodyType}
        onHide={this.handleCloseBodyType}
      >
        <Modal.Header closeButton>
          <Modal.Title>Select Body Type</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="modal-body checkbox-list">
            <ul
              id="make_list_popular"
              name="make_list"
              className="list-unstyled inline list-links clearfix"
            >
              {this.state.lbodytype.map((bodytype, index) => (
                <li key={index}>
                  <label className="checkbox">
                    <input
                      style={{ marginRight: 5 }}
                      type="checkbox"
                      onChange={this.handleBodytypeChange}
                      defaultValue={bodytype.BodyTypeID}
                      title={bodytype.BodyTypeName}
                      id={"btp-" + bodytype.BodyTypeID}
                      name={bodytype.BodyTypeName}
                    />
                    {bodytype.BodyTypeName}{" "}
                    <span className="badge badge-gray-light pull-right" />
                  </label>
                </li>
              ))}
            </ul>
          </div>
        </Modal.Body>
        <Modal.Footer>
        <Button style={{background: '#D3E428'}} onClick={this.handleCloseBodyType}>Close</Button>
      </Modal.Footer>
      </Modal>
    );
  };

  modalTransmission = () => {
    return (
      <Modal
        size="lg" 
        show={this.state.showModalTransmission}
        onHide={this.handleCloseTransmission}
      >
        <Modal.Header closeButton>
          <Modal.Title>Select Transmission</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="modal-body checkbox-list">
            <ul
              id="make_list_popular"
              name="make_list"
              className="list-unstyled clearfix"
            >
              {this.state.ltrans.map((trans, index) => (
                <li key={index}>
                  <label className="checkbox">
                    <input
                      style={{ marginRight: 5 }}
                      type="checkbox"
                      onChange={this.handleTransChange}
                      defaultValue={trans.VehicleTransmissionID}
                      title={trans.TransmissionName}
                      id={"tranp-" + trans.VehicleTransmissionID}
                      name={trans.TransmissionName}
                    />
                    {trans.TransmissionName}{" "}
                    <span className="badge badge-gray-light pull-right" />
                  </label>
                </li>
              ))}
            </ul>
          </div>
        </Modal.Body>
        <Modal.Footer>
        <Button style={{background: '#D3E428'}} onClick={this.handleCloseTransmission}>Close</Button>
      </Modal.Footer>
      </Modal>
    );
  };

  modalCity = () => {
    return (
      <Modal size="lg" show={this.state.showModalCity} onHide={this.handleCloseCity}>
        <Modal.Header closeButton>
          <Modal.Title>Select City</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="modal-body checkbox-list">
            <ul
              id="make_list_popular"
              name="make_list"
              className="list-unstyled inline list-links clearfix"
            >
              {this.state.lcity.map((city, index) => (
                <li key={index}>
                  <label className="checkbox">
                    <input
                      style={{ marginRight: 5 }}
                      type="checkbox"
                      onChange={this.handleCityChange}
                      defaultValue={city.CityID}
                      title={city.CityName}
                      id={"citym-" + city.CityID}
                      name={city.CityName}
                    />
                    {city.CityName}{" "}
                    <span className="badge badge-gray-light pull-right" />
                  </label>
                </li>
              ))}
            </ul>
          </div>
        </Modal.Body>
        <Modal.Footer>
        <Button style={{background: '#D3E428'}} onClick={this.handleCloseCity}>Close</Button>
      </Modal.Footer>
      </Modal>
    );
  };

  modalMake = () => {
    return (
      <Modal size="lg" show={this.state.showModalMake} onHide={this.handleCloseMake}>
        <Modal.Header closeButton>
          <Modal.Title>Select Make</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="modal-body checkbox-list">
            <ul
              id="make_list_popular"
              name="make_list"
              className="list-unstyled inline list-links clearfix"
            >
              {this.state.lmake.map((make, index) => (
                <li key={index}>
                  <label className="checkbox">
                    <input
                      style={{ marginRight: 5 }}
                      type="checkbox"
                      onChange={this.handleMakeChange}
                      defaultValue={make.MakeID}
                      title={make.MakeName}
                      id={"makp-" + make.MakeID}
                      name={make.MakeName}
                    />
                    <Button
                      style={{
                        fontSize: "12px",
                        textDecoration: "underline",
                        color: "#D3E428",
                        padding: 0
                      }}
                      variant="light"
                      id={make.MakeID}
                      onClick={this.handleMakeClick}
                    >
                      {make.MakeName}
                    </Button>
                    <span className="badge badge-gray-light pull-right" />
                  </label>
                </li>
              ))}
            </ul>
          </div>
        </Modal.Body>
        <Modal.Footer>
        <Button style={{background: '#D3E428'}} onClick={this.handleCloseMake}>Close</Button>
      </Modal.Footer>
      </Modal>
    );
  };

  modalModel = () => {
    return (
      <Modal size="lg" show={this.state.showModalModel} onHide={this.handleCloseModel}>
        <Modal.Header closeButton>
          <Modal.Title>Select Model</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="modal-body checkbox-list">
            <ul
              id="make_list_popular"
              name="make_list"
              className="list-unstyled inline list-links clearfix"
            >
              {this.state.lmodel.map((model, index) =>
                this.state.lmodel.length > 0  ? (
                  <li key={index}>
                    <label className="checkbox">
                      <input
                        style={{ marginRight: 5 }}
                        type="checkbox"
                        onChange={this.handleModelChange}
                        defaultValue={model.ModelID}
                        title={model.ModelName}
                        id={"modp-" + model.ModelID}
                        name={model.ModelName}
                      />
                      <Button
                        style={{
                          fontSize: "12px",
                          textDecoration: "underline",
                          color: "#D3E428",
                          padding: 0
                        }}
                        variant="light"
                        id={model.ModelID}
                        onClick={this.handleModelClick}
                      >
                        {model.ModelName}
                      </Button>
                      <span className="badge badge-gray-light pull-right" />
                    </label>
                  </li>
                ) : (
                  <span> No Version Found</span>
                )
              )}
            </ul>
          </div>
        </Modal.Body>
        <Modal.Footer>
        <Button style={{background: '#D3E428'}} onClick={this.handleCloseModel}>Close</Button>
      </Modal.Footer>
      </Modal>
    );
  };

  modalVersion = () => {
    return (
      <Modal
        size="lg" 
        show={this.state.showModalVersion}
        onHide={this.handleCloseVersion}
      >
        <Modal.Header closeButton>
          <Modal.Title>Select Version</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="modal-body checkbox-list">
            <ul
              id="make_list_popular"
              name="make_list"
              className="list-unstyled inline list-links clearfix"
            >
              {this.state.lversion.map((version, index) =>
                this.state.lversion.length > 0 ? (
                  <li key={index}>
                    <label className="checkbox">
                      <input
                        style={{ marginRight: 5 }}
                        type="checkbox"
                        onChange={this.handleVersionChange}
                        defaultValue={version.VersionID}
                        title={version.VersionName}
                        id={"verp-" + version.VersionID}
                        name={version.VersionName}
                      />
                      {version.VersionName}
                      <span className="badge badge-gray-light pull-right" />
                    </label>
                  </li>
                ) : (
                  <span>No Version Found</span>
                )
              )}
            </ul>
          </div>
        </Modal.Body>
        <Modal.Footer>
        <Button style={{background: '#D3E428'}} onClick={this.handleCloseVersion}>Close</Button>
      </Modal.Footer>
      </Modal>
    );
  };

  handleAccordionToggle = (accordionToggle) => {
    this.setState({
      accordionToggle: !accordionToggle,
    });
  }

  render() {
    const {
      ssellertype,
      svtype,
      cityid,
      sowner,
      minVal,
      maxVal,
      snewmake,
      snewmodel,
      versionid,
      versionyearfrom,
      versionyearto,
      sassembly,
      bodytypeid,
      transmissionid,
      mileagemin,
      mileagemax,
      extcolorid,
      sdoormin,
      sdoormax,
      sdriveside,
      smetermin,
      smetermax,
      sengsizemin,
      sengsizemax,
      senginetype,
      spic,
      vehiclecategoryid
    } = this.state;
    console.log(
      snewmake,
      "snewmake",
      snewmodel,
      "snewmodel",
      versionid,
      "versionid"
    );
    return (
      <div>
        {/* modal make */}
        {this.modalMake()}

        {/* modal model */}
        {this.modalModel()}

        {/* modal version */}
        {this.modalVersion()}

        {/* modal bodytype */}
        {this.modalBodyType()}

        {/* modal trans */}
        {this.modalTransmission()}

        {/* modal city */}
        {this.modalCity()}

        <div style={{ backgroundColor: "#e3e3e3", paddingBottom: "50px" }}>
          <div
            className="container-fluid"
            style={{
              backgroundColor: "#646464",
              padding: "60px 50px 40px 50px"
            }}
          >
            <div className="col-12 d-flex justify-content-center">
              <span
                style={{ fontSize: "30px", fontWeight: 800, color: "white" }}
              >
                Advance Search
              </span>
            </div>
          </div>

          <div
            className="container bg-shadow-advance rounded-1 mt-5 pb-5"
            style={{ backgroundColor: "white" }}
          >
            <form action className="form">
              {/* basic tab */}
              <div className="row pt-5">
                <div className="col-lg-6">
                  <div id="forms" className="form-group">
                    <div
                      className="row"
                      style={{
                        border: "1px solid #D3E428",
                        borderRadius: "8px",
                        padding: "10px"
                      }}
                    >
                      <div className="col-4">
                        <label htmlFor className="control-label">
                          Make
                        </label>
                      </div>
                      <div className="col-8">
                        {this.state.lmake.map((make, index) =>
                          index < 4 ? (
                            <div
                              className="form-check col-6"
                              style={{
                                display: "inline-flex",
                                alignItems: "center",
                                paddingLeft: 0
                              }}
                            >
                              <input
                                style={{
                                  position: "absolute",
                                  marginLeft: "-1.25rem"
                                }}
                                onChange={this.handleMakeChange}
                                defaultValue={make.MakeID}
                                title={make.MakeName}
                                id={"makem-" + make.MakeID}
                                name={make.MakeName}
                                type="checkbox"
                              />
                              <label
                                className="form-check-label"
                                htmlFor={make.MakeName}
                              >
                                {make.MakeName}
                              </label>
                            </div>
                          ) : (
                            ""
                          )
                        )}
                        <div
                          style={{ marginLeft: "-22px" }}
                          className="savesearch"
                        >
                          <Button
                            style={{
                              fontSize: "12px",
                              textDecoration: "underline",
                              color: "#D3E428",
                              padding: 0
                            }}
                            variant="light"
                            onClick={this.handleShowMake}
                          >
                            more choices
                          </Button>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div
                    className="b-filter-slider ui-filter-slider mt-0"
                    style={{
                      border: "1px solid #D3E428",
                      borderRadius: "8px",
                      padding: "10px",
                      marginBottom: "15px"
                    }}
                  >
                    <div className="b-filter-slider__title ml-3 mb-3 mt-2">
                      Price
                    </div>

                    <div
                      className="b-filter-slider ui-filter-slider"
                      style={{ marginTop: "10px" }}
                    >
                      <div className="b-filter-slider__main px-5">
                        <Range
                          defaultValue={[this.state.min, this.state.max]}
                          min={this.state.min}
                          max={this.state.max}
                          onChange={this.onSliderChange}
                        />

                        <div className="b-filter__row row">
                          <div
                            className="b-filter__item col-md-4 col-lg-4 col-xl-4"
                            style={{ padding: 0 }}
                          >
                            <input
                              placeholder="SAR 15000"
                              className="quick-select text-box single-line"
                              style={{ paddingRight: 1 }}
                              type="number"
                              value={minVal}
                            />
                          </div>
                          <div
                            className="b-filter__item col-md-4 col-lg-4 col-xl-4"
                            style={{ padding: 0 }}
                          >
                            <input
                              placeholder="SAR 15000000"
                              className="quick-select text-box single-line"
                              style={{ paddingRight: 1, width: "150px" }}
                              type="number"
                              value={maxVal}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div id="forms" className="form-group">
                    <div
                      className="row"
                      style={{
                        border: "1px solid #D3E428",
                        borderRadius: "8px",
                        padding: "10px"
                      }}
                    >
                      <div className="col-4">
                        <label htmlFor className="control-label">
                          Seller Type
                        </label>
                      </div>

                      <div className="col-8 pt-1">
                        <div className="label-group mb-3">
                          {this.state.lsellertype.map((sellertype, index) => (
                            <div
                              key={index}
                              style={{ display: "inline-block" }}
                            >
                              <input
                                style={{ opacity: 1, margin: "9px 10px 0 0" }}
                                type="radio"
                                value={sellertype.UserTypeID}
                                checked={ssellertype === sellertype.UserTypeID}
                                onChange={() =>
                                  this.handleSellertypeChange(
                                    sellertype.UserTypeID
                                  )
                                }
                              />

                              <label
                                className="form-check-label"
                                style={{
                                  marginLeft: "18px",
                                  marginRight: "20px"
                                }}
                              >
                                {sellertype.Name}
                              </label>
                            </div>
                          ))}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div id="forms" className="form-group">
                    <div
                      className="row"
                      style={{
                        border: "1px solid #D3E428",
                        borderRadius: "8px",
                        padding: "10px"
                      }}
                    >
                      <div className="col-4">
                        <label htmlFor className="control-label">
                          City
                        </label>
                      </div>
                      <div className="col-8">
                        {this.state.lcity.map((city, index) =>
                          index < 4 ? (
                            <div
                              key={index}
                              className="form-check col-6"
                              style={{
                                display: "inline-flex",
                                alignItems: "center",
                                paddingLeft: 0
                              }}
                            >
                              <input
                                style={{
                                  position: "absolute",
                                  marginLeft: "-1.25rem"
                                }}
                                onChange={this.handleCityChange}
                                defaultValue={city.CityID}
                                title={city.CityName}
                                id={"citym-" + city.CityID}
                                name={city.CityName}
                                type="checkbox"
                              />
                              <label
                                className="form-check-label"
                                htmlFor={city.CityName}
                              >
                                {city.CityName}
                              </label>
                            </div>
                          ) : (
                            ""
                          )
                        )}

                        <div
                          style={{ marginLeft: "-22px" }}
                          className="savesearch"
                        >
                          <Button
                            style={{
                              fontSize: "12px",
                              textDecoration: "underline",
                              color: "#D3E428",
                              padding: 0
                            }}
                            variant="light"
                            onClick={this.handleShowCity}
                          >
                            more choices
                          </Button>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div id="forms" className="form-group">
                    <div
                      className="row"
                      style={{
                        border: "1px solid #D3E428",
                        borderRadius: "8px",
                        padding: "10px"
                      }}
                    >
                      <div className="col-4">
                        <label htmlFor className="control-label">
                          Condition
                        </label>
                      </div>

                      <Consumer>
                        {({ data, handleChildVehicletypeId }) => (
                          <div className="col-8 pt-1">
                            <div className="label-group mb-3">
                              {this.state.lvtype.map((vtype, index) => (
                                <div key={index}>
                                  <input
                                    style={{
                                      opacity: 1,
                                      margin: "9px 10px 0 0"
                                    }}
                                    type="radio"
                                    value={vtype.VehicleTypeID}
                                    checked={
                                      vtype.VehicleTypeID == data.vehicleTypeID
                                    }
                                    onChange={() =>
                                      handleChildVehicletypeId(
                                        vtype.VehicleTypeID
                                      )
                                    }
                                  />
                                  <label
                                    className="form-check-label"
                                    style={{
                                      marginLeft: "18px",
                                      marginRight: "20px"
                                    }}
                                  >
                                    {vtype.VehicleTypeName}
                                  </label>
                                </div>
                              ))}
                            </div>
                          </div>
                        )}
                      </Consumer>
                    </div>
                  </div>

                  <div id="forms" className="form-group">
                    <div
                      className="row"
                      style={{
                        border: "1px solid #D3E428",
                        borderRadius: "8px",
                        padding: "10px"
                      }}
                    >
                      <div className="col-4">
                        <label htmlFor className="control-label">
                          Body Type
                        </label>
                      </div>

                      <div className="col-8">
                        {this.state.lbodytype.map((bodytype, index) =>
                          index < 4 ? (
                            <div
                              key={index}
                              className="form-check col-6"
                              style={{
                                display: "inline-flex",
                                alignItems: "center",
                                paddingLeft: 0
                              }}
                            >
                              <input
                                style={{
                                  position: "absolute",
                                  marginLeft: "-1.25rem"
                                }}
                                onChange={this.handleBodytypeChange}
                                defaultValue={bodytype.BodyTypeID}
                                title={bodytype.BodyTypeName}
                                id={"btm-" + bodytype.BodyTypeID}
                                name={bodytype.BodyTypeName}
                                type="checkbox"
                              />
                              <label className="form-check-label">
                                {bodytype.BodyTypeName}
                              </label>
                            </div>
                          ) : (
                            ""
                          )
                        )}

                        <div
                          style={{ marginLeft: "-22px" }}
                          className="savesearch"
                        >
                          <Button
                            style={{
                              fontSize: "12px",
                              textDecoration: "underline",
                              color: "#D3E428",
                              padding: 0
                            }}
                            variant="light"
                            onClick={this.handleShowBodyType}
                          >
                            more choices
                          </Button>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div id="forms" className="form-group">
                    <div
                      className="row"
                      style={{
                        border: "1px solid #D3E428",
                        borderRadius: "8px",
                        padding: "10px"
                      }}
                    >
                      <div className="col-4">
                        <label htmlFor className="control-label">
                          Fuel/Engine Type
                        </label>
                      </div>

                      <div className="col-8 pt-1">
                        <div className="label-group mb-3">
                          {this.state.fuelTypes.map((fueltype, index) => (
                            <div key={index}>
                              <input
                                style={{ opacity: 1, margin: "9px 10px 0 0" }}
                                type="radio"
                                value={fueltype.value}
                                checked={senginetype === fueltype.value}
                                onChange={() =>
                                  this.handleEnginetypeChange(fueltype.value)
                                }
                              />

                              <label
                                className="form-check-label"
                                style={{
                                  marginLeft: "18px",
                                  marginRight: "30px"
                                }}
                              >
                                {fueltype.value}
                              </label>
                            </div>
                          ))}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            <section>
              {/* ============================================================================================================ */}

              <div className="panel-group">
                <div className="panel panel-default">
                  {/* more options */}
                  <div
                    id="collapse1"
                    className="panel-collapse collapse show"
                    style={{}}
                  >
                    <Accordion>
                      <Card
                        style={{
                          flexDirection: "column-reverse",
                          border: "none "
                        }}
                      >
                        <Card.Header style={{ padding: "5px 1.25rem 20px 0" }}>
                          <Accordion.Toggle
                            as={Button}
                            variant="light"
                            eventKey="0"
                            onClick={() => this.handleAccordionToggle(this.state.accordionToggle)}
                          >
                            <div>
                              <a
                                style={{
                                  font: "600 16px/1 Montserrat",
                                  color: "#D3E428"
                                }}
                              >
                                <span>{this.state.accordionToggle ? 'Less Options' : 'More Options'}</span>
                              </a>
                              <i
                                className="fa fa-caret-down pl-2"
                                style={{ color: "#D3E428", fontSize: "14px" }}
                              />
                            </div>
                          </Accordion.Toggle>
                          <Consumer>
                            {({ handleChildQuickSrchAdPost }) => (
                              <Link
                                type="button"
                                className="btn next-step"
                                style={{
                                  position: "absolute",
                                  right: "20px",
                                  padding: "10px 80px",
                                  backgroundColor: "#D3E428",
                                  color: "white",
                                  transform: "translateY(-10px)"
                                }}
                                onClick={() =>
                                  handleChildQuickSrchAdPost(
                                    ssellertype,
                                    svtype,
                                    cityid,
                                    sowner,
                                    minVal,
                                    maxVal,
                                    snewmake,
                                    snewmodel,
                                    versionid,
                                    versionyearfrom,
                                    versionyearto,
                                    sassembly,
                                    bodytypeid,
                                    transmissionid,
                                    mileagemin,
                                    mileagemax,
                                    extcolorid,
                                    sdoormin,
                                    sdoormax,
                                    sdriveside,
                                    smetermin,
                                    smetermax,
                                    sengsizemin,
                                    sengsizemax,
                                    senginetype,
                                    spic,
                                    vehiclecategoryid
                                  )
                                }
                                to="/vehicle-listing"
                              >
                                Search
                              </Link>
                            )}
                          </Consumer>
                        </Card.Header>
                        <Accordion.Collapse eventKey="0">
                          <Card.Body>
                            <form className="form">
                              {/* basic tab */}
                              <div className="row mt-4">
                                <div className="col-lg-6">
                                  <div
                                    className="b-filter-slider ui-filter-slider mt-0"
                                    style={{
                                      border: "1px solid #D3E428",
                                      borderRadius: "8px",
                                      padding: "10px",
                                      marginBottom: "15px"
                                    }}
                                  >
                                    <div className="b-filter-slider__title ml-3 mb-5 mt-2">
                                      Version Year
                                    </div>

                                    <div
                                      className="b-filter-slider ui-filter-slider"
                                      style={{ marginTop: "10px" }}
                                    >
                                      <div className="b-filter-slider__main px-5">
                                        <Range
                                          defaultValue={[
                                            this.state.versionyearmin,
                                            this.state.versionyearmax
                                          ]}
                                          min={this.state.versionyearmin}
                                          max={this.state.versionyearmax}
                                          onChange={
                                            this.onVersionYearSliderChange
                                          }
                                        />

                                        <div className="b-filter__row row">
                                          <div
                                            className="b-filter__item col-md-4 col-lg-4 col-xl-4"
                                            style={{ padding: 0 }}
                                          >
                                            <input
                                              placeholder="1990"
                                              className="quick-select text-box single-line"
                                              style={{ paddingRight: 1 }}
                                              type="number"
                                              value={versionyearfrom}
                                            />
                                          </div>
                                          <div
                                            className="b-filter__item col-md-4 col-lg-4 col-xl-4"
                                            style={{ padding: 0 }}
                                          >
                                            <input
                                              placeholder="2020"
                                              className="quick-select text-box single-line"
                                              style={{ paddingRight: 1 }}
                                              type="number"
                                              value={versionyearto}
                                            />
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div
                                    className="b-filter-slider ui-filter-slider mt-0"
                                    style={{
                                      border: "1px solid #D3E428",
                                      borderRadius: "8px",
                                      padding: "10px",
                                      marginBottom: "15px"
                                    }}
                                  >
                                    <div className="b-filter-slider__title ml-3 mb-5 mt-2">
                                      No. of Doors
                                    </div>

                                    <div
                                      className="b-filter-slider ui-filter-slider"
                                      style={{ marginTop: "10px" }}
                                    >
                                      <div className="b-filter-slider__main px-5">
                                        <Range
                                          defaultValue={[
                                            this.state.doormin,
                                            this.state.doormax
                                          ]}
                                          min={this.state.doormin}
                                          max={this.state.doormax}
                                          onChange={this.onDoorSliderChange}
                                        />

                                        <div className="b-filter__row row">
                                          <div
                                            className="b-filter__item col-md-4 col-lg-4 col-xl-4"
                                            style={{ padding: 0 }}
                                          >
                                            <input
                                              placeholder="2"
                                              className="quick-select text-box single-line"
                                              style={{ paddingRight: 1 }}
                                              type="number"
                                              value={sdoormin}
                                            />
                                          </div>
                                          <div
                                            className="b-filter__item col-md-4 col-lg-4 col-xl-4"
                                            style={{ padding: 0 }}
                                          >
                                            <input
                                              placeholder="5"
                                              className="quick-select text-box single-line"
                                              style={{ paddingRight: 1 }}
                                              type="number"
                                              value={sdoormax}
                                            />
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div id="forms" className="form-group">
                                    <div
                                      className="row"
                                      style={{
                                        border: "1px solid #D3E428",
                                        borderRadius: "8px",
                                        padding: "10px"
                                      }}
                                    >
                                      <div className="col-3">
                                        <label
                                          htmlFor
                                          className="control-label"
                                          handleShowTransmission
                                        >
                                          Transmission
                                        </label>
                                      </div>

                                      <div className="col-9">
                                        {this.state.ltrans.map((trans, index) =>
                                          index < 4 ? (
                                            <div
                                              className="form-check col-6"
                                              style={{
                                                display: "inline-flex",
                                                alignItems: "center",
                                                paddingLeft: 0
                                              }}
                                            >
                                              <input
                                                style={{
                                                  position: "absolute",
                                                  marginLeft: "-1.25rem"
                                                }}
                                                className="form-check-input"
                                                onChange={
                                                  this.handleTransChange
                                                }
                                                defaultValue={
                                                  trans.VehicleTransmissionID
                                                }
                                                title={trans.TransmissionName}
                                                id={
                                                  "tranp-" +
                                                  trans.VehicleTransmissionID
                                                }
                                                name={trans.TransmissionName}
                                                type="checkbox"
                                              />
                                              <label
                                                className="form-check-label"
                                                htmlFor={trans.TransmissionName}
                                              >
                                                {trans.TransmissionName}
                                              </label>
                                            </div>
                                          ) : (
                                            ""
                                          )
                                        )}
                                        <div
                                          style={{ marginLeft: "-22px" }}
                                          className="savesearch"
                                        >
                                          <Button
                                            style={{
                                              fontSize: "12px",
                                              textDecoration: "underline",
                                              color: "#D3E428",
                                              padding: 0
                                            }}
                                            variant="light"
                                            onClick={
                                              this.handleShowTransmission
                                            }
                                          >
                                            more choices
                                          </Button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div
                                    className="b-filter-slider ui-filter-slider mt-0"
                                    style={{
                                      border: "1px solid #D3E428",
                                      borderRadius: "8px",
                                      padding: "10px",
                                      marginBottom: "15px"
                                    }}
                                  >
                                    <div className="b-filter-slider__title ml-3 mb-5 mt-2">
                                      Engine Size
                                    </div>

                                    <div
                                      className="b-filter-slider ui-filter-slider"
                                      style={{ marginTop: "10px" }}
                                    >
                                      <div className="b-filter-slider__main px-5">
                                        <Range
                                          defaultValue={[
                                            this.state.engsizemin,
                                            this.state.engsizemax
                                          ]}
                                          step={100}
                                          min={this.state.engsizemin}
                                          max={this.state.engsizemax}
                                          onChange={
                                            this.onEngineSizeSliderChange
                                          }
                                        />

                                        <div className="b-filter__row row">
                                          <div
                                            className="b-filter__item col-md-4 col-lg-4 col-xl-4"
                                            style={{ padding: 0 }}
                                          >
                                            <input
                                              placeholder="1000"
                                              className="quick-select text-box single-line"
                                              style={{ paddingRight: 1 }}
                                              type="number"
                                              value={sengsizemin}
                                            />
                                          </div>
                                          <div
                                            className="b-filter__item col-md-4 col-lg-4 col-xl-4"
                                            style={{ padding: 0 }}
                                          >
                                            <input
                                              placeholder="4000"
                                              className="quick-select text-box single-line"
                                              style={{ paddingRight: 1 }}
                                              type="number"
                                              value={sengsizemax}
                                            />
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div id="forms" className="form-group">
                                    <div
                                      className="row"
                                      style={{
                                        border: "1px solid #D3E428",
                                        borderRadius: "8px",
                                        padding: "10px"
                                      }}
                                    >
                                      <div className="col-4">
                                        <label
                                          htmlFor
                                          className="control-label"
                                        >
                                          Images
                                        </label>
                                      </div>

                                      <div className="col-8 pt-1">
                                        <div className="label-group mb-3">
                                          {this.state.images.map(
                                            (image, index) => (
                                              <div key={index}>
                                                <input
                                                  style={{
                                                    opacity: 1,
                                                    margin: "9px 10px 0 0"
                                                  }}
                                                  type="radio"
                                                  value={image.id}
                                                  checked={spic === image.id}
                                                  onChange={() =>
                                                    this.handleImageChange(
                                                      image.id
                                                    )
                                                  }
                                                />

                                                <label
                                                  className="form-check-label"
                                                  style={{
                                                    marginLeft: "18px",
                                                    marginRight: "30px"
                                                  }}
                                                >
                                                  {image.value}
                                                </label>
                                              </div>
                                            )
                                          )}
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="col-lg-6">
                                  <div id="forms" className="form-group">
                                    <div
                                      className="row"
                                      style={{
                                        border: "1px solid #D3E428",
                                        borderRadius: "8px",
                                        padding: "10px"
                                      }}
                                    >
                                      <div className="col-4">
                                        <label
                                          htmlFor
                                          className="control-label"
                                        >
                                          No. of Owner(s)
                                        </label>
                                      </div>

                                      <div className="col-8 pt-1">
                                        <div className="label-group mb-3">
                                          {this.state.owners.map(
                                            (owner, index) => (
                                              <div key={index}>
                                                <input
                                                  style={{
                                                    opacity: 1,
                                                    margin: "9px 10px 0 0"
                                                  }}
                                                  type="radio"
                                                  value={owner.id}
                                                  checked={sowner === owner.id}
                                                  onChange={() =>
                                                    this.handleOwnerNoChange(
                                                      owner.id
                                                    )
                                                  }
                                                />

                                                <label
                                                  className="form-check-label"
                                                  style={{
                                                    marginLeft: "18px",
                                                    marginRight: "30px"
                                                  }}
                                                >
                                                  {owner.value}
                                                </label>
                                              </div>
                                            )
                                          )}
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div id="forms" className="form-group">
                                    <div
                                      className="row"
                                      style={{
                                        border: "1px solid #D3E428",
                                        borderRadius: "8px",
                                        padding: "10px"
                                      }}
                                    >
                                      <div className="col-4">
                                        <label
                                          htmlFor
                                          className="control-label"
                                        >
                                          Drive Side
                                        </label>
                                      </div>

                                      <div className="col-8 pt-1">
                                        <div className="label-group mb-3">
                                          {this.state.driveSide.map(
                                            (driveside, index) => (
                                              <div key={index}>
                                                <input
                                                  style={{
                                                    opacity: 1,
                                                    margin: "9px 10px 0 0"
                                                  }}
                                                  type="radio"
                                                  value={driveside.id}
                                                  checked={
                                                    sdriveside === driveside.id
                                                  }
                                                  onChange={() =>
                                                    this.handleDriveSideChange(
                                                      driveside.id
                                                    )
                                                  }
                                                />

                                                <label
                                                  className="form-check-label"
                                                  style={{
                                                    marginLeft: "18px",
                                                    marginRight: "30px"
                                                  }}
                                                >
                                                  {driveside.value}
                                                </label>
                                              </div>
                                            )
                                          )}
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div id="forms" className="form-group">
                                    <div
                                      className="row"
                                      style={{
                                        border: "1px solid #D3E428",
                                        borderRadius: "8px",
                                        padding: "10px"
                                      }}
                                    >
                                      <div className="col-4">
                                        <label
                                          htmlFor
                                          className="control-label"
                                        >
                                          Assembly
                                        </label>
                                      </div>

                                      <div className="col-8 pt-1">
                                        <div className="label-group mb-3">
                                          {this.state.assembly.map(
                                            (assemb, index) => (
                                              <div key={index}>
                                                <input
                                                  style={{
                                                    opacity: 1,
                                                    margin: "9px 10px 0 0"
                                                  }}
                                                  type="radio"
                                                  value={assemb.id}
                                                  checked={
                                                    sassembly === assemb.id
                                                  }
                                                  onChange={() =>
                                                    this.handleAssemblyChange(
                                                      assemb.id
                                                    )
                                                  }
                                                />

                                                <label
                                                  className="form-check-label"
                                                  style={{
                                                    marginLeft: "18px",
                                                    marginRight: "30px"
                                                  }}
                                                >
                                                  {assemb.value}
                                                </label>
                                              </div>
                                            )
                                          )}
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div
                                    className="b-filter-slider ui-filter-slider mt-3"
                                    style={{
                                      border: "1px solid #D3E428",
                                      borderRadius: "8px",
                                      padding: "10px"
                                    }}
                                  >
                                    <div className="b-filter-slider__title ml-3 mb-5 mt-2">
                                      Meter Reading
                                    </div>

                                    <div
                                      className="b-filter-slider ui-filter-slider"
                                      style={{ marginTop: "10px" }}
                                    >
                                      <div className="b-filter-slider__main px-5">
                                        <Range
                                          defaultValue={[
                                            this.state.meterreadingmin,
                                            this.state.meterreadingmax
                                          ]}
                                          min={this.state.meterreadingmin}
                                          max={this.state.meterreadingmax}
                                          onChange={
                                            this.onMeterReadingSliderChange
                                          }
                                        />

                                        <div className="b-filter__row row">
                                          <div
                                            className="b-filter__item col-md-4 col-lg-4 col-xl-4"
                                            style={{ padding: 0 }}
                                          >
                                            <input
                                              placeholder="0"
                                              className="quick-select text-box single-line"
                                              style={{ paddingRight: 1 }}
                                              type="number"
                                              value={smetermin}
                                            />
                                          </div>
                                          <div
                                            className="b-filter__item col-md-4 col-lg-4 col-xl-4"
                                            style={{ padding: 0 }}
                                          >
                                            <input
                                              placeholder="20000000"
                                              className="quick-select text-box single-line"
                                              style={{ paddingRight: 1 }}
                                              type="number"
                                              value={smetermax}
                                            />
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div
                                    className="b-filter-slider ui-filter-slider mt-3"
                                    style={{
                                      border: "1px solid #D3E428",
                                      borderRadius: "8px",
                                      padding: "10px"
                                    }}
                                  >
                                    <div className="b-filter-slider__title ml-3 mb-5 mt-2">
                                      Mileage
                                    </div>

                                    <div
                                      className="b-filter-slider ui-filter-slider"
                                      style={{ marginTop: "10px" }}
                                    >
                                      <div className="b-filter-slider__main px-5">
                                        <Range
                                          defaultValue={[
                                            this.state.mileageminimum,
                                            this.state.mileagemaximum
                                          ]}
                                          min={this.state.mileageminimum}
                                          max={this.state.mileagemaximum}
                                          onChange={this.onMileageSliderChange}
                                        />

                                        <div className="b-filter__row row">
                                          <div
                                            className="b-filter__item col-md-4 col-lg-4 col-xl-4"
                                            style={{ padding: 0 }}
                                          >
                                            <input
                                              placeholder="10/LTR"
                                              className="quick-select text-box single-line"
                                              style={{ paddingRight: 1 }}
                                              type="number"
                                              value={mileagemin}
                                            />
                                          </div>
                                          <div
                                            className="b-filter__item col-md-4 col-lg-4 col-xl-4"
                                            style={{ padding: 0 }}
                                          >
                                            <input
                                              placeholder="39/LTR"
                                              className="quick-select text-box single-line"
                                              style={{ paddingRight: 1 }}
                                              type="number"
                                              value={mileagemax}
                                            />
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </Card.Body>
                        </Accordion.Collapse>
                      </Card>
                    </Accordion>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default Advancesearch;