import React, { Component } from "react";

export default class TermsOfUse extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <div>
        <div style={{paddingBottom: '20px'}}>
          <div className="container mt-5">
            <div className="row">
              <div className="col-12 privacypolicy">
                <h3 style={{ color: "#D3E428" }} className="mb-4">
                  <strong>Terms of use</strong>
                </h3>
                <hr />
                <p>
                  THIS IS A LEGAL AGREEMENT BETWEEN YOU AND Cararabiya.COM
                  STATING THE TERMS THAT GOVERN YOUR USE OF THE WEBSITE. BY
                  ACCESSING AND USING THE WEBSITE YOU AGREE TO THESE TERMS. IF
                  YOU DO NOT AGREE TO THESE TERMS DO NOT USE THE WEBSITE. YOU
                  MUST ACCEPT AND ABIDE BY THESE TERMS AS PRESENTED TO YOU:
                  CHANGES, ADDITIONS OR DELETIONS ARE NOT ACCEPTABLE AND
                  Cararabiya MAY TAKE LEGAL ACTION FOR NONCOMPLIANCE WITH ANY OF
                  THE TERMS OF THIS AGREEMENT.
                </p>
                <p>
                  <b>Terms of Use</b>
                </p>
                <p>
                  <b>1. Acceptance of Terms of Use</b>
                </p>
                <p>
                  Cararabiya.com (the "Website") is a community content website
                  operated by Bayt.com and its affiliates (referred to as
                  “Cararabiya” or “we”). Content (as defined in Clause 3 below)
                  is uploaded by Cararabiya and other users of the Website.
                </p>
                <p>
                  These are the terms on which Cararabiya allows you to access
                  and use the Website, whether as a guest or a registered user.
                  Please read these Terms of Use carefully before you start to
                  use the Website.
                </p>
                <p>
                  By accessing and using the Website, you are accepting these
                  Terms of Use and agreeing to abide by them.
                </p>
                <p>
                  If you do not agree to these Terms of Use, then you are not
                  permitted to use the Website.
                </p>
                <p>
                  <b>2. Cararabiya’s Proprietary Rights</b>
                </p>
                <p>
                  You agree that Cararabiya Limited (or its affiliates, as the
                  case may be) is the owner or the licensee of all intellectual
                  property rights in the Website, and the Website itself is a
                  collective work and/or compilation solely owned by Cararabiya.
                  The Website is protected by copyright laws and treaties around
                  the world.
                </p>
                <p>
                  You may not copy, distribute, reproduce, sell, lease, assign,
                  rent, sublicense, encumber any aspect of the Website or any
                  Content (as defined in Clause 3 below).
                </p>
                <p>
                  You also may not modify or create derivative works of any
                  aspect of the Website or any Content (as defined in Clause 3
                  below), other than with respect to your own Content (as
                  defined in Clause 3 below).
                </p>
                <p>
                  You may not disassemble or decompile, reverse engineer or
                  otherwise attempt to discover any source code contained in the
                  Website or any software or database connected to the Website.
                </p>
                <p>
                  <b>3. Content</b>
                </p>
                <p>
                  "Content" refers to any listing, forums, event listings,
                  reviews, postings, messages, text, files, images, photos or
                  any other material posted on the Website.
                </p>
                <p>By posting Content on the Website:</p>
                <p>
                  You grant Cararabiya the non-exclusive, worldwide, perpetual,
                  irrevocable, royalty-free, sub-licensable right to use, copy,
                  reproduce, distribute perform, display, modify or create
                  derivative works of the Content perpetually and for any lawful
                  purpose Cararabiya sees fit;
                </p>
                <p>
                  You warrant that the holder of any rights, including moral
                  rights in such Content, has completely and effectively waived
                  all such rights and validly and irrevocably granted to you the
                  right to grant the license in the preceding sentence;
                </p>
                <p>
                  You permit any other user of the Website to access, display,
                  view, store and reproduce such Content; and
                </p>
                <p>
                  You grant Cararabiya the right to prohibit the subsequent
                  access, storage and reproduction or use of said Content by any
                  party to any extent Cararabiya sees fit.
                </p>
                <p>
                  Cararabiya is not responsible for, and no reliance should be
                  placed, in any way on any Content posted, transmitted through
                  or linked to the Website.
                </p>
                <p>
                  Cararabiya reserves the right to (1) refuse to share or
                  display, (2) move; or (3) delete any Content posted on the
                  Website.
                </p>
                <p>
                  Cararabiya aims to update the Website regularly, and may
                  change the Content at any time. Any of the Content may be out
                  of date at any given time, and we are under no obligation to
                  update such material.
                </p>
                <p>
                  <b>5. Links to Third Party Sites</b>
                </p>
                <p>
                  Where the Website contains links to other sites and resources
                  provided by third parties, these links are provided for your
                  information only.
                </p>
                <p>
                  We have no control over the contents of those sites or
                  resources, and accept no responsibility for them or for any
                  loss or damage that may arise from your use of them.
                </p>
                <p>
                  <b>6. Notification of Claims of Infringement</b>
                </p>
                <p>
                  It is forbidden to post any Content that violates any
                  international, federal, national, local copyright, patent,
                  trademark or other intellectual property laws applicable in
                  theSaudi Arabia.
                </p>
                <p>
                  Cararabiya is not responsible for any Content submitted by a
                  user that infringes another’s proprietary rights.
                </p>
                <p>
                  If you are aware of any Content that you believe infringes on
                  yours or a third party’s proprietary rights, please contact
                  us.
                </p>
                <p>Please include in any such notice:</p>
                <p>A link to the alleged infringing material;</p>
                <p>A description of the alleged infringing material;</p>
                <p>
                  A statement contesting that the copyrighted material was not
                  posted by the holder or permitted licensee of said copyright;
                </p>
                <p>Your contact details (including email address); and</p>
                <p>Your signature (physical or electronic).</p>
                <p>
                  Cararabiya may, but is under no obligation to screen or
                  monitor any Content. If notified of Content which allegedly
                  does not conform to these Terms of Use or infringes a third
                  party’s proprietary rights, Cararabiya may investigate the
                  allegation and determine in its sole discretion whether to
                  remove or request the removal of the Content. Cararabiya has
                  no liability or responsibility to users for performance or
                  nonperformance of such activities.
                </p>
                <p>
                  <b>7. Privacy Policy</b>
                </p>
                <p>
                  Cararabiya respects your privacy and has developed a detailed
                  Privacy Policy that is incorporated into this Agreement.
                  Please take the time to read our Privacy Policy found here:
                  Privacy Policy. By agreeing to these Terms, you are also
                  accepting the terms of our Privacy Policy.
                </p>
                <p>
                  <b>8. Access and Linking to the Website</b>
                </p>
                <p>
                  Your access to the Website is provided on a non-exclusive,
                  temporary and revocable basis by Cararabiya, and Cararabiya
                  reserves the right to withdraw or amend your right to access
                  to the Website without notice.
                </p>
                <p>
                  Cararabiya will not be liable if for any reason the Website is
                  unavailable at any time or for any period.
                </p>
                <p>You may not:</p>
                <p>
                  Use any robot spider, scraper or other automated means to
                  access Cararabiya and collect content for any purpose or
                  otherwise copy or download content. A limited exception is
                  granted to search engines and non-commercial public archives,
                  but not for websites that include any form of classified
                  listings; and
                </p>
                <p>
                  Harvest or otherwise collect information about others,
                  including email addresses, without their consent.
                </p>
                <p>
                  You may establish a hyperlink on your website to the Website,
                  or to postings within the Website, for non-commercial use, so
                  long as you do so in a way that is fair and legal and does not
                  damage or take advantage of Cararabiya’s reputation (or of any
                  person connected to Cararabiya).
                </p>
                <p>
                  You must not establish a hyperlink in a misleading way or as
                  to suggest any form of association, approval or endorsement of
                  Cararabiya or any other third party where none exists.
                </p>
                <p>The Website must not be framed on any other website.</p>
                <p>
                  Cararabiya reserves the right to withdraw or amend the above
                  linking permission without advance notice. The website from
                  which you are linking must comply in all respects with the
                  content standards set out in these Terms of Use.
                </p>
                <p>
                  You may not: use automated means to post content on Cararabiya
                  (via the web interface) without our consent.
                </p>
                <p>
                  <b>9. Limitations on Your Usage of the Website</b>
                </p>
                <p>
                  Cararabiya may establish any limitations, in our sole
                  discretion, on your usage of the Website, including, but not
                  limited to, limitations on the length of time that Content may
                  remain on the Website, the file size of an individual item of
                  Content, number of items of Content that can be posted.
                </p>
                <p>
                  <b>10. Conduct &amp; Acceptable Use; No Spam</b>
                </p>
                <p>
                  You agree to not post, email, or make available through the
                  Website any Content that is objectionable to Cararabiya or in
                  contradiction to applicable laws or regulations. In this
                  regard, you agree not to post, email or make available through
                  the Website any Content that:
                </p>
                <p>
                  Is unlawful, harmful, threatening, abusive, defamatory,
                  libelous, invasive or any information harmful to minors;
                </p>
                <p>
                  Is of a political nature and/or is critical of any government
                  or municipal employee in any jurisdiction.
                </p>
                <p>
                  Is pornographic or any kind of nudity, sexually explicit or
                  perverse content;
                </p>
                <p>Is anti-Islamic or in any way derogatory towards Islam;</p>
                <p>
                  Is racist or discriminatory content towards any race,
                  religion, creed, nationality, gender or any other grouping of
                  individuals;
                </p>
                <p>Impersonates any another person;</p>
                <p>
                  Includes personally identifiable information about another
                  person without their permission;
                </p>
                <p>Is false or deceptive;</p>
                <p>
                  Solicits the sale of goods or services, except when (1) posted
                  in the appropriately designated area of the Website for such
                  sales, or (2) emailed to another user of the Website who
                  expressly wish to be contacted by you;
                </p>
                <p>
                  Includes links to other websites, except when (1) expressly
                  approved by Cararabiya in writing, and (2) posted in a posting
                  uploaded by you in the appropriately designated area;
                </p>
                <p>
                  Advertises the sale of anything illegal under any applicable
                  law or regulation in the jurisdiction you are selling the
                  product from or to;
                </p>
                <p>
                  Violates any applicable international, federal, national,
                  local copyright, patent, trademark or other intellectual
                  property laws;
                </p>
                <p>
                  Contains any software viruses, trojans, worms, logic bombs or
                  other material which is malicious or technologically harmful;
                </p>
                <p>
                  Disrupts the normal flow with an excessive number of messages;
                  or
                </p>
                <p>
                  Uses misleading contact details to disguise the true origin.
                </p>
                <p>You further agree not to:</p>
                <p>Contact people who do not wish to be contacted;</p>
                <p>Stalk or harass other users of the Website;</p>
                <p>
                  Collect personal information about users for commercial or
                  unlawful purposes;
                </p>
                <p>
                  Post Content that is irrelevant or unrelated to the category
                  or discussion thread where such Content is posted;
                </p>
                <p>
                  Post the same good or service in more than two categories;
                </p>
                <p>
                  To post, email, make available through the Website or contact
                  Cararabiya or any other users of the Website with any spam,
                  junk mail, chain letters, pyramid or Ponzi schemes, or
                  unsolicited email advertisements; or
                </p>
                <p>
                  Attempt to gain unauthorized access to the Website, the server
                  on which the Website is stored or any server, computer or
                  database connected to the Website,
                </p>
                <p>
                  For the avoidance of doubt, the above list of prohibited
                  content and actions is not exhaustive and Cararabiya reserves
                  the right to (i) remove any content that it considers (in its
                  absolute and sole discretion) to be objectionable or in
                  contradiction to applicable laws or regulations without
                  notice; and (ii) take necessary steps to prevent persons who
                  post content and/or conduct themselves in contravention of
                  this Clause 9 from accessing the Website.
                </p>
                <p>
                  <b>
                    11. Dealings with Third Party Organization and Individuals
                  </b>
                </p>
                <p>
                  Cararabiya operates the Website as an online community portal
                  for car buyers, sellers and community members. Cararabiya does
                  not sell any item listed on the Website directly and is never
                  a party to any transaction between buyers and sellers. As a
                  result, Cararabiya does not (a) guarantee or ensure any item
                  listed on the Website or any transaction between a buyer and
                  seller, (b) collect or process payment or transfer of title on
                  behalf of buyers or sellers, or (c) warehouse, store, ship or
                  deliver any item listed on the Website. Furthermore,
                  Cararabiya is not responsible for, nor holds itself out as
                  promoting, any interaction or dealing between you and 1 or
                  more other user(s) of the Website, and Cararabiya will not be
                  liable in any way for any such interactions or dealings.
                </p>
                <p>
                  You will not hold Cararabiya responsible for other users'
                  Content, actions or inactions, items they list or the
                  destruction of allegedly fake items. While we may help
                  facilitate the resolution of disputes, Cararabiya is not
                  obliged to participate in any dispute between you and other
                  user(s) of the Website. We have no control over and do not
                  guarantee the quality, safety or legality of items advertised,
                  the truth or accuracy of users’ Content or listings, the
                  ability of sellers to sell items, the ability of buyers to pay
                  for items, or that a buyer or seller will actually complete a
                  transaction or return an item. For certain categories,
                  particularly motor vehicles and real estate, an offer
                  initiates a transaction representing a buyer’s serious
                  expression of interest in buying the seller’s item and does
                  not create a formal contract between the buyer and the seller.
                </p>
                <p>
                  You acknowledge that Cararabiya does not monitor, verify or
                  approve Content, and that you are solely responsible for
                  evaluating and verifying the accuracy of any Content posted on
                  the Website.
                </p>
                <p>
                  <b>12. Disclaimer of Warranties</b>
                </p>
                <p>You use the Website at your own risk.</p>
                <p>
                  The Website is provided without warranty and is provided on an
                  “as is, as available” basis.
                </p>
                <p>
                  Cararabiya HEREBY DISCLAIMS ANY REPRESENTATIONS OR WARRANTIES
                  OF ANY KIND, WHETHER ORAL OR WRITTEN, WHETHER EXPRESS,
                  IMPLIED, OR ARISING BY STATUTE, CUSTOM, COURSE OF DEALING OR
                  TRADE USAGE, WITH RESPECT TO THE WEBSITE OR ANY SERVICES
                  PROVIDED HEREUNDER. NOTWITHSTANDING THE GENERALITY OF THE
                  FOREGOING, Cararabiya SPECIFICALLY DISCLAIMS THE FOLLOWING
                  WITH RESPECT TO THE WEBSITE:
                </p>
                <p>
                  IMPLIED WARRANTIES OR CONDITIONS OF TITLE, MERCHANTABILITY,
                  FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT OF ANY
                  SERVICE OR GOOD ACQUIRED BY YOU FROM A THIRD PARTY;
                </p>
                <p>
                  OF THE ACCURACY, TIMELINESS, SECURITY AND PERFORMANCE OF THE
                  WEBSITE; OR
                </p>
                <p>
                  FREEDOM FROM ANY ERRORS OR VIRUSES, TROJANS, WORMS, LOGIC
                  BOMBS OR OTHER MATERIAL WHICH IS MALICIOUS OR TECHNOLOGICALLY
                  HARMFUL.
                </p>
                <p>
                  <b>13. Limitation of Liabilities</b>
                </p>
                <p>
                  To the maximum extent permitted by applicable law, Cararabiya
                  hereby expressly excludes any liability for any damages
                  whatsoever (including, without limitation, direct, indirect,
                  special, punitive, incidental or consequential loss or damage
                  incurred by any user in connection with the Website or in
                  connection with the use, inability to use, or results of the
                  use of the Website, any websites linked to it and any
                  materials posted on it, including, without limitation any
                  liability for: loss of income or revenue; loss of business;
                  loss of profits or contracts; loss of anticipated savings;
                  loss of data; loss of goodwill; wasted management or office
                  time; and for any other loss or damage of any kind, however
                  arising and whether caused by tort (including negligence),
                  breach of contract or otherwise, even if foreseeable.
                </p>
                <p>
                  In particular, Cararabiya is not liable for any damages or
                  losses resulting to you or a third party from:
                </p>
                <p>The use or the inability to use the Website;</p>
                <p>Services or goods acquired through the Website;</p>
                <p>Any Content displayed on the Website; or</p>
                <p>
                  A distributed denial-of-service attack, viruses or other
                  technologically harmful material that may infect your computer
                  equipment, computer programs, data or other proprietary
                  material due to your use of the Website or to your downloading
                  of any material posted on it, or on any website linked to it.
                </p>
                <p>
                  <b>14. Indemnity</b>
                </p>
                <p>
                  You agree to indemnify, defend and hold harmless Cararabiya,
                  its shareholders, officers, directors, employees, and agents
                  from any claims, actions, demands, damages, losses or attorney
                  fees alleging or resulting from your use or misuse of the
                  Website or any Content, your breach of these Terms of Use,
                  your violation of any third party’s proprietary rights, or any
                  disputes between you and one or more users connected to or
                  arising from the Website in any way.
                </p>
                <p>
                  <b>15. Termination of Access to the Website</b>
                </p>
                <p>
                  Cararabiya reserves the right to block you or terminate you
                  from accessing the Website for any breach of these Terms of
                  Use at any time without any notice.
                </p>
                <p>
                  Cararabiya reserves the right to delete any of your Content
                  from the Website at any time.
                </p>
                <p>
                  Cararabiya is not liable to you or a third party for any
                  termination of your access to the Website or deletion of your
                  posted Content.
                </p>
                <p>
                  You agree not to try to use the Website after said
                  termination.
                </p>
                <p>
                  <b>16. Violations of Terms of Use</b>
                </p>
                <p>
                  Please notify us if you become aware of any violations by
                  third parties of these Terms of Use.
                </p>
                <p>
                  <b>17. Enforcement of Terms of Use</b>
                </p>
                <p>
                  Cararabiya reserves the right to take steps Cararabiya
                  believes are reasonably necessary or appropriate to enforce
                  and/or verify compliance with any of these Terms of Use
                  (including, but not limited to Cararabiya’s right to cooperate
                  with any legal process relating to your use of the Website,
                  and/or a third party claim that your use of the Website is
                  unlawful and/or infringes such third party’s rights). You
                  agree that Cararabiya has the right without liability to you
                  to disclose any registration data and/or account information
                  to law enforcement authorities, government officials and/or
                  third parties, as Cararabiya believes is reasonably necessary
                  or appropriate to enforce and/or verify compliance with any of
                  these Terms of Use (including, but not limited to Cararabiya’s
                  right to cooperate with any legal process relating to your use
                  of the Website, and/or a third party claim that your use of
                  the Website is unlawful and/or infringes such third party’s
                  rights).
                </p>
                <p>
                  <b>18. General</b>
                </p>
                <p>
                  You agree that the fact that these Terms of Use are in
                  electronic format does not affect in any way their validity or
                  enforceability.
                </p>
                <p>
                  These Terms of Use (together with any Prepaid Listing Account
                  Booking Form) constitute the entire agreement between you and
                  Cararabiya regarding its subject matter and supersede any
                  contemporaneous or prior oral or written agreements,
                  representations, understandings or discussions between you and
                  Cararabiya, whether implied or express.
                </p>
                <p>
                  Cararabiya’s delay or failure to exercise a right or act on a
                  breach of these Terms of Use does not waive our right to do so
                  in the future.
                </p>
                <p>
                  The provisions of these Terms of Use shall be deemed
                  severable, and if any provision is determined to be illegal or
                  invalid under applicable law, such provision may be changed to
                  the extent reasonably necessary to make the provision, as so
                  changed, legal, valid and binding.
                </p>
                <p>
                  You may not assign any part of these Terms of Use without
                  Cararabiya’s express written permission. Cararabiya may assign
                  these Terms of Use immediately upon posting a notice of such
                  assignment on the Website
                </p>
                <p>
                  <b>Privacy Policy</b>
                </p>
                <p>
                  In order for the website to provide a safe and useful service,
                  it is important for Cararabiya.com to collect, use, and share
                  personal information.
                </p>
                <p>
                  <b>Collection</b>
                </p>
                <p>
                  Information posted on Cararabiya.com is publicly available.
                  Our servers are located in United Kingdom and the Saudi
                  Arabia, therefore, if you choose to provide us with personal
                  information, you are consenting to the transfer and storage of
                  that information on our servers in United Kingdom and theSaudi
                  Arabia. We collect and store the following personal
                  information:
                </p>
                <p>
                  Email address, contact information, and (depending on the
                  service used) sometimes financial information;
                </p>
                <p>
                  Computer sign-on data, statistics on page views, traffic to
                  and from Cararabiya.com and response to advertisements;
                </p>
                <p>
                  Other information, including users' IP address and standard
                  web log information.
                </p>
                <p>
                  <b>Use.</b>
                </p>
                <p>We use users' personal information to:</p>
                <p>Provide our services</p>
                <p>
                  Resolve disputes, collect fees, and troubleshoot problems;
                </p>
                <p>Encourage safe trading and enforce our policies;</p>
                <p>
                  Customize users' experience, measure interest in our services,
                  improve our services and inform users about services and
                  updates;
                </p>
                <p>
                  Communicate marketing and promotional offers to you according
                  to your preferences;
                </p>
                <p>
                  Do other things for users as described when we collect the
                  information.
                </p>
                <p>
                  <b>Disclosure</b>
                </p>
                <p>
                  We don't sell or rent users' personal information to third
                  parties for their marketing purposes without users' explicit
                  consent. We may disclose personal information to respond to
                  legal requirements, enforce our policies, respond to claims
                  that a posting or other content violates other's rights, or
                  protect anyone's rights, property, or safety.
                </p>
                <p>
                  <b>Communication and email tools</b>
                </p>
                <p>
                  You agree to receive marketing communications about consumer
                  goods and services on behalf of our third party advertising
                  partners unless you tell us that you prefer not to receive
                  such communications. If you don't wish to receive marketing
                  communications from us, simply indicate your preference by
                  following directions provided with the communication. You may
                  not use our site or communication tools to harvest addresses,
                  send spam or otherwise breach our Terms of Use or Privacy
                  Policy. We may automatically scan and manually filter email
                  messages sent via our communication tools for malicious
                  activity or prohibited content. If you use our tools to send
                  content to a friend, we don't permanently store your friends'
                  addresses or use or disclose them for marketing purposes. To
                  report spam from other users, please contact
                  customercare@Cararabiya.com
                </p>
                <p />
                <p>
                  <b>Security</b>
                </p>
                <p>
                  We use lots of tools (encryption, passwords, physical
                  security) to protect your personal information against
                  unauthorized access and disclosure.
                </p>
                <p>
                  All personal electronic details will be kept private by the
                  Service except for those that you wish to disclose.
                </p>
                <p>
                  It is unacceptable to disclose the contact information of
                  others through the Service.
                </p>
                <p>
                  If you violate the laws of theSaudi Arabia and/or the terms of
                  use of the Service you forfeit your privacy rights over your
                  personal information.
                </p>
                <p>
                  <b>19. Governing Law &amp; Dispute Resolution</b>
                </p>
                <p>These terms are governed by the laws ofSaudi Arabia.</p>
                <p>
                  In the event of any disputes between you and Cararabiya,
                  Cararabiya is open to first attempting to resolve the dispute
                  in an informal and amicable manner.
                </p>
                <p>
                  However, if either party believes that the dispute cannot be
                  resolved in such a manner, then the parties agree that either
                  party may refer any dispute arising
                </p>{" "}
                out of, or in connection with, these Terms of Use, including any
                question regarding its existence, validity or termination, to be
                finally settled by binding arbitration. Such arbitration shall
                be held under the then-current Arbitration Rules of the Dubai
                International Financial Centre - London Court of International
                Arbitration by one (1) arbitrator appointed in accordance with
                such rules.
                <p>
                  The seat of arbitration shall be the Dubai International
                  Financial Centre in Dubai, UAE. The arbitration shall be
                  conducted in the English language. The arbitration award shall
                  be final and binding on the parties. The arbitral tribunal may
                  award reasonable attorneys’ fees and the cost of the
                  arbitration to the prevailing party.
                </p>
                <p>
                  Judgment upon the award rendered by the arbitrator(s) may be
                  entered by any court having jurisdiction thereof or having
                  jurisdiction over the relevant party or its assets.
                </p>
                <p>
                  If the arbitral panel has not been formed, any party may seek
                  interim relief from any court having jurisdiction thereof or
                  having jurisdiction over the relevant party or its assets, to
                  which both you and Cararabiya hereto do hereby submit for this
                  limited purpose.
                </p>
                <p>
                  <b>20. Modifications to Terms of Use</b>
                </p>
                <p>
                  Cararabiya reserves the right at any time and from time to
                  time to update, revise, supplement and otherwise modify these
                  Terms of Use and to impose new and/or additional rules,
                  policies, terms and/or conditions on your use of the Website
                  (collectively referred to as “Additional Terms”). Such
                  Additional Terms will be effective immediately and
                  incorporated into these Terms of Use. Your continued use of
                  the Website constitutes you acceptance of all such Additional
                  Terms. Any Additional Terms are hereby incorporated into these
                  Terms of Use by this reference.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}