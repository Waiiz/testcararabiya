import React, { Component, Fragment } from "react";
import {
  Listingsearch,
  Listingadvertismenttop,
  Listinglatestadsleft,
  Listingadvertismentleft,
  ListingSearchkeyword,
  Listinglist
} from "../components/listing";
import Dealeradvertisement from "./Dealeradvertisement";

class Listingbackup extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Fragment>
        <div className="container mt-5">
          <div className="row">
            <div className="col-12">
              <Listingadvertismenttop />
            </div>
          </div>

          <div className="row justify-content-between">
            <div className="list-heading">Used Vehicle for Sale</div>
            <div className="b-filter-goods__wrap col-auto">
              <div
                className="b-filter-goods__select"
                style={{ fontSize: "0.9em" }}
              >
                <select className="custom-select-adpost">
                  <option>Sort by</option>
                  <option>A-Z</option>
                  <option>Z-A</option>
                </select>
              </div>
            </div>
          </div>
          <hr />
          <div className="row">
            <div className="col-lg-3">
              <aside className="l-sidebar">
                <ListingSearchkeyword />
                <div className="accordion" id="accordion-2">
                  <Listingsearch />

                  <div className="widget section-sidebar bg-light">
                    <div className="widget-content">
                      {/* quick search widget inner close */}
                    </div>{" "}
                    {/* advance search widget inner close */}
                    <div className="container-fluid advertise-advance">
                      <Listingadvertismentleft />
                    </div>
                    <div className="container-fluid advertise-advance">
                      <div
                        className="container py-3"
                        style={{
                          backgroundColor: "#e9e9e9",
                          borderTop: "1px solid #cacaca"
                        }}
                      >
                        <div className="row mb-3 mt-2">
                          <span
                            style={{
                              fontSize: "14px",
                              fontWeight: 600,
                              color: "#000"
                            }}
                          >
                            Related Ads
                          </span>{" "}
                        </div>
                        <Listinglatestadsleft />
                      </div>
                    </div>
                  </div>
                </div>
                {/* end .b-filter*/}
                {/* end .b-brands*/}
              </aside>
            </div>
            <div className="col-lg-9">
              <Listinglist />
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Listingbackup;