import React, { Component, Fragment } from "react";
import {
  Promolatestads,
  Promolist,
  Promosearch,
} from "../components/promotions";
import Dealeradvertisement from './Dealeradvertisement';

class Promotions extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <Fragment>
        <div className="container mt-5">
          <div className="row">
            <div className="col-lg-3">
              <Promosearch />
              <Dealeradvertisement />
            </div>
            <div className="col-lg-9">
              <Promolatestads />
              <Promolist {...this.props} />
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Promotions;
