import React, { Component, Fragment } from "react";
import "./PostAd.css";
import { Tabs, Tab, Button, Alert, Image, Modal } from "react-bootstrap";
import ImageUploading from "react-images-uploading";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import "./CopyOfChildAsDots.css";
import axios from "axios";
import Loader from "./Loader";
import classNames from "classnames";
import {
  apiBaseURL,
  apiUrlAttribute,
  apiUrlUserPackageAdPost
} from "../ApiBaseURL";
import moment from "moment";
import _ from "lodash";

var j = 0;
var maxNumber = 5;
var apiAndUploadImages = [];

const responsive = {
  desktop: {
    breakpoint: {
      max: 3000,
      min: 1024
    },
    items: 1
  },
  mobile: {
    breakpoint: {
      max: 464,
      min: 0
    },
    items: 1
  },
  tablet: {
    breakpoint: {
      max: 1024,
      min: 200
    },
    items: 1
  }
};

export default class EditPostAd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //tab key
      key: 1,

      AdPostDetails: [],
      isLoadingForAdPostDetails: false,
      //exterior color
      secolor: "",
      lecolor: [],
      secolorname: "",
      //interior color
      licolor: [],
      sicolor: "",
      sicolorname: "",

      VehicleTypeID: "",
      scityid: "",

      //attribute
      lattribute: [],
      sattribute: [],
      sattributename: [],
      svattribute: [],
      //area
      sarea: "",
      //upload tab
      imageList: [],
      GetAdImage: [],

      UserPackageAdPost: [],
      NumOfImages: "",
      imageUpload: 0,
      toggleAlertMsg: false,

      //Ad Post
      AdPostID: "",
      DraftAdPostID: "",

      //modals
      showModalPhone: false,
      showModalEmail: false,
      showModalSuccess: false,
      //user
      userId: "",
      //event for del image
      savedEvent: "",
      //Guidlines
      Guidlines: [
        {
          id: 1,
          value: "Front Side"
        },
        {
          id: 2,
          value: "Back Side"
        },
        {
          id: 3,
          value: "Left Side"
        },
        {
          id: 4,
          value: "Right Side"
        },
        {
          id: 5,
          value: "Engine"
        }
      ]
    };
  }

  componentWillMount() {
    if (this.props.history.location.state !== undefined) {
      this.setState({
        AdPostID: this.props.history.location.state.AdPostID
      });
    } else {
      console.log(this.state.AdPostID, "adpostid null");
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0);

    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      var userid = parseUserData[0].UserID;
      this.setState({
        userId: parseUserData[0].UserID
      });
    } else {
      this.props.history.push({
        pathname: "/login-signup"
      });
    }

    const url = `${apiBaseURL}/user?UserID=${userid}`;
    axios
      .get(url)
      .then(res => {
        const formatteddate = moment(res.data[0].CreatedDate).format(
          "MMMM YYYY"
        );
        this.setState({
          userData: res.data,
          UserID: res.data[0].UserID,
          Firstname: res.data[0].FirstName,
          Lastname: res.data[0].LastName,
          CreatedDate: formatteddate,
          ImageUrl: res.data[0].ImageUrl
        });
      })
      .catch(error => {
        console.log(error, "userdata api");
      });

    this.setState({ isLoadingForAdPostDetails: true });

    const urlAdPostDetail = `${apiBaseURL}/AdPostDetail/AdPostDetail?AdpostID=${this.state.AdPostID}`;
    axios
      .get(urlAdPostDetail)
      .then(res => {
        if (res.data.length > 0) {
          console.log(res.data);
          this.setState({
            AdPostDetails: res.data,
            VehicleTypeID: res.data[0].VehicleTypeID,
            secolor: res.data[0].ExtColorID,
            secolorname: res.data[0].ExteriorColor,
            sicolorname: res.data[0].InteriorColor,
            sicolor: res.data[0].IntColorID,
            smeterreading: res.data[0].MeterReading,
            sdescription: res.data[0].Comments,
            scityid: res.data[0].CityID,
            sarea: res.data[0].Area
          });
          this.setState({
            isLoadingForAdPostDetails: false
          });
          this.fetchingData(res.data[0].VehicleTypeID, res.data[0].CityID);
        } else {
          console.log(res.data);
          this.setState({ isLoadingForAdPostDetails: false });
        }
      })
      .catch(error => {
        this.setState({ isLoadingForAdPostDetails: false });
        console.log(error, "from AdPostDetail api");
      });

    const urlGetAdImage = `${apiBaseURL}/AdImage?AdpostID=${this.state.AdPostID}`;
    axios
      .get(urlGetAdImage)
      .then(res => {
        if (res.data.length > 0) {
          console.log(res.data, "urlGetAdImage");
          apiAndUploadImages = [...res.data];
          this.setState({
            GetAdImage: res.data
          });
          this.FetchUserPackageAdPost();
        } else {
          console.log(res.data, "urlGetAdImage");
        }
      })
      .catch(error => {
        console.log(error, "urlGetAdImage");
      });

    var listatt = [];
    var newlist = [];
    var ouy = [];

    fetch(apiUrlAttribute)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            listatt = result;

            for (var a = 0; a < listatt.length; a++) {
              ouy = {
                AttributeID: listatt[a].AttributeID,
                AttributeName: "" + listatt[a].AttributeName + "",
                CreatedBy: listatt[a].CreatedBy,
                CreatedDate: "" + listatt[a].CreatedDate + "",
                IsActive: false,
              };
              newlist.push(ouy);
            }
            this.setState({
              lattribute: newlist,
            });
          } else {
            console.log("attr api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );
  }

  FetchUserPackageAdPost = () => {
    //upload image
    fetch(apiUrlUserPackageAdPost)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.photosRemaining = result[0].NumOfImages;
            maxNumber = maxNumber - this.state.GetAdImage.length;
            console.log("maxNumber: ", maxNumber);

            this.setState({
              UserPackageAdPost: result,
              NumOfImages:
                result[0].NumOfImages - this.state.GetAdImage.length,
              imageUpload: this.state.GetAdImage.length,
              NumOfVideos: result[0].NumOfVideos
            });
          } else {
            console.log("attr api");
          }
        },
        error => {
          this.setState({ error });
        }
      );
  };

  fetchingData = (vehicletypeid, cityid) => {
    const apiUrlecolor =
      "https://cararabiya.ahalfa.com/api/color?VehicleInfoID=" +
      vehicletypeid +
      "&&IEType=e";

    fetch(apiUrlecolor)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lecolor: result
            });
          } else {
            console.log("ecolor api");
          }
        },
        error => {
          this.setState({ error });
        }
      );

    const apiUrlicolor =
      "https://cararabiya.ahalfa.com/api/color?VehicleInfoID=" +
      vehicletypeid +
      "&&IEType=i";

    fetch(apiUrlicolor)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              licolor: result
            });
          } else {
            console.log("icolor api");
          }
        },
        error => {
          this.setState({ error });
        }
      );

    const apiUrlvattribute =
      "https://cararabiya.ahalfa.com/api/adpostattribute?AdpostID=" +
      this.state.AdPostID;

    fetch(apiUrlvattribute)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              svattribute: result
            });

            var listatt = this.state.lattribute;
            var listvatt = this.state.svattribute;
            var newlistatt = [];
            var satt = this.state.sattribute;
            var sattname = this.state.sattributename;
            var flag = false;

            for (var a = 0; a < listatt.length; a++) {
              for (var b = 0; b < listvatt.length; b++) {
                if (listatt[a].AttributeID == listvatt[b].AttributeID) {
                  flag = true;
                }
              }
              if (flag == true) {
                flag = false;
                var ouy = {
                  AttributeID: listatt[a].AttributeID,
                  AttributeName: "" + listatt[a].AttributeName + "",
                  CreatedBy: listatt[a].CreatedBy,
                  CreatedDate: "" + listatt[a].CreatedDate + "",
                  IsActive: true
                };
                newlistatt.push(ouy);
                satt.push(listatt[a].AttributeID);
                sattname.push(listatt[a].AttributeName);
              } else {
                var ouy = {
                  AttributeID: listatt[a].AttributeID,
                  AttributeName: "" + listatt[a].AttributeName + "",
                  CreatedBy: listatt[a].CreatedBy,
                  CreatedDate: "" + listatt[a].CreatedDate + "",
                  IsActive: false
                };
                newlistatt.push(ouy);
              }
            }

            this.setState({
              lattribute: newlistatt,
              sattribute: satt,
              sattributename: sattname
            });
          } else {
            console.log("vattr api");
          }
        },
        error => {
          this.setState({ error });
        }
      );

    const urlCity = `${apiBaseURL}/city?CityID=${cityid}`;
    axios
      .get(urlCity)
      .then(res => {
        if (res.data.length > 0) {
          console.log(res.data);
          this.setState({
            cityname: res.data[0].CityName
          });
        } else {
          console.log(res.data);
          this.setState({});
        }
      })
      .catch(error => {
        this.setState({});
        console.log(error, "from getCityName api");
      });
  };

  //user phone and email
  handleShowPhone = () => {
    const urlUserPhone = `${apiBaseURL}/User/UserMobile?UserID=${this.state.userId}`;
    axios
      .get(urlUserPhone)
      .then(res => {
        if (res.data.length > 0) {
          const data = res.data;
          console.log(data, "urlUserPhone");
          this.setState({
            userPhone: data,
            showModalPhone: true
          });
        } else {
          this.setState({});
        }
      })
      .catch(error => {
        this.setState({});
        console.log(error, "from urlUserPhone api");
      });
  };

  //phone modal
  handleClosePhone = () => {
    this.setState({
      showModalPhone: false
    });
  };

  modalPhone = () => {
    return (
      <Modal show={this.state.showModalPhone}>
        <Modal.Header>
          <Modal.Title>User Phone Number</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h6>{this.state.userPhone}</h6>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={this.handleClosePhone}
            style={{ backgroundColor: "#D3E428" }}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    );
  };

  handleShowEmail = () => {
    const urlUserEmail = `${apiBaseURL}/User/UserEmail?UserID=${this.state.userId}`;
    axios
      .get(urlUserEmail)
      .then(res => {
        if (res.data.length > 0) {
          const data = res.data;
          console.log(data, "urlUserEmail");
          this.setState({
            userEmail: data,
            showModalEmail: true
          });
        } else {
          this.setState({});
        }
      })
      .catch(error => {
        this.setState({});
        console.log(error, "from urlUserEmail api");
      });
  };

  //phone modal
  handleCloseEmail = () => {
    this.setState({
      showModalEmail: false
    });
  };

  modalEmail = () => {
    return (
      <Modal show={this.state.showModalEmail}>
        <Modal.Header>
          <Modal.Title>User Email</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h6>{this.state.userEmail}</h6>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={this.handleCloseEmail}
            style={{ backgroundColor: "#D3E428" }}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    );
  };

  handleCloseSuccess = () => {
    this.setState({
      showModalSuccess: false
    });
  };

  modalSuccess = () => {
    return (
      <Modal show={this.state.showModalSuccess}>
        <Modal.Body>
          <div className="my-3 px-3">
            <div className="d-flex justify-content-center my-3">
              <h4>
                <strong>Success</strong>
              </h4>
            </div>

            <div style={{ textAlign: "center" }}>Your Ad has been posted</div>

            <div className="d-flex justify-content-center mb-2 mt-3">
              <button
                onClick={this.handleCloseSuccess}
                className="btn"
                style={{
                  border: "none",
                  padding: "10px 80px",
                  backgroundColor: "#D3E428",
                  color: "white"
                }}
              >
                OK
              </button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    );
  };

  handleEcolorChange = event => {
    this.setState({ secolor: event.target.value });

    let filteredArray = this.state.lecolor.filter(
      item => event.target.value == item.ColorID
    );
    this.setState({ secolorname: filteredArray[0].ColorName });
  };

  handleIcolorChange = event => {
    this.setState({ sicolor: event.target.value });

    let filteredArray = this.state.licolor.filter(
      item => event.target.value == item.ColorID
    );
    this.setState({ sicolorname: filteredArray[0].ColorName });
  };

  handleAttributeChange = (attribute, index, event) => {
    var checkboxEnabled = !attribute.IsActive;
    attribute.IsActive = checkboxEnabled;

    var mylattribute = this.state.lattribute;

    var getIndex = mylattribute.indexOf(mylattribute[index]);
    if (getIndex > -1) {
      mylattribute.splice(getIndex, 1, attribute);
    }

    this.setState({
      lattribute: mylattribute
    });

    const searchkeyid2 = event.target.id;
    var pieces = searchkeyid2.split("-");

    const searchkeyid = pieces[1];
    const searchkeyvalue = event.target.value;
    const cheked = event.target.checked;

    if (cheked == true) {
      //var joined = this.state.sattribute;
      let filteredArray = this.state.sattribute.filter(
        item => item != searchkeyid
      );
      filteredArray.push(searchkeyid);
      this.setState({ sattribute: filteredArray });

      let filteredArray2 = this.state.sattributename.filter(
        item => item !== searchkeyvalue
      );
      filteredArray2.push(searchkeyvalue);
      this.setState({ sattributename: filteredArray2 });
    } else {
      let filteredArray = this.state.sattribute.filter(
        item => item != searchkeyid
      );
      this.setState({ sattribute: filteredArray });

      let filteredArray2 = this.state.sattributename.filter(
        item => item !== searchkeyvalue
      );
      this.setState({ sattributename: filteredArray2 });
    }
  };

  handleMeterreadingChange = event => {
    this.setState({ smeterreading: event.target.value });
  };

  handleDescriptionChange = event => {
    this.setState({ sdescription: event.target.value });
  };

  handleAreaChange = event => {
    this.setState({ sarea: event.target.value });
  };

  //basic info tab
  renderBasicInfoTab = () => {
    return (
      <div className="tab-pane fade show active" id="content-basic">
        {this.state.AdPostDetails.map((adpostdetail, index) => (
          <form
            key={index}
            className="form-horizontal"
            onSubmit={this.gotoAdditionalTab}
          >
            <div className="row mt-5">
              <div className="col-lg-6">
                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label className="control-label">City *</label>
                    </div>
                    <div className="col-8">{this.state.cityname}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label className="control-label">Sale Price *</label>
                    </div>
                    <div className="col-8">{adpostdetail.Price}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label className="control-label">Car Type *</label>
                    </div>
                    <div className="col-8">
                      {adpostdetail.VehicleCategoryName}
                    </div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Make *
                      </label>
                    </div>
                    <div className="col-8">{adpostdetail.MakeName}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Model *
                      </label>
                    </div>
                    <div className="col-8">{adpostdetail.ModelName}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Version *
                      </label>
                    </div>
                    <div className="col-8">{adpostdetail.VersionName}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Year *
                      </label>
                    </div>
                    <div className="col-8">{adpostdetail.VersionYear}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Exterior Color *
                      </label>
                    </div>
                    <div className="col-8">
                      <select
                        required
                        onChange={this.handleEcolorChange}
                        value={this.state.secolor}
                        className="custom-select-adpost"
                      >
                        {this.state.lecolor.map((ecolor, index) => (
                          <option key={ecolor.ColorID} value={ecolor.ColorID}>
                            {ecolor.ColorName}
                          </option>
                        ))}
                      </select>
                    </div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Interior Color *
                      </label>
                    </div>
                    <div className="col-8">
                      <select
                        required
                        onChange={this.handleIcolorChange}
                        value={this.state.sicolor}
                        className="custom-select-adpost"
                      >
                        {this.state.licolor.map((icolor, index) => (
                          <option key={icolor.ColorID} value={icolor.ColorID}>
                            {icolor.ColorName}
                          </option>
                        ))}
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              {/* second colomn */}
              <div className="col-lg-6">
                <div className="form-group">
                  <div className="row">
                    <div className="col-6">
                      <label htmlFor className="control-label">
                        Number of Owners *
                      </label>
                    </div>
                    <div className="col-6">{adpostdetail.NumberofOwners}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-6">
                      <label htmlFor className="control-label">
                        Fuel Type *
                      </label>
                    </div>
                    <div className="col-6">{adpostdetail.EngineType}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-6">
                      <label className="control-label">
                        Meter Reading(km) *
                      </label>
                    </div>
                    <div className="col-6">
                      <input
                        required
                        onChange={this.handleMeterreadingChange}
                        value={this.state.smeterreading}
                        type="number"
                        className="form-control"
                        placeholder="Enter Meter Reading(km)"
                      />
                    </div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-6">
                      <label className="control-label">
                        Body Condition (out of 10) *
                      </label>
                    </div>
                    <div className="col-6">{adpostdetail.BodyCondition}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-6">
                      <label className="control-label">
                        Tyre Condition (out of 10) *
                      </label>
                    </div>
                    <div className="col-6">{adpostdetail.TyreCondition}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-6">
                      <label className="control-label">
                        Mechanical Condition (out of 10)*
                      </label>
                    </div>
                    <div className="col-6">
                      {adpostdetail.MechanicalCondition}
                    </div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-6">
                      <label htmlFor className="control-label">
                        Engine Capacity (cc) *
                      </label>
                    </div>
                    <div className="col-6">{adpostdetail.FuelTankCapacity}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Ad Description *
                      </label>
                    </div>
                    <div className="col-8">
                      <textarea
                        required
                        onChange={this.handleDescriptionChange}
                        value={this.state.sdescription}
                        cols={30}
                        rows={5}
                        className="form-control"
                        placeholder="Enter description"
                        defaultValue={""}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="mt-3">
              <input
                className="btn next-step"
                type="submit"
                value="Continue"
                style={{
                  position: "absolute",
                  zIndex: 151,
                  right: 0,
                  padding: "10px 80px",
                  backgroundColor: "#D3E428",
                  color: "white"
                }}
              />
            </div>
          </form>
        ))}
        <div className="row">
          <div className="col-12 center-loader">
            {this.state.isLoadingForAdPostDetails ? <Loader /> : ""}
          </div>
        </div>
      </div>
    );
  };

  gotoAdditionalTab = event => {
    event.preventDefault();
    this.setState({
      key: 2
    });
  };

  //additional info tab
  renderAdditionalInfoTab = () => {
    return (
      <div className="tab-pane" id="content-additional">
        {this.state.AdPostDetails.map((adpostdetail, index) => (
          <div key={index}>
            <div className="row mt-5">
              <div className="col-lg-6">
                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Drive Side
                      </label>
                    </div>
                    <div className="col-8">
                      {adpostdetail.DriveSide === "RHD"
                        ? "Right Hand Side"
                        : "Left Hand Side"}
                    </div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Assembly
                      </label>
                    </div>
                    <div className="col-8">
                      {adpostdetail.Assembly === "loc" ? "Local" : "Imported"}
                    </div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Area *
                      </label>
                    </div>
                    <div className="col-8">
                      <input
                        onChange={this.handleAreaChange}
                        value={this.state.sarea}
                        type="text"
                        className="form-control"
                        placeholder="Enter Area!"
                      />
                    </div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Mileage (Km/Ltr) *
                      </label>
                    </div>
                    <div className="col-8">{adpostdetail.Mileage}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Registration City
                      </label>
                    </div>
                    <div className="col-8">{adpostdetail.RegisteredCity}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Chassis Number / VIN
                      </label>
                    </div>
                    <div className="col-8">{adpostdetail.ChasisNumber}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Transmission
                      </label>
                    </div>
                    <div className="col-8">{adpostdetail.TransmissionName}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Engine Type
                      </label>
                    </div>
                    <div className="col-8">{adpostdetail.EngineType}</div>
                  </div>
                </div>
              </div>

              <div className="col-lg-6">
                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Engine number
                      </label>
                    </div>
                    <div className="col-8">{adpostdetail.EngineNumber}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        No. of Cylinders
                      </label>
                    </div>
                    <div className="col-8">{adpostdetail.Cylinders}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        No. of Doors
                      </label>
                    </div>
                    <div className="col-8">{adpostdetail.Doors}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        No. of Gears
                      </label>
                    </div>
                    <div className="col-8">{adpostdetail.NumberofGears}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        No. of Seats
                      </label>
                    </div>
                    <div className="col-8">{adpostdetail.NumberofSeats}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-4">
                      <label htmlFor className="control-label">
                        Trim
                      </label>
                    </div>
                    <div className="col-8">{adpostdetail.Trim}</div>
                  </div>
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="mt-4 col-12">
                  <h4
                    style={{
                      fontFamily: '"Montserrat"',
                      fontSize: "18px",
                      fontWeight: 600
                    }}
                  >
                    Features
                  </h4>
                  <hr />
                </div>

                {this.state.lattribute.map((attribute, index) => (
                  <div className="col-4" key={index}>
                    <div className="form-check">
                      <input
                        onChange={event =>
                          this.handleAttributeChange(attribute, index, event)
                        }
                        type="checkbox"
                        id={"attp-" + attribute.AttributeID}
                        value={attribute.AttributeName}
                        checked={attribute.IsActive}
                      />
                      <label
                        className="form-check-label"
                        style={{ padding: "0 0 10px 10px" }}
                      >
                        {attribute.AttributeName}
                      </label>
                    </div>
                  </div>
                ))}
              </div>

              <div className="mt-5">
                <div>
                  <Button
                    className="btn previous-step"
                    onClick={this.gotoBasicTab}
                  >
                    Previous
                  </Button>

                  <Button
                    className="btn next-step"
                    onClick={this.gotoUploadTab}
                  >
                    Continue
                  </Button>
                </div>
              </div>
            </div>
          </div>
        ))}
        <div className="row">
          <div className="col-12">
            {this.state.isLoadingForAdPostDetails ? <Loader /> : ""}
          </div>
        </div>
      </div>
    );
  };

  gotoBasicTab = () => {
    this.setState({
      key: 1
    });
  };

  gotoUploadTab = () => {
    this.setState({
      key: 3
    });
  };

  //upload tab
  onChange = imageList => {
    imageList.forEach(item => {
      apiAndUploadImages.push(item);
    });

    apiAndUploadImages = _.uniqWith(apiAndUploadImages, _.isEqual);
    //console.log(apiAndUploadImages, "apiAndUploadImages");

    // data for submit
    this.setState({
      NumOfImages:
        this.photosRemaining - imageList.length - this.state.GetAdImage.length,
      imageUpload: imageList.length + this.state.GetAdImage.length,
      imageList: imageList
    });
  };

  onError = (errors, files) => {
    console.log(errors, files);
  };

  onUploadImage = (imageList, onImageUpload) => {
    onImageUpload();
  };

  onRemoveImage = (imageList, e) => {
    imageList[e.target.value].onRemove();
    apiAndUploadImages.splice(e.target.id, 1);
  };

  onDeleteAdPostImageByAdID = (getAdImage, e) => {
    this.setState({
      savedEvent: e.target.value
    });
    let splitItemBySlash = getAdImage[e.target.value].Url.split("/");
    let getImageName = splitItemBySlash[3];

    const url = `${apiBaseURL}/file/DeleteAdPostImageByAdID`;
    let ApiParamForDeleteAdPostImage = {
      AdPostID: this.state.AdPostID,
      ImgName: getImageName
    };
    axios
      .post(url, null, { params: ApiParamForDeleteAdPostImage })
      .then(res => {
        console.log(res.data, "DeleteAdPostImageByAdID");

        if (res.status === 200) {
          getAdImage.splice(this.state.savedEvent, 1);
          apiAndUploadImages.splice(this.state.savedEvent, 1);
          maxNumber = maxNumber + 1;
          console.log("maxNumber: ", maxNumber);
          this.setState({
            NumOfImages:
              this.photosRemaining -
              this.state.imageList.length -
              this.state.GetAdImage.length,
            imageUpload:
              this.state.imageList.length + this.state.GetAdImage.length,
            GetAdImage: getAdImage,
            DeleteAdPostImage: res.data
          });
        } else {
          console.log(res.status, res.statusText, "DeleteAdPostImageByAdID");
        }
      })
      .catch(error => {
        console.log(error, "DeleteAdPostImageByAdID");
      });
  };

  showUploadImages = (imageList, getAdImage) => {
    //console.log(imageList, getAdImage, '---getAdImage');
    var imagesData = [];
    j = 0;
    var getAdImageLength = getAdImage.length;
    var imageListLength = imageList.length + getAdImageLength;

    for (var i = 0; i < 5; i++) {
      if (i < getAdImageLength) {
        imagesData.push(
          <div key={i} className="image-container">
            <div className="col-2">
              <div className="ImgContainerStyle">
                <Button
                  className="close"
                  value={i}
                  onClick={event =>
                    this.onDeleteAdPostImageByAdID(getAdImage, event)
                  }
                >
                  &times;
                </Button>
                <img src={getAdImage[i].ImageUrl} className="img-style" />
              </div>
            </div>
          </div>
        );
      } else if (i < imageListLength) {
        imagesData.push(
          <div key={i} className="image-container">
            <div className="col-2">
              <div className="ImgContainerStyle">
                <Button
                  className="close"
                  value={j}
                  id={i}
                  onClick={event => this.onRemoveImage(imageList, event)}
                >
                  &times;
                </Button>
                <img src={imageList[j].dataURL} className="img-style" />
              </div>
            </div>
          </div>
        );
        j++;
      } else {
        imagesData.push(
          <div key={i} className="image-container">
            <div className="col-2">
              <div className="ContainerStyle">
                <div className="child">{this.state.Guidlines[i].value}</div>
              </div>
            </div>
          </div>
        );
      }
    }
    return imagesData;
  };

  renderUploadTab = () => {
    return (
      <Fragment>
        <div className="tab-pane" id="content-images">
          <div className="mt-5">
            <div class="row">
              <div class="col-6 image-upload">
                <p>Images Upload: {this.state.imageUpload}</p>
              </div>
              <div class="col-6 photos-remaining">
                <p>Photos Remaining: {this.state.NumOfImages}</p>
              </div>
            </div>
            <div className="col-12" style={{ padding: 0 }}>
              <form className="uploadContainer">
                <div className="mt-3 d-flex justify-content-center">
                  <ImageUploading
                    onChange={this.onChange}
                    maxNumber={maxNumber}
                    multiple
                    acceptType={["jpg", "png"]}
                    onError={this.onError}
                  >
                    {({ imageList, onImageUpload, errors }) => (
                      // write your building UI
                      <div>
                        <div>
                          {errors.maxNumber && (
                            <Alert variant="primary">
                              <p style={{ color: "#721c24", fontWeight: 600 }}>
                                Image upload limit exceed...
                              </p>
                            </Alert>
                          )}
                          {errors.acceptType && (
                            <Alert variant="primary">
                              <p style={{ color: "#721c24", fontWeight: 600 }}>
                                Your selected file type is not allow...
                              </p>
                            </Alert>
                          )}
                        </div>
                        <div class="row upload-box">
                          <div class="col-2">
                            <img
                              src="./assets/media/upload.png"
                              alt="upload-image"
                            />
                          </div>

                          <div class="col-6 upload-col">
                            <p>
                              For best results and fast selling follow the below
                              guidelines
                            </p>
                          </div>

                          <div class="col-4 upload-col">
                            <Button
                              style={{ backgroundColor: "#D3E428" }}
                              onClick={() =>
                                this.onUploadImage(imageList, onImageUpload)
                              }
                            >
                              Upload images
                            </Button>
                          </div>
                        </div>
                        <div>
                          {this.showUploadImages(
                            imageList,
                            this.state.GetAdImage
                          )}
                        </div>
                      </div>
                    )}
                  </ImageUploading>
                </div>
              </form>
            </div>
          </div>
          <div className="mt-5">
            <Button
              className="btn previous-step"
              onClick={this.gotoAdditionalTab}
            >
              Previous
            </Button>

            <Button className="btn next-step" onClick={this.gotoPreviewTab}>
              Continue
            </Button>
          </div>
        </div>
      </Fragment>
    );
  };

  gotoPreviewTab = () => {
    this.setState({
      key: 4
    });
  };

  //preview tab
  renderPreviewTab = () => {
    const children = apiAndUploadImages.map((url, index) => (
      <div key={index} className="col-12" style={{ padding: 0 }}>
        <div id="result1" className="result-box" style={{ display: "block" }}>
          <Image
            id="result"
            src={url.ImageUrl === undefined ? url.dataURL : url.ImageUrl}
            key={index}
            alt="..."
            style={{ width: "100%", height: "500px" }}
          />
        </div>
      </div>
    ));

    const images = apiAndUploadImages.map((url, index) => (
      <div className="thumbCards">
        <Image
          key={index}
          src={url.ImageUrl === undefined ? url.dataURL : url.ImageUrl}
          className="thumbImg"
        />
      </div>
    ));

    const CustomDot = ({
      index,
      onClick,
      active,
      carouselState: { currentSlide }
    }) => {
      return (
        <button
          onClick={e => {
            onClick();
            e.preventDefault();
          }}
          className={classNames("custom-dot", {
            "custom-dot--active": active
          })}
        >
          {React.Children.toArray(images)[index]}
        </button>
      );
    };

    return (
      <div>
        {this.state.AdPostDetails.map((adpostdetail, index) => (
          <div key={index} className="row mt-3">
            <div className="col-lg-8">
              <div class="row">
                <div className="col-md-12" style={{ padding: 0 }}>
                  <Carousel
                    showDots
                    slidesToSlide={1}
                    containerClass="carousel-with-custom-dots"
                    responsive={responsive}
                    infinite
                    customDot={<CustomDot />}
                  >
                    {children}
                  </Carousel>
                </div>
              </div>

              <div
                className="col-12 col-lg-12 col-md-6 col-sm-6 mt-3 mb-3"
                style={{ padding: 0 }}
              >
                <ul className="ad-detail-icon d-flex" style={{ padding: 0 }}>
                  <li>
                    <span>YEAR</span>
                    <img
                      src="assets/media/icon/Calendar.png"
                      width={30}
                      height={30}
                      alt=""
                    />
                    <strong>{adpostdetail.VersionYear}</strong>
                  </li>
                  <li>
                    <span>KILOMETERS</span>
                    <img
                      src="assets/media/icon/Km.png"
                      width={30}
                      height={30}
                      alt=""
                    />
                    <strong>{this.state.smeterreading}</strong>
                  </li>
                  <li>
                    <span>COLOR</span>
                    <img
                      src="assets/media/icon/Color.png"
                      width={30}
                      height={30}
                      alt=""
                    />
                    <strong>{this.state.secolorname}</strong>
                  </li>

                  <li>
                    <span>TRANSMISSION</span>
                    <img
                      src="assets/media/icon/Automatic.png"
                      width={30}
                      height={30}
                      alt=""
                    />
                    <strong>{adpostdetail.TransmissionName}</strong>
                  </li>
                  <li>
                    <span>BODY TYPE</span>
                    <img
                      src="assets/media/icon/BodyCondition.png"
                      width={30}
                      height={30}
                      alt=""
                    />
                    <strong>{adpostdetail.BodyTypeName}</strong>
                  </li>
                </ul>
              </div>
              <br />
              <h5>Seller Notes</h5>
              <div className="row mt-3">
                <div className="col-md-12">
                  <p>{this.state.sdescription}</p>
                </div>
              </div>
              <br />
              <h5>Basic Details</h5>
              <div className="row mt-3">
                <div className="col-md-6">
                  <dl className="b-goods-f__descr row">
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      VERSION YEAR
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.VersionYear}
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      BRAND
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.MakeName}
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      MODEL
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.ModelName}
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      BODY STYLE
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.BodyTypeName}
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      EXTERIOR COLOR
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {this.state.secolorname}
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      ENGINE #
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.EngineNumber}
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      CHASSIS #
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.ChasisNumber}
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                  </dl>
                </div>
                <div className="col-md-6">
                  <dl className="b-goods-f__descr row">
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      VEHICLE TYPE
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.VehicleTypeName}
                    </dt>
                    <hr className="col-12 mt-0 px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      METER READING
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {this.state.smeterreading}
                    </dt>
                    <hr className="col-12 mt-0 px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      ENGINE SIZE
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.EngineSize_Capacity_CC} cc
                    </dt>
                    <hr className="col-12 mt-0 px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      # OF CYLINDERS
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.Cylinders} No(s).
                    </dt>
                    <hr className="col-12 mt-0 px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      DRIVE SIDE
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.DriveSide === "RHD"
                        ? "Right Hand Side"
                        : "Left Hand Side"}
                    </dt>
                    <hr className="col-12 mt-0 px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      ASSEMBLY
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.Assembly === "loc" ? "Local" : "Imported"}
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      REGISTERED CITY
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.RegisteredCity}
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                  </dl>
                </div>
              </div>
              <br />
              <h5>Additional Info</h5>
              <div className="row mt-3">
                <div className="col-md-6">
                  <dl className="b-goods-f__descr row">
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      MILEAGE
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.Mileage} Km/Ltr
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      # OF DOORS
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.Doors} No(s).
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      # OF SEATS
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.NumberofSeats} Pers
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      # OF GEARS
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.NumberofGears} No(s).
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      # OF OWNERS
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.NumberofOwners}
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      DRIVE TYPE
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.DriveType}
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                  </dl>
                </div>
                <div className="col-md-6">
                  <dl className="b-goods-f__descr row">
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      ENGINE POWER
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.EnginePower} bhp
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      ENGINE TYPE
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.EngineType}
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      FUEL TANK SIZE
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.FuelTankCapacity} Ltr.
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      INTERIOR COLOR
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {this.state.sicolorname}
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      BODY CONDITION
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.BodyCondition} out of 10
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                    <dd
                      className="b-goods-f__descr-new col-lg-6 col-md-12"
                      style={{
                        fontSize: "13px",
                        color: "#222222"
                      }}
                    >
                      TRANSMISSION
                    </dd>
                    <dt
                      className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                      style={{ textAlign: "right" }}
                    >
                      {adpostdetail.TransmissionName}
                    </dt>
                    <hr className="col-12 mt-0  px-0" />
                  </dl>
                </div>
              </div>
              <br />
              <h5>Features</h5>
              <div className="row">
                <div className="col-12 mt-3">
                  <div className="row px-0">
                    {this.state.sattributename.map((attname, index) => (
                      <div className="col-6 px-0" key={index}>
                        <dl className="b-goods-f__descr mb-0">
                          <dd
                            className="b-goods-f__descr-title"
                            style={{ fontWeight: 400 }}
                          >
                            {attname}{" "}
                          </dd>
                        </dl>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4 px-0">
              {" "}
              <div
                className
                style={{
                  backgroundColor: "#D3E428",
                  textAlign: "center"
                }}
              >
                <div className="b-seller__title py-3">
                  <div className="b-seller__price">
                    {adpostdetail.CurCode} {adpostdetail.Price}
                  </div>
                  <span
                    style={{
                      color: "white",
                      fontSize: "11px",
                      textAlign: "center"
                    }}
                  >
                    prices are exclusive of VAT
                  </span>
                </div>
              </div>
              <div>
                <div
                  className="b-goods-f__sidebar px-4 py-3"
                  style={{ backgroundColor: "#e0e0e0" }}
                >
                  <div className="b-goods-f__price-group justify-content-center">
                    <button
                      onClick={() => this.handleShowPhone()}
                      type="button"
                      className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center justify-content-center"
                      style={{
                        borderRadius: "0px",
                        fontSize: "15px",
                        fontWeight: 600
                      }}
                    >
                      <i
                        style={{
                          position: "absolute",
                          left: "45px"
                        }}
                        className="fas fa-phone prefix"
                      />
                      <span>Show Phone Number</span>
                    </button>
                    <button
                      onClick={() => this.handleShowEmail()}
                      type="button"
                      className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center justify-content-center"
                      style={{
                        borderRadius: "0px",
                        fontSize: "15px",
                        fontWeight: 600
                      }}
                    >
                      {" "}
                      <i
                        style={{
                          position: "absolute",
                          left: "45px"
                        }}
                        className="fas fa-envelope prefix "
                      />
                      <span>Show Email</span>
                    </button>
                    <button
                      type="button"
                      className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center justify-content-center"
                      style={{
                        borderRadius: "0px",
                        fontSize: "15px",
                        fontWeight: 600
                      }}
                    >
                      {" "}
                      <i
                        style={{
                          position: "absolute",
                          left: "45px"
                        }}
                        className="far fa-comment-dots prefix"
                      />
                      <span>Chat with seller</span>
                    </button>
                  </div>
                  <span className="b-goods-f__compare"></span>
                </div>
                <div
                  className="b-goods-f__sidebar px-4 py-2"
                  style={{
                    backgroundColor: "#e0e0e0",
                    borderTop: "1px solid #cecece"
                  }}
                >
                  <div className="b-goods-f__price-group justify-content-center">
                    <button
                      type="button"
                      className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center justify-content-center"
                      style={{
                        borderRadius: "0px",
                        fontSize: "15px",
                        fontWeight: 600
                      }}
                    >
                      {" "}
                      <span>Installment Calculator</span>
                    </button>
                    <button
                      type="button"
                      className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center justify-content-center"
                      style={{
                        borderRadius: "0px",
                        fontSize: "15px",
                        fontWeight: 600
                      }}
                    >
                      {" "}
                      <span>Apply for Finance</span>
                    </button>
                  </div>
                  <span className="b-goods-f__compare"></span>
                </div>
                <div
                  className="b-goods-f__sidebar px-4 py-2"
                  style={{
                    backgroundColor: "#e0e0e0",
                    borderTop: "1px solid #cecece"
                  }}
                >
                  <div className="bg-detail-seller">
                    <div
                      className="b-seller__detail pt-3 px-0 py-2"
                      style={{ backgroundColor: "transparent" }}
                    >
                      <div className="b-seller__img">
                        <img
                          className="img-scale"
                          src={this.state.ImageUrl}
                          alt="foto"
                        />
                      </div>
                      <div className="b-seller__title">
                        <div
                          className="b-seller__name"
                          style={{
                            fontWeight: 600,
                            fontSize: "16px"
                          }}
                        >
                          {this.state.Firstname} {this.state.Lastname}
                        </div>
                        <div className="b-seller__category">
                          Member Since {this.state.CreatedDate}
                        </div>
                      </div>
                      <hr />
                      <div
                        className="b-seller__detail row d-flex justify-content-center px-0"
                        style={{ backgroundColor: "transparent" }}
                      >
                        <div className="mx-2">
                          <i
                            style={{
                              color: "#D3E428",
                              fontSize: "30px",
                              paddingRight: "10px",
                              transform: "translateY(5px)"
                            }}
                            className="fas fa-phone"
                          />
                          <span style={{ fontSize: "12px" }}>
                            {" "}
                            Phone verified
                          </span>
                        </div>
                        <div className="mx-2">
                          <i
                            style={{
                              color: "#D3E428",
                              fontSize: "30px",
                              paddingRight: "10px",
                              transform: "translateY(5px)"
                            }}
                            className="fas fa-envelope"
                          />
                          <span style={{ fontSize: "12px" }}>
                            {" "}
                            E-mail verified
                          </span>
                        </div>
                      </div>
                    </div>
                    <span
                      className="b-goods-f__compare"
                      style={{ padding: "0px" }}
                    ></span>
                  </div>
                </div>
                <div
                  className="my-3 pb-2 d-flex justify-content-center"
                  style={{ borderBottom: "1px solid #cecece" }}
                >
                  <div>
                    <a style={{ color: "grey" }} href="#">
                      <span style={{ fontSize: "1.1em" }} className="mr-3 pb-2">
                        <i className="far fa-envelope mr-1" /> Email this
                        listing
                      </span>
                    </a>
                  </div>
                </div>
                <div className="d-flex justify-content-end">
                  <a style={{ color: "grey" }} href="#">
                    <span style={{ fontSize: "0.9em" }} className="mr-3 pb-2">
                      <i className="fas fa-flag mr-1" /> Report this Ad
                    </span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        ))}
        <div className="mt-5">
          <Button
            className="btn previous-step"
            onClick={this.gotoUploadTab}
            style={{ marginRight: "2%" }}
          >
            Previous
          </Button>

          <Button className="btn next-step" onClick={this.handleEditAdPost}>
            Edit AdPost
          </Button>
        </div>
      </div>
    );
  };

  handleTabSelect = key => {
    if (key === "2" || key === "3" || key === "4") {
      if (
        this.state.smeterreading &&
        this.state.sicolor &&
        this.state.secolor &&
        this.state.sdescription
      ) {
        this.setState({
          key
        });
      }
    } else {
      this.setState({
        key
      });
    }
  };

  //EditAdPost functionality
  handleEditAdPost = () => {
    const url = `${apiBaseURL}/adpost/UpdateAdPostbyAdPostID`;
    let ApiParamForEditAdPost = {
      AdPostID: this.state.AdPostID,
      ExtColorID: this.state.secolor,
      IntColorID: this.state.sicolor,
      MeterReading: this.state.smeterreading,
      Area: this.state.sarea,
      Comments: this.state.sdescription
    };
    axios
      .post(url, null, { params: ApiParamForEditAdPost })
      .then(res => {
        console.log(res.data, "UpdateAdPostbyAdPostID");

        if (res.status === 200) {
          this.setState({
            UpdateAdPostData: res.data
          });
          this.deleteAdPostAttribute();
        } else {
          console.log(res.status, res.statusText, "UpdateAdPostbyAdPostID");
        }
      })
      .catch(error => {
        console.log(error, "UpdateAdPostbyAdPostID");
      });
  };

  deleteAdPostAttribute = () => {
    const urlDeleteAdPostAttribute = `${apiBaseURL}/AdPostAttribute/DeleteAdPostAttributebyAdPostID?AdPostID=${this.state.AdPostID}`;
    axios
      .post(urlDeleteAdPostAttribute)
      .then(res => {
        console.log(res.data, "DeleteAdPostAttributebyAdPostID");

        if (res.status === 200) {
          this.setState({
            DeleteAdPostAttributeData: res.data
          });
          this.handleAdPostAttributeApi();
        } else {
          console.log(
            res.status,
            res.statusText,
            "DeleteAdPostAttributebyAdPostID"
          );
        }
      })
      .catch(error => {
        console.log(error, "DeleteAdPostAttributebyAdPostID");
      });
  };

  handleAdPostAttributeApi = () => {
    this.state.sattribute.map((item, index) => {
      const url = `${apiBaseURL}/AdPostAttribute`;
      let ApiParamForAdPostAttribute = {
        AdPostID: this.state.AdPostID,
        DraftAdPostID: 0,
        AttributeID: item,
        AttributeValue: "true"
      };
      axios
        .post(url, ApiParamForAdPostAttribute)
        .then(res => {
          console.log(res, "res from AdPostAttribute api");

          if (res.status === 200) {
            let attributeLength = this.state.sattribute.length - 1;
            if (attributeLength === index) {
              this.deleteAdImagebyAdPostID();
            }
            console.log(
              res.status,
              res.statusText,
              "statuscode from AdPostAttribute api"
            );
          } else {
            console.log(
              res.status,
              res.statusText,
              "statuscode from AdPostAttribute api"
            );
          }
        })
        .catch(error => {
          console.log(error, "from AdPostAttribute api");
        });
    });
  };

  //no
  deleteAdImagebyAdPostID = () => {
    const urlDeleteAdImagebyAdPostID = `${apiBaseURL}/AdImage/DeleteAdImagebyAdPostID`;
    let ApiParamForDeleteAdImagebyAdPostID = {
      AdPostID: this.state.AdPostID
    };
    axios
      .post(urlDeleteAdImagebyAdPostID, null, {
        params: ApiParamForDeleteAdImagebyAdPostID
      })
      .then(res => {
        console.log(res.data, "DeleteAdImagebyAdPostID");

        if (res.status === 200) {
          this.handleAdImageApi();
        } else {
          console.log(res.status, res.statusText, "DeleteAdImagebyAdPostID");
        }
      })
      .catch(error => {
        console.log(error, "DeleteAdImagebyAdPostID");
      });
  };

  handleAdImageApi = () => {
    apiAndUploadImages.map(item => {
      var getImageName = "";
      if (item.Url !== undefined) {
        let splitItemBySlash = item.Url.split("/");
        getImageName = splitItemBySlash[3];
      } else {
        getImageName = item.file.name;
      }

      const url = `${apiBaseURL}/AdImage`;
      let ApiParamForAdImage = {
        AdPostID: this.state.AdPostID,
        ImageUrl: `Content/Uploads/${this.state.AdPostID}/${getImageName}`,
        CreatedBy: this.state.userId,
        DraftAdPostID: "0"
      };
      axios
        .post(url, ApiParamForAdImage)
        .then(res => {
          console.log(res, "res from AdImage api");

          if (res.status === 200) {
            console.log(res.status, "adimage");
          } else {
            console.log(
              res.status,
              res.statusText,
              "statuscode from AdImage api"
            );
          }
        })
        .catch(error => {
          console.log(error, "from AdImage api");
        });
    });

    this.handleUploadAdPostImageApi();
  };

  handleUploadAdPostImageApi = () => {
    apiAndUploadImages.map((item, index) => {
      var getIUrlORFileObj = "";
      if (item.Url !== undefined) {
        getIUrlORFileObj = item.Url;
      } else {
        getIUrlORFileObj = item.file;
      }
      // Create an object of formData
      let data = new FormData();

      //Update the formData object
      data.append("AdpostID", this.state.AdPostID);
      data.append("ImageUrl", getIUrlORFileObj);

      const url = `${apiBaseURL}/file/UploadAdPostImage`;

      axios
        .post(url, data)
        .then(res => {
          console.log(res, "res from UploadAdPostImage api");

          if (res.status === 200) {
            let itemLength = this.state.imageUpload - 1;
            if (index === itemLength) {
              this.setState({
                showModalSuccess: true
              });
            }
            console.log(res.status, "UploadAdPostImage");
          } else {
            console.log(res.status, "statuscode from UploadAdPostImage api");
          }
        })
        .catch(error => {
          console.log(error, "from UploadAdPostImage api");
        });
    });
  };

  render() {
    return (
      <div>
        <div style={{ paddingBottom: "50px" }}>
          {/* modal success */}
          {this.modalSuccess()}
          {/* modal phone */}
          {this.modalPhone()}
          {/* modal email */}
          {this.modalEmail()}
          <div className="container">
            <div className="row">
              <div className="col-12">
                <div className="b-find">
                  <Tabs
                    onSelect={this.handleTabSelect}
                    className="b-find-nav nav nav-tabs justify-content-between"
                    activeKey={this.state.key}
                    id="uncontrolled-tab-example"
                  >
                    <Tab eventKey={1} title="BASIC INFO" className="tab">
                      {this.renderBasicInfoTab()}
                    </Tab>

                    <Tab eventKey={2} title="ADDITIONAL INFO">
                      {this.renderAdditionalInfoTab()}
                    </Tab>

                    <Tab eventKey={3} title="UPLOAD IMAGES">
                      {this.renderUploadTab()}
                    </Tab>

                    <Tab eventKey={4} title="PREVIEW">
                      {this.renderPreviewTab()}
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}