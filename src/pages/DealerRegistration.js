import React, { Component, Fragment } from "react";
import { Tabs, Tab, Button, Alert } from "react-bootstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { apiBaseURL, apiUrlcity, apiUrlmake } from "../ApiBaseURL";
import Slider from "rc-slider";
import "rc-slider/assets/index.css";
import { Multiselect } from "multiselect-react-dropdown";
import axios from "axios";
import moment from "moment";
import ImageUploading from "react-images-uploading";

class DealerRegistration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //personalprofile
      firstname: "",
      lastname: "",
      contactno: "",
      mobileno: "",
      personalcnic: "",
      Gender: [
        { id: 1, value: "Male", icon: "fas fa-male fa-2x" },
        { id: 2, value: "Female", icon: "fas fa-female fa-2x" }
      ],
      selGender: "Male",
      firstStartDate: null,
      startDate: null,

      //companyprofile
      firstregistrationyear: null,
      registrationyear: null,
      dealername: "",
      dealeremail: "",
      dealerno: "",
      dealerwebsite: "",
      companydescription: "",
      employeesno: "",
      tradelicence: "",
      ntn: "",
      uan: "",
      firstaddress: "",
      secondaddress: "",
      city: "",
      cityname: "",
      lcity: [],
      area: "",
      branchno: 0,
      directorno: 0,

      DirectorsName: "",
      directorname: "",
      secdirectorname: "",
      thirddirectorname: "",

      DirectorsCNIC: "",
      directorcnic: "",
      secdirectorcnic: "",
      thirddirectorcnic: "",

      selDealership: false,
      DealershipList: [],
      DealershipOf: [],
      NoOfEmployees: [
        { id: 1, value: "1 to 5" },
        { id: 2, value: "5 to 10" },
        { id: 3, value: "10 to 15" },
        { id: 4, value: "15 to 25" },
        { id: 5, value: "25 to 50" },
        { id: 6, value: "50 +" }
      ],
      DealerShip: [
        {
          id: 1,
          value: "Authorized",
          icon: "fas fa-award fa-2x px-2 py-2",
          bool: true
        },
        {
          id: 2,
          value: "UnAuthorized",
          icon: "fas fa-building fa-2x px-2 py-2",
          bool: false
        }
      ],
      MakeData: [],
      isCheckedVehicleServices: false,
      isCheckedVehicleRepair: false,
      isCheckedVehicleParts: false,
      isCheckedVehicleWash: false,
      isCheckedVehicleInspection: false,

      Days: [
        {
          id: 1,
          value: "Monday",
          isChecked: false,
          from: "00:00 AM",
          to: "00:00 PM"
        },
        {
          id: 2,
          value: "Tuesday",
          isChecked: false,
          from: "00:00 AM",
          to: "00:00 PM"
        },
        {
          id: 3,
          value: "Wednesday",
          isChecked: false,
          from: "00:00 AM",
          to: "00:00 PM"
        },
        {
          id: 4,
          value: "Thursday",
          isChecked: false,
          from: "00:00 AM",
          to: "00:00 PM"
        },
        {
          id: 5,
          value: "Friday",
          isChecked: false,
          from: "00:00 AM",
          to: "00:00 PM"
        },
        {
          id: 6,
          value: "Saturday",
          isChecked: false,
          from: "00:00 AM",
          to: "00:00 PM"
        },
        {
          id: 7,
          value: "Sunday",
          isChecked: false,
          from: "00:00 AM",
          to: "00:00 PM"
        }
      ],
      ConcatDays: [],
      ConcatHours: [],
      //upload tab
      imageList: [],
      //Guidlines
      Guidlines: [
        {
          id: 1,
          value: "LOGO"
        }
      ],
      //tab key
      key: 1
    };
  }
  componentDidMount() {
    window.scrollTo(0, 0);

    //personal profile

    //company profile
    fetch(apiUrlcity)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lcity: result
            });
          } else {
            console.log("city api");
          }
        },
        error => {
          this.setState({ error });
        }
      );

    fetch(apiUrlmake)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              MakeData: result
            });
          } else {
            console.log("MakeData");
          }
        },
        error => {
          console.log(error, "MakeData");
        }
      );
  }

  handleSelectedDay = event => {
    let days = this.state.Days;
    days.forEach(day => {
      if (day.value === event.target.value)
        day.isChecked = event.target.checked;
    });
    this.setState({ Days: days });
  };

  //renderPersonalProfileTab
  handleFirstNameChange = event => {
    this.setState({ firstname: event.target.value });
  };

  handleLastNameChange = event => {
    this.setState({ lastname: event.target.value });
  };

  handleContactNumberChange = event => {
    this.setState({ contactno: event.target.value });
  };

  handleMobileNumberChange = event => {
    this.setState({ mobileno: event.target.value });
  };

  handleCnicChange = event => {
    this.setState({ personalcnic: event.target.value });
  };

  handleGenderChange = gender => {
    this.setState({
      selGender: gender
    });
  };

  handleDOBChange = date => {
    let newDOBdate = this.dateMaker(date);
    this.setState({
      firstStartDate: newDOBdate,
      startDate: date
    });
  };

  dateMaker = date => {
    let dateInString = date.toString();
    let dateInSplit = dateInString.split("GMT");
    let day = ("0" + date.getDate()).slice(-2);
    let monthIndex = ("0" + (date.getMonth() + 1)).slice(-2);
    let year = date.getFullYear();
    let seconds = (date.getSeconds() < 10 ? "0" : "") + date.getSeconds();
    let minutes = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
    let hour = ("0" + date.getHours()).slice(-2);
    let newDate = year + "-" + monthIndex + "-" + day;
    return newDate;
  };

  renderPersonalProfileTab = () => {
    return (
      <div style={{ maxWidth: "750px", margin: "0 auto" }}>
        <span
          className="logmod__heading-subtitlee"
          style={{ color: "#D3E428" }}
        >
          Dealer Registration
        </span>

        <div className="logmod__form">
          <form className="simform" onSubmit={this.gotoCompanyTab}>
            <div className="sminputs">
              <div
                className="full"
                style={{
                  textTransform: "uppercase",
                  letterSpacing: 1,
                  fontWeight: 700,
                  fontSize: "12px",
                  lineHeight: "24px",
                  padding: "0 24px"
                }}
              >
                <label className="string optional">Select Gender *</label>
                <div className="row">
                  <div className="col-6" style={{ padding: 0 }}>
                    {this.state.Gender.map((gender, index) => (
                      <label
                        key={index}
                        className="material-icons"
                        style={{ margin: "3% 5% 2% 0" }}
                      >
                        <input
                          value={gender.value}
                          checked={this.state.selGender === gender.value}
                          onChange={() => this.handleGenderChange(gender.value)}
                          type="radio"
                        />

                        <span
                          className="px-5 rounded-0"
                          style={{ padding: "20px 18px 10px 18px" }}
                        >
                          <i className={gender.icon}></i>
                        </span>
                      </label>
                    ))}
                  </div>
                </div>
              </div>
            </div>

            <div className="sminputs">
              <div className="input string optional">
                <label className="string optional">First Name *</label>
                <input
                  className="string optional"
                  required
                  onChange={this.handleFirstNameChange}
                  value={this.state.firstname}
                  type="text"
                  placeholder="Enter First Name"
                />
              </div>
              <div className="input string optional">
                <label className="string optional">Last Name *</label>
                <input
                  required
                  onChange={this.handleLastNameChange}
                  value={this.state.lastname}
                  type="text"
                  className="string optional"
                  placeholder="Enter Last Name"
                />
              </div>
            </div>

            <div className="sminputs">
              <div className="input full">
                <label className="string optional">Contact Number *</label>

                <input
                  required
                  onChange={this.handleContactNumberChange}
                  value={this.state.contactno}
                  type="number"
                  className="string optional"
                  placeholder="Enter Contact Number"
                />
              </div>
            </div>

            <div class="sminputs">
              <div class="input full">
                <label class="string optional">Mobile Number *</label>

                <input
                  required
                  onChange={this.handleMobileNumberChange}
                  value={this.state.mobileno}
                  type="number"
                  className="string optional"
                  placeholder="Enter Mobile Number"
                />
              </div>
            </div>

            <div class="sminputs">
              <div class="input full">
                <label class="string optional">CNIC Number *</label>

                <input
                  required
                  onChange={this.handleCnicChange}
                  value={this.state.personalcnic}
                  type="text"
                  className="string optional"
                  placeholder="Enter CNIC Number"
                />
              </div>
            </div>

            <div class="sminputs">
              <div class="input full">
                <label class="string optional">Date of Birth *</label>

                <DatePicker
                  className="string optional"
                  maxDate={new Date()}
                  showDisabledMonthNavigation
                  value={this.state.firstStartDate}
                  selected={this.state.startDate}
                  onChange={this.handleDOBChange}
                  peekNextMonth
                  showMonthDropdown
                  showYearDropdown
                  dropdownMode="select"
                  placeholderText="Enter Date of Birth"
                />
              </div>
            </div>

            <div className="mt-4">
              <input
                className="btn next-step"
                type="submit"
                value="Continue"
                style={{
                  position: "absolute",
                  zIndex: 0,
                  right: 0,
                  padding: "10px 80px",
                  backgroundColor: "#D3E428",
                  color: "white"
                }}
              />
            </div>
          </form>
        </div>
      </div>
    );
  };

  gotoCompanyTab = event => {
    event.preventDefault();
    this.setState({
      key: 2
    });
  };

  //renderCompanyProfileTab
  handleRegistrationYearChange = date => {
    let newRegdate = this.dateMaker(date);
    this.setState({
      firstregistrationyear: newRegdate,
      registrationyear: date
    });
  };

  handleCityChange = event => {
    if (event.target.value === "") {
      return;
    }

    this.setState({
      city: event.target.value
    });

    let filteredArray = this.state.lcity.filter(
      item => event.target.value == item.CityID
    );
    this.setState({ cityname: filteredArray[0].CityName });
  };

  handleDealerNameChange = event => {
    this.setState({ dealername: event.target.value });
  };

  handleDealerEmailChange = event => {
    this.setState({ dealeremail: event.target.value });
  };

  handleDealerNoChange = event => {
    this.setState({ dealerno: event.target.value });
  };

  handleDealerWebsiteChange = event => {
    this.setState({ dealerwebsite: event.target.value });
  };

  handleCompanyDescriptionChange = event => {
    this.setState({ companydescription: event.target.value });
  };

  handleNoOfEmployeesChange = event => {
    this.setState({ employeesno: event.target.value });
  };

  handleTradeLicenceChange = event => {
    this.setState({ tradelicence: event.target.value });
  };

  handleNTNChange = event => {
    this.setState({ ntn: event.target.value });
  };

  handleUANChange = event => {
    this.setState({ uan: event.target.value });
  };

  handleFirstAddressChange = event => {
    this.setState({ firstaddress: event.target.value });
  };

  handleSecondAddressChange = event => {
    this.setState({ secondaddress: event.target.value });
  };

  handleAreaChange = event => {
    this.setState({ area: event.target.value });
  };

  onBranchSliderChange = value => {
    this.setState({ branchno: value });
  };

  onDirectorSliderChange = value => {
    this.setState({ directorno: value });
  };

  handleDirectorNameChange = event => {
    this.setState({ directorname: event.target.value });
  };
  handleSecDirectorNameChange = event => {
    this.setState({ secdirectorname: event.target.value });
  };
  handleThirdDirectorNameChange = event => {
    this.setState({ thirddirectorname: event.target.value });
  };

  handleDirectorCNICChange = event => {
    this.setState({ directorcnic: event.target.value });
  };
  handleSecDirectorCNICChange = event => {
    this.setState({ secdirectorcnic: event.target.value });
  };
  handleThirdDirectorCNICChange = event => {
    this.setState({ thirddirectorcnic: event.target.value });
  };

  handleDealershipChange = dealership => {
    this.setState({
      selDealership: dealership
    });
  };

  onSelectDealerships = (selectedList, selectedItem) => {
    this.setState({
      DealershipList: selectedList
    });
  };

  toggleVehicleServices = () => {
    this.setState({
      isCheckedVehicleServices: !this.state.isCheckedVehicleServices
    });
  };
  toggleVehicleRepair = () => {
    this.setState({
      isCheckedVehicleRepair: !this.state.isCheckedVehicleRepair
    });
  };
  toggleVehicleParts = () => {
    this.setState({
      isCheckedVehicleParts: !this.state.isCheckedVehicleParts
    });
  };
  toggleVehicleWash = () => {
    this.setState({
      isCheckedVehicleWash: !this.state.isCheckedVehicleWash
    });
  };
  toggleVehicleInspection = () => {
    this.setState({
      isCheckedVehicleInspection: !this.state.isCheckedVehicleInspection
    });
  };

  handleFromTime = (event, id) => {
    let fromtime = moment(event).format("hh:mm A");

    let days = this.state.Days;
    days.forEach(day => {
      if (day.id === id) day.from = fromtime;
    });
    this.setState({ Days: days });
  };

  handleToTime = (event, id) => {
    let totime = moment(event).format("hh:mm A");

    let days = this.state.Days;
    days.forEach(day => {
      if (day.id === id) day.to = totime;
    });
    this.setState({ Days: days });
  };

  renderCompanyProfileTab = () => {
    return (
      <div style={{ maxWidth: "750px", margin: "0 auto" }}>
        <span
          className="logmod__heading-subtitlee"
          style={{ color: "#D3E428" }}
        >
          Dealer Registration
        </span>

        <div className="logmod__form">
          <form className="simform" onSubmit={this.gotoUploadTab}>
            <div className="sminputs">
              <div className="input full">
                <label className="string optional">Dealer Name *</label>

                <input
                  required
                  onChange={this.handleDealerNameChange}
                  value={this.state.dealername}
                  type="text"
                  className="string optional"
                  placeholder="Enter Dealer Number"
                />
              </div>
            </div>

            <div className="sminputs">
              <div
                className="full"
                style={{
                  textTransform: "uppercase",
                  letterSpacing: 1,
                  fontWeight: 700,
                  fontSize: "12px",
                  lineHeight: "24px",
                  padding: "0 24px"
                }}
              >
                <label className="string optional">Select Dealer Type *</label>
                <div className="row">
                  <div
                    className="col-6"
                    style={{ padding: 0, display: "contents" }}
                  >
                    {this.state.DealerShip.map((dealership, index) => (
                      <label
                        key={index}
                        className="material-icons"
                        style={{ margin: "3% 5% 2% 0" }}
                      >
                        <input
                          value={dealership.bool}
                          checked={this.state.selDealership === dealership.bool}
                          onChange={() =>
                            this.handleDealershipChange(dealership.bool)
                          }
                          type="radio"
                        />

                        <span
                          className="px-5 rounded-0"
                          style={{ padding: "20px 18px 10px 18px" }}
                        >
                          <i className={dealership.icon}></i> {dealership.value}
                        </span>
                      </label>
                    ))}
                  </div>
                </div>
              </div>
            </div>

            {this.state.selDealership === true ? (
              <div className="sminputs">
                <div
                  className="full"
                  style={{
                    letterSpacing: 1,
                    padding: "0 24px"
                  }}
                >
                  <label
                    className="string optional"
                    style={{
                      textTransform: "uppercase",
                      fontWeight: 700,
                      fontSize: "12px",
                      lineHeight: "24px"
                    }}
                  >
                    Authorized Dealership
                  </label>
                  <Multiselect
                    className="string optional"
                    required
                    options={this.state.MakeData}
                    onSelect={this.onSelectDealerships}
                    onRemove={this.onRemove}
                    displayValue="MakeName"
                    placeholder="Select Dealership"
                    emptyRecordMsg="No Data Found"
                    style={{ chips: { background: "#D3E428" } }}
                  />
                </div>
              </div>
            ) : (
                ""
              )}

            <div className="sminputs">
              <div className="input full">
                <label className="string optional">Dealer Website *</label>
                <input
                  className="string optional"
                  required
                  onChange={this.handleDealerWebsiteChange}
                  value={this.state.dealerwebsite}
                  type="url"
                  placeholder="Enter Website"
                />
              </div>
            </div>

            <div className="sminputs">
              <div className="input full">
                <label className="string optional">Dealer Email *</label>
                <input
                  className="string optional"
                  required
                  onChange={this.handleDealerEmailChange}
                  value={this.state.dealeremail}
                  type="email"
                  placeholder="Enter Official Email"
                />
              </div>
            </div>

            <div className="sminputs">
              <div className="input full">
                <label className="string optional">Office Number *</label>
                <input
                  className="string optional"
                  required
                  onChange={this.handleDealerNoChange}
                  value={this.state.dealerno}
                  type="number"
                  placeholder="Enter Official Number"
                />
              </div>
            </div>

            <div className="sminputs">
              <div className="input full">
                <label className="string optional">Address Line 1 *</label>
                <input
                  className="string optional"
                  required
                  onChange={this.handleFirstAddressChange}
                  value={this.state.firstaddress}
                  type="text"
                  placeholder="Enter Address Line 1"
                />
              </div>
            </div>

            <div className="sminputs">
              <div className="input full">
                <label className="string optional">Address Line 2 *</label>
                <input
                  className="string optional"
                  required
                  onChange={this.handleSecondAddressChange}
                  value={this.state.secondaddress}
                  type="text"
                  placeholder="Enter Address Line 2"
                />
              </div>
            </div>

            <div className="sminputs">
              <div className="input full">
                <label className="string optional">Area *</label>
                <input
                  className="string optional"
                  required
                  onChange={this.handleAreaChange}
                  value={this.state.area}
                  type="text"
                  placeholder="Enter Area"
                />
              </div>
            </div>

            <div className="sminputs">
              <div
                className="full"
                style={{
                  letterSpacing: 1,
                  padding: "0 24px"
                }}
              >
                <label
                  className="string optional"
                  style={{
                    textTransform: "uppercase",
                    fontWeight: 700,
                    fontSize: "12px",
                    lineHeight: "24px"
                  }}
                >
                  Select City *
                </label>
                <select
                  required
                  onChange={this.handleCityChange}
                  className="custom-select-adpost"
                  style={{
                    border: "none",
                    color: "rgba(75, 89, 102, 0.85)",
                    padding: 0
                  }}
                  value={this.state.city}
                >
                  <option value="" selected>
                    Select City
                  </option>
                  {this.state.lcity.map(city => (
                    <option key={city.CityID} value={city.CityID}>
                      {city.CityName}
                    </option>
                  ))}
                </select>
              </div>
            </div>

            <div className="sminputs">
              <div className="input full">
                <label className="string optional">Company Description *</label>
                <input
                  required
                  onChange={this.handleCompanyDescriptionChange}
                  value={this.state.companydescription}
                  cols={30}
                  rows={5}
                  className="string optional"
                  placeholder="Enter Company Description"
                />
              </div>
            </div>

            <div className="sminputs">
              <div className="input full">
                <label className="string optional">Registration Year *</label>
                <DatePicker
                  className="string optional"
                  maxDate={new Date()}
                  showDisabledMonthNavigation
                  value={this.state.firstregistrationyear}
                  selected={this.state.registrationyear}
                  onChange={this.handleRegistrationYearChange}
                  peekNextMonth
                  showMonthDropdown
                  showYearDropdown
                  dropdownMode="select"
                  placeholderText="Enter Registration Year"
                />
              </div>
            </div>

            <div className="sminputs">
              <div
                className="full"
                style={{
                  letterSpacing: 1,
                  padding: "0 24px"
                }}
              >
                <label
                  className="string optional"
                  style={{
                    textTransform: "uppercase",
                    fontWeight: 700,
                    fontSize: "12px",
                    lineHeight: "24px"
                  }}
                >
                  Select No of Employees *
                </label>
                <select
                  required
                  onChange={this.handleNoOfEmployeesChange}
                  className="custom-select-adpost"
                  style={{
                    border: "none",
                    color: "rgba(75, 89, 102, 0.85)",
                    padding: 0
                  }}
                  value={this.state.employeesno}
                >
                  <option value="" selected>
                    Select Number Of Employee
                  </option>
                  {this.state.NoOfEmployees.map(employee => (
                    <option key={employee.id} value={employee.value}>
                      {employee.value}
                    </option>
                  ))}
                </select>
              </div>
            </div>

            <div className="sminputs">
              <div className="input string optional">
                <label className="string optional">Trade Licence *</label>
                <input
                  className="string optional"
                  required
                  onChange={this.handleTradeLicenceChange}
                  value={this.state.tradelicence}
                  type="text"
                  placeholder="Enter Trade Licence Number"
                />
              </div>
              <div className="input string optional">
                <label className="string optional">NTN Number *</label>
                <input
                  required
                  onChange={this.handleNTNChange}
                  value={this.state.ntn}
                  type="text"
                  placeholder="Enter NTN Number"
                  className="string optional"
                />
              </div>
            </div>

            <div className="sminputs">
              <div className="input full">
                <label className="string optional">UAN Number *</label>
                <input
                  required
                  onChange={this.handleUANChange}
                  value={this.state.uan}
                  type="text"
                  className="string optional"
                  placeholder="Enter UAN Number"
                />
              </div>
            </div>

            <div className="sminputs" style={{ paddingBottom: "20px" }}>
              <div className="input string optional">
                <label className="string optional">No of Branches *</label>
                <Slider min={1} max={10} onChange={this.onBranchSliderChange} />
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between"
                  }}
                >
                  <span>Min 1</span>
                  <span>{this.state.branchno}</span>
                  <span>Max 10</span>
                </div>
              </div>
              <div className="input string optional">
                <label className="string optional">No of Directors *</label>
                <Slider
                  min={1}
                  max={50}
                  onChange={this.onDirectorSliderChange}
                />
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between"
                  }}
                >
                  <span>Min 1</span>
                  <span>{this.state.directorno}</span>
                  <span>Max 50</span>
                </div>
              </div>
            </div>

            <div className="sminputs">
              <div className="input string optional">
                <label className="string optional">Director Name *</label>
                <input
                  required
                  onChange={this.handleDirectorNameChange}
                  value={this.state.directorname}
                  type="text"
                  className="string optional"
                  placeholder="Enter Director Name"
                />
              </div>
              <div className="input string optional">
                <label className="string optional">CNIC *</label>
                <input
                  required
                  onChange={this.handleDirectorCNICChange}
                  value={this.state.directorcnic}
                  type="text"
                  className="string optional"
                  placeholder="Enter Director CNIC"
                />
              </div>
            </div>

            <div className="sminputs">
              <div className="input string optional">
                <label className="string optional">Director Name</label>
                <input
                  onChange={this.handleSecDirectorNameChange}
                  value={this.state.secdirectorname}
                  type="text"
                  className="string optional"
                  placeholder="Enter Director Name"
                />
              </div>
              <div className="input string optional">
                <label className="string optional">CNIC</label>
                <input
                  onChange={this.handleSecDirectorCNICChange}
                  value={this.state.secdirectorcnic}
                  type="text"
                  className="string optional"
                  placeholder="Enter Director CNIC"
                />
              </div>
            </div>

            <div className="sminputs">
              <div className="input string optional">
                <label className="string optional">Director Name</label>
                <input
                  onChange={this.handleThirdDirectorNameChange}
                  value={this.state.thirddirectorname}
                  type="text"
                  className="string optional"
                  placeholder="Enter Director Name"
                />
              </div>
              <div className="input string optional">
                <label className="string optional">CNIC</label>
                <input
                  onChange={this.handleThirdDirectorCNICChange}
                  value={this.state.thirddirectorcnic}
                  type="text"
                  className="string optional"
                  placeholder="Enter Director CNIC"
                />
              </div>
            </div>

            <div className="sminputs">
              <div className="input" style={{ height: "50px" }}>
                <label style={{ display: "inline" }}>Vehicle Services</label>
                <input
                  style={{ display: "inline", width: "50px" }}
                  onChange={this.toggleVehicleServices}
                  checked={this.state.isCheckedVehicleServices}
                  type="checkbox"
                />
              </div>
              <div className="input" style={{ height: "50px" }}>
                <label style={{ display: "inline" }}>Vehicle Parts</label>
                <input
                  style={{ display: "inline", width: "50px" }}
                  onChange={this.toggleVehicleParts}
                  checked={this.state.isCheckedVehicleParts}
                  type="checkbox"
                />
              </div>
              <div className="input" style={{ height: "50px" }}>
                <label style={{ display: "inline" }}>Vehicle Repair</label>
                <input
                  style={{ display: "inline", width: "50px" }}
                  onChange={this.toggleVehicleRepair}
                  checked={this.state.isCheckedVehicleRepair}
                  type="checkbox"
                />
              </div>

              <div className="input" style={{ height: "50px" }}>
                <label style={{ display: "inline" }}>Vehicle Wash</label>
                <input
                  style={{ display: "inline", width: "50px" }}
                  onChange={this.toggleVehicleWash}
                  checked={this.state.isCheckedVehicleWash}
                  type="checkbox"
                />
              </div>
              <div className="input" style={{ height: "50px" }}>
                <label style={{ display: "inline" }}>Vehicle Inspection</label>
                <input
                  style={{ display: "inline", width: "50px" }}
                  onChange={this.toggleVehicleInspection}
                  checked={this.state.isCheckedVehicleInspection}
                  type="checkbox"
                />
              </div>
            </div>

            <div className="sminputs">
              <div className="opinputs">
                <label className="oplabels">
                  Select your open days and hours *
                </label>

                <div style={{ width: "30%", float: "left" }}>
                  {this.state.Days.map(day => (
                    <div key={day.id} style={{ paddingBottom: "12px" }}>
                      <input
                        onClick={this.handleSelectedDay}
                        type="checkbox"
                        checked={day.isChecked}
                        value={day.value}
                      />
                      <span> {day.value}</span>
                    </div>
                  ))}
                </div>

                <div style={{ width: "70%", float: "left" }}>
                  {this.state.Days.map(day => (
                    <div key={day.id} style={{ paddingBottom: "10px" }}>
                      <div
                        style={{
                          float: "left",
                          paddingBottom: "10px"
                        }}
                      >
                        <span style={{ marginRight: 10 }}>From:</span>
                        <DatePicker
                          placeholderText={day.from}
                          id={day.id}
                          onChange={event => this.handleFromTime(event, day.id)}
                          showTimeSelect
                          showTimeSelectOnly
                          timeIntervals={15}
                          timeCaption="Time"
                          dateFormat="hh:mm A"
                        />
                      </div>

                      <div
                        style={{
                          float: "left",
                          paddingBottom: "10px"
                        }}
                      >
                        <span style={{ marginRight: 10 }}>To:</span>
                        <DatePicker
                          placeholderText={day.to}
                          id={day.id}
                          onChange={event => this.handleToTime(event, day.id)}
                          showTimeSelect
                          showTimeSelectOnly
                          timeIntervals={15}
                          timeCaption="Time"
                          dateFormat="hh:mm A"
                        />
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>

            <div className="mt-4">
              <Button
                style={{
                  zIndex: 0
                }}
                className="btn previous-step"
                onClick={this.gotoPersonalTab}
              >
                Previous
              </Button>

              <input
                className="btn next-step"
                type="submit"
                value="Continue"
                style={{
                  position: "absolute",
                  zIndex: 0,
                  right: 0,
                  padding: "10px 80px",
                  backgroundColor: "#D3E428",
                  color: "white"
                }}
              />
            </div>
          </form>
        </div>
      </div>
    );
  };

  gotoPersonalTab = () => {
    this.setState({
      key: 1
    });
  };

  gotoUploadTab = event => {
    event.preventDefault();
    this.setState({
      key: 3
    });
  };

  //upload tab
  onChange = imageList => {
    this.setState({
      imageList: imageList
    });
  };

  onRemoveImage = (imageList, e) => {
    imageList[e.target.value].onRemove();
  };

  onUploadImage = (imageList, onImageUpload) => {
    onImageUpload();
  };

  showUploadImages = imageList => {
    var imagesData = [];
    for (var i = 0; i < 1; i++) {
      if (i < imageList.length) {
        imagesData.push(
          <div key={i} className="image-container">
            <div className="col-2">
              <div className="ImgContainerStyle">
                <Button
                  className="close"
                  value={i}
                  onClick={event => this.onRemoveImage(imageList, event)}
                >
                  &times;
                </Button>
                <img src={imageList[i].dataURL} className="img-style" />
              </div>
            </div>
          </div>
        );
      } else {
        imagesData.push(
          <div key={i} className="image-container">
            <div className="col-2">
              <div className="ContainerStyle">
                <div className="child">{this.state.Guidlines[i].value}</div>
              </div>
            </div>
          </div>
        );
      }
    }
    return imagesData;
  };
  renderUploadTab = () => {
    return (
      <Fragment>
        <div className="tab-pane" id="content-images">
          <div className="mt-5">
            <div class="row">
              <div class="col-12 photos-remaining">
                <p>Upload Documents</p>
              </div>
            </div>
            <div className="col-12" style={{ padding: 0 }}>
              <form className="uploadContainer">
                <div className="mt-3 d-flex justify-content-center">
                  <ImageUploading
                    onChange={this.onChange}
                    multiple={false}
                    acceptType={["jpg", "png"]}
                    onError={this.onError}
                  >
                    {({ imageList, onImageUpload, errors }) => (
                      // write your building UI
                      <div style={{ width: "80%" }}>
                        <div>
                          {errors.acceptType && (
                            <Alert variant="primary">
                              <p style={{ color: "#721c24", fontWeight: 600 }}>
                                Your selected file type is not allow...
                              </p>
                            </Alert>
                          )}
                        </div>
                        <div class="row upload-box" style={{ width: "100%" }}>
                          <div class="col-2">
                            <img
                              src="./assets/media/upload.png"
                              alt="upload-image"
                            />
                          </div>

                          <div class="col-6 upload-col">
                            <p>Upload Your Company Logo</p>
                          </div>

                          <div class="col-4 upload-col">
                            <Button
                              style={{ backgroundColor: "#D3E428" }}
                              onClick={() =>
                                this.onUploadImage(imageList, onImageUpload)
                              }
                            >
                              Upload Image
                            </Button>
                          </div>
                        </div>
                        <div>{this.showUploadImages(imageList)}</div>
                      </div>
                    )}
                  </ImageUploading>
                </div>
              </form>
            </div>
          </div>
          <div className="mt-5">
            <Button className="btn previous-step" onClick={this.gotoCompanyTab}>
              Previous
            </Button>

            <Button
              className="btn next-step"
              onClick={this.handleDealerRegistration}
            >
              Submit
            </Button>
          </div>
        </div>
      </Fragment>
    );
  };

  handleDealerRegistration = () => {
    const {
      selGender,
      firstname,
      lastname,
      contactno,
      mobileno,
      personalcnic,
      firstStartDate,

      dealername,
      selDealership,
      DealershipList,
      DealershipOf,
      dealerwebsite,
      dealeremail,
      dealerno,
      firstaddress,
      secondaddress,
      area,
      city,
      companydescription,
      firstregistrationyear,
      employeesno,
      tradelicence,
      ntn,
      uan,
      branchno,
      directorno,
      DirectorsName,
      DirectorsCNIC,
      isCheckedVehicleServices,
      isCheckedVehicleRepair,
      isCheckedVehicleParts,
      isCheckedVehicleWash,
      isCheckedVehicleInspection,

      directorname,
      secdirectorname,
      thirddirectorname,
      directorcnic,
      secdirectorcnic,
      thirddirectorcnic,
      Days,
      ConcatDays,
      ConcatHours,

      imageList
    } = this.state;

    if (imageList.length > 0) {
      var dealerLogoUrl = imageList[0].file.name;
    } else {
      alert("Please Upload Company Logo");
      return;
    }

    var concatDirNames = DirectorsName.concat(
      directorname,
      secdirectorname === "" ? "" : "," + secdirectorname,
      thirddirectorname === "" ? "" : "," + thirddirectorname
    );
    var concatDirCnic = DirectorsCNIC.concat(
      directorcnic,
      secdirectorcnic === "" ? "" : "," + secdirectorcnic,
      thirddirectorcnic === "" ? "" : "," + thirddirectorcnic
    );

    if (selDealership === false) {
      DealershipOf.push(81);
    } else {
      DealershipList.map(item => {
        DealershipOf.push(item.MakeID);
      });
    }

    Days.forEach(day => {
      if (day.isChecked === true) {
        ConcatDays.push(day.value);
        ConcatHours.push(`${day.from} to ${day.to}`);
      }
    });

    console.log(
      dealername,
      dealerLogoUrl,
      uan,
      dealerwebsite,
      tradelicence,
      firstregistrationyear,
      ntn,
      concatDirNames,
      concatDirCnic,
      selDealership,
      DealershipOf.toString(),
      dealeremail,
      dealerno,
      firstaddress,
      secondaddress,
      city,
      area,
      companydescription,
      employeesno,
      branchno,
      directorno,
      isCheckedVehicleServices,
      isCheckedVehicleRepair,
      isCheckedVehicleParts,
      isCheckedVehicleWash,
      isCheckedVehicleInspection,
      selGender,
      firstStartDate,
      personalcnic,
      firstname,
      lastname,
      mobileno,
      contactno,
      ConcatDays.toString(),
      ConcatHours.toString()
    );
    const url = `${apiBaseURL}/Dealer`;
    let ApiParamForDealer = {
      dealerName: dealername.toString(),
      dealerLogoURL: dealerLogoUrl.toString(),
      UAN: uan.toString(),
      website: dealerwebsite.toString(),
      tradeLicenceNo: tradelicence.toString(),
      RegistrationYear: firstregistrationyear,
      NTN: ntn.toString(),
      directorName: concatDirNames.toString(),
      directorCNIC: concatDirCnic.toString(),
      opDays: ConcatDays.toString(),
      opHours: ConcatHours.toString(),
      DealerType: selDealership,
      DealershipOf: DealershipOf.toString(),
      officialemail: dealeremail.toString(),
      officeNumber: dealerno.toString(),
      addressLine1: firstaddress.toString(),
      addressLine2: secondaddress.toString(),
      cityID: city.toString(),
      AboutUS: companydescription.toString(),
      NumberOfEmployee: employeesno.toString(),
      NumberOfBranches: 4,
      NumberOfDirectors: 15,
      VehicleServices: isCheckedVehicleServices,
      VehicleRepair: isCheckedVehicleRepair,
      VehicleParts: isCheckedVehicleParts,
      VehicleWash: isCheckedVehicleWash,
      VehicleInspection: isCheckedVehicleInspection,
      Gender: selGender.toString(),
      DOB: firstStartDate.toString(),
      aqamaNo_CNIC: personalcnic.toString(),
      FirstName: firstname.toString(),
      LastName: lastname.toString(),
      Mobile: mobileno.toString(),
      PersonalEmail: "N/A",
      ContactNumber: contactno.toString(),
      Area: area.toString()
    };
    axios
      .post(url, ApiParamForDealer)
      .then(res => {
        console.log(res.data, "Dealer api");
        if (res.status === 200) {
          console.log(res.data, "Dealer api");
        } else {
          console.log(res.status, "statuscode Dealer api");
        }
      })
      .catch(error => {
        console.log(error, "from Dealer api");
      });
  };

  //tabs change
  handleTabSelect = key => {
    const {
      selGender,
      firstname,
      lastname,
      contactno,
      mobileno,
      personalcnic,
      firstStartDate,

      dealername,
      dealerwebsite,
      dealeremail,
      dealerno,
      firstaddress,
      secondaddress,
      area,
      city,
      companydescription,
      firstregistrationyear,
      employeesno,
      tradelicence,
      ntn,
      uan,
      branchno,
      directorno,
      directorname,
      directorcnic
    } = this.state;

    if (key === "2") {
      if (
        selGender &&
        firstname &&
        lastname &&
        contactno &&
        mobileno &&
        personalcnic &&
        firstStartDate
      ) {
        this.setState({
          key
        });
      }
    } else if (key === "3") {
      if (
        dealername &&
        dealerwebsite &&
        dealeremail &&
        dealerno &&
        firstaddress &&
        secondaddress &&
        area &&
        city &&
        companydescription &&
        firstregistrationyear &&
        employeesno &&
        tradelicence &&
        ntn &&
        uan &&
        branchno &&
        directorno &&
        directorname &&
        directorcnic
      ) {
        this.setState({
          key
        });
      }
    } else {
      this.setState({
        key
      });
    }
  };

  render() {
    return (
      <Fragment>
        <div className="container" style={{ paddingBottom: "80px" }}>
          <div className="row">
            <div className="col-12">
              <div className="b-find">
                <Tabs
                  onSelect={this.handleTabSelect}
                  className="b-find-nav nav nav-tabs justify-content-center"
                  activeKey={this.state.key}
                  id="uncontrolled-tab-example"
                >
                  <Tab eventKey={1} title="PERSONAL PROFILE" className="tab">
                    {this.renderPersonalProfileTab()}
                  </Tab>

                  <Tab eventKey={2} title="COMPANY PROFILE">
                    {this.renderCompanyProfileTab()}
                  </Tab>

                  <Tab eventKey={3} title="UPLOAD DOCUMENT">
                    {this.renderUploadTab()}
                  </Tab>
                </Tabs>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}
export default DealerRegistration;
