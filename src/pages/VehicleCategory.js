import React, { Component, Fragment } from "react";
import { apiBaseURL } from "../ApiBaseURL";
import axios from "axios";

class VehicleCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rolename: ''
    };
  }
  componentDidMount() {
    window.scrollTo(0, 0);

    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      this.getDealerID(parseUserData[0].UserID);
    }
  }

  getDealerID = (userid) => {
    const url = `${apiBaseURL}/user?UserID=${userid}`;
    axios
      .get(url)
      .then((res) => {
        this.setState({
          rolename: res.data[0].RoleName
        })
      })
      .catch((error) => {
        console.log(error, "userdata api");
      });
  }

  goToBikePostAd = () => {
    this.state.rolename === "Owner" ? this.props.history.push({ pathname: '/vehicle-type', state: { vcletype: 'bike' } }) : this.props.history.push("/adpost-usedbike");
  };
  goToCarPostAd = () => {
    this.state.rolename === "Owner" ? this.props.history.push({ pathname: '/vehicle-type', state: { vcletype: 'car' } }) : this.props.history.push("/adpost-usedcar");
  };

  render() {
    return (
      <Fragment>
        <div className="container-fluid register-success">
          <div className="container" style={{ marginBottom: "90px" }}>
            <div
              className="col-12"
              style={{ marginTop: "100px", padding: "50px 0" }}
            >
              <div className="row justify-content-center mb-5">
                <span className="category-style-heading">select category</span>
              </div>
              <div className="row center" style={{ height: 'auto' }}>
                <div
                  style={{ cursor: "pointer" }}
                  className="col-4 d-flex justify-content-center"
                  onClick={this.goToBikePostAd}
                >
                  <div className="bg-shadow category-style-obj">
                    <img src="assets/media/bike.svg" alt="" />
                  </div>
                </div>
                <div
                  style={{ cursor: "pointer" }}
                  className="col-4 d-flex justify-content-center"
                  onClick={this.goToCarPostAd}
                >
                  <div className="bg-shadow category-style-obj">
                    <img src="assets/media/car.svg" alt="" />
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default VehicleCategory;