import React, { Component, Fragment } from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  }
};

//Advertisement
const Advertisements = [
  {
    id: 1,
    ImageUrl:
      "./assets/media/content/ad/ad1.jpg"
  },
  {
    id: 2,
    ImageUrl:
      "./assets/media/content/ad/ad2.jpg"
  },
  {
    id: 3,
    ImageUrl:
      "./assets/media/content/ad/ad3.jpg"
  },
  {
    id: 4,
    ImageUrl:
      "./assets/media/content/ad/ad4.jpg"
  }
];

class Dealeradvertisement extends Component {
  render() {
    return (
      <Fragment>
        <div
          className="container-fluid advertise-advance"
          style={{ backgroundColor: "#e9e9e9" }}
        >
          <div className="container pt-3 pb-4">
            <div className="col-12">
              <h4 className="ui-title-ad">Advertisement</h4>
            </div>

            <div className="b-gallery js-slider">
              <Carousel
                additionalTransfrom={0}
                arrows={false}
                swipeable={false}
                draggable={false}
                showDots={false}
                centerMode={false}
                responsive={responsive}
                customTransition="all 1s linear"
                ssr={true} // means to render carousel on server-side.
                infinite={true}
                autoPlay
                autoPlaySpeed={5000}
                keyBoardControl={true}
                transitionDuration={500}
                containerClass="carousel-container"
                removeArrowOnDeviceType={["tablet", "mobile"]}
                deviceType={this.props.deviceType}
                dotListClass="custom-dot-list-style"
                itemClass="carousel-item-padding-40-px"
              >
                {Advertisements.map((advertise, index) => (
                  <div key={index} className="ad">
                    <img src={advertise.ImageUrl} key={index} alt="advertise" />
                  </div>
                ))}
              </Carousel>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Dealeradvertisement;