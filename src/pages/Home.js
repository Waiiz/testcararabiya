import React, { Component, Fragment } from "react";
import {
  Homeslider,
  Homesearch,
  Homebanner,
  Homepromo,
  Homerecentads,
  Homecounter,
  Homenews,
  Hometestimonials,
  Homebuysell,
} from "../components/home";
import BackToTop from "./BackToTop";

class Home extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <Fragment>
        <Homeslider {...this.props} />
        <Homesearch {...this.props} />
        <Homebanner {...this.props} />
        <Homepromo {...this.props} />
        <Homerecentads {...this.props} />
        <Homecounter {...this.props} />
        <Homenews {...this.props} />
        <Hometestimonials {...this.props} />
        <Homebuysell {...this.props} />
        {/* BackToTop */}
        <BackToTop scrollStepInPx="50" delayInMs="16.66" />
      </Fragment>
    );
  }
}

export default Home;