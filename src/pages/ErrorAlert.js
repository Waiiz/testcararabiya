import React, { Component } from "react";
import { Alert } from "react-bootstrap";

export default class ErrorAlert extends Component {
  render() {
    return (
      <Alert variant="primary" >
        <p style={{ color: "#721c24", fontWeight: 600 }}>Data not found...</p>
      </Alert>
    );
  }
}
