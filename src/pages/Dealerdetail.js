import React, { Component, Fragment } from "react";
import {
  Dealerdetailadsbyuser,
  Dealerdetailbranches,
  Dealerdetailmain,
  Dealerdetailpromo
} from "../components/dealerdetail";
import Dealeradvertisement from './Dealeradvertisement';

class DealerDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    if (this.props.history.location.state !== undefined) {
      this.dealerid = this.props.location.state.DealerID;
    } else {
      this.props.history.push("/dealers");
    }
  }

  render() {
    return (
      <Fragment>
        <div className="container mt-5">
          <div className="row">
            <div className="col-lg-9">
              <Dealerdetailmain {...this.props} />
              <Dealerdetailadsbyuser {...this.props} />
            </div>
            <div className="col-lg-3">
              <Dealerdetailbranches {...this.props} />
              <Dealerdetailpromo {...this.props} />
              <Dealeradvertisement {...this.props} />
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default DealerDetail;
