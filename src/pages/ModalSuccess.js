import React, { useContext } from "react";
import { Modal } from "react-bootstrap";
import { MyContext } from "../Context";
import { Link } from "react-router-dom";

const ModalSuccess = () => {
  const value = useContext(MyContext);
  const handleClose = () => {
    value.handleChildOpenModal(false);
  };

  return (
    <>
      <Modal centered show={value.data.openmodal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>CARARABIYA</Modal.Title>
        </Modal.Header>
        <Modal.Body>{value.data.modalres}</Modal.Body>
        <Modal.Footer>
          <Link
            style={{ color: "#D3E428", fontSize: 18, fontWeight: 500 }}
            variant="primary"
            onClick={handleClose}
          >
            Close
          </Link>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalSuccess;
