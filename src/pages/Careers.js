import React, { Component, Fragment } from "react";
import {Link} from 'react-router-dom';

export default class Careers extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <div>
        <Fragment>
          <div
            className="container-fluid"
            style={{
              backgroundColor: "#646464",
              padding: "60px 50px 40px 50px"
            }}
          >
            <div className="col-12 d-flex justify-content-center">
              <span
                style={{ fontSize: "30px", fontWeight: 800, color: "white" }}
              >
                Careers
              </span>
            </div>
          </div>
          <div className="container">
            <div className="col-12" style={{ padding: "50px 0" }}>
              <div style={{ textAlign: "center" }}>

                <div className="d-flex justify-content-center">
                  <div className="job-white-box">
                    <img
                      width="420px"
                      height="auto"
                      src="assets/media/No-jobs.png"
                      alt=""
                    />
                    <div className="mt-2" style={{ color: "#464646" }}>
                      <span
                        style={{
                          fontSize: "1.9rem",
                          fontWeight: 600,
                          lineHeight: 1
                        }}
                      >
                        There is no opening at this time!
                      </span>
                    </div>
                    <div className="mt-4" style={{ color: "#464646" }}>
                      <span
                        style={{
                          fontSize: "1.2rem",
                          fontWeight: 600,
                          lineHeight: 1,
                          color: "#D3E428"
                        }}
                      >
                        Please keep your eyes on the wheel
                      </span>
                    </div>
                  </div>
                </div>
                <div className="mt-5">
                  <Link
                        to="./"
                    type="button"
                    className="btn btn-primary color-arabiya"
                    style={{ backgroundColor: "#D3E428", padding: "10px 50px" }}
                  >
                    Go to Homepage
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </Fragment>
      </div>
    );
  }
}