import React, { Component } from "react";
import {Adpostdetailmain, Adpostdetailsimilar} from "../components/adpostdetailbike";

class Adpostdetailbike extends Component {
  constructor(props) {
    super(props);
    this.state = {
      adpostid: "",
    };
  }

  componentWillMount(){
    if(this.props.location.state !== undefined){
      const adpostid = this.props.location.state.AdPostID;
      this.setState({
        adpostid
      });
    }else{
      const params = new URLSearchParams(window.location.search);
      const adpostid = params.get('AdPostId');
      this.setState({
        adpostid
      });
    }  
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <Adpostdetailmain {...this.props} adpostid={this.state.adpostid}/>
        <Adpostdetailsimilar adpostid={this.state.adpostid}/>
      </>
    );
  }
}

export default Adpostdetailbike;