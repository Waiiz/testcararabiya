import React, { Component } from "react";
import Adpostdetailmain from "../components/adpostdetail/Adpostdetailmain";
import Adpostdetailsimilar from "../components/adpostdetail/Adpostdetailsimilar";
class Adpostdetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      adpostid: "",
    };
  }

  componentWillMount(){
    if(this.props.location.state !== undefined){
      const adpostid = this.props.location.state.AdPostID;
      this.setState({
        adpostid
      });
    }else{
      const params = new URLSearchParams(window.location.search);
      const adpostid = params.get('AdPostId');
      this.setState({
        adpostid
      });
    }  
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <Adpostdetailmain {...this.props} adpostid={this.state.adpostid}/>
        <Adpostdetailsimilar adpostid={this.state.adpostid}/>
      </>
    );
  }
}
export default Adpostdetail;