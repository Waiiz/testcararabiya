import React, { Component } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { apiBaseURL, apiUrlcity } from "../ApiBaseURL";
import axios from "axios";

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.isUserDataLoading = false;
    this.state = {
      userData: [],
      //gender
      Gender: [
        { id: 1, value: "Male", icon: "fas fa-male fa-2x" },
        { id: 2, value: "Female", icon: "fas fa-female fa-2x" }
      ],
      selGender: "",
      firstname: "",
      lastname: "",
      address: "",
      cnic: "",
      firstStartDate: null,
      startDate: null,
      //city
      lcity: [],
      scity: '',
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);

    let userData = localStorage.getItem("user");
    this.getUserDataById(userData);
    //city
    fetch(apiUrlcity)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lcity: result,
            });
          } else {
            console.log("city api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );
  }

  getUserDataById = (userData) => {
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null && this.state.userData.length === 0) {
      let userid = parseUserData[0].UserID;

      if (this.isUserDataLoading) {
        return;
      }
      this.isUserDataLoading = true;

      const url = `${apiBaseURL}/user?UserID=${userid}`;
      axios
        .get(url)
        .then((res) => {
          console.log(res);
          this.isUserDataLoading = false;
          var splitTimeDateWithT = (res.data[0].DOB).split("T");
          var date = splitTimeDateWithT[0]; //T00:00:00
          this.setState({
            userData: res.data,
            UserID: res.data[0].UserID,

            firstname: res.data[0].FirstName,
            lastname: res.data[0].LastName,
            firstStartDate: date,
            selGender: res.data[0].Gender,
            scity: res.data[0].CityID,
            address: res.data[0].Address1,
            ImageUrl: res.data[0].ImageUrl,
            cnic: res.data[0].aqamaNo_CNIC
          });
        })
        .catch((error) => {
          this.isUserDataLoading = false;
          console.log(error, "userdata api");
        });
    }
  };

  handleGenderChange = gender => {
    console.log(gender, 'gender');
    this.setState({
      selGender: gender
    });
  };

  handleFirstNameChange = (event) => {
    this.setState({ firstname: event.target.value });
  };

  handleCNICChange = event => {
    this.setState({ cnic: event.target.value });
  };

  handleLastNameChange = (event) => {
    this.setState({ lastname: event.target.value });
  };

  handleAddressChange = (event) => {
    this.setState({ address: event.target.value });
  };

  handleDOBChange = date => {
    console.log(date, 'date');
    var newdate = this.dateMaker(date);
    this.setState({
      firstStartDate: newdate,
      startDate: date
    });
  };

  //Date Maker Function
  dateMaker = (date) => {
    var dateInString = date.toString();
    var dateInSplit = dateInString.split('GMT');
    //var dateParts = dateInSplit[1].split(' ');
    var day = ("0" + date.getDate()).slice(-2);
    var monthIndex = ("0" + (date.getMonth() + 1)).slice(-2);
    var year = date.getFullYear();
    var seconds = (date.getSeconds() < 10 ? '0' : '') + date.getSeconds();
    var minutes = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    var hour = ("0" + date.getHours()).slice(-2);
    var newDate = year + '-' + monthIndex + '-' + day;
    this.setState({
      startDate: newDate
    })
    console.log(newDate, 'newdate');
    return newDate;
  }

  handleCityChange = event => {
    console.log(event.target.value, 'event.target.value ');
    this.setState({ scity: event.target.value });
  };

  handleEditProfile = () => {
    //console.log(this.state.selGender, this.state.firstname, this.state.lastname, this.state.address, this.state.firstStartDate, this.state.scity);

    const url = `${apiBaseURL}/user`;
    let ApiParamForEditProfile = {
      UserID: this.state.UserID,
      FirstName: this.state.firstname,
      LastName: this.state.lastname,
      DOB: this.state.firstStartDate,
      CountryID: 184,
      CityID: this.state.scity,
      Address1: this.state.address,
      ImageUrl: this.state.ImageUrl,
      Gender: this.state.selGender,
    };
    axios
      .post(url, null, { params: ApiParamForEditProfile })
      .then((res) => {
        console.log(res.data, "editprofile api");

        if (res.status === 200) {
          this.setState({
            editProfileSuccess: res.data,
          });
          this.props.history.push({
            pathname: '/',
            state: { editprofile: 'editprofile' }
          })
        } else {
          console.log(res.status, "statuscode from editprofile api");
        }
      })
      .catch((error) => {
        console.log(error, "editprofile api");
      });
  }

  render() {
    return (
      <div>
        <div style={{ padding: "50px 0" }}>
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div style={{ maxWidth: "750px", margin: "0 auto" }}>

                  <div className="container">
                    <span className="logmod__heading-password mt-4 mb-4">
                      <strong>Edit Profile</strong>
                    </span>
                    <div style={{ textAlign: 'center' }}>
                      {this.state.Gender.map((gender, index) => (
                        <label key={index} className="material-icons" style={{ marginRight: '5%' }}>
                          <input
                            value={gender.value}
                            checked={this.state.selGender === gender.value}
                            onChange={() =>
                              this.handleGenderChange(gender.value)
                            }
                            type="radio"
                          />
                          <span style={{ padding: '20px 18px 10px 18px' }}>
                            <i className={gender.icon}></i>
                          </span>
                        </label>
                      ))}
                    </div>
                  </div>
                  <hr />

                  <div>
                    <ul id="signup" style={{ padding: 0 }}>
                      <li>

                        <div className="sminputs">
                          <div
                            className="input"
                            style={{ width: "100%" }}
                          >
                            <label>
                              First Name
                            </label>
                            <input
                              className="form-control address ml-3"
                              type="text"
                              onChange={this.handleFirstNameChange}
                              value={this.state.firstname}
                            />
                          </div>
                        </div>

                        <div className="sminputs">
                          <div
                            className="input"
                            style={{ width: "100%" }}
                          >
                            <label>
                              Last Name
                            </label>
                            <input
                              className="form-control address ml-3"
                              type="text"
                              onChange={this.handleLastNameChange}
                              value={this.state.lastname}
                            />
                          </div>
                        </div>

                        <div className="sminputs">
                          <div
                            className="input"
                            style={{ width: "100%" }}
                          >
                            <label>
                              Address
                            </label>
                            <input
                              className="form-control address ml-3"
                              type="text"
                              onChange={this.handleAddressChange}
                              value={this.state.address}
                            />
                          </div>
                        </div>

                        <div className="sminputs">
                          <div
                            className="input"
                            style={{ width: "100%" }}
                          >
                            <label>
                              CNIC
                            </label>
                            <input
                              disabled
                              className="form-control address ml-3"
                              type="text"
                              onChange={this.handleCNICChange}
                              value={this.state.cnic}
                            />
                          </div>
                        </div>

                        <div className="sminputs">
                          <div
                            className="input"
                            style={{ width: "100%" }}
                          >
                            <label>
                              Date of Birth *
                            </label>
                            <div className="d-flex">
                              <div className="form-group">
                                <div className="string optional ml-3">
                                  <DatePicker
                                    value={this.state.firstStartDate}
                                    selected={this.state.startDate}
                                    onChange={this.handleDOBChange}
                                    peekNextMonth
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                  //dateFormat="YY-MM-DD"
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="sminputs">
                          <div
                            className="input"
                            style={{ width: "100%" }}
                          >
                            <label>
                              City
                            </label>
                            <select style={{ width: '100%', background: 'none', border: 'none' }} onChange={this.handleCityChange} value={this.state.scity}>
                              {this.state.lcity.map(city => (
                                <option key={city.CityID} value={city.CityID}>
                                  {city.CityName}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>

                      </li>
                    </ul>

                    <ul style={{ marginTop: '20px' }}>
                      <li>
                        <div className="simform__actions">
                          <input
                            className="sumbit btn btn-block mt-3"
                            type="submit"
                            value="Update"
                            onClick={this.handleEditProfile}
                          />
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default EditProfile;