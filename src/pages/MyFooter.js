import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";

class MyFooter extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

   //cars,bikes listing navigation
   handleChildListingClick = (vehiclecategoryid, vehicletypeid, props) => {
    this.props.handleParentListingClick(
      vehiclecategoryid,
      vehicletypeid,
      props
    );
  };

  handleOpenFeedbackModal = () => {
    this.props.handleParentFeedbackModal(true);
  };
  render() {
    const logo1 = "./assets/media/general/logo-light.png";
    return (
      <Fragment>
        <footer className="footer">
          <div className="container">
            <div className="row">
              <div className="col-lg-4 col-sm-5">
                <div className="footer-section footer-section_info">
                  <div className="footer-logo mb-3">
                    <Link className="footer-logo__link" to="/">
                      <img className="img-responsive" src={logo1} alt="Logo" />
                    </Link>
                  </div>
                  <div className="footer-info">
                    “Cararabiya” is an online virtual marketplace for car
                    shoppers and sellers. Cararabiya offers complete online and
                    seamless car buying experience.
                  </div>
                  <div className="footer-contacts">
                    <div className="footer-contacts__item">
                      <i className="ic icon-envelope"></i>
                      <a href="mailto:support@domain.com">
                        support@cararabiya.com
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-sm-3">
                <div className="row">
                  <div className="col-lg-6">
                    <section className="footer-section footer-section_link">
                      <h3 className="footer-section__title">Our Company</h3>
                      <ul className="footer-list list-unstyled">
                        <li>
                          <Link to="/about">About Us</Link>
                        </li>
                        <li>
                          <Link to="/our-partners">Our partners</Link>
                        </li>
                        <li>
                          <Link to="/careers">Careers</Link>
                        </li>
                        <li>
                          <Link to="/privacy-policy">Privacy Policy</Link>
                        </li>
                        <li>
                          <Link to="/terms-conditions">
                            Terms &amp; Conditions
                          </Link>
                        </li>
                        <li>
                          <Link to="/advertise">Advertise with Us</Link>
                        </li>
                      </ul>
                    </section>
                  </div>
                  <div className="col-lg-6">
                    <section className="footer-section footer-section_link">
                      <h3 className="footer-section__title">Useful Links</h3>
                      <ul className="footer-list list-unstyled">
                        <li>
                          <Link to="/">News &amp; Reviews</Link>
                        </li>
                        <li>
                          <Link to="/">Testimonials</Link>
                        </li>
                        <li>
                          <Link
                            onClick={() =>
                              this.handleChildListingClick(1, 1, this.props)
                            }
                          >
                            Latest Cars
                          </Link>
                        </li>
                        <li>
                          <Link onClick={this.handleOpenFeedbackModal}>
                            Leave Us a Review
                          </Link>
                        </li>
                      </ul>
                    </section>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-sm-3">
                <div className="row">
                  <div className="col-lg-6">
                    <section className="footer-section footer-section_link">
                      <h3 className="footer-section__title">Cararabiya</h3>
                      <ul className="footer-list list-unstyled">
                        <li>
                          <Link
                            onClick={() =>
                              this.handleChildListingClick(1, 2, this.props)
                            }
                          >
                            Used Cars
                          </Link>
                        </li>
                        <li>
                          <Link
                            onClick={() =>
                              this.handleChildListingClick(1, 1, this.props)
                            }
                          >
                            New Cars
                          </Link>
                        </li>
                        <li>
                          <Link
                            onClick={() =>
                              this.handleChildListingClick(1, 3, this.props)
                            }
                          >
                            Certified Cars
                          </Link>
                        </li>
                        <li>
                          <Link to="/promotions">Offers &amp; Promotions</Link>
                        </li>
                        <li>
                          <Link to="/financing">Financing</Link>
                        </li>
                        <li>
                          <Link to="/shop">Shop</Link>
                        </li>
                      </ul>
                    </section>
                  </div>
                  <div className="col-lg-6">
                    <section className="footer-section footer-section_link">
                      <h3 className="footer-section__title">Support</h3>
                      <ul className="footer-list list-unstyled">
                        <li>
                          <Link to="/faq">FAQ</Link>
                        </li>
                        <li>
                          <Link to="#">Live Chat</Link>
                        </li>
                        <li>
                          <Link to="/contact-us">Contact Us</Link>
                        </li>
                      </ul>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-lg-4 col-xs-2"></div>
              <div className="col-lg-4 col-xs-8">
                <div className="footer-social-bar">
                  <div className="text-center">
                    <ul className="footer-soc list-unstyled">
                      <li className="footer-soc__item">
                        <Link className="footer-soc__link" to="#">
                          <i className="ic fab fa-twitter fa-2x"></i>
                        </Link>
                      </li>
                      <li className="footer-soc__item">
                        <Link className="footer-soc__link" to="#">
                          <i className="ic fab fa-facebook fa-2x"></i>
                        </Link>
                      </li>
                      <li className="footer-soc__item">
                        <Link className="footer-soc__link" to="#">
                          <i className="ic fab fa-instagram fa-2x"></i>
                        </Link>
                      </li>
                      <li className="footer-soc__item">
                        <Link className="footer-soc__link" to="#">
                          <i className="ic fab fa-linkedin fa-2x"></i>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-xs-2"></div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 px-0">
              <div className="footer-copyright footer_bottom_copyright">
                Copyrights (c) 2020 Cararabiya. All rights reserved.
              </div>
            </div>
          </div>
        </footer>
      </Fragment>
    );
  }
}
export default MyFooter;