import React, { Component, Fragment } from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 4,
    slidesToSlide: 3 // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    slidesToSlide: 2 // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  }
};

const Industries = [
  { id: 1, ImageUrl: "./assets/media/souqarabiya.jpg" },
  { id: 2, ImageUrl: "./assets/media/f1.jpg" },
  { id: 3, ImageUrl: "./assets/media/tijarat.jpg" },
  { id: 4, ImageUrl: "./assets/media/KBC.jpg" }
];

export default class OurPartners extends Component {
  constructor(props) {
    super(props);
    this.state = { lbanks: [], error: null };
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    const apiUrlBanks = "https://cararabiya.ahalfa.com/api/bank";

    fetch(apiUrlBanks)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lbanks: result
            });
          } else {
            console.log("bank");
          }
        },
        error => {
          this.setState({ error });
        }
      );
  }

  render() {
    return (
      <Fragment>
        <div
          className="container-fluid"
          style={{
            backgroundColor: "#646464",
            padding: "60px 50px 40px 50px"
          }}
        >
          <div className="col-12 d-flex justify-content-center">
            <span style={{ fontSize: "30px", fontWeight: 800, color: "white" }}>
              Our Partners
            </span>
          </div>
        </div>
        <div
          className="container-fluid"
          style={{ backgroundColor: "white", padding: "80px 50px 80px 50px" }}
        >
          <div className="container">
            <div className="col-12 d-flex ">
              <span>
                <p
                  style={{
                    fontSize: "18px",
                    fontWeight: 500,
                    textAlign: "left",
                    color: "#464646",
                    lineHeight: "1.4",
                    padding: "0 20px"
                  }}
                >
                  Cararabiya is an online virtual marketplace for car shoppers
                  and sellers. Cararabiya offers complete online and seamless
                  car buying experience. We have partners across the country
                  from different sectors.
                </p>
              </span>
            </div>
          </div>
        </div>
        <div className="container-fluid" style={{ backgroundColor: "#e9e9e9" }}>
          <div className="container pt-5">
            <div className="col-12">
              <h4 className="ui-title-ad" style={{ color: "#464646" }}>
                BANKING SECTOR PARTNERS
              </h4>
            </div>
            <div className="b-gallery js-slider px-5">
              <Carousel
                additionalTransfrom={0}
                arrows={false}
                swipeable={false}
                draggable={false}
                showDots={false}
                centerMode={false}
                responsive={responsive}
                customTransition="all 1s linear"
                ssr={true} // means to render carousel on server-side.
                infinite={true}
                autoPlay
                autoPlaySpeed={2000}
                keyBoardControl={true}
                transitionDuration={500}
                containerClass="carousel-container"
                removeArrowOnDeviceType={["tablet", "mobile"]}
                deviceType={this.props.deviceType}
                dotListClass="custom-dot-list-style"
                itemClass="carousel-item-padding-40-px"
              >
                {this.state.lbanks.map((lbank, index) => (
                  <img className="partner-carousel" src={lbank.ImageUrl} key={index} alt="lbank" />
                ))}
              </Carousel>
            </div>
            <br />
            <br />
          </div>
        </div>
        <div className="container-fluid" style={{ backgroundColor: "white" }}>
          <div className="container pt-5">
            <div className="col-12 mb-4">
              <h4 className="ui-title-ad">INDUSTRY PARTNERS</h4>
            </div>
            <div
              className="b-gallery js-slider px-5"
            >
            <Carousel
                additionalTransfrom={0}
                arrows={false}
                swipeable={false}
                draggable={false}
                showDots={false}
                centerMode={false}
                responsive={responsive}
                customTransition="all 1s linear"
                ssr={true} // means to render carousel on server-side.
                infinite={true}
                autoPlay
                autoPlaySpeed={2000}
                keyBoardControl={true}
                transitionDuration={500}
                containerClass="carousel-container"
                removeArrowOnDeviceType={["tablet", "mobile"]}
                deviceType={this.props.deviceType}
                dotListClass="custom-dot-list-style"
                itemClass="carousel-item-padding-40-px"
              >
                {Industries.map((industry, index) => (
                  <img style={{border: 'none'}} className="partner-carousel" src={industry.ImageUrl} key={index} alt="industry" />
                ))}
              </Carousel>
             

            </div>
            <br />
            <br />
            <br />
          </div>
        </div>
      </Fragment>
    );
  }
}