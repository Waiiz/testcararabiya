import MyHeader from "./MyHeader";
import Home from "./Home";
import Listing from "./Listing";
import Promotions from "./Promotions";
import Financing from "./Financing";
import Shop from "./Shop";
import Dealers from "./Dealers";
import Advancesearch from "./Advancesearch";
import FAQ from "./FAQ";
import PrivacyPolicy from "./PrivacyPolicy";
import TermsOfUse from "./TermsOfUse";
import ContactUs from "./ContactUs";
import Advertise from "./Advertise";
import Careers from "./Careers";
import OurPartners from "./OurPartners";
import PlaceAnAd from "./PlaceAnAd";
import PostAd from "./PostAd";
import Newsone from "./Newsone";
import Newstwo from "./Newstwo";
import Newsthree from "./Newsthree";
import LoginSignup from "./LoginSignup";
import Forgotpassword from "./Forgotpassword";
import Changepassword from "./Changepassword";
import Adpostdetail from "./Adpostdetail";
import EditProfile from "./EditProfile";
import MyAds from "./MyAds";
import EditPostAd from "./EditPostAd";
import Loader from "./Loader";
import DraftListing from "./DraftListing";
import EditDraftAd from "./EditDraftAd";
import MySavedAds from "./MySavedAds";
import MyPromoOffers from "./MyPromoOffers";
import CreatePromotions from "./CreatePromotions";
import EditPromotions from "./EditPromotions";
import AdPostNewCar from "./AdPostNewCar";
import AdPostNewBike from "./AdPostNewBike";
import DealerDetail from "./Dealerdetail";
import MyPackages from "./MyPackages";
import DealerRegistration from "./DealerRegistration";
import RegistrationSuccess from "./RegistrationSuccess";
import MyUsers from "./MyUsers";
import CreateUser from "./CreateUser";
import EditUser from "./EditUser";
import ViewUser from "./ViewUser";
import CompareVehicle from "./CompareVehicle";
import VehicleCategory from "./VehicleCategory";
import AdPostUsedBike from "./AdPostUsedBike";
import MyFooter from "./MyFooter";
import ErrorFile from "./Error";
import ResetPassword from "./ResetPassword";
import Adpostdetailbike from "./Adpostdetailbike";
import FeedbackLeave from "./FeedbackLeave";
import BackToTop from "./BackToTop";
import Popover from "./Popover";
import ModalSuccess from "./ModalSuccess";
import MyBranches from "./MyBranches";
import CreateBranch from "./CreateBranch";
import VehicleType from "./VehicleType";

export {
  MyHeader,
  Home,
  Listing,
  Promotions,
  Financing,
  Shop,
  Dealers,
  Advancesearch,
  FAQ,
  PrivacyPolicy,
  TermsOfUse,
  ContactUs,
  Advertise,
  Careers,
  OurPartners,
  PlaceAnAd,
  PostAd,
  Newsone,
  Newstwo,
  Newsthree,
  LoginSignup,
  Forgotpassword,
  Changepassword,
  Adpostdetail,
  EditProfile,
  MyAds,
  EditPostAd,
  Loader,
  DraftListing,
  EditDraftAd,
  MySavedAds,
  MyPromoOffers,
  CreatePromotions,
  EditPromotions,
  AdPostNewCar,
  AdPostNewBike,
  DealerDetail,
  MyPackages,
  DealerRegistration,
  RegistrationSuccess,
  MyUsers,
  CreateUser,
  EditUser,
  ViewUser,
  CompareVehicle,
  VehicleCategory,
  AdPostUsedBike,
  ErrorFile,
  ResetPassword,
  Adpostdetailbike,
  FeedbackLeave,
  BackToTop,
  Popover,
  ModalSuccess,
  MyBranches,
  CreateBranch,
  VehicleType,
  MyFooter
};