import React, { Component } from "react";

class Shop extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <div>
        <div className="l-main-content-inner" style={{paddingBottom:'30px'}}>
          <div className="container">
            <div className="row">
              <div className="col-12">
                  <h3 style={{color: '#D3E428'}}>Shop Coming Soon</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Shop;