import React, { Component, Fragment } from "react";
import "./PostAd.css";
import { Tabs, Tab, Button, Alert, Image, Modal } from "react-bootstrap";
import ImageUploading from "react-images-uploading";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import classNames from "classnames";
import "./CopyOfChildAsDots.css";
import axios from "axios";
import Loader from "./Loader";
import moment from "moment";
import _ from "lodash";

import {
  apiBaseURL,
  apiUrlcity,
  apiUrlmake,
  apiUrlfueltype,
  apiUrlvehicletype,
  apiUrltransmission,
  apiUrlAttribute,
  apiUrlBodytype,
  apiUrlUserPackageAdPost
} from "../ApiBaseURL";

const responsive = {
  desktop: {
    breakpoint: {
      max: 3000,
      min: 1024
    },
    items: 1
  },
  mobile: {
    breakpoint: {
      max: 464,
      min: 0
    },
    items: 1
  },
  tablet: {
    breakpoint: {
      max: 1024,
      min: 200
    },
    items: 1
  }
};

var j = 0;
var maxNumber = 5;
var apiAndUploadImages = [];

export default class EditDraftAd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //basic info tab
      DraftAdPost: [],
      isLoadingForDraftAdPost: false,
      //city
      lcity: [],
      scity: "",
      scityname: "",
      //sale price
      ssaleprice: "",
      priceChecked: false,
      //car type
      svehicletype: "",
      lvehicletype: [],

      //cert authority
      scauthority: "",

      //upload cert
      // Initially, no file is selected
      selectedFile: null,

      //make
      smake: "",
      lmake: [],

      //model
      smodel: "",
      lmodel: [],

      //version
      lversion: [],
      lvinfo: [],
      sversion: "",

      //year
      syear: "",

      //vehicle info
      sfueltype: "",
      senginecapacity: "",
      svinfoid: "",
      sdriveside: "",
      sassembly: "",
      smileage: "",
      stransmission: "",
      senginetype: "",
      scylinders: "",
      sdoors: "",
      sgears: "",
      strim: "",
      sseats: "",
      senginepower: "",
      sfueltankcapacity: "",
      sdrivetype: "",
      sbodytype: "",
      svehiclecategoryid: "",

      //exterior color
      secolor: "",
      lecolor: [],

      //interior color
      licolor: [],
      sicolor: "",

      //meter reading
      smeterreading: "",

      //# of owners
      snoofowners: "",

      //fuel type
      lfueltype: [],

      //body,tyre,mechanical condition
      sbodycondition: "",
      smechanicalcondition: "",
      styrecondition: "",

      //description
      sdescription: "",

      //tab key
      key: 1,

      //additional info tab

      //area
      sarea: "",
      //regestration city
      sregcity: "",
      //chassis number
      schassisno: "",
      //engine number
      sengineno: "",
      //transmisson
      ltransmission: [],
      //attribute
      lattribute: [],
      sattribute: [],
      sattributename: [],
      svattribute: [],

      //upload tab
      imageList: [],
      GetAdImage: [],

      UserPackageAdPost: [],
      NumOfImages: "",
      imageUpload: 0,
      toggleAlertMsg: false,

      //preview tab
      lbodytype: [],

      smakename: "",
      smodelname: "",
      sbodytypename: "",
      secolorname: "",
      svehicletypename: "",
      sregcityname: "",
      sicolorname: "",
      stransmissionname: "",

      error: null,

      //Ad Post
      AdPostID: "",
      DraftAdPostID: "",

      //modals
      showModalPhone: false,
      showModalEmail: false,
      showModalSuccess: false,
      //user
      userId: "",
      //event for del image
      savedEvent: "",
      //Guidlines
      Guidlines: [
        {
          id: 1,
          value: "Front Side"
        },
        {
          id: 2,
          value: "Back Side"
        },
        {
          id: 3,
          value: "Left Side"
        },
        {
          id: 4,
          value: "Right Side"
        },
        {
          id: 5,
          value: "Engine"
        }
      ],
      isLoadingGetPackageAdPostbyUserID: false,
    };
  }

  componentWillMount() {
    if (this.props.history.location.state !== undefined) {
      this.setState({
        AdPostID: this.props.history.location.state.AdPostID
      });
    } else {
      console.log(this.state.AdPostID, "adpostid null");
    }
  }

  numberWithCommas = (price) => {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  componentDidMount() {
    window.scrollTo(0, 0);

    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      var userid = parseUserData[0].UserID;
      this.setState({
        userId: parseUserData[0].UserID
      });
    } else {
      this.props.history.push({
        pathname: "/login-signup"
      });
    }

    const url = `${apiBaseURL}/user?UserID=${userid}`;
    axios
      .get(url)
      .then(res => {
        const formatteddate = moment(res.data[0].CreatedDate).format(
          "MMMM YYYY"
        );
        this.setState({
          userData: res.data,
          UserID: res.data[0].UserID,
          Firstname: res.data[0].FirstName,
          Lastname: res.data[0].LastName,
          CreatedDate: formatteddate,
          ImageUrl: res.data[0].ImageUrl
        });
      })
      .catch(error => {
        console.log(error, "userdata api");
      });

    this.setState({ isLoadingForDraftAdPost: true });
    const urlDraftAdPost = `${apiBaseURL}/DraftAdPost?id=${15}`;
    axios
      .get(urlDraftAdPost)
      .then(res => {
        if (res.data.length > 0) {
          let formattedPrice = this.numberWithCommas(res.data[0].Price);
          console.log(res.data, "DraftAdPost");
          this.setState({
            //basic
            DraftAdPost: res.data,
            scity: res.data[0].CityID,
            ssaleprice: formattedPrice,
            svehicletype: res.data[0].VehicleTypeID,
            smake: res.data[0].MakeID,
            smodel: res.data[0].ModelID,
            sversion: res.data[0].VersionID,
            syear: res.data[0].VersionYear,
            secolor: res.data[0].ExtColorID,
            sicolor: res.data[0].IntColorID,
            snoofowners: res.data[0].NumberofOwners,
            sfueltype: res.data[0].FuelTypeID,
            smeterreading: res.data[0].MeterReading,
            sbodycondition: res.data[0].BodyCondition,
            styrecondition: res.data[0].TyreCondition,
            smechanicalcondition: res.data[0].MechanicalCondition,
            senginecapacity: res.data[0].EngineSize_Capacity_CC,
            sdescription: res.data[0].Comments,
            //additional
            sdriveside: res.data[0].DriveSide,
            sassembly: res.data[0].Assembly,
            sarea: res.data[0].Area,
            smileage: res.data[0].Mileage,
            sregcity: res.data[0].RegisteredCityID,
            schassisno: res.data[0].ChasisNumber,
            stransmission: res.data[0].TransmissionID,
            senginetype: res.data[0].EngineType,
            sengineno: res.data[0].EngineNumber,
            scylinders: res.data[0].Cylinders,
            sdoors: res.data[0].Doors,
            sgears: res.data[0].NumberofGears,
            sseats: res.data[0].NumberofSeats,
            strim: res.data[0].Trim,
            //preview
            secolorname: res.data[0].ExtColorName,
            sicolorname: res.data[0].IntColorName,
            stransmissionname: res.data[0].TransmissionName,
            sbodytypename: res.data[0].BodyTypeName,
            smakename: res.data[0].MakeName,
            smodelname: res.data[0].ModelName,
            svehicletypename: res.data[0].VehicleTypeName,
            sregcityname: res.data[0].RegisteredCityName,
            scityname: res.data[0].CityName,

            sfueltankcapacity: res.data[0].FuelTankCapacity,
            senginepower: res.data[0].EnginePower,
            sbodytype: res.data[0].BodyTypeID,
            sdrivetype: res.data[0].DriveType,
            svehiclecategoryid: res.data[0].VehicleCategoryID,
            overallLength: res.data[0].Overall_Length,
            widthdoorclosed: res.data[0].Width_doors_closed,
            widthdooropen: res.data[0].Width_doors_open,
            height: res.data[0].Height,
            wheelsize: res.data[0].WheelSize,
            measurementunit: res.data[0].MeasurementUnit,
            maximumspeed: res.data[0].MaximumSpeed,
            rarebreakes: res.data[0].RareBreakes,
            frontbreakes: res.data[0].FrontBreakes,
            airbags: res.data[0].Airbags,
            maximumpower: res.data[0].MaximumPower,
            electricrange: res.data[0].ElectricRange,
            fullchargingtime: res.data[0].FullChargingTime,
            batterytypeid: res.data[0].BatteryTypeID,
            numberofcells: res.data[0].NumberOfCells,
            numberofmodules: res.data[0].NumberOfModules,
            numberofcellspermodule: res.data[0].NumberOfCellsPerModule,
            totalcapacity: res.data[0].TotalCapacity,
            totalvoltage: res.data[0].TotalVoltage,
            //maxpower: res.data[0].MaxPower,
            slowchargingcharacteristics:
              res.data[0].SlowChargingCharacteristics,
            fastchargingcharacteristics:
              res.data[0].FastChargingCharacteristics,
            createdby: res.data[0].CreatedBy,
            createddate: res.data[0].CreatedDate
          });
          this.setState({
            isLoadingForDraftAdPost: false
          });

          this.fetchingData(
            res.data[0].MakeID,
            res.data[0].ModelID,
            res.data[0].VersionID,
            res.data[0].VehicleTypeID
          );
        } else {
          console.log(res.data, "DraftAdPost");
          this.setState({ isLoadingForDraftAdPost: false });
        }
      })
      .catch(error => {
        this.setState({ isLoadingForDraftAdPost: false });
        console.log(error, "from AdPostDetail api");
      });

    var listatt = [];
    var newlist = [];
    var ouy = [];

    //basic info
    fetch(apiUrlcity)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lcity: result,
            });
          } else {
            console.log("city api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    fetch(apiUrlvehicletype)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            delete result[0];
            this.setState({
              lvehicletype: result,
            });
          } else {
            console.log("vtype api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    fetch(apiUrlmake)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lmake: result,
            });
          } else {
            console.log("make api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    fetch(apiUrlfueltype)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lfueltype: result,
            });
          } else {
            console.log("fueltype api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    //additional info
    fetch(apiUrltransmission)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              ltransmission: result,
            });
          } else {
            console.log("trans api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    fetch(apiUrlAttribute)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            listatt = result;

            for (var a = 0; a < listatt.length; a++) {
              ouy = {
                AttributeID: listatt[a].AttributeID,
                AttributeName: "" + listatt[a].AttributeName + "",
                CreatedBy: listatt[a].CreatedBy,
                CreatedDate: "" + listatt[a].CreatedDate + "",
                IsActive: false,
              };
              newlist.push(ouy);
            }
            this.setState({
              lattribute: newlist,
            });
          } else {
            console.log("attr api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    fetch(apiUrlBodytype)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lbodytype: result,
            });
          } else {
            console.log("bodytype api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    //upload image
    const urlGetAdImage = `${apiBaseURL}/AdImage/AdImagebyDraftAdPostID?DraftAdPostID=${15}`;
    axios
      .get(urlGetAdImage)
      .then(res => {
        if (res.data.length > 0) {
          console.log(res.data, "urlGetAdImage");
          apiAndUploadImages = [...res.data];
          this.setState({
            GetAdImage: res.data
          });
          this.FetchUserPackageAdPost();
        } else {
          console.log(res.data, "urlGetAdImage");
        }
      })
      .catch(error => {
        console.log(error, "urlGetAdImage");
      });
  }

  FetchUserPackageAdPost = () => {
    //upload image
    fetch(apiUrlUserPackageAdPost)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.photosRemaining = result[0].NumOfImages;
            maxNumber = maxNumber - this.state.GetAdImage.length;
            this.setState({
              UserPackageAdPost: result,
              NumOfImages:
                result[0].NumOfImages - this.state.GetAdImage.length,
              imageUpload: this.state.GetAdImage.length,
              NumOfVideos: result[0].NumOfVideos
            });
          } else {
            console.log("userpackageadpost api");
          }
        },
        error => {
          this.setState({ error });
        }
      );
  };

  fetchingData = (makeid, modelid, versionid, vehicletypeid) => {
    const apiUrlmodel =
      "https://cararabiya.ahalfa.com/api/model?MakeID=" +
      makeid +
      "&&VehicleCategoryID=1";

    fetch(apiUrlmodel)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lmodel: result
            });
          } else {
            console.log("model api");
          }
        },
        error => {
          this.setState({ error });
        }
      );

    const apiUrlversion =
      "https://cararabiya.ahalfa.com/api/vehicleversion?ModelID=" + modelid + "";

    fetch(apiUrlversion)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lversion: result
            });
          } else {
            console.log("version api");
          }
        },
        error => {
          this.setState({ error });
        }
      );

    const apiUrlvinfo =
      "https://cararabiya.ahalfa.com/api/vehicleinfo?MakeID=" +
      makeid +
      "&&ModelID=" +
      modelid +
      "&&VersionID=" +
      versionid +
      "";

    fetch(apiUrlvinfo)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lvinfo: result
            });
          } else {
            console.log("vinfo api");
          }
        },
        error => {
          this.setState({ error });
        }
      );

    const apiUrlecolor =
      "https://cararabiya.ahalfa.com/api/color?VehicleInfoID=" +
      vehicletypeid +
      "&&IEType=e";

    fetch(apiUrlecolor)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lecolor: result
            });
          } else {
            console.log("ecolor api");
          }
        },
        error => {
          this.setState({ error });
        }
      );

    const apiUrlicolor =
      "https://cararabiya.ahalfa.com/api/color?VehicleInfoID=" +
      vehicletypeid +
      "&&IEType=i";

    fetch(apiUrlicolor)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              licolor: result
            });
          } else {
            console.log("icolor api");
          }
        },
        error => {
          this.setState({ error });
        }
      );

    const apiUrlvattribute =
      "https://cararabiya.ahalfa.com/api/AdPostAttribute/GetAdPostAttributebyDraftAdPostID?DraftAdPostID=" +
      15;

    fetch(apiUrlvattribute)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              svattribute: result
            });

            var listatt = this.state.lattribute;
            var listvatt = this.state.svattribute;
            var newlistatt = [];
            var satt = this.state.sattribute;
            var sattname = this.state.sattributename;
            var flag = false;

            for (var a = 0; a < listatt.length; a++) {
              for (var b = 0; b < listvatt.length; b++) {
                if (listatt[a].AttributeID == listvatt[b].AttributeID) {
                  flag = true;
                }
              }
              if (flag == true) {
                flag = false;
                var ouy = {
                  AttributeID: listatt[a].AttributeID,
                  AttributeName: "" + listatt[a].AttributeName + "",
                  CreatedBy: listatt[a].CreatedBy,
                  CreatedDate: "" + listatt[a].CreatedDate + "",
                  IsActive: true
                };
                newlistatt.push(ouy);
                satt.push(listatt[a].AttributeID);
                sattname.push(listatt[a].AttributeName);
              } else {
                var ouy = {
                  AttributeID: listatt[a].AttributeID,
                  AttributeName: "" + listatt[a].AttributeName + "",
                  CreatedBy: listatt[a].CreatedBy,
                  CreatedDate: "" + listatt[a].CreatedDate + "",
                  IsActive: false
                };
                newlistatt.push(ouy);
              }
            }

            this.setState({
              lattribute: newlistatt,
              sattribute: satt,
              sattributename: sattname
            });
          } else {
            console.log("vattr api");
          }
        },
        error => {
          this.setState({ error });
        }
      );
  };

  //user phone and email
  handleShowPhone = () => {
    const urlUserPhone = `${apiBaseURL}/User/UserMobile?UserID=${this.state.userId}`;
    axios
      .get(urlUserPhone)
      .then(res => {
        if (res.data.length > 0) {
          const data = res.data;
          console.log(data, "urlUserPhone");
          this.setState({
            userPhone: data,
            showModalPhone: true
          });
        } else {
          this.setState({});
        }
      })
      .catch(error => {
        this.setState({});
        console.log(error, "from urlUserPhone api");
      });
  };

  //phone modal
  handleClosePhone = () => {
    this.setState({
      showModalPhone: false
    });
  };

  modalPhone = () => {
    return (
      <Modal show={this.state.showModalPhone}>
        <Modal.Header>
          <Modal.Title>User Phone Number</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h6>{this.state.userPhone}</h6>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={this.handleClosePhone}
            style={{ backgroundColor: "#D3E428" }}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    );
  };

  handleShowEmail = () => {
    const urlUserEmail = `${apiBaseURL}/User/UserEmail?UserID=${this.state.userId}`;
    axios
      .get(urlUserEmail)
      .then(res => {
        if (res.data.length > 0) {
          const data = res.data;
          console.log(data, "urlUserEmail");
          this.setState({
            userEmail: data,
            showModalEmail: true
          });
        } else {
          this.setState({});
        }
      })
      .catch(error => {
        this.setState({});
        console.log(error, "from urlUserEmail api");
      });
  };

  //phone modal
  handleCloseEmail = () => {
    this.setState({
      showModalEmail: false
    });
  };

  modalEmail = () => {
    return (
      <Modal show={this.state.showModalEmail}>
        <Modal.Header>
          <Modal.Title>User Email</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h6>{this.state.userEmail}</h6>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={this.handleCloseEmail}
            style={{ backgroundColor: "#D3E428" }}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    );
  };

  handleCloseSuccess = () => {
    this.setState({
      showModalSuccess: false,
    });
    this.props.history.push('/vehicle-listing');
  };

  modalSuccess = () => {
    return (
      <Modal size="lg" show={this.state.showModalSuccess}>
        <div className="modal-body">
          <div className="mt-5">
            <div className="d-flex justify-content-center">
              <img src="assets/media/success.png" alt="" />
            </div>
            <div className="d-flex justify-content-center mt-3">
              <h4><strong>Your Ad has been Posted Successfully!</strong></h4>
            </div>
            <div className="d-flex justify-content-center mt-3">
              <p>It will be visible to buyers in few minutes</p>
            </div>

            <div className="d-flex justify-content-center mt-5">
              <p><b>Share your ad with your friends</b></p>
            </div>

            <div className="d-flex justify-content-center mt-3">
              <div className="social-login">
                <button className="btn facebook-btn social-btn" type="button"><span className="d-flex justify-content-center"><i className="fab fa-facebook-f"></i> </span> </button>
                <button className="btn twitter-btn social-btn" type="button"><span className="d-flex justify-content-center"><i className="fab fa-twitter"></i></span> </button>
                <button className="btn linkedin-btn social-btn" type="button"><span className="d-flex justify-content-center"><i className="fab fa-linkedin"></i></span> </button>
                <button className="btn instagram-btn social-btn" type="button"><span className="d-flex justify-content-center"><i className="fab fa-instagram"></i></span> </button>
              </div>
            </div>

            <div className="d-flex justify-content-center mb-2 mt-5">
              <button
                onClick={this.handleCloseSuccess}
                className="btn"
                style={{
                  border: "none",
                  padding: "10px 80px",
                  backgroundColor: "#D3E428",
                  color: "white",
                }}
              >
                View your listing
              </button>
            </div>
          </div>
        </div>
      </Modal>
    );
  };

  //basic info
  handleCityChange = event => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ scity: event.target.value });

    let filteredArray = this.state.lcity.filter(
      item => event.target.value == item.CityID
    );
    this.setState({ scityname: filteredArray[0].CityName });
  };

  handleSalepriceChange = event => {
    this.setState({ ssaleprice: event.target.value });
  };

  handleCBPrice = () => {
    if (!this.state.priceChecked) {
      this.setState({
        priceChecked: !this.state.priceChecked,
        ssaleprice: 0
      });
    } else {
      this.setState({
        priceChecked: !this.state.priceChecked,
        ssaleprice: ""
      });
    }
  };

  handleVehicletypeChange = event => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ svehicletype: event.target.value });

    let filteredArray = this.state.lvehicletype.filter(
      item => event.target.value == item.VehicleTypeID
    );
    this.setState({ svehicletypename: filteredArray[0].VehicleTypeName });
  };

  handleCauthorityChange = event => {
    this.setState({ scauthority: event.target.value });
  };

  // On file select (from the pop up)
  onFileChange = event => {
    // Update the state
    this.setState({ selectedFile: event.target.files[0] });
  };

  handleMakeChange = event => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ smake: event.target.value });

    let filteredArray = this.state.lmake.filter(
      item => event.target.value == item.MakeID
    );
    this.setState({ smakename: filteredArray[0].MakeName });

    const apiUrlmodel =
      "https://cararabiya.ahalfa.com/api/model?MakeID=" +
      event.target.value +
      "&&VehicleCategoryID=1";

    fetch(apiUrlmodel)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lmodel: result
            });
          } else {
            console.log("model api");
          }
        },
        error => {
          this.setState({ error });
        }
      );
  };

  handleModelChange = event => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ smodel: event.target.value });

    let filteredArray = this.state.lmodel.filter(
      item => event.target.value == item.ModelID
    );
    this.setState({ smodelname: filteredArray[0].ModelName });

    const apiUrlversion =
      "https://cararabiya.ahalfa.com/api/vehicleversion?ModelID=" +
      event.target.value +
      "";

    fetch(apiUrlversion)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lversion: result
            });
          } else {
            console.log("version api");
          }
        },
        error => {
          this.setState({ error });
        }
      );
  };

  handleVersionChange = event => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ sversion: event.target.value });

    let filteredArray = this.state.lversion.filter(
      item => event.target.value == item.VersionID
    );
    this.setState({ sversionname: filteredArray[0].VersionName });

    const apiUrlvinfo =
      "https://cararabiya.ahalfa.com/api/vehicleinfo?MakeID=" +
      this.state.smake +
      "&&ModelID=" +
      this.state.smodel +
      "&&VersionID=" +
      event.target.value +
      "";

    fetch(apiUrlvinfo)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lvinfo: result
            });
          } else {
            console.log("vinfo api");
          }
        },
        error => {
          this.setState({ error });
        }
      );
  };

  handleYearChange = event => {
    if (event.target.value === "") {
      return;
    }
    let idv = "";

    this.setState({ syear: event.target.value });
    this.state.lvinfo.map((vinfo, index) => {
      this.setState({
        sfueltype: vinfo.FuelTypeID,
        senginecapacity: vinfo.EngineSize_Capacity_CC,
        svinfoid: vinfo.VehicleInfoID,
        sdriveside: vinfo.DriveSide,
        sassembly: vinfo.Assembly,
        smileage: vinfo.Mileage,
        stransmission: vinfo.TransmissionID,
        senginetype: vinfo.EngineType,
        scylinders: vinfo.Cylinders,
        sdoors: vinfo.Doors,
        sgears: vinfo.NumberofGears,
        sseats: vinfo.NumberofSeats,
        strim: vinfo.Trim,
        sfueltankcapacity: vinfo.FuelTankCapacity,
        senginepower: vinfo.EnginePower,
        sbodytype: vinfo.BodyTypeID,
        sdrivetype: vinfo.DriveType,
        svehiclecategoryid: vinfo.VehicleCategoryID,
        overallLength: vinfo.Overall_Length,
        widthdoorclosed: vinfo.Width_doors_closed,
        widthdooropen: vinfo.Width_doors_open,
        height: vinfo.Height,
        wheelsize: vinfo.WheelSize,
        measurementunit: vinfo.MeasurementUnit,
        maximumspeed: vinfo.MaximumSpeed,
        rarebreakes: vinfo.RareBreakes,
        frontbreakes: vinfo.FrontBreakes,
        airbags: vinfo.Airbags,
        maximumpower: vinfo.MaximumPower,
        electricrange: vinfo.ElectricRange,
        fullchargingtime: vinfo.FullChargingTime,
        batterytypeid: vinfo.BatteryTypeID,
        numberofcells: vinfo.NumberOfCells,
        numberofmodules: vinfo.NumberOfModules,
        numberofcellspermodule: vinfo.NumberOfCellsPerModule,
        totalcapacity: vinfo.TotalCapacity,
        totalvoltage: vinfo.TotalVoltage,
        //maxpower: vinfo.MaxPower,
        slowchargingcharacteristics: vinfo.SlowChargingCharacteristics,
        fastchargingcharacteristics: vinfo.FastChargingCharacteristics,
        createdby: vinfo.CreatedBy,
        createddate: vinfo.CreatedDate
      });
      idv = vinfo.VehicleInfoID;

      let filteredArray = this.state.lfueltype.filter(
        item => vinfo.FuelTypeID == item.FuelTypeID
      );
      this.setState({ sfueltypename: filteredArray[0].FuelTypeName });
      let filteredArray2 = this.state.ltransmission.filter(
        item => vinfo.TransmissionID == item.VehicleTransmissionID
      );
      this.setState({ stransmissionname: filteredArray2[0].TransmissionName });

      let filteredArray3 = this.state.lbodytype.filter(
        item => vinfo.BodyTypeID == item.BodyTypeID
      );

      this.setState({ sbodytypename: filteredArray3[0].BodyTypeName });
    });

    const apiUrlecolor =
      "https://cararabiya.ahalfa.com/api/color?VehicleInfoID=" +
      idv +
      "&&IEType=e";

    fetch(apiUrlecolor)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lecolor: result
            });
          } else {
            console.log("ecolor api");
          }
        },
        error => {
          this.setState({ error });
        }
      );

    const apiUrlicolor =
      "https://cararabiya.ahalfa.com/api/color?VehicleInfoID=" +
      idv +
      "&&IEType=i";

    fetch(apiUrlicolor)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              licolor: result
            });
          } else {
            console.log("icolor api");
          }
        },
        error => {
          this.setState({ error });
        }
      );

    const apiUrlvattribute =
      "https://cararabiya.ahalfa.com/api/vehicleattribute?vehicleinfoid=" + idv;

    fetch(apiUrlvattribute)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              svattribute: result
            });
          } else {
            console.log("vattribute api");
          }
        },
        error => {
          this.setState({ error });
        }
      );
  };

  handleEcolorChange = event => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ secolor: event.target.value });

    let filteredArray = this.state.lecolor.filter(
      item => event.target.value == item.ColorID
    );
    this.setState({ secolorname: filteredArray[0].ColorName });

    var listatt = this.state.lattribute;
    var listvatt = this.state.svattribute;
    var newlistatt = [];
    var satt = this.state.sattribute;
    var sattname = this.state.sattributename;
    var flag = false;

    for (var a = 0; a < listatt.length; a++) {
      for (var b = 0; b < listvatt.length; b++) {
        if (listatt[a].AttributeID == listvatt[b].AttributeID) {
          flag = true;
        }
      }
      if (flag == true) {
        flag = false;
        var ouy = {
          AttributeID: listatt[a].AttributeID,
          AttributeName: "" + listatt[a].AttributeName + "",
          CreatedBy: listatt[a].CreatedBy,
          CreatedDate: "" + listatt[a].CreatedDate + "",
          IsActive: true
        };
        newlistatt.push(ouy);
        satt.push(listatt[a].AttributeID);
        sattname.push(listatt[a].AttributeName);
      } else {
        var ouy = {
          AttributeID: listatt[a].AttributeID,
          AttributeName: "" + listatt[a].AttributeName + "",
          CreatedBy: listatt[a].CreatedBy,
          CreatedDate: "" + listatt[a].CreatedDate + "",
          IsActive: false
        };
        newlistatt.push(ouy);
      }
    }

    this.setState({
      lattribute: newlistatt,
      sattribute: satt,
      sattributename: sattname
    });
  };

  handleIcolorChange = event => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ sicolor: event.target.value });

    let filteredArray = this.state.licolor.filter(
      item => event.target.value == item.ColorID
    );
    this.setState({ sicolorname: filteredArray[0].ColorName });
  };

  handleMeterreadingChange = event => {
    this.setState({ smeterreading: event.target.value });
  };

  handleNoofownersChange = event => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ snoofowners: event.target.value });
  };

  handleFueltypeChange = event => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ sfueltype: event.target.value });
  };

  handleBodyconditionChange = event => {
    if (event.target.value === "") {
      this.setState({ sbodycondition: "" });
    }

    if (event.target.value >= 1 && event.target.value <= 10) {
      this.setState({ sbodycondition: event.target.value });
    }
  };

  handleTyreconditionChange = event => {
    if (event.target.value === "") {
      this.setState({ styrecondition: "" });
    }

    if (event.target.value >= 1 && event.target.value <= 10) {
      this.setState({ styrecondition: event.target.value });
    }
  };

  handleMechanicalconditionChange = event => {
    if (event.target.value === "") {
      this.setState({ smechanicalcondition: "" });
    }

    if (event.target.value >= 1 && event.target.value <= 10) {
      this.setState({ smechanicalcondition: event.target.value });
    }
  };

  handleEnginecapacityChange = event => {
    this.setState({ senginecapacity: event.target.value });
  };

  handleDescriptionChange = event => {
    this.setState({ sdescription: event.target.value });
  };

  //additional info
  handleDrivesideChange = event => {
    this.setState({ sdriveside: event.target.value });
  };

  handleAssemblyChange = event => {
    this.setState({ sassembly: event.target.value });
  };

  handleAreaChange = event => {
    this.setState({ sarea: event.target.value });
  };

  handleMileageChange = event => {
    this.setState({ smileage: event.target.value });
  };

  handleRegcityChange = event => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ sregcity: event.target.value });

    let filteredArray = this.state.lcity.filter(
      item => event.target.value == item.CityID
    );
    this.setState({ sregcityname: filteredArray[0].CityName });
  };

  handleChassisnoChange = event => {
    this.setState({ schassisno: event.target.value });
  };

  handleTransmissionChange = event => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ stransmission: event.target.value });

    let filteredArray = this.state.ltransmission.filter(
      item => event.target.value == item.VehicleTransmissionID
    );
    this.setState({ stransmissionname: filteredArray[0].TransmissionName });
  };

  handleEnginetypeChange = event => {
    this.setState({ senginetype: event.target.value });
  };

  handleEnginenoChange = event => {
    this.setState({ sengineno: event.target.value });
  };

  handleCylindersChange = event => {
    this.setState({ scylinders: event.target.value });
  };

  handleDoorsChange = event => {
    this.setState({ sdoors: event.target.value });
  };

  handleGearsChange = event => {
    this.setState({ sgears: event.target.value });
  };

  handleSeatsChange = event => {
    this.setState({ sseats: event.target.value });
  };

  handleTrimChange = event => {
    this.setState({ strim: event.target.value });
  };

  handleAttributeChange = (attribute, index, event) => {
    var checkboxEnabled = !attribute.IsActive;
    attribute.IsActive = checkboxEnabled;

    var mylattribute = this.state.lattribute;

    var getIndex = mylattribute.indexOf(mylattribute[index]);
    if (getIndex > -1) {
      mylattribute.splice(getIndex, 1, attribute);
    }

    this.setState({
      lattribute: mylattribute
    });

    const searchkeyid2 = event.target.id;
    var pieces = searchkeyid2.split("-");

    const searchkeyid = pieces[1];
    const searchkeyvalue = event.target.value;
    const cheked = event.target.checked;

    if (cheked == true) {
      //var joined = this.state.sattribute;
      let filteredArray = this.state.sattribute.filter(
        item => item != searchkeyid
      );
      filteredArray.push(searchkeyid);
      this.setState({ sattribute: filteredArray });

      let filteredArray2 = this.state.sattributename.filter(
        item => item !== searchkeyvalue
      );
      filteredArray2.push(searchkeyvalue);
      this.setState({ sattributename: filteredArray2 });
    } else {
      let filteredArray = this.state.sattribute.filter(
        item => item != searchkeyid
      );
      this.setState({ sattribute: filteredArray });

      let filteredArray2 = this.state.sattributename.filter(
        item => item !== searchkeyvalue
      );
      this.setState({ sattributename: filteredArray2 });
    }
  };

  //basic info tab
  renderBasicInfoTab = () => {
    return (
      <div className="tab-pane fade show active" id="content-basic">
        {
          this.state.isLoadingForDraftAdPost ?
            <div className="row">
              <div className="col-12 center-loader">
                <Loader />
              </div>
            </div>
            :
            <form className="form-horizontal" onSubmit={this.gotoAdditionalTab}>
              <div className="row mt-5">
                <div className="col-lg-6">
                  <div className="form-group">
                    <div className="row">
                      <div className="col-4">
                        <label className="control-label">City *</label>
                      </div>
                      <div className="col-8">
                        <select
                          required
                          onChange={this.handleCityChange}
                          className="custom-select-adpost"
                          value={this.state.scity}
                        >
                          <option value="" selected>
                            Select City
                      </option>
                          {this.state.lcity.map(city => (
                            <option key={city.CityID} value={city.CityID}>
                              {city.CityName}
                            </option>
                          ))}
                        </select>
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="row">
                      <div className="col-4">
                        <label className="control-label">Sale Price *</label>
                      </div>
                      <div className="col-5">
                        <input
                          required
                          onChange={this.handleSalepriceChange}
                          value={
                            this.state.priceChecked
                              ? "Call For Price!"
                              : this.state.ssaleprice
                          }
                          type="text"
                          className="form-control"
                          placeholder="Enter Price!"
                        />
                      </div>
                      <div className="col-3" style={{ padding: "0" }}>
                        <label style={{ marginTop: "6px" }}>
                          <input
                            style={{ marginRight: "5px" }}
                            type="checkbox"
                            checked={this.state.priceChecked}
                            onChange={this.handleCBPrice}
                          />
                          Call For Price!
                    </label>
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="row">
                      <div className="col-4">
                        <label className="control-label">Car Type *</label>
                      </div>
                      <div className="col-8">
                        <select
                          required
                          onChange={this.handleVehicletypeChange}
                          value={this.state.svehicletype}
                          className="custom-select-adpost"
                        >
                          <option value="">Select Car Type</option>
                          {this.state.lvehicletype.map(vehicletype => (
                            <option
                              key={vehicletype.VehicleTypeID}
                              value={vehicletype.VehicleTypeID}
                            >
                              {vehicletype.VehicleTypeName}
                            </option>
                          ))}
                        </select>
                      </div>
                    </div>
                  </div>

                  {this.state.svehicletype == 3 ? (
                    <div className="form-group">
                      <div className="row">
                        <div className="col-4">
                          <label
                            className="control-label"
                            style={{ textAlign: "right" }}
                          >
                            Certification Authority *
                      </label>
                        </div>
                        <div className="col-8">
                          <input
                            required
                            onChange={this.handleCauthorityChange}
                            value={this.state.scauthority}
                            type="text"
                            className="form-control"
                            placeholder="Enter Certification Authority"
                          />
                        </div>
                      </div>
                    </div>
                  ) : (
                      ""
                    )}
                  {this.state.svehicletype == 3 ? (
                    <div className="form-group">
                      <div className="row">
                        <div className="col-4">
                          <label
                            className="control-label"
                            style={{ textAlign: "right" }}
                          >
                            Upload Certification*
                      </label>
                        </div>
                        <div className="col-8">
                          <input
                            style={{ display: "block" }}
                            required
                            type="file"
                            onChange={this.onFileChange}
                            className="form-control"
                          />
                        </div>
                      </div>
                    </div>
                  ) : (
                      ""
                    )}

                  <div className="form-group">
                    <div className="row">
                      <div className="col-4">
                        <label htmlFor className="control-label">
                          Make *
                    </label>
                      </div>
                      <div className="col-8">
                        <select
                          required
                          onChange={this.handleMakeChange}
                          className="custom-select-adpost"
                          value={this.state.smake}
                        >
                          <option value="" selected>
                            Select Make
                      </option>
                          {this.state.lmake.map((make, index) => (
                            <option key={make.MakeID} value={make.MakeID}>
                              {make.MakeName}
                            </option>
                          ))}
                        </select>
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="row">
                      <div className="col-4">
                        <label htmlFor className="control-label">
                          Model *
                    </label>
                      </div>
                      <div className="col-8">
                        <select
                          required
                          onChange={this.handleModelChange}
                          className="custom-select-adpost"
                          value={this.state.smodel}
                        >
                          <option value="" selected>
                            Select Model
                      </option>
                          {this.state.lmodel.map((model, index) => (
                            <option key={model.ModelID} value={model.ModelID}>
                              {model.ModelName}
                            </option>
                          ))}
                        </select>
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="row">
                      <div className="col-4">
                        <label htmlFor className="control-label">
                          Version *
                    </label>
                      </div>
                      <div className="col-8">
                        <select
                          required
                          onChange={this.handleVersionChange}
                          className="custom-select-adpost"
                          value={this.state.sversion}
                        >
                          <option value="" selected>
                            Select Version
                      </option>
                          {this.state.lversion.map((version, index) => (
                            <option
                              key={version.VersionID}
                              value={version.VersionID}
                            >
                              {version.VersionName}
                            </option>
                          ))}
                        </select>
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="row">
                      <div className="col-4">
                        <label className="control-label">Year *</label>
                      </div>
                      <div className="col-8">
                        <input
                          required
                          onChange={this.handleYearChange}
                          value={this.state.syear}
                          className="form-control"
                          placeholder="Enter Year"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="row">
                      <div className="col-4">
                        <label htmlFor className="control-label">
                          Exterior Color *
                    </label>
                      </div>
                      <div className="col-8">
                        <select
                          required
                          onChange={this.handleEcolorChange}
                          value={this.state.secolor}
                          className="custom-select-adpost"
                        >
                          {this.state.lecolor.map((ecolor, index) => (
                            <option key={ecolor.ColorID} value={ecolor.ColorID}>
                              {ecolor.ColorName}
                            </option>
                          ))}
                        </select>
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="row">
                      <div className="col-4">
                        <label htmlFor className="control-label">
                          Interior Color *
                    </label>
                      </div>
                      <div className="col-8">
                        <select
                          required
                          onChange={this.handleIcolorChange}
                          value={this.state.sicolor}
                          className="custom-select-adpost"
                        >
                          {this.state.licolor.map((icolor, index) => (
                            <option key={icolor.ColorID} value={icolor.ColorID}>
                              {icolor.ColorName}
                            </option>
                          ))}
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                {/* second colomn */}
                <div className="col-lg-6">
                  <div className="form-group">
                    <div className="row">
                      <div className="col-6">
                        <label htmlFor className="control-label">
                          Number of Owners *
                    </label>
                      </div>
                      <div className="col-6">
                        <select
                          required
                          onChange={this.handleNoofownersChange}
                          value={this.state.snoofowners}
                          className="custom-select-adpost"
                        >
                          <option value="">Select No. of Owner</option>
                          <option value="1">1st Owner</option>
                          <option value="2">2nd Owner</option>
                          <option value="3">3rd Owner</option>
                          <option value="4">3rd and above</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="row">
                      <div className="col-6">
                        <label htmlFor className="control-label">
                          Fuel Type *
                    </label>
                      </div>
                      <div className="col-6">
                        <select
                          required
                          onChange={this.handleFueltypeChange}
                          className="custom-select-adpost"
                          value={this.state.sfueltype}
                        >
                          <option value="">Select Fuel Type</option>
                          {this.state.lfueltype.map((fueltype, index) => (
                            <option
                              key={fueltype.FuelTypeID}
                              value={fueltype.FuelTypeID}
                            >
                              {fueltype.FuelTypeName}
                            </option>
                          ))}
                        </select>
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="row">
                      <div className="col-6">
                        <label className="control-label">Meter Reading(km) *</label>
                      </div>
                      <div className="col-6">
                        <input
                          required
                          onChange={this.handleMeterreadingChange}
                          value={this.state.smeterreading}
                          type="number"
                          className="form-control"
                          placeholder="Enter Meter Reading(km)"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="row">
                      <div className="col-6">
                        <label className="control-label">
                          Body Condition (out of 10) *
                    </label>
                      </div>
                      <div className="col-6">
                        <input
                          required
                          onChange={this.handleBodyconditionChange}
                          value={this.state.sbodycondition}
                          type="number"
                          className="form-control"
                          placeholder="Enter Number"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="row">
                      <div className="col-6">
                        <label className="control-label">
                          Tyre Condition (out of 10) *
                    </label>
                      </div>
                      <div className="col-6">
                        <input
                          required
                          onChange={this.handleTyreconditionChange}
                          value={this.state.styrecondition}
                          type="number"
                          className="form-control"
                          placeholder="Enter Number"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="row">
                      <div className="col-6">
                        <label className="control-label">
                          Mechanical Condition (out of 10)*
                    </label>
                      </div>
                      <div className="col-6">
                        <input
                          required
                          onChange={this.handleMechanicalconditionChange}
                          value={this.state.smechanicalcondition}
                          type="number"
                          className="form-control"
                          placeholder="Enter Number"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="row">
                      <div className="col-6">
                        <label htmlFor className="control-label">
                          Engine Capacity (cc) *
                    </label>
                      </div>
                      <div className="col-6">
                        <input
                          required
                          onChange={this.handleEnginecapacityChange}
                          value={this.state.senginecapacity}
                          type="text"
                          className="form-control"
                          placeholder="Enter Engine Capacity"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="row">
                      <div className="col-4">
                        <label htmlFor className="control-label">
                          Ad Description *
                    </label>
                      </div>
                      <div className="col-8">
                        <textarea
                          required
                          onChange={this.handleDescriptionChange}
                          value={this.state.sdescription}
                          cols={30}
                          rows={5}
                          className="form-control"
                          placeholder="Enter description"
                          defaultValue={""}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="mt-3">
                <input
                  className="btn next-step"
                  type="submit"
                  value="Continue"
                  style={{
                    position: "absolute",
                    zIndex: 151,
                    right: 0,
                    padding: "10px 80px",
                    backgroundColor: "#D3E428",
                    color: "white"
                  }}
                />
              </div>
            </form>
        }
      </div>
    );
  };

  gotoAdditionalTab = event => {
    event.preventDefault();
    this.setState({
      key: 2
    });
  };

  //additional info tab
  renderAdditionalInfoTab = () => {
    return (
      <div className="tab-pane" id="content-additional">
        <div className="row mt-5">
          <div className="col-lg-6">
            <div className="form-group">
              <div className="row">
                <div className="col-4">
                  <label htmlFor className="control-label">
                    Drive Side
                  </label>
                </div>
                <div className="col-8">
                  <select
                    onChange={this.handleDrivesideChange}
                    value={this.state.sdriveside}
                    className="custom-select-adpost"
                  >
                    <option value="">Select Drive Side</option>
                    <option value="LHD">Left Hand Drive</option>
                    <option value="RHD">Right Hand Drive</option>
                  </select>
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="col-4">
                  <label htmlFor className="control-label">
                    Assembly
                  </label>
                </div>
                <div className="col-8">
                  <select
                    onChange={this.handleAssemblyChange}
                    value={this.state.sassembly}
                    className="custom-select-adpost"
                  >
                    <option value="">Select Assembly</option>
                    <option value="imp">Import</option>
                    <option value="loc">Local</option>
                  </select>
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="col-4">
                  <label htmlFor className="control-label">
                    Area *
                  </label>
                </div>
                <div className="col-8">
                  <input
                    onChange={this.handleAreaChange}
                    value={this.state.sarea}
                    type="text"
                    className="form-control"
                    placeholder="Enter Area!"
                  />
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="col-4">
                  <label htmlFor className="control-label">
                    Mileage (Km/Ltr) *
                  </label>
                </div>
                <div className="col-8">
                  <input
                    onChange={this.handleMileageChange}
                    value={this.state.smileage}
                    type="number"
                    className="form-control"
                    placeholder="Enter Mileage"
                  />
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="col-4">
                  <label htmlFor className="control-label">
                    Registration City
                  </label>
                </div>
                <div className="col-8">
                  <select
                    onChange={this.handleRegcityChange}
                    className="custom-select-adpost"
                    value={this.state.sregcity}
                  >
                    <option value="">Select Registration City</option>
                    {this.state.lcity.map(city => (
                      <option key={city.CityID} value={city.CityID}>
                        {city.CityName}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="col-4">
                  <label htmlFor className="control-label">
                    Chassis Number / VIN
                  </label>
                </div>
                <div className="col-8">
                  <input
                    onChange={this.handleChassisnoChange}
                    value={this.state.schassisno}
                    type="number"
                    className="form-control"
                    placeholder="Enter vehicle chasis number"
                  />
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="col-4">
                  <label htmlFor className="control-label">
                    Transmission
                  </label>
                </div>
                <div className="col-8">
                  <select
                    onChange={this.handleTransmissionChange}
                    className="custom-select-adpost"
                    value={this.state.stransmission}
                  >
                    <option value="">Select Transmission</option>
                    {this.state.ltransmission.map(transmission => (
                      <option
                        key={transmission.VehicleTransmissionID}
                        value={transmission.VehicleTransmissionID}
                      >
                        {transmission.TransmissionName}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="col-4">
                  <label htmlFor className="control-label">
                    Engine Type
                  </label>
                </div>
                <div className="col-8">
                  <input
                    onChange={this.handleEnginetypeChange}
                    value={this.state.senginetype}
                    type="text"
                    className="form-control"
                    placeholder="Enter Engine Type"
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-6">
            <div className="form-group">
              <div className="row">
                <div className="col-4">
                  <label htmlFor className="control-label">
                    Engine number
                  </label>
                </div>
                <div className="col-8">
                  <input
                    onChange={this.handleEnginenoChange}
                    value={this.state.sengineno}
                    type="number"
                    className="form-control"
                    placeholder="Enter vehicle engine number"
                  />
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="col-4">
                  <label htmlFor className="control-label">
                    No. of Cylinders
                  </label>
                </div>
                <div className="col-8">
                  <input
                    onChange={this.handleCylindersChange}
                    value={this.state.scylinders}
                    type="number"
                    className="form-control"
                    placeholder="Enter number of Cylinders"
                  />
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="col-4">
                  <label htmlFor className="control-label">
                    No. of Doors
                  </label>
                </div>
                <div className="col-8">
                  <input
                    onChange={this.handleDoorsChange}
                    value={this.state.sdoors}
                    type="number"
                    className="form-control"
                    placeholder="Enter number of doors"
                  />
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="col-4">
                  <label htmlFor className="control-label">
                    No. of Gears
                  </label>
                </div>
                <div className="col-8">
                  <input
                    onChange={this.handleGearsChange}
                    value={this.state.sgears}
                    type="number"
                    className="form-control"
                    placeholder="Enter number of Gears"
                  />
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="col-4">
                  <label htmlFor className="control-label">
                    No. of Seats
                  </label>
                </div>
                <div className="col-8">
                  <input
                    onChange={this.handleSeatsChange}
                    value={this.state.sseats}
                    type="number"
                    className="form-control"
                    placeholder="Enter number of Seats"
                  />
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <div className="col-4">
                  <label htmlFor className="control-label">
                    Trim
                  </label>
                </div>
                <div className="col-8">
                  <input
                    onChange={this.handleTrimChange}
                    value={this.state.strim}
                    type="text"
                    className="form-control"
                    placeholder="Enter vehicle trim"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="form-group">
          <div className="row">
            <div className="mt-4 col-12">
              <h4
                style={{
                  fontFamily: '"Montserrat"',
                  fontSize: "18px",
                  fontWeight: 600
                }}
              >
                Features
              </h4>
              <hr />
            </div>

            {this.state.lattribute.map((attribute, index) => (
              <div className="col-4" key={index}>
                <div className="form-check">
                  <input
                    onChange={event =>
                      this.handleAttributeChange(attribute, index, event)
                    }
                    type="checkbox"
                    id={"attp-" + attribute.AttributeID}
                    value={attribute.AttributeName}
                    checked={attribute.IsActive}
                  />
                  <label
                    className="form-check-label"
                    style={{ padding: "0 0 10px 10px" }}
                  >
                    {attribute.AttributeName}
                  </label>
                </div>
              </div>
            ))}
          </div>

          <div className="mt-5">
            <div>
              <Button className="btn previous-step" onClick={this.gotoBasicTab}>
                Previous
              </Button>

              <Button className="btn next-step" onClick={this.gotoUploadTab}>
                Continue
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  };

  gotoBasicTab = () => {
    this.setState({
      key: 1
    });
  };

  gotoUploadTab = () => {
    this.setState({
      key: 3
    });
  };

  //upload tab
  onChange = imageList => {
    imageList.forEach(item => {
      apiAndUploadImages.push(item);
    });

    apiAndUploadImages = _.uniqWith(apiAndUploadImages, _.isEqual);
    //console.log(apiAndUploadImages, "apiAndUploadImages");

    // data for submit
    this.setState({
      NumOfImages:
        this.photosRemaining - imageList.length - this.state.GetAdImage.length,
      imageUpload: imageList.length + this.state.GetAdImage.length,
      imageList: imageList
    });
  };

  onError = (errors, files) => {
    console.log(errors, files);
  };

  onUploadImage = (imageList, onImageUpload) => {
    onImageUpload();
  };

  onRemoveImage = (imageList, e) => {
    imageList[e.target.value].onRemove();
    apiAndUploadImages.splice(e.target.id, 1);
  };

  onDeleteAdPostImageByAdID = (getAdImage, e) => {
    this.setState({
      savedEvent: e.target.value
    });
    let splitItemBySlash = getAdImage[e.target.value].Url.split("/");
    let getImageName = splitItemBySlash[3];

    const url = `${apiBaseURL}/file/DeleteAdPostImageByAdID`;
    let ApiParamForDeleteAdPostImage = {
      AdPostID: this.state.AdPostID,
      ImgName: getImageName
    };
    axios
      .post(url, null, { params: ApiParamForDeleteAdPostImage })
      .then(res => {
        console.log(res.data, "DeleteAdPostImageByAdID");

        if (res.status === 200) {
          getAdImage.splice(this.state.savedEvent, 1);
          apiAndUploadImages.splice(this.state.savedEvent, 1);
          maxNumber = maxNumber + 1;
          console.log("maxNumber: ", maxNumber);
          this.setState({
            NumOfImages:
              this.photosRemaining -
              this.state.imageList.length -
              this.state.GetAdImage.length,
            imageUpload:
              this.state.imageList.length + this.state.GetAdImage.length,
            GetAdImage: getAdImage,
            DeleteAdPostImage: res.data
          });
        } else {
          console.log(res.status, res.statusText, "DeleteAdPostImageByAdID");
        }
      })
      .catch(error => {
        console.log(error, "DeleteAdPostImageByAdID");
      });
  };

  showUploadImages = (imageList, getAdImage) => {
    //console.log(imageList, getAdImage, '---getAdImage');
    var imagesData = [];
    j = 0;
    var getAdImageLength = getAdImage.length;
    var imageListLength = imageList.length + getAdImageLength;

    for (var i = 0; i < 5; i++) {
      if (i < getAdImageLength) {
        imagesData.push(
          <div key={i} className="image-container">
            <div className="col-2">
              <div className="ImgContainerStyle">
                <Button
                  className="close"
                  value={i}
                  onClick={event =>
                    this.onDeleteAdPostImageByAdID(getAdImage, event)
                  }
                >
                  &times;
                </Button>
                <img src={getAdImage[i].ImageUrl} className="img-style" />
              </div>
            </div>
          </div>
        );
      } else if (i < imageListLength) {
        imagesData.push(
          <div key={i} className="image-container">
            <div className="col-2">
              <div className="ImgContainerStyle">
                <Button
                  className="close"
                  value={j}
                  id={i}
                  onClick={event => this.onRemoveImage(imageList, event)}
                >
                  &times;
                </Button>
                <img src={imageList[j].dataURL} className="img-style" />
              </div>
            </div>
          </div>
        );
        j++;
      } else {
        imagesData.push(
          <div key={i} className="image-container">
            <div className="col-2">
              <div className="ContainerStyle">
                <div className="child">{this.state.Guidlines[i].value}</div>
              </div>
            </div>
          </div>
        );
      }
    }
    return imagesData;
  };

  renderUploadTab = () => {
    return (
      <Fragment>
        <div className="tab-pane" id="content-images">
          <div className="mt-5">
            <div class="row">
              <div class="col-6 image-upload">
                <p>Images Upload: {this.state.imageUpload}</p>
              </div>
              <div class="col-6 photos-remaining">
                <p>Photos Remaining: {this.state.NumOfImages}</p>
              </div>
            </div>
            <div className="col-12" style={{ padding: 0 }}>
              <form className="uploadContainer">
                <div className="mt-3 d-flex justify-content-center">
                  <ImageUploading
                    onChange={this.onChange}
                    maxNumber={maxNumber}
                    multiple
                    acceptType={["jpg", "png"]}
                    onError={this.onError}
                  >
                    {({ imageList, onImageUpload, errors }) => (
                      // write your building UI
                      <div>
                        <div>
                          {errors.maxNumber && (
                            <Alert variant="primary">
                              <p style={{ color: "#721c24", fontWeight: 600 }}>
                                Image upload limit exceed...
                              </p>
                            </Alert>
                          )}
                          {errors.acceptType && (
                            <Alert variant="primary">
                              <p style={{ color: "#721c24", fontWeight: 600 }}>
                                Your selected file type is not allow...
                              </p>
                            </Alert>
                          )}
                        </div>
                        <div class="row upload-box">
                          <div class="col-2">
                            <img
                              src="./assets/media/upload.png"
                              alt="upload-image"
                            />
                          </div>

                          <div class="col-6 upload-col">
                            <p>
                              For best results and fast selling follow the below
                              guidelines
                            </p>
                          </div>

                          <div class="col-4 upload-col">
                            <Button
                              style={{ backgroundColor: "#D3E428" }}
                              onClick={() =>
                                this.onUploadImage(imageList, onImageUpload)
                              }
                            >
                              Upload images
                            </Button>
                          </div>
                        </div>
                        <div>
                          {this.showUploadImages(
                            imageList,
                            this.state.GetAdImage
                          )}
                        </div>
                      </div>
                    )}
                  </ImageUploading>
                </div>
              </form>
            </div>
          </div>
          <div className="mt-5">
            <Button
              className="btn previous-step"
              onClick={this.gotoAdditionalTab}
            >
              Previous
            </Button>

            <Button className="btn next-step" onClick={this.gotoPreviewTab}>
              Continue
            </Button>
          </div>
        </div>
      </Fragment>
    );
  };

  gotoPreviewTab = () => {
    this.setState({
      key: 4
    });
  };

  //preview tab
  renderPreviewTab = () => {
    const children = apiAndUploadImages.map((url, index) => (
      <div key={index} className="col-12" style={{ padding: 0 }}>
        <div id="result1" className="result-box" style={{ display: "block" }}>
          <Image
            id="result"
            src={url.ImageUrl === undefined ? url.dataURL : url.ImageUrl}
            key={index}
            alt="..."
            style={{ width: "100%", height: "500px" }}
          />
        </div>
      </div>
    ));

    const images = apiAndUploadImages.map((url, index) => (
      <div className="thumbCards">
        <Image
          key={index}
          src={url.ImageUrl === undefined ? url.dataURL : url.ImageUrl}
          className="thumbImg" />
      </div>
    ));

    const CustomDot = ({
      index,
      onClick,
      active,
      carouselState: { currentSlide }
    }) => {
      return (
        <button
          onClick={e => {
            onClick();
            e.preventDefault();
          }}
          className={classNames("custom-dot", {
            "custom-dot--active": active
          })}
        >
          {React.Children.toArray(images)[index]}
        </button>
      );
    };

    return (
      <div>
        <div className="row mt-3">
          <div className="col-lg-8">
            <div class="row">
              <div className="col-md-12" style={{ padding: 0 }}>
                <Carousel
                  showDots
                  slidesToSlide={1}
                  containerClass="carousel-with-custom-dots"
                  responsive={responsive}
                  infinite
                  customDot={<CustomDot />}
                >
                  {children}
                </Carousel>
              </div>
            </div>
            <div
              className="col-12 col-lg-12 col-md-6 col-sm-6 mt-3 mb-3"
              style={{ padding: 0 }}
            >
              <ul className="ad-detail-icon d-flex" style={{ padding: 0 }}>
                <li>
                  <span>YEAR</span>
                  <img
                    src="assets/media/icon/Calendar.png"
                    width={30}
                    height={30}
                    alt=""
                  />
                  <strong>{this.state.syear}</strong>
                </li>
                <li>
                  <span>KILOMETERS</span>
                  <img
                    src="assets/media/icon/Km.png"
                    width={30}
                    height={30}
                    alt=""
                  />
                  <strong>{this.state.smeterreading}</strong>
                </li>
                <li>
                  <span>COLOR</span>
                  <img
                    src="assets/media/icon/Color.png"
                    width={30}
                    height={30}
                    alt=""
                  />
                  <strong>{this.state.secolorname}</strong>
                </li>
                <li>
                  <span>TRANSMISSION</span>
                  <img
                    src="assets/media/icon/Automatic.png"
                    width={30}
                    height={30}
                    alt=""
                  />
                  <strong>{this.state.stransmissionname}</strong>
                </li>
                <li>
                  <span>BODY TYPE</span>
                  <img
                    src="assets/media/icon/BodyCondition.png"
                    width={30}
                    height={30}
                    alt=""
                  />
                  <strong>{this.state.sbodytypename}</strong>
                </li>
              </ul>
            </div>
            <br />
            <h5>Seller Notes</h5>
            <div className="row mt-3">
              <div className="col-md-12">
                <p>{this.state.sdescription}</p>
              </div>
            </div>
            <br />
            <h5>Basic Details</h5>
            <div className="row mt-3">
              <div className="col-md-6">
                <dl className="b-goods-f__descr row">
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    VERSION YEAR
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.syear}
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    BRAND
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.smakename}
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    MODEL
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.smodelname}
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    BODY STYLE
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.sbodytypename}
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    EXTERIOR COLOR
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.secolorname}
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    ENGINE #
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.sengineno}
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    CHASSIS #
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.schassisno}
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                </dl>
              </div>
              <div className="col-md-6">
                <dl className="b-goods-f__descr row">
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    VEHICLE TYPE
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.svehicletypename}
                  </dt>
                  <hr className="col-12 mt-0 px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    METER READING
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.smeterreading}
                  </dt>
                  <hr className="col-12 mt-0 px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    ENGINE SIZE
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.senginecapacity} cc
                  </dt>
                  <hr className="col-12 mt-0 px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    # OF CYLINDERS
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.scylinders} No(s).
                  </dt>
                  <hr className="col-12 mt-0 px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    DRIVE SIDE
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.sdriveside === "RHD"
                      ? "Right Hand Side"
                      : "Left Hand Side"}
                  </dt>
                  <hr className="col-12 mt-0 px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    ASSEMBLY
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.sassembly === "imp" ? "IMPORTED" : "Local"}
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    REGISTERED CITY
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.sregcityname}
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                </dl>
              </div>
            </div>
            <br />
            <h5>Additional Info</h5>
            <div className="row mt-3">
              <div className="col-md-6">
                <dl className="b-goods-f__descr row">
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    MILEAGE
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.smileage} Km/Ltr
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    # OF DOORS
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.sdoors} No(s).
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    # OF SEATS
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.sseats} Pers
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    # OF GEARS
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.sgears} No(s).
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    # OF OWNERS
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.snoofowners}
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    DRIVE TYPE
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.sdrivetype}
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                </dl>
              </div>
              <div className="col-md-6">
                <dl className="b-goods-f__descr row">
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    ENGINE POWER
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.senginepower} bhp
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    ENGINE TYPE
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.senginetype}
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    FUEL TANK SIZE
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.sfueltankcapacity} Ltr.
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    INTERIOR COLOR
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.sicolorname}
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    BODY CONDITION
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.sbodycondition} out of 10
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                  <dd
                    className="b-goods-f__descr-new col-lg-6 col-md-12"
                    style={{
                      fontSize: "13px",
                      color: "#222222"
                    }}
                  >
                    TRANSMISSION
                  </dd>
                  <dt
                    className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                    style={{ textAlign: "right" }}
                  >
                    {this.state.stransmissionname}
                  </dt>
                  <hr className="col-12 mt-0  px-0" />
                </dl>
              </div>
            </div>
            <br />
            <h5>Features</h5>
            <div className="row">
              <div className="col-12 mt-3">
                <div className="row px-0">
                  {this.state.sattributename.map((attname, index) => (
                    <div className="col-6 px-0" key={index}>
                      <dl className="b-goods-f__descr mb-0">
                        <dd
                          className="b-goods-f__descr-title"
                          style={{ fontWeight: 400 }}
                        >
                          {attname}{" "}
                        </dd>
                      </dl>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
          {/* end .b-seller*/}
          <div className="col-lg-4 px-0">
            {" "}
            {/* price section */}
            <div
              className
              style={{
                backgroundColor: "#D3E428",
                textAlign: "center"
              }}
            >
              <div className="b-seller__title py-3">
                <div className="b-seller__price">{this.state.ssaleprice}</div>
                <span
                  style={{
                    color: "white",
                    fontSize: "11px",
                    textAlign: "center"
                  }}
                >
                  prices are exclusive of VAT
                </span>
              </div>
            </div>
            <div>
              {" "}
              {/* buttons */}
              <div
                className="b-goods-f__sidebar px-4 py-3"
                style={{ backgroundColor: "#e0e0e0" }}
              >
                <div className="b-goods-f__price-group justify-content-center">
                  {/* <span class="b-goods-f__price">
                                 <span class="b-goods-f__price-numb">SAR &nbsp; 50,000</span>
                              </span> */}
                  <button
                    onClick={() => this.handleShowPhone()}
                    type="button"
                    className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center justify-content-center"
                    style={{
                      borderRadius: "0px",
                      fontSize: "15px",
                      fontWeight: 600
                    }}
                  >
                    {" "}
                    <i
                      style={{
                        position: "absolute",
                        left: "45px"
                      }}
                      className="fas fa-phone prefix"
                    />
                    <span>Show Phone Number</span>
                  </button>
                  <button
                    onClick={() => this.handleShowEmail()}
                    type="button"
                    className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center justify-content-center"
                    style={{
                      borderRadius: "0px",
                      fontSize: "15px",
                      fontWeight: 600
                    }}
                  >
                    {" "}
                    <i
                      style={{
                        position: "absolute",
                        left: "45px"
                      }}
                      className="fas fa-envelope prefix "
                    />
                    <span>Show Email</span>
                  </button>
                  <button
                    type="button"
                    className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center justify-content-center"
                    style={{
                      borderRadius: "0px",
                      fontSize: "15px",
                      fontWeight: 600
                    }}
                  >
                    {" "}
                    <i
                      style={{
                        position: "absolute",
                        left: "45px"
                      }}
                      className="far fa-comment-dots prefix"
                    />
                    <span>Chat with seller</span>
                  </button>
                </div>
                <span className="b-goods-f__compare"></span>
              </div>
              <div
                className="b-goods-f__sidebar px-4 py-2"
                style={{
                  backgroundColor: "#e0e0e0",
                  borderTop: "1px solid #cecece"
                }}
              >
                <div className="b-goods-f__price-group justify-content-center">
                  {/* <span class="b-goods-f__price">
                                 <span class="b-goods-f__price-numb">SAR &nbsp; 50,000</span>
                              </span> */}
                  <button
                    type="button"
                    className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center justify-content-center"
                    style={{
                      borderRadius: "0px",
                      fontSize: "15px",
                      fontWeight: 600
                    }}
                  >
                    {" "}
                    <span>Installment Calculator</span>
                  </button>
                  <button
                    type="button"
                    className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center justify-content-center"
                    style={{
                      borderRadius: "0px",
                      fontSize: "15px",
                      fontWeight: 600
                    }}
                  >
                    {" "}
                    <span>Apply for Finance</span>
                  </button>
                </div>
                <span className="b-goods-f__compare"></span>
              </div>
              <div
                className="b-goods-f__sidebar px-4 py-2"
                style={{
                  backgroundColor: "#e0e0e0",
                  borderTop: "1px solid #cecece"
                }}
              >
                <div className="bg-detail-seller">
                  <div
                    className="b-seller__detail pt-3 px-0 py-2"
                    style={{ backgroundColor: "transparent" }}
                  >
                    <div className="b-seller__img">
                      <img
                        className="img-scale"
                        src={this.state.ImageUrl}
                        alt="foto"
                      />
                    </div>
                    <div className="b-seller__title">
                      <div
                        className="b-seller__name"
                        style={{
                          fontWeight: 600,
                          fontSize: "16px"
                        }}
                      >
                        {this.state.Firstname} {this.state.Lastname}
                      </div>
                      <div className="b-seller__category">
                        Member Since {this.state.CreatedDate}
                      </div>
                    </div>
                    <hr />
                    <div
                      className="b-seller__detail row d-flex justify-content-center px-0"
                      style={{ backgroundColor: "transparent" }}
                    >
                      <div className="mx-2">
                        <i
                          style={{
                            color: "#D3E428",
                            fontSize: "30px",
                            paddingRight: "10px",
                            transform: "translateY(5px)"
                          }}
                          className="fas fa-phone"
                        />
                        <span style={{ fontSize: "12px" }}>
                          {" "}
                          Phone verified
                        </span>
                      </div>
                      <div className="mx-2">
                        <i
                          style={{
                            color: "#D3E428",
                            fontSize: "30px",
                            paddingRight: "10px",
                            transform: "translateY(5px)"
                          }}
                          className="fas fa-envelope"
                        />
                        <span style={{ fontSize: "12px" }}>
                          {" "}
                          E-mail verified
                        </span>
                      </div>
                    </div>
                  </div>
                  <span
                    className="b-goods-f__compare"
                    style={{ padding: "0px" }}
                  ></span>
                </div>
              </div>
              <div
                className="my-3 pb-2 d-flex justify-content-center"
                style={{ borderBottom: "1px solid #cecece" }}
              >
                <div>
                  <a style={{ color: "grey" }} href="#">
                    <span style={{ fontSize: "1.1em" }} className="mr-3 pb-2">
                      <i className="far fa-envelope mr-1" /> Email this listing
                    </span>
                  </a>
                </div>
              </div>
              <div className="d-flex justify-content-end">
                <a style={{ color: "grey" }} href="#">
                  <span style={{ fontSize: "0.9em" }} className="mr-3 pb-2">
                    <i className="fas fa-flag mr-1" /> Report this Ad
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div className="mt-5">
          <Button
            className="btn previous-step"
            onClick={this.gotoUploadTab}
            style={{
              position: "absolute",
              right: "510px",
              padding: "10px 80px",
              backgroundColor: "#D3E428",
              color: "white"
            }}
          >
            Previous
          </Button>

          <Button
            className="btn previous-step"
            onClick={this.handleSaveAsDraftPending}
            style={{
              position: "absolute",
              right: "250px",
              padding: "10px 60px",
              backgroundColor: "#666",
              color: "white"
            }}
          >
            Save as Draft
          </Button>

          <Button
            className="btn next-step"
            onClick={this.handlePostAd}
            style={{
              position: "absolute",
              right: 0,
              padding: "10px 80px",
              backgroundColor: "#D3E428",
              color: "white"
            }}
          >
            Post Ad
          </Button>
        </div>
      </div>
    );
  };

  //PostAd functionality
  handlePostAd = () => {
    const url = `${apiBaseURL}/adpost`;
    let ApiParamForAdPost = {
      UserID: this.state.UserID,
      SellerTypeID: 1,
      CityID: this.state.scity,
      Area: this.state.sarea,
      RegisteredCityID: this.state.sregcity,
      certifiedBy: "N/A",
      CertificateURL: "N/A",
      CertificateDate: "2020-01-01T00:00:00",
      VehicleCategoryID: this.state.svehiclecategoryid,
      VehicleTypeID: this.state.svehicletype,
      NumberofOwners: this.state.snoofowners,
      BodyCondition: this.state.sbodycondition,
      MechanicalCondition: this.state.smechanicalcondition,
      TyreCondition: this.state.styrecondition,
      Price: this.state.ssaleprice,
      MakeID: this.state.smake,
      ModelID: this.state.smodel,
      VersionID: this.state.sversion,
      VersionYear: this.state.syear,
      Trim: this.state.strim,
      NumberofSeats: this.state.sseats,
      Assembly: this.state.sassembly,
      BodyTypeID: this.state.sbodytype,
      Doors: this.state.sdoors,
      FuelTypeID: this.state.sfueltype,
      FuelTankCapacity: this.state.sfueltankcapacity,
      TransmissionID: this.state.stransmission,
      NumberofGears: this.state.sgears,
      DriveType: this.state.sdrivetype,
      MeterReading: this.state.smeterreading,
      EngineSize_Capacity_CC: this.state.senginecapacity,
      Cylinders: this.state.scylinders,
      Overall_Length: this.state.overallLength,
      Width_doors_closed: this.state.widthdoorclosed,
      Width_doors_open: this.state.widthdooropen,
      Height: this.state.height,
      WheelSize: this.state.wheelsize,
      MeasurementUnit: this.state.measurementunit,
      MaximumSpeed: this.state.maximumspeed,
      Mileage: this.state.smileage,
      RareBreakes: this.state.rarebreakes,
      FrontBreakes: this.state.frontbreakes,
      IntColorID: this.state.sicolor,
      ExtColorID: this.state.secolor,
      Airbags: this.state.airbags,
      EnginePower: this.state.senginepower,
      EngineType: this.state.senginetype,
      MaximumPower: this.state.maximumpower,
      ElectricRange: this.state.electricrange,
      FullChargingTime: this.state.fullchargingtime,
      BatteryTypeID: 11,
      NumberOfCells: this.state.numberofcells,
      NumberOfModules: this.state.numberofmodules,
      NumberOfCellsPerModule: this.state.numberofcellspermodule,
      TotalCapacity: this.state.totalcapacity,
      TotalVoltage: this.state.totalvoltage,
      //MaxPower: this.state.maxpower,
      SlowChargingCharacteristics: this.state.slowchargingcharacteristics,
      FastChargingCharacteristics: this.state.fastchargingcharacteristics,
      DriveSide: this.state.sdriveside,
      Comments: this.state.sdescription,
      CreatedDate: this.state.createddate,
      EngineNumber: this.state.sengineno,
      ChasisNumber: this.state.schassisno,
    };
    axios
      .post(url, ApiParamForAdPost)
      .then((res) => {
        console.log(res.data, "adpostid from adpost api");

        if (res.status === 200) {
          this.setState({
            AdPostID: res.data,
          });
          this.handleAdPostAttributeApi(res.data);
        } else {
          console.log(res.status, res.statusText, "statuscode from adpost api");
        }
      })
      .catch((error) => {
        console.log(error, "from adpost api");
      });
  };

  handleAdPostAttributeApi = AdPostID => {
    this.state.sattribute.map(item => {
      const url = `${apiBaseURL}/AdPostAttribute`;
      let ApiParamForAdPostAttribute = {
        AdPostID: AdPostID,
        DraftAdPostID: 0,
        AttributeID: item,
        AttributeValue: "true"
      };
      axios
        .post(url, ApiParamForAdPostAttribute)
        .then(res => {
          console.log(res, "res from AdPostAttribute api");

          if (res.status === 200) {
            console.log(
              res.status,
              res.statusText,
              "statuscode from AdPostAttribute api"
            );
          } else {
            console.log(
              res.status,
              res.statusText,
              "statuscode from AdPostAttribute api"
            );
          }
        })
        .catch(error => {
          console.log(error, "from AdPostAttribute api");
        });
    });

    this.handleAdImageApi();
  };

  handleAdImageApi = () => {
    apiAndUploadImages.map(item => {
      var getImageName = "";
      if (item.Url !== undefined) {
        let splitItemBySlash = item.Url.split("/");
        getImageName = splitItemBySlash[3];
      } else {
        getImageName = item.file.name;
      }

      const url = `${apiBaseURL}/AdImage`;
      let ApiParamForAdImage = {
        AdPostID: this.state.AdPostID,
        ImageUrl: `Content/Uploads/${this.state.AdPostID}/${getImageName}`,
        CreatedBy: this.state.UserID,
        DraftAdPostID: "0"
      };
      axios
        .post(url, ApiParamForAdImage)
        .then(res => {
          console.log(res, "res from AdImage api");

          if (res.status === 200) {
            console.log(res.status, "adimage");
          } else {
            console.log(
              res.status,
              res.statusText,
              "statuscode from AdImage api"
            );
          }
        })
        .catch(error => {
          console.log(error, "from AdImage api");
        });
    });

    this.handleUploadAdPostImageApi();
  };

  handleUploadAdPostImageApi = () => {
    apiAndUploadImages.map((item, index) => {
      var getIUrlORFileObj = "";
      if (item.Url !== undefined) {
        getIUrlORFileObj = item.Url;
      } else {
        getIUrlORFileObj = item.file;
      }

      // Create an object of formData
      let data = new FormData();

      // Update the formData object
      data.append("AdpostID", this.state.AdPostID);
      data.append("ImageUrl", getIUrlORFileObj);

      const url = `${apiBaseURL}/file/UploadAdPostImage`;

      axios
        .post(url, data)
        .then(res => {
          console.log(res, "res from UploadAdPostImage api");

          if (res.status === 200) {
            let itemLength = this.state.imageUpload - 1;
            if (index === itemLength) {
              this.loadGetPackageAdPostbyUserID(this.state.UserID);
            }
            console.log(res.status, "UploadAdPostImage");
          } else {
            console.log(res.status, "statuscode from UploadAdPostImage api");
          }
        })
        .catch(error => {
          console.log(error, "from UploadAdPostImage api");
        });
    });
  };

  loadGetPackageAdPostbyUserID = (userid) => {
    this.setState({ isLoadingGetPackageAdPostbyUserID: true });
    const urlgetpackagebyuserid = `${apiBaseURL}/UserPackageAdPost/GetPackageAdPostbyUserID?UserID=${userid}`;
    axios
      .get(urlgetpackagebyuserid)
      .then(res => {
        this.setState({ isLoadingGetPackageAdPostbyUserID: false });
        console.log(res.data, "UserPackageAdPost");
        if (res.data.length > 0) {
          this.handleAdPostOrder(res.data[0]['PackageID'], res.data[0]['UserID'], res.data[0]['OrderID'], res.data[0]['NumOfDays']);
        } else {
          console.log(res.data, "UserPackageAdPost");
        }
      })
      .catch(error => {
        this.setState({
          isLoadingGetPackageAdPostbyUserID: false
        });
        console.log(error, "from GetPackageAdPostbyUserID api");
      });
  };

  handleAdPostOrder = (packageid, userid, orderid, numofdays) => {
    var currentDate = new Date();
    var currentdateafteradddays = moment(currentDate).format();
    var new_date = moment(currentdateafteradddays).add('days', numofdays).format();

    const url = `${apiBaseURL}/AdPostOrder`;
    let ApiParamForAdPostOrder = {
      PackageID: packageid,
      UserID: userid,
      OrderID: orderid,
      CreatedDate: currentDate,
      Days: numofdays,
      DateExpiry: new_date,
      AdPostID: this.state.AdPostID,
      IsPublish: 'true',
    };
    axios
      .post(url, ApiParamForAdPostOrder)
      .then((res) => {
        console.log(res, "res from AdPostOrder api");
        if (res.status === 200) {
          this.setState({ showModalSuccess: true });
          console.log(res.status, "AdPostOrder");
        } else {
          console.log(
            res.status,
            res.statusText,
            "statuscode from AdPostOrder api"
          );
        }
      })
      .catch((error) => {
        console.log(error, "from AdPostOrder api");
      });
  };

  //SaveDraft functionality
  handleSaveAsDraft = () => {
    const url = `${apiBaseURL}/DraftAdPost`;
    let ApiParamForDraftAdPost = {
      UserID: this.state.UserID,
      SellerTypeID: 1,
      CityID: this.state.scity,
      Area: this.state.sarea,
      RegisteredCityID: this.state.sregcity,
      certifiedBy: "N/A",
      CertificateURL: "N/A",
      CertificateDate: "2020-01-01T00:00:00",
      VehicleCategoryID: this.state.svehiclecategoryid,
      VehicleTypeID: this.state.svehicletype,
      NumberofOwners: this.state.snoofowners,
      BodyCondition: this.state.sbodycondition,
      MechanicalCondition: 0,
      TyreCondition: 0,
      Price: this.state.ssaleprice,
      MakeID: this.state.smake,
      ModelID: this.state.smodel,
      VersionID: this.state.sversion,
      VersionYear: this.state.syear,
      Trim: this.state.strim,
      NumberofSeats: this.state.sseats,
      Assembly: this.state.sassembly,
      BodyTypeID: this.state.sbodytype,
      Doors: this.state.sdoors,
      FuelTypeID: this.state.sfueltype,
      FuelTankCapacity: this.state.sfueltankcapacity,
      TransmissionID: this.state.stransmission,
      NumberofGears: this.state.sgears,
      DriveType: this.state.sdrivetype,
      MeterReading: this.state.smeterreading,
      EngineSize_Capacity_CC: this.state.senginecapacity,
      Cylinders: this.state.scylinders,
      Overall_Length: this.state.overallLength,
      Width_doors_closed: this.state.widthdoorclosed,
      Width_doors_open: this.state.widthdooropen,
      Height: this.state.height,
      WheelSize: this.state.wheelsize,
      MeasurementUnit: this.state.measurementunit,
      MaximumSpeed: this.state.maximumspeed,
      Mileage: this.state.smileage,
      RareBreakes: this.state.rarebreakes,
      FrontBreakes: this.state.frontbreakes,
      IntColorID: this.state.sicolor,
      ExtColorID: this.state.secolor,
      Airbags: this.state.airbags,
      EnginePower: this.state.senginepower,
      EngineType: this.state.senginetype,
      MaximumPower: this.state.maximumpower,
      ElectricRange: this.state.electricrange,
      FullChargingTime: this.state.fullchargingtime,
      BatteryTypeID: 11,
      NumberOfCells: this.state.numberofcells,
      NumberOfModules: this.state.numberofmodules,
      NumberOfCellsPerModule: this.state.numberofcellspermodule,
      TotalCapacity: this.state.totalcapacity,
      TotalVoltage: this.state.totalvoltage,
      //MaxPower: this.state.maxpower,
      SlowChargingCharacteristics: this.state.slowchargingcharacteristics,
      FastChargingCharacteristics: this.state.fastchargingcharacteristics,
      DriveSide: this.state.sdriveside,
      Comments: this.state.sdescription,
      CreatedDate: this.state.createddate,
      EngineNumber: this.state.sengineno,
      ChasisNumber: this.state.schassisno,
    };
    axios
      .post(url, ApiParamForDraftAdPost)
      .then(res => {
        console.log(res.data, "DraftAdPostid from DraftAdPost api");

        if (res.status === 200) {
          this.setState({
            DraftAdPostID: res.data
          });
          this.handleDraftAdPostAttributeApi(res.data);
        } else {
          console.log(
            res.status,
            res.statusText,
            "statuscode from DraftAdPost api"
          );
        }
      })
      .catch(error => {
        console.log(error, "from DraftAdPost api");
      });
  };

  handleDraftAdPostAttributeApi = DraftAdPostID => {
    this.state.sattribute.map(item => {
      const url = `${apiBaseURL}/AdPostAttribute`;
      let ApiParamForDraftAdPostAttribute = {
        AdPostID: 0,
        DraftAdPostID: DraftAdPostID,
        AttributeID: item,
        AttributeValue: "true"
      };
      axios
        .post(url, ApiParamForDraftAdPostAttribute)
        .then(res => {
          console.log(res, "res from DraftAdPostAttribute api");

          if (res.status === 200) {
            console.log(
              res.status,
              res.statusText,
              "statuscode from DraftAdPostAttribute api"
            );
          } else {
            console.log(
              res.status,
              res.statusText,
              "statuscode from DraftAdPostAttribute api"
            );
          }
        })
        .catch(error => {
          console.log(error, "from DraftAdPostAttribute api");
        });
    });

    this.handleDraftAdImageApi();
  };

  handleDraftAdImageApi = () => {
    this.state.pictures.map(item => {
      const url = `${apiBaseURL}/AdImage`;
      let ApiParamForDraftAdImage = {
        AdPostID: "0",
        ImageUrl: `Content/Uploads/${this.state.DraftAdPostID}/${item.name}`,
        CreatedBy: this.state.UserID,
        DraftAdPostID: this.state.DraftAdPostID
      };
      axios
        .post(url, ApiParamForDraftAdImage)
        .then(res => {
          console.log(res, "res from DraftAdImage api");

          if (res.status === 200) {
            console.log(res.status, "DraftAdImage");
          } else {
            console.log(
              res.status,
              res.statusText,
              "statuscode from DraftAdImage api"
            );
          }
        })
        .catch(error => {
          console.log(error, "from DraftAdImage api");
        });
    });

    this.handleDraftAdPostUploadApi();
  };

  handleDraftAdPostUploadApi = () => {
    this.state.pictures.map(item => {
      // Create an object of formData
      let data = new FormData();

      // Update the formData object
      data.append("ImageUrl", item);

      const url = `${apiBaseURL}/file/DraftAdPostUpload?DraftAdPostID=${this.state.DraftAdPostID}`;

      axios
        .post(url, data)
        .then(res => {
          console.log(res, "res from DraftAdPostUpload api");

          if (res.status === 200) {
            console.log(res.status, "DraftAdPostUpload");
          } else {
            console.log(res.status, "statuscode from DraftAdPostUpload api");
          }
        })
        .catch(error => {
          console.log(error, "from DraftAdPostUpload api");
        });
    });
  };

  handleTabSelect = (key) => {
    if (key === "2" || key === "3" || key === "4") {
      if (this.state.scity && this.state.svehicletype && this.state.snoofowners && this.state.sbodycondition && this.state.smechanicalcondition && this.state.styrecondition
        && this.state.ssaleprice && this.state.smake && this.state.smodel && this.state.sversion && this.state.syear && this.state.smeterreading && this.state.senginecapacity
        && this.state.sicolor && this.state.secolor && this.state.sfueltype && this.state.sdescription) {
        this.setState({
          key,
        });
      }
    } else {
      this.setState({
        key,
      });
    }
  };

  render() {
    return (
      <div>
        <div style={{ paddingBottom: "50px" }}>
          {/* modal success */}
          {this.modalSuccess()}
          {/* modal phone */}
          {this.modalPhone()}
          {/* modal email */}
          {this.modalEmail()}
          <div className="container">
            <div className="row">
              <div className="col-12">
                <div className="b-find">
                  <Tabs
                    onSelect={this.handleTabSelect}
                    className="b-find-nav nav nav-tabs justify-content-between"
                    activeKey={this.state.key}
                    id="uncontrolled-tab-example"
                  >
                    <Tab eventKey={1} title="BASIC INFO" className="tab">
                      {this.renderBasicInfoTab()}
                    </Tab>

                    <Tab eventKey={2} title="ADDITIONAL INFO">
                      {this.renderAdditionalInfoTab()}
                    </Tab>

                    <Tab eventKey={3} title="UPLOAD IMAGES">
                      {this.renderUploadTab()}
                    </Tab>

                    <Tab eventKey={4} title="PREVIEW">
                      {this.renderPreviewTab()}
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}