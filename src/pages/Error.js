import React, { Component, Fragment } from "react";

class ErrorFile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <Fragment>
        <div className="l-main-content-inner" style={{paddingBottom:'30px'}}>
          <div className="container">
            <div className="row">
              <div className="col-12" style={{height: '40vh'}}>
              <h3 style={{color: '#D3E428'}}>
                    Page Not Found
                </h3>

                <h5 style={{marginTop: '2%'}}>
                    We searched everywhere but couldn't find the page you were looking for.
                </h5>

              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default ErrorFile;