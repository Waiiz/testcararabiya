import React, { Component, Fragment } from "react";
import { apiBaseURL } from "../ApiBaseURL";
import axios from "axios";

class VehicleType extends Component {
  constructor(props) {
    super(props);
    this.state = {
      vcletype: ''
    };
  }
  componentWillMount() {
    if (this.props.location.state !== undefined) {
      const vcletype = this.props.location.state.vcletype;
      this.setState({
        vcletype: vcletype
      });
    } else {
      this.props.history.push("/vehicle-category")
    }
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  goToUsedPostAd = () => {
    this.state.vcletype === 'car' ? this.props.history.push("/adpost-usedcar") : this.props.history.push("/adpost-usedbike");
  };

  render() {
    return (
      <Fragment>
        <div className="container-fluid register-success">
          <div className="container" style={{ marginBottom: "90px" }}>
            <div
              className="col-12"
              style={{ marginTop: "100px", padding: "50px 0" }}
            >
              <div className="row justify-content-center mb-5">
                <span className="category-style-heading">select Type</span>
              </div>
              <div className="row center" style={{ height: 'auto' }}>
                <div
                  style={{ cursor: "pointer" }}
                  className="col-4 d-flex justify-content-center"
                >
                  <div className="bg-shadow category-style-obj">
                    {this.state.vcletype === 'car' ? <img src="assets/media/car.svg" alt="" /> : <img src="assets/media/bike.svg" alt="" />}
                    <span style={{ fontSize: '2em', fontWeight: 500 }}>NEW</span>
                  </div>
                </div>
                <div
                  style={{ cursor: "pointer" }}
                  className="col-4 d-flex justify-content-center"
                  onClick={this.goToUsedPostAd}
                >
                  <div className="bg-shadow category-style-obj">
                    {this.state.vcletype === 'car' ? <img src="assets/media/car.svg" alt="" /> : <img src="assets/media/bike.svg" alt="" />}
                    <span style={{ fontSize: '2em', fontWeight: 500 }}>USED</span>
                  </div>
                </div>
                <div
                  style={{ cursor: "pointer" }}
                  className="col-4 d-flex justify-content-center"
                >
                  <div className="bg-shadow category-style-obj">
                    {this.state.vcletype === 'car' ? <img src="assets/media/car.svg" alt="" /> : <img src="assets/media/bike.svg" alt="" />}
                    <span style={{ fontSize: '2em', fontWeight: 500 }}>CERTIFIED</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default VehicleType;