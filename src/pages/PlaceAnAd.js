import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { MyContext } from "../Context";
import AdPostConditions from '../components/AdPostConditions';

class PlaceAnAd extends Component {
  static contextType = MyContext;
  constructor(props) {
    super(props);
    this.ispackageloading = false;
    this.state = {
    }
  }

  handleMySavedAds = () => {
    let userData = localStorage.getItem("user");
    if (userData === null) {
      this.context.handleChildOpenPopover(true);
    } else {
      this.props.history.push("/mysaved-ads");
    }
  };

  render() {
    return (
      <Fragment>
        <AdPostConditions props={this.props} classname="feedback" />

        <div id="saved">
          <Link onClick={this.handleMySavedAds}>
            <i className="fas fa-heart" />
          </Link>
        </div>
      </Fragment>
    );
  }
}
PlaceAnAd.contextType = MyContext;
export default PlaceAnAd;