import React, { Component } from "react";

export default class Advertise extends Component {
  render() {
    return (
      <div>
        <div

        >
          <div
            className="container-fluid"
            style={{
              backgroundColor: "#646464",
              padding: "60px 50px 40px 50px",
            }}
          >
            <div className="col-12 d-flex justify-content-center">
              <span
                style={{ fontSize: "30px", fontWeight: 800, color: "white" }}
              >
                Advertise with Us
              </span>
            </div>
          </div>
          <div className="container">
            <div className="row mt-5">
              <div className="col-md-4">
                <div className="mb-3">
                  <span className="heading-primary">
                    Cararabiya is the right platform to reach out to your
                    customers. We ensure that your advertising messages reach
                    the relevant customer in the right way
                  </span>
                </div>
                <div className="mb-3">
                  <span className="heading-secondary">
                    For any advertising options and corporate tie-ups, you may
                    contact us at the below details:
                  </span>
                </div>
                <div className="mb-3">
                  <b>Head Office:</b> <br />
                  Phone: +91 129 209 291
                  <br />
                  E-mail:{" "}
                  <a
                    href="mailto:advertisewithus@cararabiya.com"
                    style={{ color: "#D3E428" }}
                  >
                    advertisewithus@cararabiya.com
                  </a>
                </div>
              </div>
              <div className="col-md-8">
                <div className="heading-primary mb-3">
                  <span style={{ fontSize: "14px", fontWeight: 400 }}>
                    OR -
                  </span>{" "}
                  Fill this Enquiry Form
                </div>
                <form action="/post" method="post">
                  <input
                    className="form-control"
                    name="name"
                    placeholder="Name..."
                    style={{ border: "1px solid #D3E42899" }}
                  />
                  <br />
                  <input
                    className="form-control"
                    name="phone"
                    placeholder="Phone..."
                    style={{ border: "1px solid #D3E42899" }}
                  />
                  <br />
                  <input
                    className="form-control"
                    name="email"
                    placeholder="E-mail..."
                    style={{ border: "1px solid #D3E42899" }}
                  />
                  <br />
                  <textarea
                    className="form-control"
                    name="text"
                    placeholder="Hi! I would like to place a banner on Cararabiya"
                    style={{ height: "150px", border: "1px solid #D3E42899" }}
                    defaultValue={""}
                  />
                  <br />
                  <input
                    style={{ backgroundColor: "#D3E428" }}
                    className="btn btn-primary"
                    type="submit"
                    defaultValue="Send"
                  />
                  <br />
                  <br />
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}