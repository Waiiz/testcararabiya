import React, { useContext } from "react";
import { Modal } from "react-bootstrap";
import { MyContext } from "../Context";
import { Link } from 'react-router-dom';

const Popover = () => {
  const value = useContext(MyContext);
  const handleClose = () => {
    value.handleChildOpenPopover(false);
  };

  return (
    <>
      <Modal centered show={value.data.openpopover} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>CARARABIYA</Modal.Title>
        </Modal.Header>
        <Modal.Body>Authentication is required. You need to sign in to your Account.</Modal.Body>
        <Modal.Footer>
          <Link style={{ color: '#D3E428', fontSize: 18, fontWeight: 500 }} to="/login-signup" variant="primary" onClick={handleClose}>
            Sign In
          </Link>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default Popover;
