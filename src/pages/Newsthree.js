import React, { Component } from "react";
import Dealeradvertisement from './Dealeradvertisement';

export default class Newsthree extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <div>
        <div className="l-main-content" style={{ paddingTop: "50px" }}>
          <div className="container">
            <div className="row">
              <div className="col-lg-9">
                <main className="b-post-full-wrap">
                  <article className="b-post b-post-full clearfix">
                    <div className="entry-media">
                      <a
                        className="js-zoom-images"
                        href="./assets/media/content/b-posts/830x450/subaru/1.jpg"
                      >
                        <img
                          className="img-fluid"
                          src="./assets/media/content/b-posts/830x450/subaru/1.jpg"
                          alt="Foto"
                        />
                      </a>
                    </div>
                    <div className="entry-main">
                      <div className="entry-meta">
                        <div className="entry-date bg-primary">
                          <span className="entry-date__number">20</span>
                          <span className="entry-date__month">dec</span>
                        </div>
                        <span className="entry-meta__item">
                          <a className="entry-meta__link" href="#">
                            <i className="ic icon-user" />
                            by ALEX LEANSE
                          </a>
                        </span>
                      </div>
                      <div className="entry-header">
                        <h1 className="entry-title">
                          2019 Subaru WRX STI S209 First Test: What Makes You So
                          Special?
                        </h1>
                      </div>
                      <br />
                      <div className="entry-content">
                        <p>
                          Subaru stans rejoice: A mythical S-model has finally
                          landed on American soil. Subaru Tecnica
                          International's motorsport-inspired,
                          limited-production creations were previously reserved
                          for Japan, but now the Pleiades have aligned, and we
                          get our own. Building off the WRX STI Type RA,
                          Subaru's speed-obsessed engineers proceeded to widen,
                          stiffen, lower, and boost, resulting in the S209. It
                          is, quite simply, the most powerful and performant
                          factory-built Subaru ever.
                        </p>
                        <br />
                        <p>
                          As a week of hammering the S209 revealed, it
                          represents peak STI. The car delivers a visceral,
                          engaging driving experience that's ever rarer in this
                          increasingly digitized and electrified world. It
                          demands and rewards your focus, goading you to dig
                          deeper and work harder. If you want this analog thrill
                          before it's lost to kilowatts and computers, act
                          immediately: Only 209 S209s will ever be built.
                        </p>
                        <br />
                        <p>
                          There's a problem, however. Whereas the standard STI
                          offers good performance value, the S209, well,
                          doesn't. The one we tested cost—sitting down?—$64,880.
                          That's approximately $27,000 more than a stock STI, a
                          difference equivalent to a brand-new 2020 Outback.
                        </p>
                        <blockquote className="entry-blockquote">
                          <p>
                            Walton thinks the S209 could borrow something from
                            the aforementioned Audi, calling it the "number one
                            candidate for a dual-clutch transmission (with
                            launch control) ever."
                          </p>
                        </blockquote>
                        <div className="entry-media">
                          <img
                            className="img-fluid"
                            src="./assets/media/content/b-posts/830x450/subaru/2.jpg"
                            alt="Foto"
                          />
                        </div>
                        <br />
                        <p>
                          Price is the S209's curse, for as good it is, it's the
                          factor upon which it will be endlessly compared. Would
                          you rather have it, or a mildly optioned BMW M2
                          Competition, or Audi RS3, or Porsche 718 Cayman? Or a
                          stock STI, and a stack of cash with which to modify
                          the hell out of it? Or, perhaps, a two-car garage of
                          the aforementioned Outback and STI? Here even the most
                          steadfast Subie bro must do some soul searching, and
                          decide whether this ultra-exclusive super-roo is worth
                          it.
                          <br />
                          <br />
                          The S209 looks like someone already dropped some coin
                          to add street style to an STI. Those looks work, too:
                          Every vent, scoop, and strake is functional. Gold rims
                          under bulging arches look like they were swiped from a
                          rally team garage. The regular STI's infamous wing is
                          replaced with a smaller adjustable unit, which, like
                          the roof, is crafted from carbon fiber. Quad exhaust
                          outlets are an STI trademark, here with a bespoke
                          exhaust system behind them. Inside, the only clues
                          that this isn't a standard STI are the faux suede
                          steering wheel and numbered dedication plate behind
                          the shifter.
                          <br />
                          <br />
                          Subaru wrung everything it could from its EJ-series
                          2.5-liter turbocharged flat-four, resulting in 341 hp
                          and 330 lb-ft of torque. How that power is delivered
                          is as old school as the engine itself (its lineage
                          traces back to the 2003 STI). Low in the rev range you
                          can keep the throttle floored and not worry about
                          breaking speed limits. After about an hour of lag the
                          car lunges forward, commanding decisive gear changes
                          and thoughtful planning to keep the engine on boost.
                          Riding that surge gives the S209 dimension, and fun.
                        </p>
                        <br />
                        <p>
                          One S209 trick is the intercooler water spray,
                          activated by paddles behind the steering wheel. Tug
                          one and a mist hits the heat exchanger, cooling the
                          charged air and feeding a more oxygen-dense, fuel-rich
                          mixture to the cylinders—basically, extra power on
                          demand. It's not much and the effect is subtle, but it
                          works. The higher the revs, the more you'll feel it.
                        </p>
                        <br />
                        <div className="entry-media">
                          <img
                            className="img-fluid"
                            src="./assets/media/content/b-posts/830x450/subaru/3.jpg"
                            alt="Foto"
                          />
                        </div>{" "}
                        <br />
                        <p>
                          Still, "this is not a drag racer—it hates launching,"
                          said road test editor Chris Walton. Conspiring against
                          it are that peaky engine and a driveline-protecting
                          clutch damper which forces slow shifts. Walton found
                          launching "requires more than 4,000 rpm to be in the
                          torque, and major clutch slippage." Despite his best
                          efforts, "it bogs bigly," and the S209's quickest 0-60
                          mph time disappointed at 5.9 seconds—two tenths behind
                          the standard STI. The much-less-expensive Honda Civic
                          Type R did it in 5.0 seconds flat, while the similarly
                          priced Audi RS3 did the sprint in just 3.6. Through
                          the quarter mile it barely beats the regular STI,
                          tying at 14.1 seconds, but ahead at 101.2 mph against
                          98.5 mph.
                        </p>
                        <br />
                        <p>
                          Walton thinks the S209 could borrow something from the
                          aforementioned Audi, calling it the "number one
                          candidate for a dual-clutch transmission (with launch
                          control) ever."
                        </p>
                        <br />
                        <p>
                          For better or worse, like the standard STI, it's
                          solely available with a six-speed manual transmission.
                          Dastardly damper aside, it's a pretty sweet setup.
                          Shifter throws are short, tight, and require some
                          shoulder. The lever itself feels very stiff, but
                          there's some softness in the gates. Consistent takeup
                          starts just off the floor for the hefty clutch, and
                          there's a nice interplay between left foot and right
                          hand. Short gearing benefits windy roads, not
                          highways: Revs hold around 3,000 rpm at 70 mph.
                          <br />
                          <br />
                          Contrasting the S209's drivetrain delays is its
                          braking immediacy. Brembo six-piston front and
                          two-piston rear calipers carry over from the STI but
                          now hold high-friction pads. Typically soft-spoken
                          testing director Kim Reynolds abandoned form, calling
                          the brakes "Eh, so-so… That is, so, so amazing!"
                          Brushing or stomping on the extremely firm pedal
                          results in "instant bite, zero dive, and straight,
                          steady stops," according to Walton. He found the
                          fade-free stoppers "extremely effective and
                          trustworthy." The S209 halted from 60 mph in 103 feet,
                          improved over the regular STI's 109 feet, and ahead of
                          the M2 Competition's 106 feet. Don't worry about the
                          squeaking and scraping noises—that's just the
                          track-spec pads working.
                        </p>
                        <br />
                        <div className="url-style">
                          <span >
                            <b>Source : </b>
                          </span>
                          <span>
                          
                            <a
                              className="special"
                              href="https://www.motortrend.com/cars/subaru/wrx/2019/2019-subaru-wrx-sti-s209-first-test-makes-special/"
                            >
                              https://www.motortrend.com/cars/subaru/wrx/2019/2019-subaru-wrx-sti-s209-first-test-makes-special/
                            </a>
                          </span>
                        </div>
                      </div>
                    </div>
                  </article>
                  {/* end .section-reply-form*/}
                </main>
              </div>
              <div className="col-lg-3">
                <aside className="l-sidebar l-sidebar_last">
                  {/* end .widget*/}
                  <section className="widget section-sidebar bg-light">
                    <h3 className="widget-title bg-cararabiya">recent posts</h3>
                    <div className="widget-content">
                      <div className="widget-inner">
                        <section className="post-widget clearfix">
                          <div className="post-widget__media">
                            <a href="News-1.html">
                              <img
                                className="img-fluid"
                                src="./assets/media/content/b-posts/80x80/1.jpg"
                                alt="foto"
                              />
                            </a>
                          </div>
                          <div className="post-widget__inner">
                            <h2 className="post-widget__title">
                              <a href="News-1.html">
                                2020 Mazda CX-30 First Drive: Recipe for Success
                              </a>
                            </h2>
                            <div className="post-widget__date">
                              <time dateTime="2019-10-27 15:20">
                                Dec 20, 2019
                              </time>
                            </div>
                          </div>
                          {/* end .widget-post*/}
                        </section>
                        <section className="post-widget clearfix">
                          <div className="post-widget__media">
                            <a href="News-2.html">
                              <img
                                className="img-fluid"
                                src="./assets/media/content/b-posts/80x80/2.jpg"
                                alt="foto"
                              />
                            </a>
                          </div>
                          <div className="post-widget__inner">
                            <h2 className="post-widget__title">
                              <a href="News-2.html">
                                Inside the Porsche Taycan Factory: How Porsche
                                Will Build Its Electric Sports Car
                              </a>
                            </h2>
                            <div className="post-widget__date">
                              <time dateTime="2019-10-27 15:20">
                                Dec 20, 2019
                              </time>
                            </div>
                          </div>
                          {/* end .widget-post*/}
                        </section>
                      </div>
                    </div>
                  </section>
                  <Dealeradvertisement />
                  {/* end .widget-post*/}
                </aside>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}