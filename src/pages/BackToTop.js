﻿import React, { Component } from 'react';
import './BackToTop.css';

class BackToTop extends Component {
    constructor() {
        super();
        this.state = {
            intervalId: 0
        };
    }

    scrollStep() {
        if (window.scrollY === 0) {
            clearInterval(this.state.intervalId);
        }
        window.scroll(0, window.scrollY - this.props.scrollStepInPx);
    }

    scroll() {
        let intervalId = setInterval(this.scrollStep.bind(this), this.props.delayInMs);
        //store the intervalId inside the state, 
        //so we can use it later to cancel the scrolling
        this.setState({ intervalId: intervalId });
    }

    render() {
        return (
            <button title='Back to top' id='scrolls' className='scrolls'
                onClick={(event) => {
                    event.preventDefault(); this.scroll();
            }}>

<i class="fas fa-chevron-up"></i>

            </button>
        );
    }
}
export default BackToTop;
