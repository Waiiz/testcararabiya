import React, { useState, useEffect } from "react";
import md5 from "md5";
import axios from "axios";
import { apiChangePassword } from "../ApiBaseURL";
import { Modal } from "react-bootstrap";
const Changepassword = (props) => {
  let [current, setCurrent] = useState("");
  let [newPassword, setNewPassword] = useState("");
  let [confirmNewPassword, setConfirmNewPassword] = useState("");
  let [resdata, setResData] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [showMessage, setShowMessage] = useState("");
  const handleCloseModal = () => {
    if (resdata === "Password change Successful") {
      props.history.push("/");
      setShowModal(false);
    } else {
      props.history.push("/change-password");
      setShowModal(false);
    }
  };
  const handleChangePassword = () => {
    if (current && newPassword && confirmNewPassword) {
      const userData = localStorage.getItem("user");
      const parseUserDate = JSON.parse(userData);
      console.log(parseUserDate);
      if (newPassword != confirmNewPassword) {
        setShowModal(true);
        setShowMessage("Confirm Password does not match");
      } else {
        axios({
          method: "POST",
          url:
            apiChangePassword +
            `UserID=${parseUserDate[0].UserID}&OldPassword=${md5(
              current + "s0mRIdlKvI"
            )}&NewPassword=${md5(newPassword + "s0mRIdlKvI")}`,
        }).then(
          (res) => {
            if (res.data === "Password change Successful") {
              setResData(res.data);
              setShowModal(true);
              setShowMessage("Password Change Successfully");
            } else {
              setShowModal(true);
              setShowMessage("Please Check Old Password");
            }
          },
          (error) => {
            console.log(error, "Change password");
          }
        );
      }
    }
  };
  useEffect(() => {
    window.scrollTo(0, 0)
  }, []);
  return (
    <>
      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Change Password</Modal.Title>
        </Modal.Header>
        <Modal.Body>{showMessage}</Modal.Body>
      </Modal>
      <div className="logmod__wrapper">
        <div className="logmod__container">
          <div className="logmod__heading">
            <span className="logmod__heading-password mt-4 mb-4">
              <strong>Change Password</strong>
            </span>
          </div>
          <ul className="logmod__tabs">
            <li data-tabtar="lgm-4">
              <div className="d-flex justify-content-center mb-3">
                <input
                  type="password"
                  className="form-control address"
                  id="address"
                  placeholder="Current Password"
                  required
                  onChange={(e) => setCurrent(e.target.value)}
                />
              </div>
              <div className="d-flex justify-content-center mb-3">
                <input
                  type="password"
                  className="form-control address"
                  id="address"
                  placeholder="New Password"
                  required
                  onChange={(e) => setNewPassword(e.target.value)}
                />
              </div>
              <div className="d-flex justify-content-center mb-3">
                <input
                  type="password"
                  className="form-control address"
                  id="address"
                  placeholder="Confirm New Password"
                  required
                  onChange={(e) => setConfirmNewPassword(e.target.value)}
                />
              </div>
              <div className="d-flex justify-content-center mb-5 mt-5">
                <button
                  type="submit"
                  role="button"
                  className="btn btn-block btn-dark-blue"
                  onClick={handleChangePassword}
                >
                  CHANGE PASSWORD
                </button>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
};
export default Changepassword;