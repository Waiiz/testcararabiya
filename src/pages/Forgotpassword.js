import React, { useState, useEffect } from "react";
import "./Forgotpassword.css";
import axios from "axios";
import { apiGetUserByEmail, apiForgetPassword } from "../ApiBaseURL";
import { Modal } from "react-bootstrap";

const ForgetPassword = (props) => {
  const [email, setEmail] = useState("");
  const [status, setStatus] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [showMessage, setShowMessage] = useState("");

  const handleForgetPassword = () => {
    if (email !== "") {
      axios({
        method: "GET",
        url: apiGetUserByEmail + email,
      }).then(
        (res) => {
          if (res && res.status == 200) {
            if (Array.isArray(res["data"])) {
              if (res["data"][0]["registeredAt"] == "ca") {
                axios({
                  method: "POST",
                  url:
                    apiForgetPassword +
                    `UserID=${res["data"][0]["UserID"]}&Email=${res["data"][0]["Email"]}`,
                }).then(
                  (res) => {
                    if (res && res.status == 200) {
                      if (res["data"]) {
                        setStatus(res.status);
                        setShowModal(true);
                        setShowMessage(
                          "Please check your email and click on the provided link to reset your password."
                        );
                      } else {
                        setShowModal(true);
                        setShowMessage("Something Went Wrong...");
                      }
                    }
                  },
                  (error) => {
                    setShowModal(true);
                    setShowMessage("Something Went Wrong...");
                    console.log(error);
                  }
                );
              } else {
                setShowModal(true);
                setShowMessage(
                  "This email is registered with the" +
                    res["data"][0]["registeredAt"] +
                    ", you can not change it's password please try to login from " +
                    res["data"][0]["registeredAt"]
                );
              }
            }
          }
        },
        (error) => {
          setShowModal(true);
          setShowMessage("Something Went Wrong...");
          console.log(error);
        }
      );
    } else {
      setShowModal(true);
      setShowMessage("Please enter your email address.");
    }
  };
  const handleCloseModal = () => {
    if (status === 200) {
      setShowModal(false);
      props.history.push("/");
    } else {
      setShowModal(false);
    }
  };
  useEffect(() => {
    window.scrollTo(0, 0)
  }, []);
  return (
    <>
      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Cararabiya</Modal.Title>
        </Modal.Header>
        <Modal.Body>{showMessage}</Modal.Body>
      </Modal>
      <div className="logmod__wrapper">
        <div className="logmod__container">
          <div className="logmod__heading">
            <span className="logmod__heading-password mt-4 mb-4">
              <strong>Forgot your password?</strong>
            </span>
          </div>
          <ul className="logmod__tabs">
            <li data-tabtar="lgm-4">
              <div className="d-flex justify-content-center mb-3">
                <input
                  type="email"
                  className="form-control address"
                  id="address"
                  placeholder="e.g. email@domain.com"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
              <div className="d-flex justify-content-center mb-5 mt-5">
                <button
                  type="submit"
                  role="button"
                  className="btn btn-block btn-dark-blue"
                  onClick={handleForgetPassword}
                >
                  RESET PASSWORD
                </button>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
};

export default ForgetPassword;