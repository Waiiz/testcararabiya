import React, { Component, Fragment } from "react";
import { apiBaseURL } from "../ApiBaseURL";
import axios from "axios";
import { Modal } from "react-bootstrap";

class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      subject: "",
      description: "",
      showMessage: "",
      showModal: false,
      status: "",
    };
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  handleNameChange = (event) => {
    this.setState({ name: event.target.value });
  };
  handleEmailChange = (event) => {
    this.setState({ email: event.target.value });
  };
  handleSubjectChange = (event) => {
    this.setState({ subject: event.target.value });
  };
  handleDescriptionChange = (event) => {
    this.setState({ description: event.target.value });
  };

  handleContactUs = (event) => {
    event.preventDefault();
    const url = `${apiBaseURL}/ContactUs`;
    let ApiParamForContactUs = {
      email: this.state.email,
      name: this.state.name,
      subject: this.state.subject,
      message: this.state.description,
    };
    axios
      .post(url, null, { params: ApiParamForContactUs })
      .then((res) => {
        console.log(res, "ContactUs api");

        if (res.status === 200) {
          this.setState({
            status: res.status,
            showModal: true,
            showMessage:
              "Thank you! for contacting us. Our representative will contact you soon.",
          });
        } else {
          console.log(res.status, "statuscode from ContactUs api");
        }
      })
      .catch((error) => {
        this.setState({
          showModal: true,
          showMessage: "Something Went Wrong...",
        });
        console.log(error, "ContactUs api");
      });
  };
  handleCloseModal = () => {
    if (this.state.status === 200) {
      this.setState({
        showModal: false,
      });
      this.props.history.push("/");
    } else {
      this.setState({
        showModal: false,
      });
    }
  };
  showModal = () => {
    return (
      <Modal show={this.state.showModal} onHide={this.handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Cararabiya</Modal.Title>
        </Modal.Header>
        <Modal.Body>{this.state.showMessage}</Modal.Body>
      </Modal>
    );
  };
  render() {
    return (
      <Fragment>
        {this.showModal()}
        <div
          className="container-fluid"
          style={{
            backgroundColor: "#646464",
            padding: "60px 50px 40px 50px",
          }}
        >
          <div className="col-12 d-flex justify-content-center">
            <span style={{ fontSize: "30px", fontWeight: 800, color: "white" }}>
              Contact Us
            </span>
          </div>
        </div>
        <div className="container mt-5 mb-5">
          <div className="row">
            <div className="col-md-8">
              <form onSubmit={this.handleContactUs}>
                <input
                  required
                  className="form-control"
                  placeholder="Enter Name"
                  style={{ border: "1px solid #D3E42899" }}
                  onChange={this.handleNameChange}
                  value={this.state.name}
                />
                <br />
                <input
                  required
                  className="form-control"
                  placeholder="Enter Email"
                  style={{ border: "1px solid #D3E42899" }}
                  onChange={this.handleEmailChange}
                  value={this.state.email}
                />
                <br />
                <input
                  required
                  className="form-control"
                  placeholder="Enter Subject"
                  style={{ border: "1px solid #D3E42899" }}
                  onChange={this.handleSubjectChange}
                  value={this.state.subject}
                />
                <br />
                <textarea
                  required
                  className="form-control"
                  placeholder="How can we help you?"
                  style={{ border: "1px solid #D3E42899" }}
                  onChange={this.handleDescriptionChange}
                  value={this.state.description}
                  cols={30}
                  rows={5}
                />
                <br />
                <input
                  style={{ backgroundColor: "#D3E428" }}
                  className="btn btn-primary"
                  type="submit"
                  value="Send"
                />
                <br />
                <br />
              </form>
            </div>
            <div className="col-md-4">
              <b>Customer service:</b> <br />
              Phone: +91 129 209 291
              <br />
              E-mail:{" "}
              <a
                href="mailto:support@cararabiya.com"
                style={{ color: "#D3E428" }}
              >
                support@cararabiya.com
              </a>
              <br />
              <br />
              <br />
              <b>Headquarter:</b>
              <br />
              Company Inc, <br />
              Sheikh Abdullah road 201
              <br />
              55001 Jeddah, KSA
              <br />
              Phone: +91 145 000 101
              <br />
              <a href="mailto:hq@cararabiya.com" style={{ color: "#D3E428" }}>
                hq@cararabiya.com
              </a>
              <br />
              <br />
              <b>Pakistan:</b>
              <br />
              Company CA Ltd, <br />
              25/F.168 Queen
              <br />
              Ghazi District, Karachi
              <br />
              Phone: +92 129 209 291
              <br />
              <a href="mailto:info@cararabiya.com" style={{ color: "#D3E428" }}>
                info@cararabiya.com
              </a>
              <br />
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}
export default ContactUs;