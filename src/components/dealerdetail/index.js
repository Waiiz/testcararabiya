import Dealerdetailadsbyuser from './Dealerdetailadsbyuser';
import Dealerdetailbranches from './Dealerdetailbranches';
import Dealerdetailmain from './Dealerdetailmain';
import Dealerdetailpromo from './Dealerdetailpromo';

export {
    Dealerdetailadsbyuser, Dealerdetailbranches, Dealerdetailmain, Dealerdetailpromo
};