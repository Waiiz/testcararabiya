import React, { Component, Fragment } from "react";
import { apiBaseURL } from "../../ApiBaseURL";
import axios from "axios";
import Loader from "../../pages/Loader";
import ErrorAlert from "../../pages/ErrorAlert";

class Dealerdetailbranches extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //branch
      BranchData: [],
      dealerid: "",
      isLoadingForBranch: false,
      branchAlertMsg: false
    };
  }

  componentWillMount() {
    if (this.props.history.location.state !== undefined) {
      this.setState({
        dealerid: this.props.location.state.DealerID
      });
    } else {
      this.props.history.push("/dealers");
    }
  }

  componentDidMount() {
    this.getBranchData();
  }

  getBranchData = () => {
    this.setState({ isLoadingForBranch: true });
    const apiUrlBranch = `${apiBaseURL}/DBranch/GetDBranchbyDealerID?DealerID=${this.state.dealerid}`;
    axios
      .get(apiUrlBranch)
      .then(res => {
        if (res.data.length > 0) {
          this.setState({
            BranchData: res.data,
            isLoadingForBranch: false,
            branchAlertMsg: false
          });
        } else {
          this.setState({
            isLoadingForBranch: false,
            branchAlertMsg: true
          });
        }
      })
      .catch(error => {
        this.setState({
          isLoadingForBranch: false,
          branchAlertMsg: true
        });
        console.log(error, "from GetDBranchbyDealerID api");
      });
  };

  showBranchData = (branch, index) => {
    return (
      <Fragment key={index}>
        <div className="b-dealers-info__contacts col-lg mb-4">
          <div className="b-dealers-info__contacts-title mb-1">
            {branch.dBranchName}
          </div>
          <hr />
          <div
            className="b-dealers-info__contacts-item"
            style={{ paddingBottom: "10px", marginBottom: "10px" }}
          >
            <i className="ic icon-location-pin text-primary" />
            <div
              className="b-dealers-info__contacts-title mb-1"
              style={{ fontSize: "13px", color: "#464646" }}
            >
              Location
            </div>
            <div
              className="b-dealers-info__contacts-info"
              style={{ fontSize: "13px" }}
            >
              {branch.addressLine1} {branch.addressLine2}, {branch.cityName}
            </div>
          </div>
          <div
            className="b-dealers-info__contacts-item"
            style={{ paddingBottom: "10px", marginBottom: "10px" }}
          >
            <i className="ic icon-call-in text-primary" />
            <div
              className="b-dealers-info__contacts-title mb-1"
              style={{ fontSize: "13px" }}
            >
              Branch Phone
            </div>
            <div
              className="b-dealers-info__contacts-info"
              style={{ fontSize: "13px" }}
            >
              {branch.contactNumber}
            </div>
          </div>
        </div>
      </Fragment>
    );
  };
  render() {
    const { BranchData } = this.state;
    return (
      <Fragment>
        <div className="widget widget-search section-sidebar bg-light">
          <div className="widget-title bg-cararabiya">Branches</div>
          <div className="widget-inner">
            {this.state.branchAlertMsg === true ? (
              <ErrorAlert show={this.state.branchAlertMsg} />
            ) : this.state.isLoadingForBranch ? (
              <Loader />
            ) : (
              BranchData.map((branch, index) =>
                this.showBranchData(branch, index)
              )
            )}
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Dealerdetailbranches;