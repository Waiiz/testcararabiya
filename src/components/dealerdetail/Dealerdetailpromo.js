import React, { Component, Fragment } from "react";
import { apiBaseURL } from "../../ApiBaseURL";
import axios from "axios";
import Loader from "../../pages/Loader";
import ErrorAlert from "../../pages/ErrorAlert";
import moment from "moment";
import { Modal } from "react-bootstrap";

class Dealerdetailpromo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //dealer
      PromoData: [],
      dealerid: "",
      isLoadingForPromo: false,
      promoAlertMsg: false,
      //modals
      showModal: false,
      imageurl: ""
    };
  }

  componentWillMount() {
    if (this.props.history.location.state !== undefined) {
      this.setState({
        dealerid: this.props.location.state.DealerID
      });
    } else {
      this.props.history.push("/dealers");
    }
  }

  componentDidMount() {
    this.getPromoData();
  }

  getPromoData = () => {
    this.setState({ isLoadingForPromo: true });
    const apiUrlPromo = `${apiBaseURL}/PromoAndOffer/PromoByDealerID?DealerID=${this.state.dealerid}`;
    axios
      .get(apiUrlPromo)
      .then(res => {
        if (res.data.length > 0) {
          this.setState({
            isLoadingForPromo: false,
            promoAlertMsg: false
          });

          if (res.data.length > 3) {
            var newlist = [];
            for (var i = 0; i < 3; i++) {
              newlist.push(res.data[i]);
            }
            this.setState({
              PromoData: newlist
            });
          } else {
            this.setState({
              PromoData: res.data
            });
          }
        } else {
          this.setState({
            isLoadingForPromo: false,
            promoAlertMsg: true
          });
        }
      })
      .catch(error => {
        this.setState({
          isLoadingForPromo: false,
          promoAlertMsg: true
        });
        console.log(error, "from PromoByDealerID api");
      });
  };

  formattedDate = datetime => {
    let splitTimeDateWithT = datetime.split("T");
    let date = splitTimeDateWithT[0]; //T00:00:00
    let formatteddate = moment(date).format("D MMMM, YYYY");
    return formatteddate;
  };

  //Gallery
  handleCloseModal = () => {
    this.setState({
      showModal: false
    });
  };
  handleShowModal = imageurl => {
    this.setState({
      imageurl: imageurl,
      showModal: true
    });
  };

  modalGallery = () => {
    return (
      <Modal
        size="lg"
        centered
        show={this.state.showModal}
        onHide={this.handleCloseModal}
      >
        <Modal.Header closeButton></Modal.Header>
        <Modal.Body>
          <img className="img-fluid" src={this.state.imageurl} alt="Foto" />
        </Modal.Body>
      </Modal>
    );
  };

  showPromoData = (promo, index) => {
    return (
      <Fragment key={index}>
        <section className="post-widget clearfix">
          <div className="post-widget__media">
            <img
              onClick={() => this.handleShowModal(promo.ImageUrl)}
              style={{ width: "86px", height: "80px", cursor: "pointer" }}
              className="img-fluid"
              src={promo.ImageUrl}
              alt="foto"
            />
          </div>
          <div class="post-widget__inner">
            <h2 class="post-widget__title">{promo.PromotionName}</h2>
            <div class="post-widget__date">
              <time datetime={promo.Valid_Till}>
                {" "}
                {this.formattedDate(promo.Valid_Till)}{" "}
              </time>
            </div>
          </div>
        </section>
      </Fragment>
    );
  };

  render() {
    const { PromoData } = this.state;
    return (
      <Fragment>
        {this.modalGallery()}
        <section className="widget section-sidebar bg-light mt-4">
          <h3 className="widget-title bg-cararabiya ">My Promotions</h3>
          <div className="widget-content">
            <div className="widget-inner">
              {this.state.promoAlertMsg === true ? (
                <ErrorAlert show={this.state.promoAlertMsg} />
              ) : this.state.isLoadingForPromo ? (
                <Loader />
              ) : (
                PromoData.map((promo, index) =>
                  this.showPromoData(promo, index)
                )
              )}
            </div>
          </div>
        </section>
      </Fragment>
    );
  }
}

export default Dealerdetailpromo;