import React, { Component, Fragment } from "react";
import { apiBaseURL } from "../../ApiBaseURL";
import axios from "axios";
import Loader from "../../pages/Loader";
import ErrorAlert from "../../pages/ErrorAlert";
import { Modal } from "react-bootstrap";
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => <div>{text}</div>;

const DEALER_MAIN =
  "./assets/media/dealer-main/abdullah/1.jpg";
class Dealerdetailmain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //dealer
      DealerData: [],
      dealerid: "",
      isLoadingForDealer: false,
      dealerAlertMsg: false,

      //docs
      DocsData: [],
      isLoadingForDocs: false,
      docAlertMsg: false,

      //modals
      showModal: false,
      imageurl: "",
      //map
      center: {
        lat: 24.733952,
        lng: 46.801369,
      },
      zoom: 11
    };
  }

  componentWillMount() {
    if (this.props.history.location.state !== undefined) {
      this.setState({
        dealerid: this.props.location.state.DealerID
      });
    } else {
      this.props.history.push("/dealers");
    }
  }

  componentDidMount() {
    this.getDealerData();
    this.getDocsData();
  }

  getDealerData = () => {
    this.setState({ isLoadingForDealer: true });
    const apiUrlDealer = `${apiBaseURL}/dealer?id=${this.state.dealerid}`;
    axios
      .get(apiUrlDealer)
      .then(res => {
        if (res.data.length > 0) {
          this.setState({
            DealerData: res.data,
            isLoadingForDealer: false,
            dealerAlertMsg: false
          });
        } else {
          this.setState({
            isLoadingForDealer: false,
            dealerAlertMsg: true
          });
        }
      })
      .catch(error => {
        this.setState({
          isLoadingForDealer: false,
          dealerAlertMsg: true
        });
        console.log(error, "from dealer api");
      });
  };

  getDocsData = () => {
    this.setState({ isLoadingForDocs: true });
    const apiUrlDocs = `${apiBaseURL}/DealerDocument/GetDealerDocumentByDealerID?dealerid=${this.state.dealerid}`;
    axios
      .get(apiUrlDocs)
      .then(res => {
        if (res.data.length > 0) {
          this.setState({
            DocsData: res.data,
            isLoadingForDocs: false,
            docAlertMsg: false
          });
        } else {
          this.setState({
            isLoadingForDocs: false,
            docAlertMsg: true
          });
        }
      })
      .catch(error => {
        this.setState({
          isLoadingForDocs: false,
          docAlertMsg: true
        });
        console.log(error, "from docs api");
      });
  };

  //Gallery
  handleCloseModal = () => {
    this.setState({
      showModal: false
    });
  };
  handleShowModal = imageurl => {
    this.setState({
      imageurl: imageurl,
      showModal: true
    });
  };

  modalGallery = () => {
    return (
      <Modal
        size="lg"
        centered
        show={this.state.showModal}
        onHide={this.handleCloseModal}
      >
        <Modal.Header closeButton></Modal.Header>
        <Modal.Body>
          <img className="img-fluid" src={this.state.imageurl} alt="Foto" />
        </Modal.Body>
      </Modal>
    );
  };

  showDealerData = (dealer, index) => {
    return (
      <Fragment key={index}>
        <div className="b-dealers-info__header">
          <div className="row justify-content-between align-items-center">
            <div className="col">
              <h2 className="b-dealers-info__title ui-title-inner">
                {dealer.dealerName}
              </h2>
            </div>
            <div className="col-auto">
              <span
                className="b-dealers-info__brand b-dealers__brand"
                style={{ textAlign: "center" }}
              >
                <img
                  className="b-dealers-info__brand-img img-fluid"
                  src={dealer.dealerLogoURL}
                  alt="brand"
                  style={{ height: "55px" }}
                />
              </span>
            </div>
          </div>
        </div>
        <div className="entry-media">
          <img
            style={{ cursor: "pointer" }}
            onClick={() => this.handleShowModal(DEALER_MAIN)}
            className="img-fluid"
            src={DEALER_MAIN}
            alt="Foto"
          />
        </div>
        <div className="b-dealers-info__desrip">
          <h2 className="ui-title-sm">About Us</h2>
          <p>{dealer.AboutUs}</p>
        </div>

        <div className="row mt-5">

          <div className="b-dealers-info__contacts col-6" style={{padding: '0 10px 0 0'}}>
            <div className="b-dealers-info__contacts-item">
              <i className="ic icon-location-pin text-primary" />
              <div className="b-dealers-info__contacts-title mb-1">
                Location
              </div>
              <div className="b-dealers-info__contacts-info">
                {dealer.addressLine1} , {dealer.addressLine2} ,{" "}
                {dealer.cityName}
              </div>
            </div>
            <div className="b-dealers-info__contacts-item">
              <i className="ic icon-call-in text-primary" />
              <div className="b-dealers-info__contacts-title mb-1">
                Dealer Phone
              </div>
              <div className="b-dealers-info__contacts-info">
                {dealer.contactNumber}
              </div>
            </div>
          </div>

          <div className="col-6" style={{padding: '0 0 0 10px'}}>
            <div style={{ height: '280px', width: '100%' }}>
              <GoogleMapReact
                bootstrapURLKeys={{ key: 'AIzaSyCrkjlpYYhjaMFq4i6CQrznyw7x8zmPFH0' }}
                defaultCenter={this.state.center}
                defaultZoom={this.state.zoom}
              >
                <AnyReactComponent
                  lat={24.733952}
                  lng={46.801369}
                />
              </GoogleMapReact>
            </div>
          </div>
        </div>

      </Fragment>
    );
  };

  showDocsData = (doc, index) => {
    return (
      <Fragment key={index}>
        <div className="col-12 col-lg-3" style={{ paddingLeft: 0 }}>
          <div className="entry-media">
            <img
              style={{ cursor: "pointer" }}
              onClick={() => this.handleShowModal(doc.dealerDocURL)}
              className="img-fluid img-thumbnail"
              src={doc.dealerDocURL}
              alt="Foto"
            />
          </div>
        </div>
      </Fragment>
    );
  };
  render() {
    const { DealerData, DocsData } = this.state;
    return (
      <Fragment>
        {this.modalGallery()}
        {this.state.dealerAlertMsg === true ? (
          <ErrorAlert show={this.state.dealerAlertMsg} />
        ) : this.state.isLoadingForDealer ? (
          <Loader />
        ) : (
          DealerData.map((dealer, index) => this.showDealerData(dealer, index))
        )}

        <div className="b-dealers-info__desrip mb-3">
          <h2 className="ui-title-sm">Documents</h2>
        </div>

        {this.state.docAlertMsg === true ? (
          <ErrorAlert show={this.state.docAlertMsg} />
        ) : this.state.isLoadingForDocs ? (
          <Loader />
        ) : (
          <div className="row">
            {DocsData.map((doc, index) => this.showDocsData(doc, index))}
          </div>
        )}
      </Fragment>
    );
  }
}

export default Dealerdetailmain;