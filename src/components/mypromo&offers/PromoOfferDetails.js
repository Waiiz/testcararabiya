import React, { Component, Fragment } from "react";
import { apiBaseURL } from "../../ApiBaseURL";
import axios from "axios";
import ReactPaginate from "react-paginate";
import "../listing/paginate.css";
import _ from "lodash";
import { Link } from "react-router-dom";
import Loader from "../../pages/Loader";
import ErrorAlert from "../../pages/ErrorAlert";
import Switch from "react-switch";

class PromoOfferDetails extends Component {
  constructor(props) {
    super(props);
    this.isTotalAdminSummaryLoading = false;
    this.simpleWatermarkImg = "./assets/media/watermark.png";
    this.isFetchingDataforEnablePromo = false;

    this.usertypeid = "";
    this.branchid = "";
    this.userid = "";
    this.state = {
      TotalAdminSummaryData: [],
      //promolisting
      isLoadingForPromoListing: false,
      PromolistingData: [],
      GlobalPromolistingData: [],
      //pagination
      offset: 0,
      perPage: 7,
      currentPage: 0,
      listingData: [],
      toggleAlertMsg: false,

      start: "",
      end: "",
      totalResults: "",

      //sortby
      SortPromoListing: [
        { id: 1, value: "Newest to Oldest", method: "new" },
        { id: 2, value: "Oldest to Newest", method: "old" }
      ],
      selSortPromoValue: ""
    };
  }

  componentDidMount() {
    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      this.userid = parseUserData[0].UserID;
      this.usertypeid = parseUserData[0].UserTypeID;
      this.branchid = parseUserData[0].BranchID;
      this.getDealerID(this.userid);
    }

    this.getTotalAdminSummary(this.branchid);
  }

  getDealerID = (userid) => {
    this.setState({ isLoadingForPromoListing: true });
    const url = `${apiBaseURL}/user?UserID=${userid}`;
    axios
      .get(url)
      .then((res) => {
        console.log(res.data);
        this.setState({
          userData: res.data,
        });
        this.getPromoAndOffer(res.data[0].RoleName, res.data[0].CARegistrationID, userid);
      })
      .catch((error) => {
        console.log(error, "userdata api");
      });
  }

  getPromoAndOffer = (rolename, dealerid, userid) => {
    let url = '';
    if (rolename === "Owner") {
      url = `${apiBaseURL}/PromoandOffer/PromoByDealerID?DealerID=${dealerid}`;
    }
    else {
      url = `${apiBaseURL}/PromoAndOffer/PromoByUserID?UserID=${userid}`;
    }
    axios
      .get(url)
      .then(res => {
        console.log(res, "PromoAndOffer");
        this.setState({
          GlobalPromolistingData: res.data
        });
        this.setState({ isLoadingForPromoListing: false });
        this.receivedData(res.data);
      })
      .catch(error => {
        this.setState({
          start: 0,
          end: 0,
          offset: 0,
          perPage: 7,
          currentPage: 0,
          totalResults: 0,
          toggleAlertMsg: true,
          listingData: [],
          isLoadingForPromoListing: false
        });
        console.log(error, "from PromoAndOffer api");
      });
  }

  getTotalAdminSummary = branchid => {
    if (this.isTotalAdminSummaryLoading) {
      return;
    }
    this.isTotalAdminSummaryLoading = true;

    const url = `${apiBaseURL}/TotalAdminSummary/TotalPromoAdminBranch?BranchID=${branchid}`;
    axios
      .get(url)
      .then(res => {
        if (res.data.length > 0) {
          this.isTotalAdminSummaryLoading = false;
          this.setState({
            TotalAdminSummaryData: res.data
          });
        } else {
          console.log(res.data, "res.data");
        }
      })
      .catch(error => {
        this.isTotalAdminSummaryLoading = false;
        console.log(error, "TotalAdminSummaryData api");
      });
  };

  receivedData = promolisting => {
    if (promolisting.length > 0) {
      this.setState({
        PromolistingData: promolisting,
        totalResults: promolisting.length,
        toggleAlertMsg: false
      });

      const data = promolisting;

      if (this.state.offset + 1 + this.state.perPage > promolisting.length) {
        this.setState({
          end: promolisting.length
        });
      } else {
        this.setState({
          end: this.state.offset + this.state.perPage
        });
      }

      const slice = data.slice(
        this.state.offset,
        this.state.offset + this.state.perPage
      );

      const listingData = slice.map((promolist, index) =>
        this.showPromoListing(promolist, index)
      );

      this.setState({
        pageCount: Math.ceil(data.length / this.state.perPage),
        start: this.state.offset + 1,
        listingData
      });
    } else {
      this.setState({
        start: 0,
        end: 0,
        offset: 0,
        perPage: 7,
        currentPage: 0,
        totalResults: 0,
        toggleAlertMsg: true,
        listingData: []
      });
    }
  };

  extractDate = datetime => {
    let splitTimeDateWithT = datetime.split("T");
    var date = splitTimeDateWithT[0];
    return date;
  };

  //handle Change Enable IsActive  listadpost.IsActive,listadpost.AdPostID
  handleEn_Ds_Promo = (promolist, index) => {
    var promochecked = !promolist.IsActive;
    promolist.IsActive = promochecked;

    if (!this.isFetchingDataforEnablePromo) {
      this.UpdateIsActivebyPromotionID(promolist, index);
    }
  };

  UpdateIsActivebyPromotionID = (promolist, index) => {
    this.isFetchingDataforEnablePromo = true;

    const url = `${apiBaseURL}/PromoAndOffer/En_Ds_Promo`;
    let ApiParamForIsActivePromo = {
      PromotionID: promolist.PromotionID,
      isActive: promolist.IsActive
    };
    axios
      .post(url, null, { params: ApiParamForIsActivePromo })
      .then(res => {
        this.isFetchingDataforEnablePromo = false;
        console.log(res, "UpdateIsActivebyPromotionID api");

        if (res.status === 200) {
          var mylistingpromodata = this.state.PromolistingData;

          var getIndex = mylistingpromodata.indexOf(mylistingpromodata[index]);

          if (getIndex > -1) {
            mylistingpromodata.splice(getIndex, 1, promolist);
          }

          this.setState({
            PromolistingData: mylistingpromodata
          });

          this.receivedData(this.state.PromolistingData);
          this.getTotalAdminSummary(this.branchid);
        } else {
          this.isFetchingDataforEnablePromo = false;
          console.log(
            res.status,
            "statuscode from UpdateIsActivebyPromotionID api"
          );
        }
      })
      .catch(error => {
        this.isFetchingDataforEnablePromo = false;
        console.log(error, "UpdateIsActivebyPromotionID api");
      });
  };

  showPromoListing = (promolist, index) => {
    return (
      <div
        key={index}
        className="bg-shadow b-goods-f col-12 b-box row mx-0 mt-4"
      >
        <div className="col-12 col-lg-3">
          <div className="b-goods-f__listing mb-2">
            <Link to="/">
              <img
                style={{
                  width: "140px",
                  height: "150px",
                  objectFit: "unset"
                }}
                className="b-goods-f__img img-scale"
                src={promolist.ImageUrl}
                alt={promolist.PromotionID}
              />
            </Link>
          </div>
        </div>
        <div className="col-12 col-lg-7">
          <div className="b-goods-f__descrip">
            <div
              className="b-goods-f__info mt-2"
              style={{
                display: "block",
                backgroundColor: "#f7f3db",
                padding: "5px"
              }}
            >
              <Link
                to="/"
                style={{ color: "#555", fontSize: "1.1em", fontWeight: 600 }}
              >
                {promolist.PromotionName}
              </Link>
            </div>
            <div
              className="b-goods-f__info mt-2"
              style={{
                display: "block",
                fontSize: "0.9em",
                backgroundColor: "white",
                padding: "5px"
              }}
            >
              {promolist.Description}
            </div>
            <div
              style={{
                display: "block",
                color: "#555",
                fontSize: "0.9em",
                fontWeight: 400,
                backgroundColor: "#f3f3f3",
                padding: "5px"
              }}
            >
              <span className="pr-3">
                <b>URL :</b>
              </span>{" "}
              <span>
                {" "}
                <Link to={promolist.Ext_URL} className="special">
                  {promolist.Ext_URL}
                </Link>
              </span>
            </div>
          </div>
        </div>

        <div
          className="col-12 col-lg-2 justify-content-end align-items-center align-self-center"
          style={{ marginBottom: "20px" }}
        >
          <span className="b-goods-f__price d-flex pt-2">
            <div className="mr-4">
              <i style={{ color: "#D3E428" }} className="fas fa-envelope"></i>
              <span className="likeicon">
                {" "}
                <strong> {promolist.ViewCount} </strong>
              </span>
            </div>
            <div className="mr-4">
              <i style={{ color: "#D3E428" }} className="far fa-thumbs-up"></i>
              <span className="likeicon">
                {" "}
                <strong> {promolist.LikeCount} </strong>
              </span>
            </div>
            <div className="">
              <i
                style={{ color: "#D3E428" }}
                className="far fa-thumbs-down"
              ></i>
              <span className="likeicon">
                {" "}
                <strong> {promolist.DislikeCount} </strong>
              </span>
            </div>
          </span>

          <label style={{ marginTop: "1rem" }}>
            <Switch
              value={promolist.IsActive}
              onChange={() => this.handleEn_Ds_Promo(promolist, index)}
              checked={promolist.IsActive}
              onColor="#eee"
              onHandleColor="#D3E428"
              handleDiameter={20}
              uncheckedIcon={false}
              checkedIcon={false}
              boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
              activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
              height={20}
              width={40}
              className="react-switch"
              id="material-switch"
            />

            <span
              style={{
                position: "relative",
                bottom: "5px",
                left: "5px"
              }}
            >
              {promolist.IsActive === true ? "Enable" : "Disable"}
            </span>
          </label>

          <div className="mt-3">
            <Link
              to={{
                pathname: "/edit-promotion",
                state: { PromotionID: promolist.PromotionID },
              }}
              className="btn btn-primary btn-sm btn-block color-arabiya"
              style={{ backgroundColor: "#D3E428" }}
            >
              {" "}
              Edit{" "}
            </Link>
          </div>
        </div>
        <div
          className="col-12 px-0 mt-2 d-flex justify-content-end"
          style={{ borderTop: "1px solid #D3E428", paddingTop: "5px" }}
        >
          <div className="ml-2" style={{ color: "grey", fontSize: "0.9em" }}>
            <span className="ml-2">
              <b>Date created:</b> {this.extractDate(promolist.CreateDate)}
            </span>

            <span className="ml-2">
              <b>Valid till:</b> {this.extractDate(promolist.Valid_Till)}
            </span>

            <span className="ml-2">
              <b>Status:</b>{" "}
              {promolist.IsActive === true ? "Active" : "Inactive"}
            </span>
          </div>
        </div>
      </div>
    );
  };

  handlePageClick = e => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.perPage;

    this.setState(
      {
        currentPage: selectedPage,
        offset: offset
      },
      () => {
        this.receivedData(this.state.PromolistingData);
      }
    );
  };

  showTotalAdminSummary = () => {
    return (
      <Fragment>
        <aside className="l-sidebar">
          <div className="widget section-sidebar">
            <div className="widget-content">
              <div className="d-flex justify-content-between px-3">
                <Link
                  style={{ color: "#D3E428", fontWeight: "700" }}
                  to="/create-promotion"
                >
                  Create New
                </Link>
              </div>
              <hr />
              {this.state.TotalAdminSummaryData.map(
                (totaladminsummary, index) => (
                  <Fragment key={index}>
                    <div className="d-flex justify-content-between px-3">
                      <strong>Active Promotions</strong>
                      <span>{totaladminsummary.Active}</span>
                    </div>
                    <hr />
                    <div className="d-flex justify-content-between px-3">
                      <strong>Disabled Promotions</strong>
                      <span>{totaladminsummary.Inactive}</span>
                    </div>
                    <hr />
                  </Fragment>
                )
              )}
            </div>
          </div>
        </aside>
      </Fragment>
    );
  };

  showPromolistHeader = () => {
    return (
      <div className="row">
        <div className="col-8 px-0">
          <div className="list-heading">Promotions & Offers</div>
          <div className="b-filter-goods__info col-auto">
            Showing
            <strong>
              {" "}
              {this.state.start} - {this.state.end}{" "}
            </strong>{" "}
            out of <strong> {this.state.totalResults}</strong> listings
          </div>
        </div>

        <div className="col-4">
          <div
            className="b-filter-goods__wrap col-auto"
            style={{ float: "right", padding: 0 }}
          >
            <div className="b-filter-goods__select d-inline-flex">
              <select
                style={{ inlineSize: "auto" }}
                className="custom-select-adpost"
                onChange={this.handleSortByPromoListing}
                value={this.state.selSortPromoValue}
              >
                <option selected>Sort By</option>
                {this.state.SortPromoListing.map(sortpromo => (
                  <option key={sortpromo.id} value={sortpromo.method}>
                    {sortpromo.value}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
      </div>
    );
  };

  //sortby
  handleSortByPromoListing = event => {
    this.setState({ selSortPromoValue: event.target.value });
    let PromoData = [];
    PromoData = this.state.GlobalPromolistingData;

    if (event.target.value === "new") {
      PromoData = _.orderBy(PromoData, ["CreateDate"], ["desc"]);
    } else if (event.target.value === "old") {
      PromoData = _.orderBy(PromoData, ["CreateDate"], ["asc"]);
    } else {
      console.log("sort error");
    }

    this.setState({ PromolistingData: PromoData });
    this.receivedData(PromoData);
  };

  render() {
    return (
      <Fragment>
        <div className="row">
          <div className="col-lg-3">{this.showTotalAdminSummary()}</div>

          <div className="col-lg-9">
            {this.showPromolistHeader()}
            <br />

            {this.state.toggleAlertMsg === true ? (
              <ErrorAlert show={this.state.toggleAlertMsg} />
            ) : (
                <Fragment>
                  {this.state.isLoadingForPromoListing ? (
                    <Loader />
                  ) : (
                      <span>
                        {this.state.listingData}
                        <br />
                        <ReactPaginate
                          previousLabel={"prev"}
                          nextLabel={"next"}
                          breakLabel={"..."}
                          breakClassName={"break-me"}
                          pageCount={this.state.pageCount}
                          marginPagesDisplayed={2}
                          pageRangeDisplayed={5}
                          onPageChange={this.handlePageClick}
                          containerClassName={"pagination"}
                          subContainerClassName={"pages pagination"}
                          activeClassName={"active"}
                          forcePage={this.state.currentPage}
                        />
                      </span>
                    )}
                </Fragment>
              )}
          </div>
        </div>
      </Fragment>
    );
  }
}
export default PromoOfferDetails;