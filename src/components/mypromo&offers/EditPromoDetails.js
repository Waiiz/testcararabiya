import React, { Component, Fragment } from "react";
import { apiBaseURL } from "../../ApiBaseURL";
import axios from "axios";
import { Link } from "react-router-dom";
import { apiUrlcity } from "../../ApiBaseURL";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class EditPromoDetails extends Component {
  constructor(props) {
    super(props);
    this.isTotalAdminSummaryLoading = false;
    this.usertypeid = "";
    this.branchid = "";
    this.userid = "";
    this.ispromoandofferloading = false;
    this.state = {
      TotalAdminSummaryData: [],
      promotitle: "",
      description: "",
      tags: "",
      //city
      lcity: [],
      scity: "",
      startDate: null,
      firstStartDate: null,
      url: "",
      // Initially, no file is selected
      selectedFile: null,
      //edit
      PromotionID: "",
      PromoDataById: [],
      docTypebyapi: "",
      filenamebyapi: "",
    };
  }

  componentDidMount() {
    if (this.props.history.location.state !== undefined) {
      this.setState({
        PromotionID: this.props.history.location.state.PromotionID,
      });
      this.getPromoAndOfferById(this.props.history.location.state.PromotionID);
    } else {
      this.props.history.push("./dealerofferpromotions");
    }

    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      this.userid = parseUserData[0].UserID;
      this.branchid = parseUserData[0].BranchID;

      let getfirstusertypechar = parseUserData[0].UserTypeID.toString();
      this.usertypeid = getfirstusertypechar.substring(0, 1);
    }

    //city
    fetch(apiUrlcity)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lcity: result,
            });
          } else {
            console.log("city api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    this.getTotalAdminSummary(this.branchid);
  }

  getPromoAndOfferById = (promotionid) => {
    if (this.ispromoandofferloading) {
      return;
    }
    this.ispromoandofferloading = true;

    const url = `${apiBaseURL}/promoandoffer?id=${promotionid}`;
    axios
      .get(url)
      .then((res) => {
        this.ispromoandofferloading = false;
        var splitTimeDateWithT = res.data[0].Valid_Till.split("T");
        var date = splitTimeDateWithT[0]; //T00:00:00

        var str = res.data[0].ImageUrl;
        var test = str.split("/");

        this.setState({
          PromoDataById: res.data,
          promotitle: res.data[0].PromotionName,
          description: res.data[0].Description,
          tags: res.data[0].SearchTags,
          scity: res.data[0].PromoCities,
          firstStartDate: date,
          url: res.data[0].Ext_URL,
          docTypebyapi: res.data[0].docType,
          filenamebyapi: test.slice(-1)[0],
          selectedFile: null,
        });
      })
      .catch((error) => {
        this.ispromoandofferloading = false;
        console.log(error, "promoandoffer api");
      });
  };

  getTotalAdminSummary = (branchid) => {
    if (this.isTotalAdminSummaryLoading) {
      return;
    }
    this.isTotalAdminSummaryLoading = true;

    const url = `${apiBaseURL}/TotalAdminSummary/TotalPromoAdminBranch?BranchID=${branchid}`;
    axios
      .get(url)
      .then((res) => {
        if (res.data.length > 0) {
          this.isTotalAdminSummaryLoading = false;
          this.setState({
            TotalAdminSummaryData: res.data,
          });
        } else {
          console.log(res.data, "res.data");
        }
      })
      .catch((error) => {
        this.isTotalAdminSummaryLoading = false;
        console.log(error, "TotalAdminSummaryData api");
      });
  };

  handlePromoTitleChange = (event) => {
    this.setState({ promotitle: event.target.value });
  };

  handleDescriptionChange = (event) => {
    this.setState({ description: event.target.value });
  };

  handleTagsChange = (event) => {
    this.setState({ tags: event.target.value });
  };

  handleCityChange = (event) => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ scity: event.target.value });
  };

  handleValidDateChange = (date) => {
    var newdate = this.dateMaker(date);
    this.setState({
      firstStartDate: newdate,
      startDate: date,
    });
  };

  //Date Maker Function
  dateMaker = (date) => {
    if (date === null || date === "") {
      return;
    }
    var dateInString = date.toString();
    var dateInSplit = dateInString.split("GMT");
    var day = ("0" + date.getDate()).slice(-2);
    var monthIndex = ("0" + (date.getMonth() + 1)).slice(-2);
    var year = date.getFullYear();

    var newDate = year + "-" + monthIndex + "-" + day;
    return newDate;
  };

  handleUrlChange = (event) => {
    this.setState({ url: event.target.value });
  };

  // On file select (from the pop up)
  onFileChange = (event) => {
    // Update the state
    this.setState({
      selectedFile: event.target.files[0],
    });
  };

  showEditPromo = () => {
    return (
      <div className="row">
        <div className="col-12 px-0">
          <div className="list-heading">Edit Promotion</div>
        </div>

        <div className="logmod__form mt-4" style={{ width: "100%" }}>
          <form onSubmit={this.handleEditPromotion}>
            <div className="sminputs">
              <div className="input full">
                <label>Promotion Title</label>

                <input
                  className="string optional"
                  placeholder="Promotion Title"
                  required
                  type="text"
                  onChange={this.handlePromoTitleChange}
                  value={this.state.promotitle}
                />
              </div>
            </div>

            <div className="sminputs">
              <div className="input full" style={{ height: "initial" }}>
                <label>Description</label>

                <textarea
                  className="form-control"
                  cols="30"
                  placeholder="Enter Description"
                  required
                  rows="5"
                  type="text"
                  onChange={this.handleDescriptionChange}
                  value={this.state.description}
                ></textarea>
              </div>
            </div>

            <div className="sminputs">
              <div className="input string optional">
                <label>Tags</label>

                <input
                  className="string optional"
                  placeholder="tags"
                  required
                  type="text"
                  onChange={this.handleTagsChange}
                  value={this.state.tags}
                />
              </div>
            </div>

            <div className="sminputs">
              <div className="input" style={{ width: "100%" }}>
                <label>City</label>
                <select
                  required
                  style={{
                    width: "100%",
                    background: "none",
                    border: "none",
                    marginLeft: "-13px",
                  }}
                  onChange={this.handleCityChange}
                  value={this.state.scity}
                >
                  <option value="" selected>
                    Select City
                  </option>
                  {this.state.lcity.map((city) => (
                    <option key={city.CityID} value={city.CityName}>
                      {city.CityName}
                    </option>
                  ))}
                </select>
              </div>
            </div>

            <div className="sminputs">
              <div className="input" style={{ width: "100%" }}>
                <label>Valid Till</label>
                <div className="d-flex">
                  <div className="form-group" style={{ marginLeft: "-15px" }}>
                    <div className="string optional ml-3">
                      <DatePicker
                        value={this.state.firstStartDate}
                        minDate={new Date()}
                        showDisabledMonthNavigation
                        selected={this.state.startDate}
                        onChange={this.handleValidDateChange}
                        peekNextMonth
                        showMonthDropdown
                        showYearDropdown
                        dropdownMode="select"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="sminputs">
              <div className="input full">
                <label>URL</label>

                <input
                  className="string optional"
                  placeholder="First URL"
                  required
                  type="url"
                  onChange={this.handleUrlChange}
                  value={this.state.url}
                />
              </div>
            </div>

            <div className="row mt-3">
              <div className="col-6 col-lg-6 col-sm-12">
                <label
                  for="file-upload"
                  className="custom-file-upload"
                  style={{ width: "100%", padding: "12px 15px" }}
                >
                  Upload Media
                </label>
                <input
                  id="file-upload"
                  onChange={this.onFileChange}
                  type="file"
                />
              </div>

              <div className="col-6 col-lg-6 col-sm-12">
                <button
                  type="submit"
                  className="btn btn-primary color-cararabiya btn-block"
                  style={{ backgroundColor: "#D3E428" }}
                >
                  Edit
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  };

  handleEditPromotion = (event) => {
    event.preventDefault();
    let {
      PromotionID,
      promotitle,
      description,
      url,
      firstStartDate,
      tags,
      scity,
      selectedFile,
      docTypebyapi,
      filenamebyapi,
    } = this.state;

    if (selectedFile !== null) {
      var splitfilename = selectedFile.name.split(".");
      var doctype = "." + splitfilename[1];
    }

    let imageurl =
      "https://cararabiya.ahalfa.com/Content/Uploads/Dealer/Branch/" +
      this.branchid +
      "/Promotion/";
    console.log(
      PromotionID,
      promotitle,
      description,
      url,
      firstStartDate,
      tags,
      scity,
      selectedFile,
      doctype,
      this.userid,
      this.usertypeid,
      this.branchid,
      imageurl
    );

    //Create Promotion And Offer of branch admin
    const urlPromoAndOffer = `${apiBaseURL}/PromoAndOffer/UpdatePromoAndOffer`;
    let ApiParamForUpdatePromoAndOffer = {
      PromotionID: PromotionID,
      PromotionName: promotitle,
      Description: description,
      Ext_URL: url,
      Valid_Till: firstStartDate,
      ImageUpload: true,
      SearchTags: tags,
      ImageUrl: imageurl,
      docType: this.state.selectedFile === null ? docTypebyapi : doctype,
      fileName:
        this.state.selectedFile === null ? filenamebyapi : selectedFile.name,
      PromoCities: scity,
    };
    axios
      .post(urlPromoAndOffer, ApiParamForUpdatePromoAndOffer)
      .then((res) => {
        console.log(res.data, "PromoAndOffer");

        if (res.status === 200) {
          if (this.state.selectedFile !== null) {
            this.handleUploadPromoImgApi(PromotionID);
          } else {
            this.props.history.push({
              pathname: "/dealerofferpromotions",
            });
          }
        } else {
          console.log(
            res.status,
            res.statusText,
            "statuscode from PromoAndOffer api"
          );
        }
      })
      .catch((error) => {
        console.log(error, "from PromoAndOffer api");
      });
  };

  handleUploadPromoImgApi = (promotionid) => {
    // Create an object of formData
    let data = new FormData();

    // Update the formData object
    data.append("PromotionID", promotionid);
    data.append("branchID", this.branchid);
    data.append("PromoURL", this.state.selectedFile);

    const url = `${apiBaseURL}/file/UploadPromoImg`;

    axios
      .post(url, data)
      .then((res) => {
        console.log(res, "res from UploadPromoImg api");

        if (res.status === 200) {
          this.props.history.push({
            pathname: "/dealerofferpromotions",
          });
        } else {
          console.log(res.status, "statuscode from UploadPromoImg api");
        }
      })
      .catch((error) => {
        console.log(error, "from UploadPromoImg api");
      });
  };

  showTotalAdminSummary = () => {
    return (
      <Fragment>
        <aside className="l-sidebar">
          <div className="widget section-sidebar">
            <div className="widget-content">
              <div className="d-flex justify-content-between px-3">
                <Link
                  style={{ color: "#D3E428", fontWeight: "700" }}
                  to="/dealerofferpromotions"
                >
                  View Promotions
                </Link>
              </div>
              <hr />
              {this.state.TotalAdminSummaryData.map(
                (totaladminsummary, index) => (
                  <Fragment key={index}>
                    <div className="d-flex justify-content-between px-3">
                      <strong>Active Promotions</strong>
                      <span>{totaladminsummary.Active}</span>
                    </div>
                    <hr />
                    <div className="d-flex justify-content-between px-3">
                      <strong>Disabled Promotions</strong>
                      <span>{totaladminsummary.Inactive}</span>
                    </div>
                    <hr />
                  </Fragment>
                )
              )}
            </div>
          </div>
        </aside>
      </Fragment>
    );
  };

  render() {
    return (
      <Fragment>
        <div className="row">
          <div className="col-lg-3">{this.showTotalAdminSummary()}</div>

          <div className="col-lg-9">{this.showEditPromo()}</div>
        </div>
      </Fragment>
    );
  }
}
export default EditPromoDetails;