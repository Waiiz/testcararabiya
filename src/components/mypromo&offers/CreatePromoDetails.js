import React, { Component, Fragment } from "react";
import { apiBaseURL } from "../../ApiBaseURL";
import axios from "axios";
import "../listing/paginate.css";
import _ from "lodash";
import { Link } from "react-router-dom";
import { apiUrlcity } from "../../ApiBaseURL";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class CreatePromoDetails extends Component {
  constructor(props) {
    super(props);
    this.isTotalAdminSummaryLoading = false;
    this.usertypeid = "";
    this.branchid = "";
    this.userid = "";
    this.state = {
      TotalAdminSummaryData: [],
      promotitle: "",
      description: "",
      tags: "",
      //city
      lcity: [],
      scity: "",
      startDate: new Date(),
      url: "",
      // Initially, no file is selected
      selectedFile: null,
      filemessage: '',
      PromotionID: '',
    };
  }

  componentDidMount() {
    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      this.userid = parseUserData[0].UserID;
      this.branchid = parseUserData[0].BranchID;

      let getfirstusertypechar = (parseUserData[0].UserTypeID).toString();
      this.usertypeid = getfirstusertypechar.substring(0, 1);
    }
    
    //city
    fetch(apiUrlcity)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
          this.setState({
            lcity: result
          });
        } else {
          console.log("Listing");
        }
        },
        error => {
          this.setState({ error });
        }
      );

    this.getTotalAdminSummary(this.branchid);
  }

  getTotalAdminSummary = branchid => {
    if (this.isTotalAdminSummaryLoading) {
      return;
    }
    this.isTotalAdminSummaryLoading = true;

    const url = `${apiBaseURL}/TotalAdminSummary/TotalPromoAdminBranch?BranchID=${branchid}`;
    axios
      .get(url)
      .then(res => {
        if (res.data.length > 0) {
          this.isTotalAdminSummaryLoading = false;
          this.setState({
            TotalAdminSummaryData: res.data
          });
        } else {
          console.log(res.data, "res.data");
        }
      })
      .catch(error => {
        this.isTotalAdminSummaryLoading = false;
        console.log(error, "TotalAdminSummaryData api");
      });
  };

  handlePromoTitleChange = event => {
    this.setState({ promotitle: event.target.value });
  };

  handleDescriptionChange = event => {
    this.setState({ description: event.target.value });
  };

  handleTagsChange = event => {
    this.setState({ tags: event.target.value });
  };

  handleCityChange = event => {
    if (event.target.value === "") {
      return;
    }
    this.setState({ scity: event.target.value });
  };

  handleValidDateChange = date => {
    this.setState({
      startDate: date
    });
  };

  //Date Maker Function
  dateMaker = date => {
    if (date === null || date === "") {
      return;
    }
    var dateInString = date.toString();
    var dateInSplit = dateInString.split("GMT");
    var day = ("0" + date.getDate()).slice(-2);
    var monthIndex = ("0" + (date.getMonth() + 1)).slice(-2);
    var year = date.getFullYear();

    var newDate = year + "-" + monthIndex + "-" + day + "T00:00:00";
    return newDate;
  };

  handleUrlChange = event => {
    this.setState({ url: event.target.value });
  };

  // On file select (from the pop up)
  onFileChange = event => {
    // Update the state
    this.setState({ 
      selectedFile: event.target.files[0],
      filemessage: ''
     });
  };

  showCreatePromo = () => {
    return (
      <div className="row">
        <div className="col-12 px-0">
          <div className="list-heading">Create Promotion</div>
        </div>

        <div className="logmod__form mt-4" style={{ width: "100%" }}>
          <form onSubmit={this.handleCreatePromotion}>
            <div className="sminputs">
              <div className="input full">
                <label>Promotion Title</label>

                <input
                  className="string optional"
                  placeholder="Promotion Title"
                  required
                  type="text"
                  onChange={this.handlePromoTitleChange}
                  value={this.state.promotitle}
                />
              </div>
            </div>

            <div className="sminputs">
              <div className="input full" style={{ height: "initial" }}>
                <label>Description</label>

                <textarea
                  className="form-control"
                  cols="30"
                  placeholder="Enter Description"
                  required
                  rows="5"
                  type="text"
                  onChange={this.handleDescriptionChange}
                  value={this.state.description}
                ></textarea>
              </div>
            </div>

            <div className="sminputs">
              <div className="input string optional">
                <label>Tags</label>

                <input
                  className="string optional"
                  placeholder="tags"
                  required
                  type="text"
                  onChange={this.handleTagsChange}
                  value={this.state.tags}
                />
              </div>
            </div>

            <div className="sminputs">
              <div className="input" style={{ width: "100%" }}>
                <label>City</label>
                <select
                  required
                  style={{
                    width: "100%",
                    background: "none",
                    border: "none",
                    marginLeft: "-13px"
                  }}
                  onChange={this.handleCityChange}
                  value={this.state.scity}
                >
                  <option value="" selected>
                    Select City
                  </option>
                  {this.state.lcity.map(city => (
                    <option key={city.CityID} value={city.CityName}>
                      {city.CityName}
                    </option>
                  ))}
                </select>
              </div>
            </div>

            <div className="sminputs">
              <div className="input" style={{ width: "100%" }}>
                <label>Valid Till</label>
                <div className="d-flex">
                  <div className="form-group" style={{ marginLeft: "-15px" }}>
                    <div className="string optional ml-3">
                      <DatePicker
                        minDate={new Date()}
                        showDisabledMonthNavigation
                        selected={this.state.startDate}
                        onChange={this.handleValidDateChange}
                        peekNextMonth
                        showMonthDropdown
                        showYearDropdown
                        dropdownMode="select"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="sminputs">
              <div className="input full">
                <label>URL</label>

                <input
                  className="string optional"
                  placeholder="First URL"
                  required
                  type="url"
                  onChange={this.handleUrlChange}
                  value={this.state.url}
                />
              </div>
            </div>

            <div className="row mt-3">
              <div className="col-6 col-lg-6 col-sm-12">
                <label
                  for="file-upload"
                  className="custom-file-upload"
                  style={{ width: "100%", padding: "12px 15px" }}
                >
                  Upload Media
                </label>
                <input
                  id="file-upload"
                  onChange={this.onFileChange}
                  type="file"
                />
                <span style={{fontWeight: 500,color: 'red'}}>{this.state.filemessage}</span>
              </div>

              <div className="col-6 col-lg-6 col-sm-12">
                <button
                  type="submit"
                  className="btn btn-primary color-cararabiya btn-block"
                  style={{ backgroundColor: "#D3E428" }}
                >
                  Create
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  };

  handleCreatePromotion = event => {
    event.preventDefault();
    if(this.state.selectedFile === null){
      this.setState({
        filemessage: '* Please select any file.'
      })
      return;
    }
    
    let {
      promotitle,
      description,
      tags,
      scity,
      startDate,
      url,
      selectedFile
    } = this.state;
    let newdate = this.dateMaker(startDate);
    let splitfilename = selectedFile.name.split(".");
    let doctype = "." + splitfilename[1];
    //let imageurl = `http://cararabiya.ahalfa.com/Content/Uploads/Dealer/Branch/${this.branchid}/Promotion/`;
    let imageurl = "https://cararabiya.ahalfa.com/Content/Uploads/Dealer/Branch/" + this.branchid + "/Promotion/";
    console.log(
      promotitle,
      description,
      tags,
      scity,
      newdate,
      url,
      selectedFile,
      doctype,
      this.userid,
      this.usertypeid,
      this.branchid,
      imageurl
    );

    //Create Promotion And Offer of branch admin
    const urlPromoAndOffer = `${apiBaseURL}/PromoAndOffer`;
    let ApiParamForPromoAndOffer = {
      BranchID: this.branchid,
      docType: doctype,
      CreateDate: "2020-08-26T11:00:00.00",
      CreatedBy: this.userid,
      Description: description,
      DislikeCount: "0",
      DocumentUpload: false,
      Ext_URL: url,
      ImageUpload: true,
      IsActive: false,
      LikeCount: 0,
      PromotionName: promotitle,
      SearchTags: tags,
      UserTypeID: this.usertypeid,
      Valid_Till: newdate,
      ViewCount: 0,
      ImageUrl: imageurl,
      fileName: selectedFile.name,
      PromoCities: scity,
    };
    axios
      .post(urlPromoAndOffer, ApiParamForPromoAndOffer)
      .then(res => {
        console.log(res.data, "PromoAndOffer");

        if (res.status === 200) {
          this.setState({
            PromotionID: res.data
          });
          this.handleUploadPromoImgApi(res.data);
        } else {
          console.log(
            res.status,
            res.statusText,
            "statuscode from PromoAndOffer api"
          );
        }
      })
      .catch(error => {
        console.log(error, "from PromoAndOffer api");
      });
  };

  handleUploadPromoImgApi = (promotionid) => {
    // Create an object of formData
    let data = new FormData();
console.log(this.state.selectedFile, 'handleUploadPromoImgApi');
    // Update the formData object
    data.append("PromotionID", promotionid);
    data.append("branchID", this.branchid);
    data.append("PromoURL", this.state.selectedFile);

    const url = `${apiBaseURL}/file/UploadPromoImg`;

    axios
      .post(url, data)
      .then((res) => {
        console.log(res, "res from UploadPromoImg api");

        if (res.status === 200) {
          this.props.history.push({
            pathname: '/dealerofferpromotions',
          })
        } else {
          console.log(res.status, "statuscode from UploadPromoImg api");
        }
      })
      .catch((error) => {
        console.log(error, "from UploadPromoImg api");
      });
  };

  showTotalAdminSummary = () => {
    return (
      <Fragment>
        <aside className="l-sidebar">
          <div className="widget section-sidebar">
            <div className="widget-content">
              <div className="d-flex justify-content-between px-3">
                <Link
                  style={{ color: "#D3E428", fontWeight: "700" }}
                  to="/dealerofferpromotions"
                >
                  View Promotions
                </Link>
              </div>
              <hr />
              {this.state.TotalAdminSummaryData.map(
                (totaladminsummary, index) => (
                  <Fragment key={index}>
                    <div className="d-flex justify-content-between px-3">
                      <strong>Active Promotions</strong>
                      <span>{totaladminsummary.Active}</span>
                    </div>
                    <hr />
                    <div className="d-flex justify-content-between px-3">
                      <strong>Disabled Promotions</strong>
                      <span>{totaladminsummary.Inactive}</span>
                    </div>
                    <hr />
                  </Fragment>
                )
              )}
            </div>
          </div>
        </aside>
      </Fragment>
    );
  };

  render() {
    return (
      <Fragment>
        <div className="row">
          <div className="col-lg-3">{this.showTotalAdminSummary()}</div>

          <div className="col-lg-9">{this.showCreatePromo()}</div>
        </div>
      </Fragment>
    );
  }
}
export default CreatePromoDetails;