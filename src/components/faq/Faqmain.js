import React, { Component } from 'react';

class Faqmain extends Component {
    render() {
        return (
            <div>
                  <div className="container">
                        <div className="col-12 mt-5">
                        <section>
                            {/*Accordion wrapper*/}
                            <div className="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
                            {/* Accordion card */}
                            <div className="card" style={{borderBottom: '1px solid #99999978'}}>
                                {/* Card header */}
                                <div className="card-header" role="tab" id="headingOne1" style={{padding: '1rem 1.5rem'}}>
                                <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="false" aria-controls="collapseOne1" className="collapsed" style={{color: '#D3E428'}}>
                                    <h5 className="mb-0">
                                    Q.1: What is Cararabiya? <i className="fas fa-angle-down rotate-icon" />
                                    </h5>
                                </a>
                                </div>
                                {/* Card body */}
                                <div id="collapseOne1" className="collapse" role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx" style={{}} aria-expanded="false">
                                <div className="card-body faq" style={{fontSize: '.9rem', fontWeight: 400, lineHeight: '1.7', color: '#464646', padding: '0px 55px 16px 66px'}}>
                                    <p>Cararabiya is an online service community that offers auto enthusiasts a friendly home where they can find their dream car.</p>
                                </div>
                                </div>
                            </div>
                            {/* Accordion card */}
                            {/* Accordion card */}
                            <div className="card faq" style={{borderBottom: '1px solid #99999978'}}>
                                {/* Card header */}
                                <div className="card-header" role="tab" id="headingTwo2" style={{padding: '1rem 1.5rem'}}>
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2" style={{color: '#D3E428'}}>
                                    <h5 className="mb-0">
                                    Q.2: How do I access Cararabiya? <i className="fas fa-angle-down rotate-icon" />
                                    </h5>
                                </a>
                                </div>
                                {/* Card body */}
                                <div id="collapseTwo2" className="collapse" role="tabpanel" aria-labelledby="headingTwo2" data-parent="#accordionEx" style={{}} aria-expanded="false">
                                <div className="card-body faq" style={{fontSize: '.9rem', fontWeight: 400, lineHeight: '1.7', color: '#464646', padding: '0px 55px 16px 66px'}}>
                                    {/*  ● Searching for cars on our website is easier and faster than ever. To begin, just go to our home page and decide how you would like to search. You can use the buttons ,you can specify several criteria at once, or you can search by keywords that you type in. */}
                                    <ul>
                                    <li>Cararabiya.com consists of four sub-categories --New Car Details, New Car Search By Make, Vehicle Type and Price Range, Dealer Details and Latest Car Details.</li>
                                    <li>Cararabiya.com features latest car news, car photos, all car model detailed specification , photo galleries, classics, videos and more.</li>
                                    <li>Cararabiya is an online service community that offers auto enthusiasts a friendly home where they can find their dream car.</li>
                                    </ul>
                                </div>
                                </div>
                            </div>
                            {/* Accordion card */}
                            {/* Accordion card */}
                            <div className="card" style={{borderBottom: '1px solid #99999978'}}>
                                {/* Card header */}
                                <div className="card-header" role="tab" id="headingThree3" style={{padding: '1rem 1.5rem'}}>
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3" aria-expanded="false" aria-controls="collapseThree3" style={{color: '#D3E428'}}>
                                    <h5 className="mb-0">
                                    Q.3: How is Cararabiya content organized? <i className="fas fa-angle-down rotate-icon" />
                                    </h5>
                                </a>
                                </div>
                                {/* Card body */}
                                <div id="collapseThree3" className="collapse" role="tabpanel" aria-labelledby="headingThree3" data-parent="#accordionEx" aria-expanded="false" style={{}}>
                                <div className="card-body faq" style={{fontSize: '.9rem', fontWeight: 400, lineHeight: '1.7', color: '#464646', padding: '0px 55px 16px 66px'}}>
                                    <ol>
                                    <li>Cararabiya.com consists of four sub-categories --New Car Details, New Car Search By Make, Vehicle Type and Price Range, Dealer Details and Latest Car Details.</li>
                                    <li>Cararabiya.com features latest car news, car photos, all car model detailed specification , photo galleries, classics, videos and more.</li>
                                    <li>Cararabiya is an online service community that offers auto enthusiasts a friendly home where they can find their dream car.</li>
                                    </ol>
                                </div>
                                </div>
                            </div>
                            {/* Accordion card */}
                            <div className="card" style={{borderBottom: '1px solid #99999978'}}>
                                {/* Card header */}
                                <div className="card-header" role="tab" id="headingFour4" style={{padding: '1rem 1.5rem'}}>
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseFour4" aria-expanded="false" aria-controls="collapseFour4" style={{color: '#D3E428'}}>
                                    <h5 className="mb-0">
                                    Q.4: What kind of topics can be found on Cararabiya.com? <i className="fas fa-angle-down rotate-icon" />
                                    </h5>
                                </a>
                                </div>
                                {/* Card body */}
                                <div id="collapseFour4" className="collapse" role="tabpanel" aria-labelledby="headingFour4" data-parent="#accordionEx" aria-expanded="false" style={{}}>
                                <div className="card-body faq" style={{fontSize: '.9rem', fontWeight: 400, lineHeight: '1.7', color: '#464646', padding: '0px 55px 16px 66px'}}>
                                    <ul>
                                    <li>Cararabiya.com consists of four sub-categories --New Car Details, New Car Search By Make, Vehicle Type and Price Range, Dealer Details and Latest Car Details.</li>
                                    <li>Cararabiya.com features latest car news, car photos, all car model detailed specification , photo galleries, classics, videos and more.</li>
                                    <li>Cararabiya is an online service community that offers auto enthusiasts a friendly home where they can find their dream car.</li>
                                    </ul>
                                </div>
                                </div>
                            </div>
                            {/* Accordion card */}
                            {/* Accordion card */}
                            <div className="card" style={{borderBottom: '1px solid #99999978'}}>
                                {/* Card header */}
                                <div className="card-header" role="tab" id="headingFive5" style={{padding: '1rem 1.5rem'}}>
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseFive5" aria-expanded="false" aria-controls="collapseFive5" style={{color: '#D3E428'}}>
                                    <h5 className="mb-0">
                                    Q.5: How frequently is Cararabiya updated? <i className="fas fa-angle-down rotate-icon" />
                                    </h5>
                                </a>
                                </div>
                                {/* Card body */}
                                <div id="collapseFive5" className="collapse" role="tabpanel" aria-labelledby="headingFive5" data-parent="#accordionEx" aria-expanded="false" style={{}}>
                                <div className="card-body faq" style={{fontSize: '.9rem', fontWeight: 400, lineHeight: '1.7', color: '#464646', padding: '0px 55px 16px 66px'}}>
                                    <ol>
                                    <li>Cararabiya.com consists of four sub-categories --New Car Details, New Car Search By Make, Vehicle Type and Price Range, Dealer Details and Latest Car Details.</li>
                                    <li>Cararabiya.com features latest car news, car photos, all car model detailed specification , photo galleries, classics, videos and more.</li>
                                    <li>Cararabiya is an online service community that offers auto enthusiasts a friendly home where they can find their dream car.</li>
                                    </ol>
                                </div>
                                </div>
                            </div>
                            {/* Accordion card */}
                            {/* Accordion card */}
                            <div className="card" style={{borderBottom: '1px solid #99999978'}}>
                                {/* Card header */}
                                <div className="card-header" role="tab" id="headingSix6" style={{padding: '1rem 1.5rem'}}>
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseSix6" aria-expanded="false" aria-controls="collapseSix6" style={{color: '#D3E428'}}>
                                    <h5 className="mb-0">
                                    Q.6: what you do....Why do you do?  <i className="fas fa-angle-down rotate-icon" />
                                    </h5>
                                </a>
                                </div>
                                {/* Card body */}
                                <div id="collapseSix6" className="collapse" role="tabpanel" aria-labelledby="headingSix6" data-parent="#accordionEx" aria-expanded="false" style={{}}>
                                <div className="card-body faq" style={{fontSize: '.9rem', fontWeight: 400, lineHeight: '1.7', color: '#464646', padding: '0px 55px 16px 66px'}}>
                                    <p>
                                    Cararabiya.com consists of four sub-categories --New Car Details, New Car Search By Make, Vehicle Type and Price Range, Dealer Details and Latest Car Details.
                                    Cararabiya.com features latest car news, car photos, all car model detailed specification , photo galleries, classics, videos and more.
                                    Cararabiya is an online service community that offers auto enthusiasts a friendly home where they can find their dream car.
                                    </p>
                                </div>
                                </div>
                            </div>
                            {/* Accordion card */}
                            {/* Accordion card */}
                            <div className="card" style={{borderBottom: '1px solid #99999978'}}>
                                {/* Card header */}
                                <div className="card-header" role="tab" id="headingSeven7" style={{padding: '1rem 1.5rem'}}>
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseSeven7" aria-expanded="false" aria-controls="collapseSeven7" style={{color: '#D3E428'}}>
                                    <h5 className="mb-0">
                                    Q.7: How do I access Cararabiya?  <i className="fas fa-angle-down rotate-icon" />
                                    </h5>
                                </a>
                                </div>
                                {/* Card body */}
                                <div id="collapseSeven7" className="collapse" role="tabpanel" aria-labelledby="headingSeven7" data-parent="#accordionEx" aria-expanded="false" style={{}}>
                                <div className="card-body faq" style={{fontSize: '.9rem', fontWeight: 400, lineHeight: '1.7', color: '#464646', padding: '0px 55px 16px 66px', listStyleType: 'square'}}>
                                    <ul>
                                    <li style={{listStyleType: 'square'}}>Cararabiya.com consists of four sub-categories --New Car Details, New Car Search By Make, Vehicle Type and Price Range, Dealer Details and Latest Car Details.</li>
                                    <li style={{listStyleType: 'square'}}>Cararabiya.com features latest car news, car photos, all car model detailed specification , photo galleries, classics, videos and more.</li>
                                    <li style={{listStyleType: 'square'}}>Cararabiya is an online service community that offers auto enthusiasts a friendly home where they can find their dream car.</li>
                                    </ul>
                                </div>
                                </div>
                            </div>
                            {/* Accordion card */}
                            </div>
                            {/* Accordion wrapper */}
                        </section>
                        </div>
                    </div>
            </div>
        );
    }
}

export default Faqmain;