import React, { Component, Fragment } from "react";
import { apiBaseURL } from "../../ApiBaseURL";
import axios from "axios";
import ReactPaginate from "react-paginate";
import "../listing/paginate.css";
import { Button, Modal } from "react-bootstrap";
import { Link } from "react-router-dom";
import moment from "moment";
import Loader from "../../pages/Loader";
import ErrorAlert from "../../pages/ErrorAlert";

class DraftsDetail extends Component {
  constructor(props) {
    super(props);
    this.isTotalMyAdsCountLoading = false;
    this.state = {
      userId: "",
      isLoading: false,
      totalMyAdsCountData: [],
      DraftListingData: [],
      //pagination
      offset: 0,
      perPage: 7,
      currentPage: 0,
      listingData: [],
      toggleAlertMsg: false,

      start: "",
      end: "",
      totalResults: "",
    };
  }
  componentDidMount() {
    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      let userid = parseUserData[0].UserID;
      this.setState({
        userId: parseUserData[0].UserID,
      });
      this.setState({ isLoading: true });
      const url = `${apiBaseURL}/Listing/DraftListingAdPostbyUserID?UserID=${userid}`;
      axios
        .get(url)
        .then((res) => {
          this.receivedData(res.data);
        })
        .catch((error) => {
          this.setState({
            start: 0,
            end: 0,
            offset: 0,
            perPage: 7,
            currentPage: 0,
            totalResults: 0,
            toggleAlertMsg: true,
            listingData: [],
          });
          console.log(error, "from DraftListingAdPostbyUserID api");
        });

      this.getTotalMyAdsCountById(userid);
    } else {
      this.props.history.push("/login-signup");
    }
  }

  getTotalMyAdsCountById = (userid) => {
    if (this.isTotalMyAdsCountLoading) {
      return;
    }
    this.isTotalMyAdsCountLoading = true;

    const url = `${apiBaseURL}/TotalMyAdsCount/TotalMyAdsCount?UserID=${userid}`;
    axios
      .get(url)
      .then((res) => {
        if (res.data.length > 0) {
          this.isTotalMyAdsCountLoading = false;
          this.setState({
            totalMyAdsCountData: res.data,
          });
        } else {
          console.log(res.data, "res.data");
        }
      })
      .catch((error) => {
        this.isTotalMyAdsCountLoading = false;
        console.log(error, "TotalMyAdsCount api");
      });
  };

  numberWithCommas = (price) => {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };
  formattedDate = (datetime) => {
    let splitTimeDateWithT = datetime.split("T");
    let date = splitTimeDateWithT[0]; //T00:00:00
    let formatteddate = moment(date).format("D MMMM YYYY");
    return formatteddate;
  };

  receivedData = (draftlistingdata) => {
    if (draftlistingdata.length > 0) {
      this.setState({
        DraftListingData: draftlistingdata,
        totalResults: draftlistingdata.length,
        toggleAlertMsg: false,
        isLoading: false
      });
      const data = draftlistingdata;

      if (
        this.state.offset + 1 + this.state.perPage >
        draftlistingdata.length
      ) {
        this.setState({
          end: draftlistingdata.length,
        });
      } else {
        this.setState({
          end: this.state.offset + this.state.perPage,
        });
      }

      const slice = data.slice(
        this.state.offset,
        this.state.offset + this.state.perPage
      );

      const listingData = slice.map((draftlist, index) =>
        this.showDraftAdPostListingData(draftlist, index)
      );

      this.setState({
        pageCount: Math.ceil(data.length / this.state.perPage),
        start: this.state.offset + 1,
        listingData,
      });
    } else {
      this.setState({
        start: 0,
        end: 0,
        offset: 0,
        perPage: 7,
        currentPage: 0,
        totalResults: 0,
        toggleAlertMsg: true,
        listingData: [],
        isLoading: false
      });
    }
  };

  handleRemoveList = (userid) => {
    console.log(userid, 'remove');
  }

  handleEditList = (adpostid) => {
    this.props.history.push({
      pathname: '/editdraft-ad',
      state: { AdPostID: adpostid },
    })

    console.log(adpostid, 'edit');
  }

  showDraftAdPostListingData = (draftlist, index) => {
    return (
      <Fragment key={index}>
        <div className="col-12 line-o my-3 px-0 bg-shadow">
          {/*Listing start*/}
          <div className="row">
            <div className="col-lg-8 px-0">
              <div className="row">
                <div className="col-12 px-0 pl-3 mt-2 mb-3">
                  <div className="d-flex align-items-center Ad-top-sec">
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12 col-md-5">
                  <div
                    className="b-goods-f__listing mb-2"
                    style={{ height: "134px" }}
                  >

                    <img
                      className="b-goods-f__img img-scale"
                      src={draftlist.ImageUrl}
                      style={{ height: "134px" }}
                      alt="foto"
                    />
                  </div>

                  <span
                    className="Ad-location"
                    style={{ display: "flex" }}
                  >
                    <i className="fas fa-map-marker-alt" style={{ color: '#D3E428', marginTop: '5px' }} />
                    <span style={{ margin: "0 0 10px 8px" }}>
                      {draftlist.Area}
                    </span>
                  </span>
                </div>
                <div className="col-12 col-md-7">
                  <div className="b-goods-f__main d-flex">
                    <div className="b-goods-f__descrip">
                      <div className="b-goods-f__title_myad">
                        {draftlist.Vehicle} for Sale
                      </div>
                      <div className="b-goods-f__info Ad-posted-date">
                        {this.formattedDate(draftlist.CreateDate)}
                      </div>
                      <ul className="b-goods-f__list list-unstyled mt-1 fsize-car-descr">
                        <li className="b-goods-f__list-item">
                          <span className="b-goods-f__list-title">
                            Version Year :
                          </span>
                          <span className="b-goods-f__list-info Ad-Transmission">
                            {draftlist.VersionYear}
                          </span>
                        </li>
                        <li className="b-goods-f__list-item">
                          <span className="b-goods-f__list-title">Model :</span>
                          <span className="b-goods-f__list-info Ad-Model">
                            {draftlist.ModelName}
                          </span>
                        </li>
                        <li className="b-goods-f__list-item">
                          <span className="b-goods-f__list-title">
                            Transmission :
                          </span>
                          <span className="b-goods-f__list-info Ad-Transmission">
                            {draftlist.TransmissionName}
                          </span>
                        </li>
                        <li className="b-goods-f__list-item">
                          <span className="b-goods-f__list-title">Color :</span>
                          <span className="b-goods-f__list-info Ad-Color">
                            {draftlist.ColorName}
                          </span>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-sm-12 px-0 list-sec-r">
              {" "}
              {/* price section */}
              <div className="Ad-price-sec">
                <span className="Ad-price-style">
                  {draftlist.Price === 0 ? 'Call For Price' : draftlist.CurCode + ' ' + this.numberWithCommas(draftlist.Price)}
                </span>
              </div>
              <div>
                {" "}
                {/* buttons */}
                <div className="b-goods-f__sidebar px-3">
                  <span className="b-goods-f__price-group">
                    <button
                      onClick={() => this.handleRemoveList(draftlist.UserID)}
                      type="button"
                      className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center Ad-btn-grey justify-content-center"
                    >
                      {" "}
                      <span>Remove</span>
                    </button>
                    <button
                      onClick={() => this.handleEditList(draftlist.AdPostID)}
                      type="button"
                      className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center Ad-btn-grey justify-content-center"
                    >
                      {" "}
                      <span>Edit</span>
                    </button>
                  </span>
                </div>
              </div>
            </div>
          </div>{" "}
          {/* end row */}
        </div>
      </Fragment>
    );
  };

  handlePageClick = (e) => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.perPage;

    this.setState(
      {
        currentPage: selectedPage,
        offset: offset,
      },
      () => {
        this.receivedData(this.state.DraftListingData);
      }
    );
  };

  showMyAdsHeader = () => {
    return (
      <div className="row">
        <div className="col-6 px-0">
          <div className="list-heading">Draft</div>
          <div className="b-filter-goods__info col-auto">
            Showing
            <strong>
              {" "}
              {this.state.start} - {this.state.end}{" "}
            </strong>{" "}
            out of <strong> {this.state.totalResults}</strong> listings
          </div>
        </div>
      </div>
    );
  };

  showMyAdsSummary = () => {
    return (
      <Fragment>
        {this.state.totalMyAdsCountData.map((totalmyads, index) => (
          <aside key={index} className="l-sidebar">
            <div className="widget section-sidebar">
              <div className="widget-content">
                <div className="d-flex justify-content-between px-3">
                  <strong>Active</strong>
                  <span>{totalmyads.TotalMyAdsActive}</span>
                </div>
                <hr />
                <div className="d-flex justify-content-between px-3">
                  <strong>Draft</strong>
                  <span>{totalmyads.TotalMyAdsDraft}</span>
                </div>
                <hr />
                <div className="d-flex justify-content-between px-3">
                  <strong>Inactive</strong>
                  <span>{totalmyads.TotalMyAdsInactive}</span>
                </div>
                <hr />
                <div className="d-flex justify-content-between px-3">
                  <strong>Expired</strong>
                  <span>{totalmyads.TotalMyAdsExpired}</span>
                </div>
                <hr />
                <div className="d-flex justify-content-between px-3">
                  <strong>Expiring in 15 days</strong>
                  <span>{totalmyads.TotalMyAdsExpiring15}</span>
                </div>
                <hr />{" "}
                <div className="d-flex justify-content-between px-3">
                  <strong>Expiring in 30 days</strong>
                  <span>{totalmyads.TotalMyAdsExpiring30}</span>
                </div>
                <hr />
              </div>
            </div>
          </aside>
        ))}
      </Fragment>
    );
  };

  render() {
    return (
      <Fragment>
        {this.state.toggleAlertMsg === true ? (
          <ErrorAlert show={this.state.toggleAlertMsg} />
        ) : (
            <Fragment>
              {this.state.isLoading ? (
                <Loader />
              ) : (
                  <div className="row">
                    <div className="col-lg-3">{this.showMyAdsSummary()}</div>

                    <div className="col-lg-9">
                      {this.showMyAdsHeader()}
                      <br />
                      {this.state.listingData}

                      <ReactPaginate
                        previousLabel={"prev"}
                        nextLabel={"next"}
                        breakLabel={"..."}
                        breakClassName={"break-me"}
                        pageCount={this.state.pageCount}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={5}
                        onPageChange={this.handlePageClick}
                        containerClassName={"pagination"}
                        subContainerClassName={"pages pagination"}
                        activeClassName={"active"}
                        forcePage={this.state.currentPage}
                      />
                    </div>
                  </div>
                )}
            </Fragment>
          )}
      </Fragment>
    );
  }
}
export default DraftsDetail;