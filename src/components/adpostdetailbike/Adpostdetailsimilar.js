import React, { Component } from "react";
import { apiBaseURL } from "../../ApiBaseURL";
import axios from "axios";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { Alert } from "react-bootstrap";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 4,
    slidesToSlide: 1, // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    slidesToSlide: 2, // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1, // optional, default to 1.
  },
};

class Adpostdetailsimilar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      adpostid: "",
      llisting: [],
      toplist: [],
      error: null,
      AdsListHeight: "",

      //alert
      toggleAlertMsg: false,
    };
  }

  componentWillMount() {
    const adpostid = this.props.adpostid;
    this.setState({
      adpostid: adpostid,
    });
  }

  componentDidMount() {
    const urlSimilarListing = `${apiBaseURL}/Listing/SimilarListingByAdPostID?AdPostID=${this.state.adpostid}`;
    axios
      .get(urlSimilarListing)
      .then((res) => {
        if (res.data.length > 0) {
          const data = res.data;
          this.setState({ llisting: data, toggleAlertMsg: false });
          if (data.length > 7) {
            var newlist = [];
            for (var i = 0; i < 7; i++) {
              newlist.push(data[i]);
            }
            this.setState({
              toplist: newlist,
            });
          } else {
            this.setState({
              toplist: data,
            });
          }
        } else {
          this.setState({ toggleAlertMsg: true });
        }
      })
      .catch((error) => {
        this.setState({ toggleAlertMsg: true });
        console.log(error, "from urlSimilarListing api");
      });

    this.timer = setTimeout(
      function () {
        //Start the timer
        try {
          const getAdsListHeight = this["listItem_" + 0].offsetHeight;
          this.setState({
            AdsListHeight: getAdsListHeight + 12,
          });
        } catch (e) {
          console.log(e);
        }
      }.bind(this),
      1000
    );
  }
  
  numberWithCommas = (price) => {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  render() {
    return (
      <div class="container">
        <div class="margin">
          <div class="row">
            <div class="col-8 col-xl-10 col-lg-10 col-md-10 col-sm-9">
              <h4 class="ui-title-promo">Similar Ads</h4>
            </div>
          </div>
          <hr />
          {this.state.toggleAlertMsg === true ? (
            <Alert variant="primary" show={this.state.toggleAlertMsg}>
              <p style={{ color: "#721c24", fontWeight: 600 }}>
                Data not found...
              </p>
            </Alert>
          ) : (
            <Carousel
              additionalTransfrom={0}
              arrows={true}
              swipeable={false}
              draggable={false}
              showDots={true}
              centerMode={false}
              responsive={responsive}
              customTransition="all 1s linear"
              ssr={true} // means to render carousel on server-side.
              infinite={true}
              autoPlay
              autoPlaySpeed={2000}
              keyBoardControl={true}
              transitionDuration={500}
              containerClass="carousel-container"
              removeArrowOnDeviceType={["tablet", "mobile"]}
              deviceType={this.props.deviceType}
              dotListClass="custom-dot-list-style"
              itemClass="carousel-item-padding-40-px"
            >
              {this.state.toplist.map((toplist, index) => (
                <div key={index} className="b-goods-f col-lg-12 col-md-12">
                  <div
                    className="b-goods-f__media"
                    style={{ height: "200px", backgroundColor: "whitesmoke" }}
                  >
                    <img
                      className="watermark-recent"
                      src="./assets/media/watermark.png"
                      style={{ height: "200px" }}
                    />
                    <img
                      className="b-goods-f__img img-scale"
                      src={toplist.ImageUrl}
                      style={{ height: "200px" }}
                      alt={toplist.AdPostID}
                    />
                    <span className="b-goods-f__media-inner">
                      <span className="b-goods-f__favorite">
                        <i className="ic far fa-star" />
                      </span>
                      <span className="b-goods-f__label bg-primary">
                        Featured
                      </span>
                    </span>
                  </div>
                  <div className="b-goods-f__main bg-shadow">
                    <div className="b-goods-f__descrip">
                      <div
                        className="b-goods-f__title b-goods-f__title_myad"
                        style={{ marginBottom: "0px" }}
                      >
                        <a style={{ fontSize: "18px", fontWeight: 600 }} to="/">
                          {toplist.Vehicle}
                        </a>
                      </div>

                      <ul
                        ref={(listItem) => {
                          this["listItem_" + index] = listItem;
                        }}
                        className="b-goods-f__list list-unstyled row"
                        style={{
                          height: this.state.AdsListHeight + "px",
                          fontSize: "11px",
                          padding: "5px 10px",
                          marginTop: "0px",
                          justifyContent: "space-evenly",
                        }}
                      >
                        <div className="row">
                          <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                            <div>
                              <i
                                className="fas fa-calendar-alt"
                                style={{ color: "#D3E428", fontSize: "12px" }}
                              />
                            </div>
                            <div>
                              <span
                                className="b-goods-f__list-info"
                                style={{ color: "black" }}
                              >
                                {toplist.VersionYear}
                              </span>
                            </div>
                          </div>

                          <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                            <div>
                              <i
                                className="fas fa-tachometer-alt"
                                style={{ color: "#D3E428", fontSize: "12px" }}
                              />
                            </div>
                            <div>
                              <span
                                className="b-goods-f__list-info"
                                style={{ color: "black" }}
                              >
                                {toplist.Mileage}
                              </span>
                            </div>
                          </div>

                          <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                            <div>
                              <i
                                className="fas fa-car-alt"
                                style={{ color: "#D3E428", fontSize: "12px" }}
                              />
                            </div>
                            <div>
                              <span
                                className="b-goods-f__list-info"
                                style={{ color: "black" }}
                              >
                                {toplist.VehicleTypeName}
                              </span>
                            </div>
                          </div>

                          <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                            <div>
                              <i
                                className="fas fa-gas-pump"
                                style={{ color: "#D3E428", fontSize: "12px" }}
                              />
                            </div>
                            <div>
                              <span
                                className="b-goods-f__list-info"
                                style={{ color: "black" }}
                              >
                                {toplist.FuelTypeName}
                              </span>
                            </div>
                          </div>
                        </div>
                      </ul>
                    </div>
                    <div
                      className="b-goods-f__sidebar"
                      style={{
                        paddingTop: "10px",
                        borderTop: "1px solid #ddd",
                      }}
                    >
                      <a className="b-goods-f__bnr" href="#">
                        <img
                          src="./assets/media/content/b-goods/auto-check.png"
                          alt="auto check"
                        />
                      </a>
                      <span className="b-goods-f__price-group">
                        <span className="b-goods-f__pricee">
                          <span className="b-goods-f__price_col">
                            PRICE:&nbsp;
                          </span>
                          <span className="b-goods-f__price-numbb">
                            {toplist.Price === 0 ? 'Call For Price' : toplist.CurCode +' '+ this.numberWithCommas(toplist.Price)}
                          </span>
                        </span>
                      </span>
                    </div>
                  </div>
                </div>
              ))}
            </Carousel>
          )}
        </div>
      </div>
    );
  }
}
export default Adpostdetailsimilar;