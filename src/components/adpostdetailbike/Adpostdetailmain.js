import React, { Component } from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import classNames from "classnames";
import "../../pages/CopyOfChildAsDots.css";
import axios from "axios";
import { Image, Button, Modal, Breadcrumb, Alert } from "react-bootstrap";
import { apiBaseURL } from "../../ApiBaseURL";
import { Link } from "react-router-dom";
import FinanceCalculator from '../FinanceCalculator';
import { MyContext } from "../../Context";

const responsive = {
  desktop: {
    breakpoint: {
      max: 3000,
      min: 1024,
    },
    items: 1,
  },
  mobile: {
    breakpoint: {
      max: 464,
      min: 0,
    },
    items: 1,
  },
  tablet: {
    breakpoint: {
      max: 1024,
      min: 200,
    },
    items: 1,
  },
};

class Adpostdetailmain extends Component {
  static contextType = MyContext;
  constructor(props) {
    super(props);
    this.state = {
      userId: "",
      adpostid: "",
      pictureDataURLs: [],
      sattributename: [],
      AdPostDetail: [],
      userData: [],
      //modals
      showModalPhone: false,
      showModalEmail: false,
      //modal
      showModalEmailListing: false,
      showModalSuccess: false,
      SuccessMessage: "",
      taMessage: "",
      modalTitle: "",
      txtSubject: "",
      txtEmail: "",

      vcname: "",
      vehicle: "",
      //alert
      toggleAlertMsg: false,
      showResultPrice: false,
    };
  }

  componentWillMount() {
    if (this.props.adpostid !== undefined) {
      const adpostid = this.props.adpostid;
      this.setState({
        adpostid,
      });
    } else {
      const params = new URLSearchParams(window.location.search);
      const adpostid = params.get("AdPostId");
      this.setState({
        adpostid,
      });
    }
  }

  componentDidMount() {
    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      this.setState({
        userId: parseUserData[0].UserID,
      });
    } else {
      console.log("localstorage null");
    }
    const urlAdImage = `${apiBaseURL}/AdImage?AdPostID=${this.state.adpostid}`;
    axios
      .get(urlAdImage)
      .then((res) => {
        if (res.data.length > 0) {
          const data = res.data;
          console.log(data, "urlAdImage");
          this.setState({ pictureDataURLs: data, toggleAlertMsg: false });
        } else {
          this.setState({ toggleAlertMsg: true });
        }
      })
      .catch((error) => {
        this.setState({ toggleAlertMsg: true });
        console.log(error, "from urlAdImage api");
      });

    const urlAdPostDetail = `${apiBaseURL}/AdPostDetail/AdPostDetail?AdPostID=${this.state.adpostid}`;
    axios
      .get(urlAdPostDetail)
      .then((res) => {
        if (res.data.length > 0) {
          const data = res.data;
          console.log(data, "urlAdPostDetail");
          this.getUserDataById(data[0].UserID);
          //const uppercasedata = _.mapValues(data, _.method('toUpperCase'));
          this.setState({ AdPostDetail: data, toggleAlertMsg: false });
        } else {
          this.setState({ toggleAlertMsg: true });
        }
      })
      .catch((error) => {
        this.setState({ toggleAlertMsg: true });
        console.log(error, "from urlAdPostDetail api");
      });

    const urlAdPostAttribute = `${apiBaseURL}/AdPostAttribute?AdPostID=${this.state.adpostid}`;
    axios
      .get(urlAdPostAttribute)
      .then((res) => {
        if (res.data.length > 0) {
          const data = res.data;
          console.log(data, "urlAdPostAttribute");
          this.setState({ sattributename: data,  toggleAlertMsg: false });
        } else {
          this.setState({ toggleAlertMsg: true });
        }
      })
      .catch((error) => {
        this.setState({ toggleAlertMsg: true });
        console.log(error, "from urlAdPostAttribute api");
      });
  }

  handleShowPhone = (userid) => {
    const urlUserPhone = `${apiBaseURL}/User/UserMobile?UserID=${userid}`;
    axios
      .get(urlUserPhone)
      .then((res) => {
        if (res.data.length > 0) {
          const data = res.data;
          console.log(data, "urlUserPhone");
          this.setState({
            userPhone: data,
            showModalPhone: true,
          });
        } else {
          this.setState({});
        }
      })
      .catch((error) => {
        this.setState({});
        console.log(error, "from urlUserPhone api");
      });
  };

  getUserDataById = (userid) => {
    if (userid !== null) {
      const url = `${apiBaseURL}/user?UserID=${userid}`;
      axios
        .get(url)
        .then((res) => {
          this.setState({
            userData: res.data,
            ImageUrl: res.data[0].ImageUrl,
          });
        })
        .catch((error) => {
          console.log(error, "userdata api");
        });
    }
  };

  //phone modal
  handleClosePhone = () => {
    this.setState({
      showModalPhone: false,
    });
  };

  modalPhone = () => {
    return (
      <Modal show={this.state.showModalPhone}>
        <Modal.Header>
          <Modal.Title>User Phone Number</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h6>{this.state.userPhone}</h6>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={this.handleClosePhone}
            style={{ backgroundColor: "#D3E428" }}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    );
  };

  handleShowEmail = (userid) => {
    const urlUserEmail = `${apiBaseURL}/User/UserEmail?UserID=${userid}`;
    axios
      .get(urlUserEmail)
      .then((res) => {
        if (res.data.length > 0) {
          const data = res.data;
          console.log(data, "urlUserEmail");
          this.setState({
            userEmail: data,
            showModalEmail: true,
          });
        } else {
          this.setState({});
        }
      })
      .catch((error) => {
        this.setState({});
        console.log(error, "from urlUserEmail api");
      });
  };

  //phone modal
  handleCloseEmail = () => {
    this.setState({
      showModalEmail: false,
    });
  };

  modalEmail = () => {
    return (
      <Modal show={this.state.showModalEmail}>
        <Modal.Header>
          <Modal.Title>User Email</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h6>{this.state.userEmail}</h6>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={this.handleCloseEmail}
            style={{ backgroundColor: "#D3E428" }}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    );
  };

  numberWithCommas = (price) => {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

   //Email Listing
   handleCloseEmailListing = () => {
    this.setState({
      txtEmail: "",
      txtSubject: "",
      taMessage: "",
      showModalEmailListing: false,
    });
  };
  handleShowEmailListing = (title, msg, vehicle, adpostid, vcname) => {
    let userData = localStorage.getItem("user");
    if (userData === null) {
      this.context.handleChildOpenPopover(true);
    } else {
    this.setState({
      modalTitle: title,
      taMessage: msg,
      vehicle,
      adpostid,
      vcname,
      showModalEmailListing: true,
    });
  }
  };

  handleCloseSuccess = () => {
    this.setState({
      showModalSuccess: false,
    });
  };
  handletxtEmailChange = (event) => {
    this.setState({ txtEmail: event.target.value });
  };
  handletxtSubjectChange = (event) => {
    this.setState({ txtSubject: event.target.value });
  };

  handletaMessageChange = (event) => {
    this.setState({ taMessage: event.target.value });
  };

  handleEmailListing = (event) => {
    event.preventDefault();
    const { userId, txtSubject, taMessage, adpostid, vcname, txtEmail } = this.state;
    var adposturl = "";
    if (vcname === "Cars") {
      adposturl = `https://cararabiya.netlify.app/postad-detail?AdPostId=${adpostid}`;
    } else {
      adposturl = `https://cararabiya.netlify.app/postad-detailbike?AdPostId=${adpostid}`;
    }

    this.setState({
      txtEmail: "",
      txtSubject: "",
      taMessage: "Hi, I've found this Ad on #Cararabiya. What do you think? ",
    });

    const url = `${apiBaseURL}/Email/EmailThisAd`;
    let ApiParamForEmailThisAd = {
      UID: userId,
      ToEmail: txtEmail,
      subLine: txtSubject,
      mailMessage: taMessage,
      AdPostURL: adposturl,
    };
    axios
      .post(url, null, { params: ApiParamForEmailThisAd })
      .then(res => {
        console.log(res.data, "EmailThisAd api");

        if (res.status === 200) {
          this.setState({
            SuccessMessage: res.data,
            showModalEmailListing: false,
            showModalSuccess: true
          });
        } else {
          console.log(res.status, "statuscode from EmailThisAd api");
        }
      })
      .catch(error => {
        console.log(error, "EmailThisAd api");
      });
  };

  //report this ad
  handleShowReportAd = (title, msg, vehicle, adpostid, vcname) => {
    let userData = localStorage.getItem("user");
    if (userData === null) {
      this.context.handleChildOpenPopover(true);
    } else {
    this.setState({
      modalTitle: title,
      taMessage: msg,
      vehicle,
      adpostid,
      vcname,
      showModalEmailListing: true,
    });
  }
  };

  handleReportAd = (event) => {
    event.preventDefault();
    const { userId, txtSubject, taMessage, adpostid, vcname } = this.state;
    var adposturl = "";
    if (vcname === "Cars") {
      adposturl = `https://cararabiya.netlify.app/postad-detail?AdPostId=${adpostid}`;
    } else {
      adposturl = `https://cararabiya.netlify.app/postad-detailbike?AdPostId=${adpostid}`;
    }

    this.setState({
      txtSubject: "",
      taMessage: taMessage,
    });

    const url = `${apiBaseURL}/Email/ReportThisAd`;
    let ApiParamForReportThisAd = {
      ReportUserID: userId,
      subLine: txtSubject,
      mailMessage: taMessage,
      AdPostID: adpostid,
      AdPostURL: adposturl,
    };
    axios
      .post(url, null, { params: ApiParamForReportThisAd })
      .then((res) => {
        console.log(res.data, "ReportThisAd api");

        if (res.status === 200) {
          this.setState({
            SuccessMessage: res.data,
            showModalEmailListing: false,
            showModalSuccess: true,
          });
        } else {
          console.log(res.status, "statuscode from ReportThisAd api");
        }
      })
      .catch((error) => {
        console.log(error, "ReportThisAd api");
      });
  }

  modalListingEmail = () => {
    return (
      <Modal
        show={this.state.showModalEmailListing}
        onHide={this.handleCloseEmailListing}
      >
        <Modal.Body>
          <div className="my-3 px-3">
            <div className="d-flex justify-content-center my-3">
              <h4>
                <strong>{this.state.modalTitle}</strong>
              </h4>
            </div>
            <form onSubmit={this.state.modalTitle !== "Report this Ad" ? this.handleEmailListing : this.handleReportAd }>
              <div>
                <div className="form-group">
                  <label style={{ fontSize: "16px" }}>Title: </label>
                  <div>
                    <span style={{ fontSize: "16px" }}>
                      {this.state.vehicle}
                    </span>
                  </div>
                </div>

                {this.state.modalTitle !== "Report this Ad" ? 
                  (
                    <>
                    <input
                    required
                    onChange={this.handletxtEmailChange}
                    value={this.state.txtEmail}
                    type="email"
                    className="form-control"
                    placeholder="Email *"
                  />
                  <br/>
                  </>
                  ) : 
                  (
                    ""
                  )
                }

                <input
                  name="subject"
                  required
                  onChange={this.handletxtSubjectChange}
                  value={this.state.txtSubject}
                  type="text"
                  className="form-control"
                  placeholder="Subject *"
                />
              </div>
              <div className="mt-3">
                <textarea
                  name="message"
                  required
                  cols={30}
                  rows={5}
                  className="form-control"
                  placeholder="Message *"
                  onChange={this.handletaMessageChange}
                  value={this.state.taMessage}
                />
              </div>
              <div className="d-flex justify-content-center mb-2 mt-3">
                <button
                  type="submit"
                  className="btn"
                  style={{
                    border: "none",
                    padding: "10px 80px",
                    backgroundColor: "#D3E428",
                    color: "white",
                  }}
                >
                  SEND
                </button>
              </div>
            </form>
          </div>
        </Modal.Body>
      </Modal>
    );
  };

  modalSuccess = () => {
    return (
      <Modal show={this.state.showModalSuccess}>
        <Modal.Body>
          <div className="my-3 px-3">
            <div className="d-flex justify-content-center my-3">
              <h4>
                <strong>Success</strong>
              </h4>
            </div>

            <div style={{ textAlign: "center" }}>
              {this.state.SuccessMessage}
            </div>

            <div className="d-flex justify-content-center mb-2 mt-3">
              <button
                onClick={this.handleCloseSuccess}
                className="btn"
                style={{
                  border: "none",
                  padding: "10px 80px",
                  backgroundColor: "#D3E428",
                  color: "white",
                }}
              >
                OK
              </button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    );
  };

  handleFinanceCalulator = (price) => {
    if(price === "0.00"){
      this.props.history.push('/financing');
    }else{
      this.setState({
        showResultPrice: !this.state.showResultPrice,
      });
    }
  }


  render() {
    const children = this.state.pictureDataURLs.map((picturedata, index) => (
      <div key={index} className="col-12" style={{ padding: 0 }}>
        <div id="result1" className="result-box" style={{ display: "block" }}>
          <img src="./assets/media/watermark.png" className="mywatermark" />
          <Image
            id="result"
            src={picturedata.ImageUrl}
            key={index}
            alt="..."
            style={{ width: "100%", height: "500px" }}
          />
        </div>
      </div>
    ));

    const images = this.state.pictureDataURLs.map((picturedata, index) => (
      <div className="thumbCards">
        <Image key={index} src={picturedata.ImageUrl} className="thumbImg" />
      </div>
    ));

    const CustomDot = ({
      index,
      onClick,
      active,
      carouselState: { currentSlide },
    }) => {
      return (
        <button
          onClick={(e) => {
            onClick();
            e.preventDefault();
          }}
          className={classNames("custom-dot", {
            "custom-dot--active": active,
          })}
        >
          {React.Children.toArray(images)[index]}
        </button>
      );
    };

    return (
      <div className="l-main-content-inner">
        <div className="container">
          {this.state.toggleAlertMsg === true ? (
            <Alert variant="primary" show={this.state.toggleAlertMsg}>
              <p style={{ color: "#721c24", fontWeight: 600 }}>
                Data not found...
              </p>
            </Alert>
          ) : (
            <div>
              {/* modal phone */}
              {this.modalPhone()}
              {/* modal email */}
              {this.modalEmail()}
              {/* modal emailad */}
              {this.modalListingEmail()}
              {/* modal success */}
              {this.modalSuccess()}
              {this.state.AdPostDetail.map((adpostdetail, index) => (
                <div key={index}>
                  <div className="col-12 px-0" style={{ marginLeft: "-10px" }}>
                    <Breadcrumb>
                      <Breadcrumb.Item>Home</Breadcrumb.Item>
                      <Breadcrumb.Item>
                        {adpostdetail.VehicleTypeName} Bikes for Sale
                      </Breadcrumb.Item>
                      <Breadcrumb.Item>
                        {adpostdetail.MakeName} Bikes for Sale
                      </Breadcrumb.Item>
                      <Breadcrumb.Item active>
                        {adpostdetail.ModelName}
                      </Breadcrumb.Item>
                    </Breadcrumb>
                  </div>
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="row justify-content-between">
                        <div className="list-heading">
                          <span>
                            {adpostdetail.VehicleTypeName}{" "}
                            {adpostdetail.VehicleName} for Sale
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row mt-3">
                    <div className="col-lg-8">
                      <div class="row">
                        <div className="col-md-12" style={{ padding: 0 }}>
                          <Carousel
                            showDots
                            slidesToSlide={1}
                            containerClass="carousel-with-custom-dots"
                            responsive={responsive}
                            infinite
                            customDot={<CustomDot />}
                          >
                            {children}
                          </Carousel>
                        </div>
                      </div>
                      <div
                        className="col-12 col-lg-12 col-md-6 col-sm-6 mt-3 mb-3"
                        style={{ padding: 0 }}
                      >
                        <ul
                          className="ad-detail-icon d-flex"
                          style={{ padding: 0 }}
                        >
                          <li>
                            <span>YEAR</span>
                            <img
                              src="assets/media/icon/Calendar.png"
                              width={30}
                              height={30}
                              alt=""
                            />
                            <strong> {adpostdetail.VersionYear} </strong>
                          </li>
                          <li>
                            <span>KILOMETERS</span>
                            <img
                              src="assets/media/icon/Km.png"
                              width={30}
                              height={30}
                              alt=""
                            />
                            <strong> {this.numberWithCommas(adpostdetail.MeterReading)} KM</strong>
                          </li>
                          <li>
                            <span>COLOR</span>
                            <img
                              src="assets/media/icon/Color.png"
                              width={30}
                              height={30}
                              alt=""
                            />
                            <strong> {adpostdetail.ExteriorColor} </strong>
                          </li>
                          <li>
                            <span>TRANSMISSION</span>
                            <img
                              src="assets/media/icon/Automatic.png"
                              width={30}
                              height={30}
                              alt=""
                            />
                            <strong>{adpostdetail.TransmissionName}</strong>
                          </li>
                          <li>
                            <span>BODY TYPE</span>
                            <img 
                              src="assets/media/newbike/bike.png"
                              width={40}
                              height={30}
                              alt=""
                            />
                            <strong>{adpostdetail.BodyTypeName}</strong>
                          </li>
                        </ul>
                      </div>
                      <br />
                      <h5>Seller Notes</h5>
                      <div className="row mt-3">
                        <div className="col-md-12">
                          <p>{adpostdetail.Comments}</p>
                        </div>
                      </div>
                      <br />
                      <h5>Basic Details</h5>
                      <div className="row mt-3">
                        <div className="col-md-6">
                          <dl className="b-goods-f__descr row">
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              VERSION YEAR
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.VersionYear}
                            </dt>
                            <hr className="col-12 mt-0  px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              BRAND
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.MakeName}
                            </dt>
                            <hr className="col-12 mt-0  px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              MODEL
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.ModelName}
                            </dt>
                            <hr className="col-12 mt-0  px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              BODY STYLE
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.BodyTypeName}
                            </dt>
                            <hr className="col-12 mt-0  px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              EXTERIOR COLOR
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.ExteriorColor}
                            </dt>
                            <hr className="col-12 mt-0  px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              ENGINE #
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.EngineNumber}
                            </dt>
                            <hr className="col-12 mt-0  px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              CHASSIS #
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.ChasisNumber}
                            </dt>
                            <hr className="col-12 mt-0  px-0" />
                          </dl>
                        </div>
                        <div className="col-md-6">
                          <dl className="b-goods-f__descr row">
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              VEHICLE TYPE
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.VehicleTypeName}
                            </dt>
                            <hr className="col-12 mt-0 px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              METER READING
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.MeterReading}
                            </dt>
                            <hr className="col-12 mt-0 px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              ENGINE SIZE
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.EngineSize_Capacity_CC} cc
                            </dt>
                            <hr className="col-12 mt-0 px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              # OF CYLINDERS
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.Cylinders} No(s).
                            </dt>
                     
                            <hr className="col-12 mt-0 px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              ASSEMBLY
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.Assembly === "imp"
                                ? "IMPORTED"
                                : "LOCAL"}
                            </dt>
                            <hr className="col-12 mt-0  px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              REGISTERED CITY
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.RegisteredCity}
                            </dt>
                            <hr className="col-12 mt-0  px-0" />
                          </dl>
                        </div>
                      </div>
                      <br />
                      <h5>Additional Info</h5>
                      <div className="row mt-3">
                        <div className="col-md-6">
                          <dl className="b-goods-f__descr row">
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              MILEAGE
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.Mileage}
                            </dt>
                  
                            <hr className="col-12 mt-0  px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              # OF SEATS
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.NumberofSeats} Pers
                            </dt>
                            <hr className="col-12 mt-0  px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              # OF GEARS
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.NumberofGears} No(s).
                            </dt>
                            <hr className="col-12 mt-0  px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              # OF OWNERS
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.NumberofOwners}
                            </dt>
                           
                            <hr className="col-12 mt-0  px-0" />
                          </dl>
                        </div>
                        <div className="col-md-6">
                          <dl className="b-goods-f__descr row">
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              ENGINE POWER
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.EnginePower} bhp
                            </dt>
                          
                            <hr className="col-12 mt-0  px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              FUEL TANK SIZE
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.FuelTankCapacity} Ltr.
                            </dt>
                           
                            <hr className="col-12 mt-0  px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              BODY CONDITION
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.BodyCondition} out of 10
                            </dt>
                            <hr className="col-12 mt-0  px-0" />
                            <dd
                              className="b-goods-f__descr-new col-lg-6 col-md-12"
                              style={{
                                fontSize: "13px",
                                color: "#222222",
                              }}
                            >
                              TRANSMISSION
                            </dd>
                            <dt
                              className="b-goods-f__descr-info col-lg-6 col-md-12 px-0"
                              style={{ textAlign: "right" }}
                            >
                              {adpostdetail.TransmissionName}
                            </dt>
                            <hr className="col-12 mt-0  px-0" />
                          </dl>
                        </div>
                      </div>
                      <br />
                      <h5>Features</h5>
                      <div className="row">
                        <div className="col-12 mt-3">
                          <div className="row px-0">
                            {this.state.sattributename.map((attname, index) => (
                              <div className="col-6 px-0" key={index}>
                                <dl className="b-goods-f__descr mb-0">
                                  <dd
                                    className="b-goods-f__descr-title"
                                    style={{ fontWeight: 400 }}
                                  >
                                    {attname.AttributeName}{" "}
                                  </dd>
                                </dl>
                              </div>
                            ))}
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* end .b-seller*/}
                    <div className="col-lg-4 px-0">
                      {" "}
                      {/* price section */}
                      <div
                        className
                        style={{
                          backgroundColor: "#D3E428",
                          textAlign: "center",
                        }}
                      >
                        <div className="b-seller__title py-3">
                          <div className="b-seller__price">
                            {adpostdetail.Price === '0.00' ? 'Call For Price' : adpostdetail.CurCode +' '+ this.numberWithCommas(adpostdetail.Price)}
                          </div>
                          <span
                            style={{
                              color: "white",
                              fontSize: "12px",
                              textAlign: "center",
                            }}
                          >
                            Prices are exclusive of VAT
                          </span>
                        </div>
                      </div>
                      <div>
                        {" "}
                        {/* buttons */}
                        <div
                          className="b-goods-f__sidebar px-4 py-3"
                          style={{ backgroundColor: "#e0e0e0" }}
                        >
                          <div className="b-goods-f__price-group justify-content-center">
                            {/* <span class="b-goods-f__price">
                                 <span class="b-goods-f__price-numb">SAR &nbsp; 50,000</span>
                              </span> */}
                            <button
                              onClick={() =>
                                this.handleShowPhone(adpostdetail.UserID)
                              }
                              type="button"
                              className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center justify-content-center"
                              style={{
                                borderRadius: "0px",
                                fontSize: "15px",
                                fontWeight: 600,
                              }}
                            >
                              {" "}
                              <i
                                style={{
                                  position: "absolute",
                                  left: "45px",
                                }}
                                className="fas fa-phone prefix"
                              />
                              <span>Show Phone Number</span>
                            </button>
                            <button
                              onClick={() =>
                                this.handleShowEmail(adpostdetail.UserID)
                              }
                              type="button"
                              className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center justify-content-center"
                              style={{
                                borderRadius: "0px",
                                fontSize: "15px",
                                fontWeight: 600,
                              }}
                            >
                              {" "}
                              <i
                                style={{
                                  position: "absolute",
                                  left: "45px",
                                }}
                                className="fas fa-envelope prefix "
                              />
                              <span>Show Email</span>
                            </button>
                            <button
                              type="button"
                              className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center justify-content-center"
                              style={{
                                borderRadius: "0px",
                                fontSize: "15px",
                                fontWeight: 600,
                              }}
                            >
                              {" "}
                              <i
                                style={{
                                  position: "absolute",
                                  left: "45px",
                                }}
                                className="far fa-comment-dots prefix"
                              />
                              <span>Chat with seller</span>
                            </button>
                          </div>
                          <span className="b-goods-f__compare"></span>
                        </div>
                        <div
                          className="b-goods-f__sidebar px-4 py-2"
                          style={{
                            backgroundColor: "#e0e0e0",
                            borderTop: "1px solid #cecece",
                          }}
                        >
                          <div className="b-goods-f__price-group justify-content-center">
                            {/* <span class="b-goods-f__price">
                                 <span class="b-goods-f__price-numb">SAR &nbsp; 50,000</span>
                              </span> */}
                            <button
                              type="button"
                              className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center justify-content-center"
                              style={{
                                borderRadius: "0px",
                                fontSize: "15px",
                                fontWeight: 600,
                              }}
                              onClick={() =>
                                this.handleFinanceCalulator(adpostdetail.Price)
                              }
                            >
                              {" "}
                              <span>Installment Calculator</span>
                            </button>
                            <button
                              type="button"
                              className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center justify-content-center"
                              style={{
                                borderRadius: "0px",
                                fontSize: "15px",
                                fontWeight: 600,
                              }}
                            >
                              {" "}
                              <span>Apply for Finance</span>
                            </button>
                          </div>
                          <span className="b-goods-f__compare"></span>
                        </div>
                        <div
                          className="b-goods-f__sidebar px-4 py-2"
                          style={{
                            backgroundColor: "#e0e0e0",
                            borderTop: "1px solid #cecece",
                          }}
                        >
                          <div className="bg-detail-seller">
                            <div
                              className="b-seller__detail pt-3 px-0 py-2"
                              style={{ backgroundColor: "transparent" }}
                            >
                              <div className="b-seller__img">
                                <img
                                  className="img-scale"
                                  src={this.state.ImageUrl}
                                  alt="foto"
                                />
                              </div>
                              <div className="b-seller__title">
                                <div
                                  className="b-seller__name"
                                  style={{
                                    fontWeight: 600,
                                    fontSize: "16px",
                                  }}
                                >
                                  {adpostdetail.UserName}
                                </div>
                                <div className="b-seller__category">
                                  Member Since {adpostdetail.MemberSince}
                                </div>
                              </div>
                              <hr />
                              <div
                                className="b-seller__detail row d-flex justify-content-center px-0"
                                style={{ backgroundColor: "transparent" }}
                              >
                                <div className="mx-2">
                                  <i
                                    style={{
                                      color: "#D3E428",
                                      fontSize: "30px",
                                      paddingRight: "10px",
                                      transform: "translateY(5px)",
                                    }}
                                    className="fas fa-phone"
                                  />
                                  <span style={{ fontSize: "12px" }}>
                                    {" "}
                                    Phone verified
                                  </span>
                                </div>
                                <div className="mx-2">
                                  <i
                                    style={{
                                      color: "#D3E428",
                                      fontSize: "30px",
                                      paddingRight: "10px",
                                      transform: "translateY(5px)",
                                    }}
                                    className="fas fa-envelope"
                                  />
                                  <span style={{ fontSize: "12px" }}>
                                    {" "}
                                    E-mail verified
                                  </span>
                                </div>
                              </div>
                            </div>
                            <span
                              className="b-goods-f__compare"
                              style={{ padding: "0px" }}
                            ></span>
                          </div>
                        </div>
                        <div
                          className="my-3 pb-2 d-flex justify-content-center"
                          style={{ borderBottom: "1px solid #cecece" }}
                        >
                          <div>
                          <Link
                              onClick={() =>
                                this.handleShowEmailListing(
                                  'Email this Ad',
                                  'Hi, I have found this Ad on #Cararabiya. What do you think?',
                                  adpostdetail.VehicleName,
                                  adpostdetail.AdPostID,
                                  adpostdetail.VehicleCategoryName
                                )
                              }
                              style={{ color: "grey" }}
                            >
                              <span
                                style={{ fontSize: "1.1em" }}
                                className="mr-3 pb-2"
                              >
                                <i className="far fa-envelope mr-1" /> Email
                                this Ad
                              </span>
                            </Link>
                      
                          </div>
                        </div>
                        <div className="d-flex justify-content-end">
                        <Link
                            onClick={() =>
                              this.handleShowReportAd(
                                'Report this Ad',
                                'Reason To Report Abuse',
                                adpostdetail.VehicleName,
                                adpostdetail.AdPostID,
                                adpostdetail.VehicleCategoryName
                              )
                            }
                            style={{ color: "grey" }}
                          >
                            <span
                              style={{ fontSize: "0.9em" }}
                              className="mr-3 pb-2"
                            >
                              <i className="fas fa-flag mr-1" /> Report this Ad
                            </span>
                          </Link>
                        </div>
                      </div>
                      {this.state.showResultPrice && (<FinanceCalculator price={adpostdetail.Price}/>) }
                    </div>
                  </div>
                </div>
              ))}
            </div>
          )}
        </div>
      </div>
    );
  }
}
Adpostdetailmain.contextType = MyContext;
export default Adpostdetailmain;