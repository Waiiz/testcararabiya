import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class Homeslider extends Component {
    render() {
        return (
            <div id="main-slider" data-slider-width="100%" data-slider-height="650px" data-slider-arrows="false" data-slider-buttons="false">
            <div className="sp-slides">
              <div className="main-slider__slide sp-slide">
                <div className="sp-layer" data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration={800} data-show-delay={400} data-hide-delay={400}>
                  <div className="container">
                    <div className="main-slider__wrap">
                      <div className="main-slider__title">New &amp; Used Cars <br />For Sale in Saudi Arabia
                      </div>
                      <Link role="button" className="btn btn-outline-light" to="/vehicle-listing">Explore</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
    }
}

export default Homeslider;