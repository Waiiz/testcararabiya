import React, { Component } from "react";
import { Link } from "react-router-dom";
import AdPostConditions from '../AdPostConditions';

class Homecounter extends Component {
  render() {
    return (
      <div>
        <section className="b-bnr bg-dark">
          <div className="container">
            <div className="row">
              <div className="col-xl-8">
                <div className="b-bnr__main">
                  <h2 className="b-bnr__title">
                    Buy/Sell your car quickly with Cararabiya
                  </h2>
                </div>
              </div>
              <div className="col-xl-4">
                {localStorage.getItem("user") === null || localStorage.getItem("user") === undefined ? (
                  <Link
                    className="btn btn-primary btn-block"
                    to="/login-signup"
                  >
                    SIGN UP NOW
                    </Link>
                ) : (
                    <div
                      className=""
                    >
                      <AdPostConditions props={this.props} classname="btn btn-primary btn-block" />
                    </div>
                  )}
              </div>
            </div>
          </div>
        </section>
        <div className="section-progress">
          <div className="container">
            <ul className="b-progress-list row list-unstyled">
              <li className="b-progress-list__item col-md-3 col-12">
                <div className="b-progress-list__wrap bg-light">
                  <span className="b-progress-list__name">
                    Registered Users
                  </span>
                  <span
                    className="b-progress-list__percent js-chart"
                    data-percent="3874"
                  >
                    <span className="js-percent">3874</span>
                    <canvas height="0" width="0"></canvas>
                  </span>
                </div>
              </li>
              <li className="b-progress-list__item col-md-3 col-12">
                <div className="b-progress-list__wrap bg-light">
                  <span className="b-progress-list__name">dealers listed</span>
                  <span
                    className="b-progress-list__percent js-chart"
                    data-percent="299"
                  >
                    <span className="js-percent">299</span>
                    <span>+</span>
                    <canvas height="0" width="0"></canvas>
                  </span>
                </div>
              </li>
              <li className="b-progress-list__item col-md-3 col-12">
                <div className="b-progress-list__wrap bg-light">
                  <span className="b-progress-list__name">Active ads</span>
                  <span
                    className="b-progress-list__percent js-chart"
                    data-percent="6403"
                  >
                    <span className="js-percent">6403</span>
                    <canvas height="0" width="0"></canvas>
                  </span>
                </div>
              </li>
              <li className="b-progress-list__item col-md-3 col-12">
                <div className="b-progress-list__wrap bg-light">
                  <span className="b-progress-list__name">
                    vehicles on sale
                  </span>
                  <span
                    className="b-progress-list__percent js-chart"
                    data-percent="1450"
                  >
                    <span className="js-percent">1450</span>
                    <span>+</span>
                    <canvas height="0" width="0"></canvas>
                  </span>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default Homecounter;