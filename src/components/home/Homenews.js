import React, { Component } from "react";
import { Link } from "react-router-dom";

class Homenews extends Component {
  render() {
    const image1 =
      "assets/images/n-1.jpg";
    const image2 =
    "assets/images/n-2.jpg";
    const image3 =
    "assets/images/n-3.jpg";
    return (
      <div className="section-news section-default">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="text-center">
                <h2 className="ui-title">News &amp; Reviews</h2>
              </div>
            </div>
          </div>
          <div className="b-post-group-2">
            <div className="row">
              <div className="col-md-4">
                <section className="b-post b-post-1 clearfix">
                  <div className="entry-media">
                    <Link to="/news-one">
                      <img className="img-fluid" src={image1} alt="Foto" />
                    </Link>
                  </div>
                  <div className="entry-main">
                    <div className="entry-header">
                      <div className="entry-meta">
                        <span className="entry-meta__item">
                          <Link className="entry-meta__link" to="/">
                            20 DEC 2019
                          </Link>
                        </span>
                        <span className="entry-meta__item">
                          <Link className="entry-meta__link" to="/">
                            BY Erick Ayapana
                          </Link>
                        </span>
                      </div>
                      <h2 className="entry-title">
                        2020 Mazda CX-30 First Drive: Recipe for Success
                      </h2>
                    </div>
                    <div className="entry-content">
                      <p>
                        Mazda3 bones serve up crossover goodness. The 2020 Mazda
                        CX-30 hopes to make lemonade out of lemons.
                      </p>
                    </div>
                    <div className="entry-footer">
                      <Link className="entry-link btn-link" to="/news-one">
                        READ article
                      </Link>
                    </div>
                  </div>
                </section>
              </div>
              <div className="col-md-4">
                <section className="b-post b-post-1 clearfix">
                  <div className="entry-media">
                    <Link to="/news-two">
                      <img className="img-fluid" src={image2} alt="Foto" />
                    </Link>
                  </div>
                  <div className="entry-main">
                    <div className="entry-header">
                      <div className="entry-meta">
                        <span className="entry-meta__item">
                          <Link className="entry-meta__link" to="/">
                            20 DEC 2019
                          </Link>
                        </span>
                        <span className="entry-meta__item">
                          <Link className="entry-meta__link" to="/">
                            BY Angus MacKenzie
                          </Link>
                        </span>
                      </div>
                      <h2 className="entry-title">
                        Inside the Porsche Taycan Factory: How Porsche Will
                        Build Its Electric Sports Car
                      </h2>
                    </div>
                    <div className="entry-content">
                      <p>
                        Automaker is investing heavily in its new Tesla-fighter
                      </p>
                    </div>
                    <div className="entry-footer">
                      <Link className="entry-link btn-link" to="/news-two">
                        READ article
                      </Link>
                    </div>
                  </div>
                </section>
              </div>
              <div className="col-md-4">
                <section className="b-post b-post-1 clearfix">
                  <div className="entry-media">
                    <Link to="/news-three">
                      <img className="img-fluid" src={image3} alt="Foto" />
                    </Link>
                  </div>
                  <div className="entry-main">
                    <div className="entry-header">
                      <div className="entry-meta">
                        <span className="entry-meta__item">
                          <Link className="entry-meta__link" to="/">
                            20 DEC 2019
                          </Link>
                        </span>
                        <span className="entry-meta__item">
                          <Link className="entry-meta__link" to="/">
                            BY Alex Leanse
                          </Link>
                        </span>
                      </div>
                      <h2 className="entry-title">
                        2019 Subaru WRX STI S209 First Test: What Makes You So
                        Special?
                      </h2>
                    </div>
                    <div className="entry-content">
                      <p>
                        We flog this super-Subie to find out if it’s worth its
                        astronomical price
                      </p>
                    </div>
                    <div className="entry-footer">
                      <Link className="entry-link btn-link" to="/news-three">
                        READ article
                      </Link>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Homenews;