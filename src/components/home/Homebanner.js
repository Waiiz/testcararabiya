import React, { Component, Fragment } from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 5,
    slidesToSlide: 1 // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    slidesToSlide: 2 // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  }
};

const Campaigns = [
  { id: 1, ImageUrl: "assets/images/ad-1.jpg" },
  { id: 2, ImageUrl: "assets/images/ad-2.jpg" },
  { id: 3, ImageUrl: "assets/images/ad-3.jpg" },
  { id: 4, ImageUrl: "assets/images/ad-4.jpg" },
  { id: 1, ImageUrl: "assets/images/ad-1.jpg" },
  { id: 2, ImageUrl: "assets/images/ad-2.jpg" },
  { id: 3, ImageUrl: "assets/images/ad-3.jpg" },
  { id: 4, ImageUrl: "assets/images/ad-4.jpg" },
];


class Homebanner extends Component {
  render() {
    return (
      <div className="container-fluid pattern-bg margin-top-120">
        <div className="container py-5">
          <div className="col-12">
            <h4 className="home-heading">live campaigns</h4>
          </div>
          <div
              className="b-gallery js-slider"
            >
            <Carousel
                additionalTransfrom={0}
                arrows={false}
                swipeable={false}
                draggable={false}
                showDots={false}
                centerMode={false}
                responsive={responsive}
                customTransition="all 1s linear"
                ssr={true} // means to render carousel on server-side.
                infinite={true}
                autoPlay
                autoPlaySpeed={2000}
                keyBoardControl={true}
                transitionDuration={400}
                containerClass="carousel-container"
                removeArrowOnDeviceType={["tablet", "mobile"]}
                deviceType={this.props.deviceType}
                dotListClass="custom-dot-list-style"
                itemClass="carousel-item-padding-40-px"
              >
                {Campaigns.map((campaign, index) => (
                  <img className="campaign-carousel" src={campaign.ImageUrl} key={index} alt="campaign" />
                ))}
              </Carousel>
             

            </div>
        </div>
      </div>
    );
  }
}

export default Homebanner;
