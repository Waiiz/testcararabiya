import React, { Component, Fragment } from "react";
import { Tabs, Tab } from "react-bootstrap";
import "./HomeComponent.css";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 6,
    slidesToSlide: 3 // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    slidesToSlide: 2 // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  }
};
//Dealers
const Dealers = [
  { id: 1, ImageUrl: "assets/media/d-1.png" },
  { id: 2, ImageUrl: "assets/media/d-2.png" },
  { id: 3, ImageUrl: "assets/media/d-3.png" },
  { id: 4, ImageUrl: "assets/media/d-4.png" },
  { id: 5, ImageUrl: "assets/media/d-5.png" },
  { id: 6, ImageUrl: "assets/media/d-6.png" },
];
class Homepromo extends Component {
  constructor(props) {
    super(props);
    this.state = { lbanks: [], ldealer:[], key: 0, error: null };
  }

  componentDidMount() {
    this.setState({
      ldealer: Dealers
    });
    const apiUrlBanks = "https://cararabiya.ahalfa.com/api/bank";

    fetch(apiUrlBanks)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lbanks: result
            });
          } else {
            console.log("bank");
          }
        },
        error => {
          this.setState({ error });
        }
      );
  }

  handleTabSelect = (key) => {
    this.setState({
      key,
    });
  };


  render() {
    return (
      <Fragment>
        <div className="section-area bg-promotions car-pattern">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12"> 
                <div className="b-find" id="test">
                  <Tabs
                    onSelect={this.handleTabSelect}
                    style={{ borderBottom: "0px", justifyContent: "center", textAlign: 'center' }}
                    defaultActiveKey={this.state.key}
                    id="uncontrolled-tab-example"
                  >
                    <Tab
                      eventKey="0"
                      title="BANK OFFERINGS"
                    >
                      <div className="tabContainer">
                        <Carousel
                          additionalTransfrom={0}
                          arrows={false}
                          swipeable={false}
                          draggable={false}
                          showDots={false}
                          centerMode={false}
                          responsive={responsive}
                          customTransition="all 1s linear"
                          ssr={true} // means to render carousel on server-side.
                          infinite={true}
                          autoPlay
                          autoPlaySpeed={2000}
                          keyBoardControl={true}
                          transitionDuration={500}
                          containerClass="carousel-container"
                          removeArrowOnDeviceType={["tablet", "mobile"]}
                          deviceType={this.props.deviceType}
                          dotListClass="custom-dot-list-style"
                          itemClass="carousel-item-padding-40-px"
                        >
                          {this.state.lbanks.map((lbank, index) => (
                            <img className="carousel-img" src={lbank.ImageUrl} key={index} alt="lbank" />
                          ))}
                        </Carousel>
                      </div>
                    </Tab>

                    <Tab eventKey="1" title="DEALERS OFFERINGS">
                      <div className="tabContainer">
                        <Carousel
                          additionalTransfrom={0}
                          arrows={false}
                          swipeable={false}
                          draggable={false}
                          showDots={false}
                          centerMode={false}
                          responsive={responsive}
                          customTransition="all 1s linear"
                          ssr={true} // means to render carousel on server-side.
                          infinite={true}
                          autoPlay
                          autoPlaySpeed={2000}
                          keyBoardControl={true}
                          transitionDuration={500}
                          containerClass="carousel-container"
                          removeArrowOnDeviceType={["tablet", "mobile"]}
                          deviceType={this.props.deviceType}
                          dotListClass="custom-dot-list-style"
                          itemClass="carousel-item-padding-40-px"
                        >
                        {this.state.ldealer.map((dealer, index) => (
                            <img
                              src={dealer.ImageUrl}
                              key={index}
                              alt="Dealers"
                              style={{backgroundColor: 'white'}}
                            />
                          ))}
                                                  </Carousel>
                      </div>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Homepromo;