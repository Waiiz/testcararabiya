import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./HomeComponent.css";
import { apiUrlcity, apiUrlmake, apiUrlvehicletype } from "../../ApiBaseURL";
import { Consumer } from "../../Context";
import { MyContext } from "../../Context";

class Homesearch extends Component {
  static contextType = MyContext;
  constructor(props) {
    super(props);
    this.state = {
      lbrand: [],
      ltype: [],
      lcity: [],
      error: null,
      validationError: "",

      //params
      vehiclecategoryid:1,
      ssellertype: 0,
      svtype: 0,
      cityid: 0,
      sowner: 0,
      minVal: "NA",
      maxVal: "NA",
      snewmake: 0,
      smodel: 0,
      sversion: 0,
      versionyearfrom: "NA",
      versionyearto: "NA",
      sassembly: "NA",
      bodytypeid: 0,
      transmissionid: 0,
      mileagemin: "NA",
      mileagemax: "NA",
      extcolorid: 0,
      sdoormin: "NA",
      sdoormax: "NA",
      sdriveside: "NA",
      smetermin: "NA",
      smetermax: "NA",
      sengsizemin: "NA",
      sengsizemax: "NA",
      senginetype: "NA",
      spic: true,     
    };
  }

  componentDidMount() {
    fetch(apiUrlcity)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lcity: result,
            });
          } else {
            console.log(result, "city api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    fetch(apiUrlmake)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lbrand: result,
            });
          } else {
            console.log(result, "make api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    fetch(apiUrlvehicletype)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              ltype: result,
            });
          } else {
            console.log(result, "vtype api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );
  }

  handleMakeChange = event => {
    this.setState({ snewmake: event.target.value });
  };
  handleCityChange = event => {
    this.setState({ cityid: event.target.value });
  };
  handleConditionChange = event => {
    this.context.handleChildVehicletypeId(event.target.value);
    this.setState({ svtype: event.target.value });
  };
  render() {
    const {
      ssellertype,
      svtype,
      cityid,
      sowner,
      minVal,
      maxVal,
      snewmake,
      smodel,
      sversion,
      versionyearfrom,
      versionyearto,
      sassembly,
      bodytypeid,
      transmissionid,
      mileagemin,
      mileagemax,
      extcolorid,
      sdoormin,
      sdoormax,
      sdriveside,
      smetermin,
      smetermax,
      sengsizemin,
      sengsizemax,
      senginetype,
      spic,
      vehiclecategoryid,
    } = this.state;
    return (
      <div className="container-fluid bg-orange home-advance-offset">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="b-find b-find_sm">
                <form className="b-find__form">
                  <div className="b-find__row">
                    <div className="b-find__main pr-0">
                      <div className="b-find__inner responsive-800">
                        <div className="b-find__item">
                          <div className="b-find__selector search_text_color">
                            <select data-width="100%"
                              onChange={this.handleMakeChange}
                              value={snewmake}
                            >
                              <option value="" selected>
                                Brand
                              </option>
                              {this.state.lbrand.map((brand) => (
                                <option key={brand.MakeID} value={brand.MakeID}>
                                  {brand.MakeName}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                        
                        <div className="b-find__item">
                          <div className="b-find__selector">
                            <select
                              onChange={this.handleConditionChange}
                              value={svtype}
                              >
                              <option value="" selected>
                                New / Used 
                              </option>
                              {this.state.ltype.map((type) => (
                                <option
                                  key={type.VehicleTypeID}
                                  value={type.VehicleTypeID}
                                >
                                  {type.VehicleTypeName}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>

                        <div className="b-find__item">
                          <div className="b-find__selector">
                            <select
                              onChange={this.handleCityChange}
                              value={cityid}>
                              <option value="" selected>
                                Select City
                              </option>
                              {this.state.lcity.map((city) => (
                                <option key={city.CityID} value={city.CityID}>
                                  {city.CityName}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>

                        <div className="b-find__item">
                          <div className="b-find__selector">
                          <Consumer>
                  {({ handleChildQuickSrchAdPost }) => (
                              <Link 
                                className="b-find__btn btn btn-primary"
                                onClick={() =>
                                  handleChildQuickSrchAdPost(
                                    ssellertype,
                                    svtype,
                                    cityid,
                                    sowner,
                                    minVal,
                                    maxVal,
                                    snewmake,
                                    smodel,
                                    sversion,
                                    versionyearfrom,
                                    versionyearto,
                                    sassembly,
                                    bodytypeid,
                                    transmissionid,
                                    mileagemin,
                                    mileagemax,
                                    extcolorid,
                                    sdoormin,
                                    sdoormax,
                                    sdriveside,
                                    smetermin,
                                    smetermax,
                                    sengsizemin,
                                    sengsizemax,
                                    senginetype,
                                    spic,
                                    vehiclecategoryid
                                  )
                                }
                                 to="/vehicle-listing">Search
                              <i className="fas fa-search pl-2" />
                              </Link>
                              )}
                            </Consumer>
                          </div>
                        </div>
                        <div className="b-find__item d-flex justify-content-center align-items-center">
                          <div id="advsearch" className="b-find__selector">
                            <Link
                              to="advance-search"
                              style={{ borderBottom: "1px solid white" }}
                            >
                              Advance Search
                            </Link>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
Homesearch.contextType = MyContext;
export default Homesearch;