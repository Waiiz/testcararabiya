import React, { Component } from "react";
import { Link } from "react-router-dom";
import { apiBaseURL } from "../../ApiBaseURL";
import axios from "axios";
class Homerecentads extends Component {
  constructor(props) {
    super(props);
    this.isImageLoading = false;
    this.state = {
      llisting: [],
      toplist: [],
      error: null,
      validationError: "",
      AdsListHeight: ''
    };
  }

  componentDidMount() {
    const apiUrlListing = "https://cararabiya.ahalfa.com/api/Listing/AllListing";

    fetch(apiUrlListing)
      .then((res) => res.json())
      .then(
        (result) => {
          console.log(result)
          if (result.length > 0) {
          this.setState({
            llisting: result,
          });
          if (result.length > 4) {
            var newlist = [];
            var list = [];
            for (var i = 0; i < 4; i++) {
              newlist.push(result[i]);
            }
            this.setState({
              toplist: newlist,
            });

            try {
              const getAdsListHeight = this['listItem_' + 0].offsetHeight;
              this.setState({
                AdsListHeight: getAdsListHeight + 12,
              });
            }
            catch (e) {
                console.log(e);
            }

          } else {
            this.setState({
              toplist: result,
            });
          }
        } else {
          console.log("Listing");
        }
        },
        (error) => {
          this.setState({ error });
        }
      );
  }

  CheckImage = (imgpath) => {
    if (this.isImageLoading) {
      return;
    }
    this.isImageLoading = true;

    const url = `${apiBaseURL}/file/ImgisExist?path=${imgpath}`;
    axios
      .get(url)
      .then((res) => {
        this.isImageLoading = false;
        if (res.status === 200) {
          console.log(res.status, "ImgisExist");
        }
        else {
          console.log(
            res.status,
            res.statusText,
            "statuscode from ImgisExist api"
          );
        }
      })
      .catch((error) => {
        this.isImageLoading = false;
        console.log(error, "ImgisExist api");
      });
  }

  numberWithCommas = (price) => {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  render() {
    return (
      <div>
        <div className="container">
          <div className="margin">
            <div className="row">
              <div className="col-8 col-xl-10 col-lg-10 col-md-10 col-sm-9">
                <h4 className="ui-title-promo">Most Recent Ads</h4>
              </div>
              <div className="col-4 col-xl-2 col-lg-2 col-md-2 col-sm-3 pt-3" style={{display: 'none'}}>
                <a
                  className="btn btn-primary btn-sm"
                  href="#"
                  style={{ backgroundColor: "#D3E428" }}
                >
                  View all
                </a>
              </div>
            </div>

            <div className="row">
              {this.state.toplist.map((toplist, index) => (
                <div key={index} className="col-lg-3 col-md-6 col-sm-12 col-12" style={{width: '100%'}}>
                  <div className="b-goods-f">
                    <div className="b-goods-f__media" style={{height: '200px',backgroundColor: 'rgb(70, 78, 96)'}}>
                      <img
                        className="b-goods-f__img img-scale"
                        //src={this.CheckImage("http://cararabiya.ahalfa.com/Content/Uploads/Draft/23/download.jfif")}
                        src={toplist.ImageUrl}
                        alt="foto"
                        style={{ height: "200px", backgroundColor: 'rgb(70, 78, 96)' }}
                      />
                    </div>
                    <div className="b-goods-f__main bg-shadow">
                      <div className="b-goods-f__descrip">
                        <div
                          className="b-goods-f__title b-goods-f__title_myad"
                          style={{ marginBottom: "0px" }}
                        >
                          <Link
                            style={{ fontSize: "18px", fontWeight: 600 }}
                            to="/"
                          >
                            {toplist.Vehicle}
                          </Link>
                        </div>

                        <ul
                          ref={(listItem) => { this['listItem_' + index] = listItem; }}
                          className="b-goods-f__list list-unstyled row"
                          style={{
                            height: this.state.AdsListHeight + 'px',
                            fontSize: "11px",
                            padding: "5px 10px",
                            marginTop: "0px",
                            justifyContent: 'space-evenly',
                          }}
                        >
                          <div className="row">
                            <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                              <div>
                                <i
                                  className="fas fa-calendar-alt"
                                  style={{ color: "#D3E428", fontSize: "12px" }}
                                />
                              </div>
                              <div>
                                <span
                                  className="b-goods-f__list-info"
                                  style={{ color: "black" }}
                                >
                                  {toplist.VersionYear}
                                </span>
                              </div>
                            </div>

                            <div className="col-lg-3 col-md-3 col-sm-12 col-12" >
                              <div>
                                <i
                                  className="fas fa-tachometer-alt"
                                  style={{ color: "#D3E428", fontSize: "12px" }}
                                />
                              </div>
                              <div>
                                <span
                                  className="b-goods-f__list-info"
                                  style={{ color: "black" }}
                                >
                                  {toplist.Mileage === "" ? 0 : toplist.Mileage} 
                                </span>
                              </div>
                            </div>

                            <div className="col-lg-3 col-md-3 col-sm-12 col-12" >
                              <div>
                                <i
                                  className="fas fa-car-alt"
                                  style={{ color: "#D3E428", fontSize: "12px" }}
                                />
                              </div>
                              <div>
                                <span
                                  className="b-goods-f__list-info"
                                  style={{ color: "black" }}
                                >
                                  {toplist.VehicleTypeName}
                                </span>
                              </div>
                            </div>

                            <div className="col-lg-3 col-md-3 col-sm-12 col-12" >
                              <div>
                                <i
                                  className="fas fa-gas-pump"
                                  style={{ color: "#D3E428", fontSize: "12px" }}
                                />
                              </div>
                              <div>
                                <span
                                  className="b-goods-f__list-info"
                                  style={{ color: "black" }}
                                >
                                  {toplist.FuelTypeName}
                                </span>
                              </div>
                            </div>
                          </div>
                        </ul>
                      </div>

                      <div
                        className="b-goods-f__sidebar"
                        style={{
                          paddingTop: "10px",
                          borderTop: "1px solid #ddd",
                        }}
                      >
                        <a className="b-goods-f__bnr" href="#">
                          <img
                            src="./assets/media/content/b-goods/auto-check.png"
                            alt="auto check"
                          />
                        </a>
                        <span className="b-goods-f__price-group">
                          <span className="b-goods-f__pricee">
                            <span className="b-goods-f__price_col">
                              PRICE:&nbsp;
                            </span>
                            <span className="b-goods-f__price-numbb">
                              {toplist.Price === 0 ? 'Call For Price' : toplist.CurCode +' '+ this.numberWithCommas(toplist.Price)}
                            </span>
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Homerecentads;