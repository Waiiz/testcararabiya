import React, { Component } from 'react';

class Hometeststar extends Component {

    createStar = () => {
        let table = []
    
        for (let i = 0; i < this.props.number; i++) {
          table.push(<i key={i} className="fas fa-star"></i>)
        }
        return table
      }

    render() {
        return (
            <div className="mb-1">
                <span className="heading-secondary pr-2">User Rating: </span>
                {this.createStar()}
            </div>
        );
    }
}

export default Hometeststar;