import React, { Component } from "react";
import { Link } from "react-router-dom";
import AdPostConditions from '../AdPostConditions';

class Homebuysell extends Component {
  render() {
    return (
      <div>
        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-6 col-sm-12 sec-orange">
              <div className="b-bnr-2__section b-bnr-2__section_first">
                <span className="b-bnr-2__title">Want To Buy A Car?</span>
                <br />
                <br />
                <span className="b-bnr-2__text">
                  Search cars listed from thousand of individuals and auto
                  dealers
                </span>
                <div className="mt-4">
                  <Link className="btn btn-outline-light" to="/vehicle-listing">
                    VIEW LISTINGS
                  </Link>
                </div>
              </div>
            </div>
            <div className="col-lg-6 col-sm-12 sec-grey">
              <div className="b-bnr-2__section b-bnr-2__section_first">
                <span className="b-bnr-2__title">Want To Sell A Car?</span>
                <br />
                <br />
                <span className="b-bnr-2__text">
                  Would you like to sell your car? List your car and find
                  potential buyers.
                </span>
                <div className="mt-4">
                  {localStorage.getItem("user") === null || localStorage.getItem("user") === undefined ? (
                    <Link
                      className="btn btn-outline-light"
                      to="/login-signup"
                    >
                      SIGN UP NOW
                    </Link>
                  ) : (
                      <div
                        className=""
                      >
                        <AdPostConditions props={this.props} classname="btn btn-outline-light" />
                      </div>
                    )}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="car-top car-down">
          <span>
            <img src="./assets/media/general/go_top.png" />
          </span>
        </div>
      </div>
    );
  }
}
export default Homebuysell;