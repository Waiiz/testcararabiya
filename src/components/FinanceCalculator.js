import React, { Component } from "react";

class FinanceCalculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      carprice: "",
      downpayment: "",
      tenure: "",
      interestrate: "",
      priceMI: "0",
      priceTLA: "0",
      priceTA: "0",
      priceTIA: "0",
      showResultPrice: false,
    };
  }

  componentWillMount() {
    this.setState({
      carprice: this.props.price,
    });
  }

  handleChangeDownPayment = (event) => {
    this.setState({
      downpayment: event.target.value,
    });
  };

  handleChangeTenure = (event) => {
    this.setState({
      tenure: event.target.value,
    });
  };

  handleChangeInterestRate = (event) => {
    this.setState({
      interestrate: event.target.value,
    });
  };

  handlepriceonClick = (event) => {
    event.preventDefault();
    this.setState({
      showResultPrice: !this.state.showResultPrice,
    });

    //tenure
    var Tm = this.state.tenure * 12;

    // interest rate
    var Im = this.state.interestrate / 100 / 12;

    // downpayment
    var DownpaymentPER = this.state.downpayment / 100;
    var DownpaymentAMT = this.state.carprice * DownpaymentPER;

    //Lamt
    var Lamt = this.state.carprice - DownpaymentAMT;

    var Y = Im * Lamt;

    var a = Math.pow(Im + 1, Tm);
    var b = 1 / a;

    var Z = 1 - b;

    var totalloanamount = this.state.carprice - DownpaymentPER;
    var actualemi = Math.round(Y / Z);
    var TotalAmountto = Math.round(actualemi * Tm);

    this.setState({
      priceTLA: totalloanamount,
      priceMI: actualemi,
      priceTA: TotalAmountto,
      priceTIA: Y,
    });
  };

  handleisNumberKey = (event) => {
    var charCode = event.which ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  };

  numberWithCommas = (price) => {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  render() {
    return (
      <>
        <div
          className="mt-3 finance"
          style={{
            backgroundColor: "white",
            padding: "15px 30px",
            border: "1px solid #D3E428",
            boxShadow: "0 2px 3px 0 rgba(0, 0, 0, 0.1)",
            borderRadius: "8px",
          }}
        >
          <div className="mb-3">
            <span
              className="finance-title"
              style={{
                color: "#363636",
                fontSize: "18px",
                fontWeight: 700,
                textTransform: "uppercase",
              }}
            >
              Finance Calculator
            </span>
          </div>
          <form onSubmit={this.handlepriceonClick}>
            <div
              className="form-group"
              style={{ fontSize: "13px", marginTop: "30px" }}
            >
              <div className="row my-3">
                <div className="col-6 align-self-center px-0">
                  <label className="control-label" style={{ fontWeight: 600 }}>
                    CAR PRICE* <span style={{ color: "#D3E428" }}>(SAR)</span>
                  </label>
                </div>

                <div className="col-6">
                  <input
                    className="form-control"
                    style={{ color: "#363636" }}
                    value={this.state.carprice}
                    disabled
                  />
                </div>
              </div>
              <div className="row my-3">
                <div className="col-6 align-self-center px-0">
                  <label className="control-label" style={{ fontWeight: 600 }}>
                    DOWN PAYMENT* <span style={{ color: "#D3E428" }}>(%)</span>
                  </label>
                </div>

                <div className="col-6">
                  <input
                    className="form-control"
                    placeholder="Enter"
                    value={this.state.downpayment}
                    onChange={this.handleChangeDownPayment}
                    onKeyPress={this.handleisNumberKey}
                    style={{ color: "#363636" }}
                    type="number"
                    required
                  />
                </div>
              </div>
              <div className="row my-3">
                <div className="col-6 align-self-center px-0">
                  <label className="control-label" style={{ fontWeight: 600 }}>
                    TENURE* <span style={{ color: "#D3E428" }}>(YEAR)</span>
                  </label>
                </div>

                <div className="col-6">
                  <input
                    className="form-control"
                    value={this.state.tenure}
                    onChange={this.handleChangeTenure}
                    onKeyPress={this.handleisNumberKey}
                    placeholder="Enter"
                    style={{ color: "#363636" }}
                    type="number"
                    required
                  />
                </div>
              </div>
              <div className="row my-3">
                <div className="col-6 align-self-center px-0">
                  <label className="control-label" style={{ fontWeight: 600 }}>
                    INTEREST RATE* <span style={{ color: "#D3E428" }}>(%)</span>
                  </label>
                </div>

                <div className="col-6">
                  <input
                    className="form-control"
                    value={this.state.interestrate}
                    onChange={this.handleChangeInterestRate}
                    onKeyPress={this.handleisNumberKey}
                    placeholder="Enter"
                    style={{ color: "#363636" }}
                    type="text"
                    required
                  />
                </div>
              </div>
              <div className="pt-3">
                <button
                  type="submit"
                  className="btn btn-block"
                  style={{
                    border: "none",
                    backgroundColor: "#D3E428",
                    color: "white",
                  }}
                  value="Calculate"
                >
                  Calculate
                </button>
              </div>
              {this.state.showResultPrice && (
                <div
                  className="input full my-3"
                  style={{
                    padding: "20px",
                    backgroundColor: "#495057",
                    color: "white",
                    fontSize: "15px",
                    fontWeight: 700,
                    shadow: "0 2px 3px 0 rgba(0, 0, 0, 0.2)",
                    borderRadius: "8px",
                  }}
                >
                  <div className="row mb-3">
                    <div
                      className="col-6"
                      style={{ borderRight: "1px solid grey" }}
                    >
                      <label
                        className="string optional"
                        style={{
                          textAlign: "left",
                          marginBottom: 0,
                          lineHeight: 1.3,
                        }}
                      >
                        Monthly Installment
                      </label>
                    </div>
                    <div className="col-6 align-self-center">
                      <label
                        className="string optional my-0"
                        style={{ textAlign: "left", fontSize: "17px" }}
                      >
                        <span style={{ fontSize: "12px" }}>
                          {this.numberWithCommas(this.state.priceMI)}
                        </span>
                      </label>
                    </div>
                  </div>
                  <hr style={{ borderTop: "1px solid #e9ecef38" }} />

                  <div className="row mb-3">
                    <div
                      className="col-6"
                      style={{ borderRight: "1px solid grey" }}
                    >
                      <label
                        className="string optional"
                        style={{
                          textAlign: "left",
                          marginBottom: 0,
                          lineHeight: 1.3,
                        }}
                      >
                        Total Loan Amount
                      </label>
                    </div>
                    <div className="col-6 align-self-center">
                      <label
                        className="string optional my-0"
                        style={{ textAlign: "left", fontSize: "17px" }}
                      >
                        <span style={{ fontSize: "12px" }}>
                          {this.numberWithCommas(this.state.priceTLA)}
                        </span>
                      </label>
                    </div>
                  </div>
                  <hr style={{ borderTop: "1px solid #e9ecef38" }} />

                  <div className="row mb-3">
                    <div
                      className="col-6"
                      style={{ borderRight: "1px solid grey" }}
                    >
                      <label
                        className="string optional"
                        style={{
                          textAlign: "left",
                          marginBottom: 0,
                          lineHeight: 1.3,
                        }}
                      >
                        Total Amount
                      </label>
                    </div>
                    <div className="col-6 align-self-center">
                      <label
                        className="string optional my-0"
                        style={{ textAlign: "left", fontSize: "17px" }}
                      >
                        <span style={{ fontSize: "12px" }}>
                          {this.numberWithCommas(this.state.priceTA)}
                        </span>
                      </label>
                    </div>
                  </div>
                  <hr style={{ borderTop: "1px solid #e9ecef38" }} />

                  <div className="row mb-3">
                    <div
                      className="col-6"
                      style={{ borderRight: "1px solid grey" }}
                    >
                      <label
                        className="string optional"
                        style={{
                          textAlign: "left",
                          marginBottom: 0,
                          lineHeight: 1.3,
                        }}
                      >
                        Total Interest Amount
                      </label>
                    </div>
                    <div className="col-6 align-self-center">
                      <label
                        className="string optional my-0"
                        style={{ textAlign: "left", fontSize: "17px" }}
                      >
                        <span style={{ fontSize: "12px" }}>
                          {this.numberWithCommas(this.state.priceTIA)}
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </form>
        </div>
      </>
    );
  }
}

export default FinanceCalculator;