import React, { Fragment, Component } from "react";
import ReactPaginate from "react-paginate";
import axios from "axios";
import { apiBaseURL } from "../../ApiBaseURL";
import "../listing/paginate.css";
import { Link } from "react-router-dom";
import ErrorAlert from "../../pages/ErrorAlert";
import Loader from "../../pages/Loader";
import { MyContext } from "../../Context";
import _ from "lodash";

class DealerListing extends Component {
  static contextType = MyContext;
  constructor(props) {
    super(props);
    this.isLoading = false;
    this.previousContext = "";
    this.state = {
      //pagination
      offset: 0,
      perPage: 7,
      currentPage: 0,
      DealerListingData: [],
      isLoadingForDealerListing: false,
      toggleAlertMsg: false
    };
  }

  componentDidMount() {
    console.log("componentDidMount", this.context.data);
    this.previousContext = this.context.data;
    this.receivedData();
  }

  componentDidUpdate() {
    if (this.context.data.searchbyname === "searchbyname") {
      if (
        !_.isEqual(
          this.previousContext.searchname,
          this.context.data.searchname
        )
      ) {
        console.log("componentDidUpdate", this.context.data);
        //pagination
        this.setState({
          offset: 0,
          perPage: 7,
          currentPage: 0,
          DealerListingData: []
        });
        let searchname = this.context.data.searchname;
        this.previousContext = this.context.data;
        this.receivedDataBySearchname(searchname);
      }
    }
    if (this.context.data.reset === "reset") {
      if (!_.isEqual(this.previousContext, this.context.data)) {
        console.log("componentDidUpdate", this.context.data);
        //pagination
        this.setState({
          offset: 0,
          perPage: 7,
          currentPage: 0,
          DealerListingData: []
        });

        this.previousContext = this.context.data;
        this.receivedData();
      }
    }

    if (this.context.data.advsearch === "advsearch") {
      if (!_.isEqual(this.previousContext, this.context.data)) {
        console.log("componentDidUpdate", this.context.data);
        //pagination
        this.setState({
          offset: 0,
          perPage: 7,
          currentPage: 0,
          DealerListingData: []
        });
        this.context.data.searchname = "";
        let cityString = this.context.data.cityid;
        let makeString = this.context.data.makeString;
        let selPicDealer = this.context.data.selPicDealer;

        this.previousContext = this.context.data;
        this.receivedDataByAdvSearchDealer(
          cityString,
          makeString,
          selPicDealer
        );
      }
    }
  }

  receivedData = () => {
    this.setState({ isLoadingForDealerListing: true });
    const url = `${apiBaseURL}/dealer`;
    axios
      .get(url)
      .then(res => {
        if (res.data.length > 0) {
          this.setState({
            isLoadingForDealerListing: false,
            toggleAlertMsg: false
          });
          const data = res.data;
          const slice = data.slice(
            this.state.offset,
            this.state.offset + this.state.perPage
          );

          const DealerListingData = slice.map((dealerlist, index) =>
            this.showDealerListingData(dealerlist, index)
          );

          this.setState({
            pageCount: Math.ceil(data.length / this.state.perPage),
            DealerListingData
          });
        } else {
          this.setState({
            isLoadingForDealerListing: false,
            toggleAlertMsg: true,
            offset: 0,
            perPage: 7,
            currentPage: 0,
            DealerListingData: []
          });
        }
      })
      .catch(error => {
        this.setState({
          isLoadingForDealerListing: false,
          toggleAlertMsg: true,
          offset: 0,
          perPage: 7,
          currentPage: 0,
          DealerListingData: []
        });
        console.log(error, "from AdvSearchDealer api");
      });
  };

  receivedDataBySearchname = searchname => {
    const url = `${apiBaseURL}/Dealer/DealerbyKeywords?Keywords=${searchname}`;
    console.log(url, "url");
    axios
      .get(url)
      .then(res => {
        if (res.data.length > 0) {
          this.setState({
            isLoadingForDealerListing: false,
            toggleAlertMsg: false
          });
          const data = res.data;
          const slice = data.slice(
            this.state.offset,
            this.state.offset + this.state.perPage
          );

          const DealerListingData = slice.map((dealerlist, index) =>
            this.showDealerListingData(dealerlist, index)
          );

          this.setState({
            pageCount: Math.ceil(data.length / this.state.perPage),
            DealerListingData
          });
        } else {
          this.setState({
            isLoadingForDealerListing: false,
            toggleAlertMsg: true,
            offset: 0,
            perPage: 7,
            currentPage: 0,
            DealerListingData: []
          });
        }
      })
      .catch(error => {
        this.setState({
          isLoadingForDealerListing: false,
          toggleAlertMsg: true,
          offset: 0,
          perPage: 7,
          currentPage: 0,
          DealerListingData: []
        });
        console.log(error, "from AdvSearchDealer api");
      });
  };

  receivedDataByAdvSearchDealer = (cityString, makeString, selPicDealer) => {
    const url = `${apiBaseURL}/Search/AdvSearchDealer?CityID=${cityString}&MakeID=${makeString}&Image=${selPicDealer}`;
    console.log(url, "url");
    axios
      .get(url)
      .then(res => {
        if (res.data.length > 0) {
          this.setState({
            isLoadingForDealerListing: false,
            toggleAlertMsg: false
          });
          const data = res.data;
          const slice = data.slice(
            this.state.offset,
            this.state.offset + this.state.perPage
          );

          const DealerListingData = slice.map((dealerlist, index) =>
            this.showDealerListingData(dealerlist, index)
          );

          this.setState({
            pageCount: Math.ceil(data.length / this.state.perPage),
            DealerListingData
          });
        } else {
          this.setState({
            isLoadingForDealerListing: false,
            toggleAlertMsg: true,
            offset: 0,
            perPage: 7,
            currentPage: 0,
            DealerListingData: []
          });
        }
      })
      .catch(error => {
        this.setState({
          isLoadingForDealerListing: false,
          toggleAlertMsg: true,
          offset: 0,
          perPage: 7,
          currentPage: 0,
          DealerListingData: []
        });
        console.log(error, "from AdvSearchDealer api");
      });
  };
 
  handleUpdateDealerViewCount = (dealerid) => {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    const apiUrlUpdateDealerViewCount = `${apiBaseURL}/Dealer/UpdateDealerViewCount?DealerID=${dealerid}`;
    axios
      .post(apiUrlUpdateDealerViewCount)
      .then(res => {
        this.isLoading = false;
        console.log(res);
        if (res.status === 200) {
        } else {
          this.isLoading = false;
        }
      })
      .catch(error => {
        this.isLoading = false;
        console.log(error, "from GetDBranchbyDealerID api");
      });
  }

  showDealerListingData = (dealerlist, index) => {
    return (
      <div
        key={index}
        className="bg-shadow b-goods-f col-12 b-box row mx-0 my-4"
      >
        <div className="col-12 col-lg-12 px-0 mb-4 mt-1">
          <h4 className="b-heading">{dealerlist.dealerName}</h4>
        </div>

        <div className="col-12 col-lg-8 px-0 mb-3 pr-5">
          <div className="b-table">
            <div className="d-flex justify-content-between px-3">
              <span>About Us</span>
              <span style={{ color: "#D3E428" }}>
                {dealerlist.AboutUs}
              </span>
            </div>
          </div>
          <div className="b-table">
            <div className="d-flex justify-content-between px-3">
              <span>City</span>
              <span style={{ color: "#D3E428" }}> {dealerlist.cityName} </span>
            </div>
          </div>
          <div className="b-table">
            <div className="d-flex justify-content-between px-3">
              <span>Website</span>
              <span style={{ color: "#D3E428" }}> {dealerlist.website} </span>
            </div>
          </div>
          <div className="b-table">
            <div className="d-flex justify-content-between px-3">
              <span>UAN</span>
              <span style={{ color: "#D3E428" }}> {dealerlist.UAN}</span>
            </div>
          </div>
          <div className="b-table">
            <div className="d-flex justify-content-between px-3">
              <span>Contact Number</span>
              <span style={{ color: "#D3E428" }}>
                {" "}
                {dealerlist.contactNumber}{" "}
              </span>
            </div>
          </div>
        </div>

        <div
          className="col-12 col-lg-4 d-flex justify-content-center"
          style={{
            border: "1px solid #D3E428",
            borderRadius: "8px",
            marginBottom: "20px"
          }}
        >
          <img
            className="b-image align-self-center"
            src={dealerlist.dealerLogoURL}
            alt={dealerlist.dealerID}
            style={{ maxWidth: "245px", maxHeight: "190px" }}
          />
        </div>

        <div
          className="col-12 d-flex justify-content-end pr-0"
          style={{ fontSize: "15px" }}
        >
          <Link className="btn btn-secondary btn-sm" to={dealerlist.website}>
            Visit Website
          </Link>
          <Link
            className="btn btn-sm"
            to={{
              pathname: "/dealer-detail",
              state: { DealerID: dealerlist.dealerID }
            }}
            onClick={() => this.handleUpdateDealerViewCount(dealerlist.dealerID)}
            style={{
              marginLeft: "10px",
              border: "none",
              backgroundColor: "#D3E428",
              color: "white"
            }}
          >
            More Info
          </Link>
        </div>
      </div>
    );
  };
  
  handlePageClick = e => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.perPage;
    let searchname = this.context.data.searchname;
    let cityString = this.context.data.cityid;
    let makeString = this.context.data.makeString;
    let selPicDealer = this.context.data.selPicDealer;
    console.log(cityString, makeString, selPicDealer, 'test');
    this.setState(
      {
        currentPage: selectedPage,
        offset: offset
      },
      () => {
        if (searchname !== "") {
          this.receivedDataBySearchname(searchname);
        } else if (
          cityString === '0' ||
          makeString === '0' ||
          selPicDealer === true
        ) {
          this.receivedData();
        }
        else if (
          cityString !== undefined ||
          makeString !== undefined ||
          selPicDealer !== undefined
        ) {
          this.receivedDataByAdvSearchDealer(
            cityString,
            makeString,
            selPicDealer
          );
        } else {
          this.receivedData();
        }
      }
    );
  };

  render() {
    return (
      <Fragment>
        {this.state.toggleAlertMsg === true ? (
          <ErrorAlert show={this.state.toggleAlertMsg} />
        ) : (
          <Fragment>
          {this.state.isLoadingForDealerListing ? (
            <Loader />
          ) : (
            <span>
              {this.state.DealerListingData}
            <div style={{ padding: "40px 0px 20px" }}>
              <ReactPaginate
                previousLabel={"prev"}
                nextLabel={"next"}
                breakLabel={"..."}
                breakClassName={"break-me"}
                pageCount={this.state.pageCount}
                marginPagesDisplayed={2}
                pageRangeDisplayed={5}
                onPageChange={this.handlePageClick}
                containerClassName={"pagination"}
                subContainerClassName={"pages pagination"}
                activeClassName={"active"}
                forcePage={this.state.currentPage}
              />
            </div>
            </span>
            )}
          </Fragment>
        )}
      </Fragment>
    );
  }
}
DealerListing.contextType = MyContext;
export default DealerListing;