import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Consumer } from "../../Context";
import { apiUrlcity, apiUrlmake } from "../../ApiBaseURL";
import { Button, Modal } from "react-bootstrap";

class Dealersearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchname: "",

      lmake: [],
      smake: [],

      lcity: [],
      scity: [],
      cityString: "0",

      error: null,

      //images
      images: [
        { id: true, value: "Dealer with images" },
        { id: false, value: "Dealer without images" },
      ],
      //modals
      showModalCity: false,
      showModalMake: false,
      //advsearchpromo 3params
      makeString: "0",
      selPicDealer: "true",
    };
  }

  componentDidMount() {
    fetch(apiUrlcity)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lcity: result,
            });
          } else {
            console.log(result, "city api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );

    fetch(apiUrlmake)
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.length > 0) {
            this.setState({
              lmake: result,
            });
          } else {
            console.log(result, "make api");
          }
        },
        (error) => {
          this.setState({ error });
        }
      );
  }

  //searchbyname
  handlesearchNameChange = (event) => {
    this.setState({
      searchname: event.target.value,
    });
  };

  //searchbycity,make,image
  handlecityChange = (event) => {
    const searchkeyid = event.target.id;
    const cheked = event.target.checked;
    if (cheked == true) {
      var joined = this.state.scity;
      joined.push(searchkeyid);
      let joinedString = joined.toString();
      this.setState({
        cityString: joinedString,
        scity: joined,
      });
    } else {
      let filteredArray = this.state.scity.filter(
        (item) => item !== searchkeyid
      );
      let filteredArrayString = filteredArray.toString();
      this.setState({
        cityString: filteredArrayString,
        scity: filteredArray,
      });
    }
  };

  handlemakeChange = (event) => {
    const searchkeyid = event.target.id;
    const cheked = event.target.checked;

    if (cheked == true) {
      var joined = this.state.smake;
      joined.push(searchkeyid);
      let joinedString = joined.toString();
      this.setState({
        makeString: joinedString,
        smake: joined,
      });
    } else {
      let filteredArray = this.state.smake.filter(
        (item) => item !== searchkeyid
      );
      let filteredArrayString = filteredArray.toString();
      this.setState({
        makeString: filteredArrayString,
        smake: filteredArray,
      });
    }
  };

  handleImageChange = (image) => {
    this.setState({
      selPicDealer: image,
    });
  };

  //city modal
  handleCloseCity = () => {
    this.setState({
      showModalCity: false,
    });
  };
  handleShowCity = () => {
    this.setState({
      showModalCity: true,
    });
  };
  //make modal
  handleCloseMake = () => {
    this.setState({
      showModalMake: false,
    });
  };
  handleShowMake = () => {
    this.setState({
      showModalMake: true,
    });
  };

  modalCity = () => {
    return (
      <Modal show={this.state.showModalCity}>
        <Modal.Header>
          <Modal.Title>Select City</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="modal-body checkbox-list">
            <ul
              id="make_list_popular"
              name="make_list"
              className="list-unstyled inline list-links clearfix"
            >
              {this.state.lcity.map((city, index) => (
                <li key={index}>
                  <label className="checkbox">
                    <input
                      style={{ marginRight: 5 }}
                      id={city.CityID}
                      value={city.CityName}
                      onChange={this.handlecityChange}
                      type="checkbox"
                    />
                    {city.CityName}{" "}
                    <span className="badge badge-gray-light pull-right" />
                  </label>
                </li>
              ))}
            </ul>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.handleCloseCity}>
            Done
          </Button>
        </Modal.Footer>
      </Modal>
    );
  };

  modalMake = () => {
    return (
      <Modal show={this.state.showModalMake}>
        <Modal.Header>
          <Modal.Title>Select Make</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="modal-body checkbox-list">
            <ul
              id="make_list_popular"
              name="make_list"
              className="list-unstyled inline list-links clearfix"
            >
              {this.state.lmake.map((make, index) => (
                <li key={index}>
                  <label className="checkbox">
                    <input
                      style={{ marginRight: 5 }}
                      id={make.MakeID}
                      value={make.MakeName}
                      onChange={this.handlemakeChange}
                      type="checkbox"
                    />
                    &nbsp;{make.MakeName}
                    <span className="badge badge-gray-light pull-right" />
                  </label>
                </li>
              ))}
            </ul>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.handleCloseMake}>
            Done
          </Button>
        </Modal.Footer>
      </Modal>
    );
  };

  render() {
    const { cityString, makeString, selPicDealer } = this.state;
    return (
      <aside className="l-sidebar">
        <div className="widget widget-search section-sidebar ">
          <div className="b-filter-slider__title" style={{ fontSize: "14px" }}>
            Search by Name
          </div>
          <Consumer>
            {({ handleChildSearchName }) => (
              <form className="form-sidebar-adv row" id="search-global-form">
                <input
                  className="form-sidebar-adv__input form-control"
                  value={this.state.searchname}
                  onChange={this.handlesearchNameChange}
                  placeholder="Enter Name"
                />
                <button
                  type="button"
                  className="form-sidebar-adv__btn"
                  onClick={() => handleChildSearchName(this.state.searchname)}
                >
                  <i className="ic icon-magnifier"></i>
                </button>
              </form>
            )}
          </Consumer>
        </div>

        <div className="accordion" id="accordion-2">
          <div className="card">
            <div
              className="card-header px-0 py-0"
              id="headingTwo"
              data-toggle="collapse"
              data-target="#collapseTwo"
              aria-expanded="false"
              aria-controls="collapseTwo"
            >
              <h3
                className="widget-title"
                style={{
                  backgroundColor: "#e8e8e8",
                  color: "#253241",
                  textTransform: "none",
                  fontSize: "16px",
                  fontWeight: 600,
                }}
              >
                Quick Search
                <Consumer>
                  {({ handleChildResetSettings }) => (
                    <button
                      onClick={() =>
                        handleChildResetSettings(
                          cityString,
                          makeString,
                          selPicDealer,
                          this.state.searchname
                        )
                      }
                    >
                      <i class="fas fa-redo-alt"></i>
                    </button>
                  )}
                </Consumer>
              </h3>
            </div>
            <div
              className="accordion-body"
              id="collapseTwo"
              aria-labelledby="headingTwo"
              data-parent="#accordion-2"
            >
              <div className="card-body">
                <div className="widget-inner">
                  <div className="widget widget-search section-sidebar">
                    <div className="b-filter-slider__title">City</div>

                    {this.state.lcity.map((city, index) =>
                      index < 4 ? (
                        <div
                          key={index}
                          className="form-check col-12"
                          style={{ padding: 0 }}
                        >
                          <input
                            type="checkbox"
                            id={city.CityID}
                            value={city.CityName}
                            onChange={this.handlecityChange}
                          />
                          &nbsp;{city.CityName}
                        </div>
                      ) : (
                        ""
                      )
                    )}

                    <div style={{ marginLeft: "-22px" }} className="savesearch">
                      <Button
                        style={{
                          fontSize: "12px",
                          textDecoration: "underline",
                          color: "#D3E428",
                          border: 0,
                        }}
                        variant="light"
                        onClick={this.handleShowCity}
                      >
                        more choices
                      </Button>
                    </div>

                    <br />
                    <div className="b-filter-slider__title">Make</div>

                    {this.state.lmake.map((make, index) =>
                      index < 4 ? (
                        <div
                          key={index}
                          className="form-check col-12"
                          style={{ padding: 0 }}
                        >
                          <input
                            type="checkbox"
                            id={make.MakeID}
                            value={make.MakeName}
                            onChange={this.handlemakeChange}
                          />
                          &nbsp;{make.MakeName}
                        </div>
                      ) : (
                        ""
                      )
                    )}

                    <div style={{ marginLeft: "-22px" }} className="savesearch">
                      <Button
                        style={{
                          fontSize: "12px",
                          textDecoration: "underline",
                          color: "#D3E428",
                          border: 0,
                        }}
                        variant="light"
                        onClick={this.handleShowMake}
                      >
                        more choices
                      </Button>
                    </div>

                    <br />
                    <div className="b-filter-slider__title">Images</div>

                    <div id="forms" className="form-group">
                      <div className="row">
                        {this.state.images.map((image, index) => (
                          <div
                            key={index}
                            className="col-12 pt-1"
                            style={{ padding: 0 }}
                          >
                            <div className="label-group mb-3">
                              <div>
                                <input
                                  style={{
                                    opacity: 1,
                                    margin: "9px 10px 0 0",
                                  }}
                                  type="radio"
                                  value={image.id}
                                  checked={this.state.selPicDealer === image.id}
                                  onChange={() =>
                                    this.handleImageChange(image.id)
                                  }
                                />

                                <label
                                  className="form-check-label"
                                  style={{
                                    marginLeft: "18px",
                                    marginRight: "30px",
                                  }}
                                >
                                  {image.value}
                                </label>
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>

                    <hr />
                  </div>
                  <div id="special">
                    <Consumer>
                      {({ handleChildAdvSearchDealer }) => (
                        <Link
                          type="button"
                          className="btn btn-primary btn-block"
                          onClick={() =>
                            handleChildAdvSearchDealer(
                              cityString,
                              makeString,
                              selPicDealer
                            )
                          }
                        >
                          Search
                        </Link>
                      )}
                    </Consumer>
                  </div>
                  {/* modal city */}
                  {this.modalCity()}
                  {/* modal city */}
                  {this.modalMake()}
                </div>
              </div>
            </div>
          </div>
        </div>
      </aside>
    );
  }
}

export default Dealersearch;