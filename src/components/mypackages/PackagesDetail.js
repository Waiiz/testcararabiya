import React, { Component, Fragment } from "react";
import { apiBaseURL } from "../../ApiBaseURL";
import axios from "axios";
import ReactPaginate from "react-paginate";
import "../listing/paginate.css";
import _ from "lodash";
import { Button, Modal } from "react-bootstrap";
import { Link } from "react-router-dom";
import moment from "moment";
import Loader from "../../pages/Loader";
import ErrorAlert from "../../pages/ErrorAlert";
import Dealeradvertisement from "../../pages/Dealeradvertisement";

class PackagesDetail extends Component {
  constructor(props) {
    super(props);
    this.ispackagesummaryloading = false;
    this.isuserpackageadpostloading = false;
    this.ispackageloading = false;
    this.state = {
      userId: "",
      userpackage: "",
      isLoadingForOrderListing: false,
      GetPackageSummaryData: [],
      UserPackageAdPostData: [],
      PackagesData: [],
      ListingOrderData: [],
      GlobalListingOrderData: [],
      //pagination
      offset: 0,
      perPage: 7,
      currentPage: 0,
      listingData: [],
      toggleAlertMsg: false,

      start: "",
      end: "",
      totalResults: "",

      //sortby
      SortOrderListing: [
        { id: 1, value: "Newest to Oldest", method: "new" },
        { id: 2, value: "Oldest to Newest", method: "old" },
        { id: 3, value: "Price Highest to Lowest", method: "high" },
        { id: 4, value: "Price Lowest to Highest", method: "low" },
      ],
      selSortOrderValue: "",
      //modals
      showModal: false,
    };
  }

  componentDidMount() {
    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      let userid = parseUserData[0].UserID;
      this.setState({
        userId: parseUserData[0].UserID,
      });

      this.getOrderbyUserID(userid);
      this.getMyPackageSummary(userid);
      this.getUserPackageAdPost(userid);
    } else {
      this.props.history.push("/login-signup");
    }
  }

  getOrderbyUserID = (userid) => {
    this.setState({ isLoadingForOrderListing: true });
    const url = `${apiBaseURL}/Order/OrderbyUserID?UserID=${userid}`;
    axios
      .get(url)
      .then((res) => {
        this.setState({
          GlobalListingOrderData: res.data,
        });
        const ListingOrderData = res.data;
        this.setState({ isLoadingForOrderListing: false });

        this.receivedData(ListingOrderData);
      })
      .catch((error) => {
        this.setState({
          start: 0,
          end: 0,
          offset: 0,
          perPage: 7,
          currentPage: 0,
          totalResults: 0,
          toggleAlertMsg: true,
          listingData: [],
          isLoadingForOrderListing: false,
        });
        console.log(error, "from Order api");
      });
  };

  getMyPackageSummary = (userid) => {
    if (this.ispackagesummaryloading) {
      return;
    }
    this.ispackagesummaryloading = true;
    const url = `${apiBaseURL}/package/GetPackageSummarybyUserID?UserID=${userid}`;
    axios
      .get(url)
      .then((res) => {
        if (res.data.length > 0) {
          this.ispackagesummaryloading = false;
          this.setState({
            GetPackageSummaryData: res.data,
          });
        } else {
          this.ispackagesummaryloading = false;
          console.log(res.data, "GetPackageSummarybyUserID api");
        }
      })
      .catch((error) => {
        this.ispackagesummaryloading = false;
        console.log(error, "GetPackageSummarybyUserID api");
      });
  };

  getUserPackageAdPost = (userid) => {
    if (this.isuserpackageadpostloading) {
      return;
    }
    this.isuserpackageadpostloading = true;
    const url = `${apiBaseURL}/UserPackageAdPost/GetPackageAdPostbyUserID?UserID=${userid}`;
    axios
      .get(url)
      .then((res) => {
        if (res.data.length > 0) {
          this.isuserpackageadpostloading = false;
          console.log();
          this.setState({
            UserPackageAdPostData: res.data,
            userpackage: res.data[0].Cost,
          });
        } else {
          this.isuserpackageadpostloading = false;
          console.log(res.data, "UserPackageAdPost api");
        }
      })
      .catch((error) => {
        this.isuserpackageadpostloading = false;
        console.log(error, "UserPackageAdPost api");
      });
  };

  numberWithCommas = (price) => {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  receivedData = (listingorderdata) => {
    if (listingorderdata.length > 0) {
      this.setState({
        ListingOrderData: listingorderdata,
        totalResults: listingorderdata.length,
        toggleAlertMsg: false,
      });

      const data = listingorderdata;

      if (
        this.state.offset + 1 + this.state.perPage >
        listingorderdata.length
      ) {
        this.setState({
          end: listingorderdata.length,
        });
      } else {
        this.setState({
          end: this.state.offset + this.state.perPage,
        });
      }

      const slice = data.slice(
        this.state.offset,
        this.state.offset + this.state.perPage
      );

      const listingData = slice.map((listingorderdata, index) =>
        this.showUserOrderListingData(listingorderdata, index)
      );

      this.setState({
        pageCount: Math.ceil(data.length / this.state.perPage),
        start: this.state.offset + 1,
        listingData,
      });
    } else {
      this.setState({
        start: 0,
        end: 0,
        offset: 0,
        perPage: 7,
        currentPage: 0,
        totalResults: 0,
        toggleAlertMsg: true,
        listingData: [],
      });
    }
  };

  formattedDate = (datetime) => {
    let splitTimeDateWithT = datetime.split("T");
    let date = splitTimeDateWithT[0]; //T00:00:00
    let formatteddate = moment(date).format("D MMM YYYY");
    return formatteddate;
  };

  showUserOrderListingData = (listingorderdata, index) => {
    return (
      <Fragment key={index}>
        <div className="col-12 my-3 px-0">
          <div style={{ backgroundColor: "#e8e8e8" }}>
            <div
              className="row justify-content-between"
              style={{ padding: "30px", marginBottom: "60px" }}
            >
              <div>
                <span style={{ font: "600 17px Montserrat" }}>
                  {listingorderdata.PackageName} Package
                </span>
                <br />
                <span style={{ font: "font: 500 13px Montserrat" }}>
                  ORDER ID: {listingorderdata.OrderID}
                </span>
              </div>
              <div>
                <div style={{ position: "relative", float: "right" }}>
                  <span
                    className="ml-3"
                    style={{ font: "600 14px Montserrat" }}
                  >
                    SAR{" "}
                    {this.numberWithCommas(listingorderdata.Price) === "0"
                      ? "Free"
                      : this.numberWithCommas(listingorderdata.Price)}
                  </span>
                </div>
              </div>
            </div>
            <div
              className="row justify-content-between"
              style={{ padding: "15px 30px", borderTop: "1px solid #cecece" }}
            >
              <div className="savesearch">
                <span style={{ display: "none" }}>
                  <Link to="/">Renew</Link>
                </span>
              </div>

              <div
                style={{ color: "grey", fontSize: "0.9em" }}
                className="mr-2"
              >
                <span className="ml-2">
                  Subscribed:{" "}
                  <span style={{ color: "black" }}>
                    {this.formattedDate(listingorderdata.FromDate)}
                  </span>
                </span>
                <span className="ml-2">
                  Expiry:{" "}
                  <span style={{ color: "black" }}>
                    {this.formattedDate(listingorderdata.ToDate)}
                  </span>
                </span>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  };

  handlePageClick = (e) => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.perPage;

    this.setState(
      {
        currentPage: selectedPage,
        offset: offset,
      },
      () => {
        this.receivedData(this.state.ListingOrderData);
      }
    );
  };

  //sortby
  handleSortByOrderListing = (event) => {
    this.setState({ selSortOrderValue: event.target.value });
    let adpostData = [];
    adpostData = this.state.GlobalListingOrderData;

    if (event.target.value === "new") {
      adpostData = _.orderBy(adpostData, ["CreatedDate"], ["desc"]);
    } else if (event.target.value === "old") {
      adpostData = _.orderBy(adpostData, ["CreatedDate"], ["asc"]);
    } else if (event.target.value === "high") {
      adpostData = _.orderBy(adpostData, ["Price"], ["desc"]);
    } else if (event.target.value === "low") {
      adpostData = _.orderBy(adpostData, ["Price"], ["asc"]);
    } else {
      console.log("filter error");
    }

    this.setState({ ListingOrderData: adpostData });
    this.receivedData(adpostData);
  };

  showMyPackagesHeader = () => {
    return (
      <div className="row">
        <div className="col-6 px-0">
          <div className="list-heading">My Packages</div>
          <div className="b-filter-goods__info col-auto">
            Showing
            <strong>
              {" "}
              {this.state.start} - {this.state.end}{" "}
            </strong>{" "}
            out of <strong> {this.state.totalResults}</strong> Packages
          </div>
        </div>

        <div className="col-6" style={{ padding: 0 }}>
          <div className="b-filter-goods__wrap" style={{ float: "right" }}>
            <div className="b-filter-goods__select d-inline-flex">
              <select
                style={{ inlineSize: "auto" }}
                className="custom-select-adpost"
                onChange={this.handleSortByOrderListing}
                value={this.state.selSortOrderValue}
              >
                <option selected>Sort By</option>
                {this.state.SortOrderListing.map((sortlist) => (
                  <option key={sortlist.id} value={sortlist.method}>
                    {sortlist.value}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
      </div>
    );
  };

  showMyPackageSummary = () => {
    return (
      <Fragment>
        <aside className="l-sidebar">
          {this.state.GetPackageSummaryData.map((packagesummary, index) => (
            <div key={index} className="widget section-sidebar">
              <div className="widget-content">
                <div className="d-flex justify-content-between px-3">
                  <strong>Total Packages</strong>
                  <span>{packagesummary.TotalPackages}</span>
                </div>
                <hr />
                <div className="d-flex justify-content-between px-3">
                  <strong>Expired Packages</strong>
                  <span>{packagesummary.ExpiredCount}</span>
                </div>
                <hr />
              </div>
            </div>
          ))}
          {this.state.UserPackageAdPostData.map((packageadpost, index) => (
            <div key={index} className="widget section-sidebar">
              <div className="widget-content">
                <div
                  style={{
                    borderRadius: "8px",
                    border: "1px solid #D3E428",
                    padding: "20px 25px",
                    fontSize: "13px",
                  }}
                >
                  <div className="row">
                    <span>Active Package:</span>
                  </div>
                  <div className="row justify-content-end">
                    <span style={{ font: "800 16px Montserrat" }}>
                      {packageadpost.PackageName}
                    </span>
                  </div>
                  <hr />
                  <div className="d-flex justify-content-between py-1">
                    <span>Subscribed</span>
                    <span>{this.formattedDate(packageadpost.FromDate)}</span>
                  </div>

                  <div className="d-flex justify-content-between">
                    <span>Expired</span>
                    <span>{this.formattedDate(packageadpost.ToDate)}</span>
                  </div>
                  <hr />
                  <div className="d-flex justify-content-between py-1">
                    <span>Total Ads</span>
                    <span>{packageadpost.NumOfAds}</span>
                  </div>

                  <div className="d-flex justify-content-between py-1">
                    <span>Used Ads</span>
                    <span>{packageadpost.UsedAdPost}</span>
                  </div>
                  <div className="d-flex justify-content-between">
                    <span>Available Ads</span>
                    <span>{packageadpost.RemainingAds}</span>
                  </div>
                  <div className="mt-3">
                    <button
                      onClick={this.handleOpenModal}
                      className="btn btn-block"
                      style={{
                        border: "none",
                        backgroundColor: "#D3E428",
                        color: "white",
                      }}
                    >
                      Upgrade Package
                    </button>
                  </div>
                </div>
              </div>
            </div>
          ))}

          <div>
            <Dealeradvertisement />
          </div>
        </aside>
      </Fragment>
    );
  };

  // modal
  handleOpenModal = () => {
    if (this.ispackageloading) {
      return;
    }
    this.ispackageloading = true;
    const url = `${apiBaseURL}/Package`;
    axios
      .get(url)
      .then((res) => {
        if (res.data.length > 0) {
          this.ispackageloading = false;
          this.setState({
            PackagesData: res.data,
            showModal: true,
          });
        } else {
          this.ispackageloading = false;
          console.log(res.data, "Packages api");
        }
      })
      .catch((error) => {
        this.ispackageloading = false;
        console.log(error, "Packages api");
      });
  };

  handleCloseModal = () => {
    this.setState({
      showModal: false,
    });
  };

  showModal = () => {
    return (
      <Modal
        show={this.state.showModal}
        size="lg"
        centered
        onHide={this.handleCloseModal}
      >
        <div className="modal-body">
          <section className="pricing-area pb-50" id="pricing">
            <div className="container">
              <div className="row">
                <div className="col-xl-8 mx-auto text-center">
                  <div className="section-title">
                    <h2>select package</h2>
                  </div>
                </div>
              </div>

              <div className="row justify-content-center">
                {this.state.PackagesData.map((packages, index) => (
                  <div key={index} className="col-xl-4">
                    <div className="single-price bg-shadow">
                      <div className="price-title">
                        <h4>{packages.PackageName}</h4>
                      </div>
                      <div className="price-tag">
                        <h2>
                          {packages.Cost === "0" ? (
                            "FREE"
                          ) : (
                            <Fragment>
                              <span
                                style={{ fontSize: "20px", fontWeight: "600" }}
                              >
                                SAR
                              </span>{" "}
                              {packages.Cost} <span>/ AD</span>
                            </Fragment>
                          )}
                        </h2>
                      </div>
                      <div className="price-item">
                        <ul>
                          <li>
                            <i
                              className="fas fa-check pr-1"
                              style={{ color: "#47c77e" }}
                            ></i>
                            Upto{" "}
                            {packages.NumOfImages === -1
                              ? "Unlimited"
                              : packages.NumOfImages}{" "}
                            photos
                          </li>
                          <li>
                            <i
                              className="fas fa-check pr-1"
                              style={{ color: "#47c77e" }}
                            ></i>
                            {packages.NumOfDays} Days of Run time
                          </li>
                          <li>
                            <i
                              className="fas fa-check pr-1"
                              style={{ color: "#47c77e" }}
                            ></i>
                            Allow{" "}
                            {packages.NumOfAds === 1 ? (
                              <Fragment>
                                {packages.NumOfAds} Ad to
                                <br />
                                posting
                              </Fragment>
                            ) : (
                              <Fragment>
                                {packages.NumOfAds} Ads to
                                <br />
                                posting
                              </Fragment>
                            )}
                          </li>
                          <li>
                            <i
                              className={
                                packages.Featured === false
                                  ? "fas fa-times pr-1"
                                  : "fas fa-check pr-1"
                              }
                              style={{
                                color:
                                  packages.Featured === false
                                    ? "red"
                                    : "#47c77e",
                              }}
                            ></i>
                            Featured Listing
                          </li>
                          <li>
                            <i
                              className={
                                packages.NumOfVideos === 0
                                  ? "fas fa-times pr-1"
                                  : "fas fa-check pr-1"
                              }
                              style={{
                                color:
                                  packages.NumOfVideos === 0
                                    ? "red"
                                    : "#47c77e",
                              }}
                            ></i>
                            Allow{" "}
                            {packages.NumOfVideos === 0
                              ? ""
                              : packages.NumOfVideos === -1
                              ? "Unlimited"
                              : packages.NumOfVideos}{" "}
                            Videos
                          </li>
                        </ul>
                      </div>
                      {this.state.userpackage >= packages.Cost ? (
                        <Link
                          style={{ cursor: "auto", color: "black" }}
                          className="box-btn"
                        >
                          SELECTED
                        </Link>
                      ) : (
                        <Link
                          onClick={() =>
                            this.handleUpdatePackage(packages.PackageID)
                          }
                          style={{ cursor: "pointer", color: "white" }}
                          className="box-btn"
                        >
                          SELECT PLAN
                        </Link>
                      )}
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </section>
        </div>
      </Modal>
    );
  };

  handleUpdatePackage = (packageid) => {
    const url = `${apiBaseURL}/UserPackageAdpost/GetUpdatedPackagebyUserIDPackageID?UserID=${this.state.userId}&PackageID=${packageid}`;
    axios
      .get(url)
      .then((res) => {
        console.log(res, "res from ApiParamForUpdatePackage api");
        if (res.status === 200) {
          this.setState({
            UserPackageAdPostData: res.data,
            userpackage: res.data[0].Cost,
            showModal: false,
          });
          this.getOrderbyUserID(this.state.userId);
          this.getMyPackageSummary(this.state.userId);
          this.showMyPackageSummary();
          console.log(res.status, "ApiParamForUpdatePackage");
        } else {
          console.log(
            res.status,
            "statuscode from ApiParamForUpdatePackage api"
          );
        }
      })
      .catch((error) => {
        console.log(error, "from ApiParamForUpdatePackage api");
      });
  };

  render() {
    return (
      <Fragment>
        {this.showModal()}
        <div className="row">
          <div className="col-lg-3">{this.showMyPackageSummary()}</div>

          <div className="col-lg-9">
            {this.showMyPackagesHeader()}
            <br />

            {this.state.toggleAlertMsg === true ? (
              <ErrorAlert show={this.state.toggleAlertMsg} />
            ) : (
              <Fragment>
                {this.state.isLoadingForOrderListing ? (
                  <Loader />
                ) : (
                  <span>
                    {this.state.listingData}
                    <br />
                    <ReactPaginate
                      previousLabel={"prev"}
                      nextLabel={"next"}
                      breakLabel={"..."}
                      breakClassName={"break-me"}
                      pageCount={this.state.pageCount}
                      marginPagesDisplayed={2}
                      pageRangeDisplayed={5}
                      onPageChange={this.handlePageClick}
                      containerClassName={"pagination"}
                      subContainerClassName={"pages pagination"}
                      activeClassName={"active"}
                      forcePage={this.state.currentPage}
                    />
                  </span>
                )}
              </Fragment>
            )}
          </div>
        </div>
      </Fragment>
    );
  }
}
export default PackagesDetail;