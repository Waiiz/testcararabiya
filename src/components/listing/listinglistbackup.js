import React, { Component } from "react";
import Listinglatestadstop from "./Listinglatestadstop";
import Listingadvertisementmiddle from "./Listingadvertisementmiddle";
import ReactPaginate from "react-paginate";
import axios from "axios";
import { apiBaseURL } from "../../ApiBaseURL";
import "./paginate.css";
import { MyContext } from "../../Context";
import _ from "lodash";
import { Alert } from "react-bootstrap";
import { Link } from "react-router-dom";

class listinglistbackup extends Component {
  static contextType = MyContext;
  constructor(props) {
    super(props);
    this.previousContext = "";
    this.state = {
      //pagination
      offset: 0,
      perPage: 7,
      currentPage: 0,
      listingData: [],
      toggleAlertMsg: false,

      start: "",
      end: "",
      totalResults: ""
    };
  }

  componentDidMount() {
    //console.log("componentDidMount", this.context.data);
    this.previousContext = this.context.data;

    let sellertypeid = this.previousContext.ssellertype;
    let vehicletypeid = this.previousContext.vehicleTypeID;
    let cityid = this.previousContext.scity;
    let numberofowners = this.previousContext.sowner;
    let pricemin = this.previousContext.minVal;
    let pricemax = this.previousContext.maxVal;
    let makeid = this.previousContext.smake;
    let modelid = this.previousContext.smodel;
    let versionid = this.previousContext.sversion;
    let versionyearfrom = this.previousContext.versionyearfrom;
    let versionyearto = this.previousContext.versionyearto;
    let assembly = this.previousContext.sassembly;
    let bodytypeid = this.previousContext.bodytypeid;
    let transmissionid = this.previousContext.transmissionid;
    let mileagemin = this.previousContext.mileagemin;
    let mileagemax = this.previousContext.mileagemax;
    let extcolorid = this.previousContext.extcolorid;
    let sdoormin = this.previousContext.sdoormin;
    let sdoormax = this.previousContext.sdoormax;
    let sdriveside = this.previousContext.sdriveside;
    let smetermin = this.previousContext.smetermin;
    let smetermax = this.previousContext.smetermax;
    let sengsizemin = this.previousContext.sengsizemin;
    let sengsizemax = this.previousContext.sengsizemax;
    let senginetype = this.previousContext.senginetype;
    let spic = this.previousContext.spic;

    this.receivedData(
      sellertypeid,
      vehicletypeid,
      cityid,
      numberofowners,
      pricemin,
      pricemax,
      makeid,
      modelid,
      versionid,
      versionyearfrom,
      versionyearto,
      assembly,
      bodytypeid,
      transmissionid,
      mileagemin,
      mileagemax,
      extcolorid,
      sdoormin,
      sdoormax,
      sdriveside,
      smetermin,
      smetermax,
      sengsizemin,
      sengsizemax,
      senginetype,
      spic
    );
  }

  componentDidUpdate() {
    if (!_.isEqual(this.previousContext, this.context.data)) {
      //console.log("componentDidUpdate", this.context.data);
      this.setState({
        start: 0,
        end: 0,
        offset: 0,
        currentPage: 0,
        perPage: 7
      });
      let sellertypeid = this.context.data.ssellertype;
      let vehicletypeid = this.context.data.vehicleTypeID;
      let cityid = this.context.data.scity;
      let numberofowners = this.context.data.sowner;
      let pricemin = this.context.data.minVal;
      let pricemax = this.context.data.maxVal;
      let makeid = this.context.data.smake;
      let modelid = this.context.data.smodel;
      let versionid = this.context.data.sversion;
      let versionyearfrom = this.context.data.versionyearfrom;
      let versionyearto = this.context.data.versionyearto;
      let assembly = this.context.data.sassembly;
      let bodytypeid = this.context.data.bodytypeid;
      let transmissionid = this.context.data.transmissionid;
      let mileagemin = this.context.data.mileagemin;
      let mileagemax = this.context.data.mileagemax;
      let extcolorid = this.context.data.extcolorid;
      let sdoormin = this.context.data.sdoormin;
      let sdoormax = this.context.data.sdoormax;
      let sdriveside = this.context.data.sdriveside;
      let smetermin = this.context.data.smetermin;
      let smetermax = this.context.data.smetermax;
      let sengsizemin = this.context.data.sengsizemin;
      let sengsizemax = this.context.data.sengsizemax;
      let senginetype = this.context.data.senginetype;
      let spic = this.context.data.spic;

      this.previousContext = this.context.data;

      this.receivedData(
        sellertypeid,
        vehicletypeid,
        cityid,
        numberofowners,
        pricemin,
        pricemax,
        makeid,
        modelid,
        versionid,
        versionyearfrom,
        versionyearto,
        assembly,
        bodytypeid,
        transmissionid,
        mileagemin,
        mileagemax,
        extcolorid,
        sdoormin,
        sdoormax,
        sdriveside,
        smetermin,
        smetermax,
        sengsizemin,
        sengsizemax,
        senginetype,
        spic
      );
    }
  }

  receivedData = (
    sellertypeid,
    vehicletypeid,
    cityid,
    numberofowners,
    pricemin,
    pricemax,
    makeid,
    modelid,
    versionid,
    versionyearfrom,
    versionyearto,
    assembly,
    bodytypeid,
    transmissionid,
    mileagemin,
    mileagemax,
    extcolorid,
    sdoormin,
    sdoormax,
    sdriveside,
    smetermin,
    smetermax,
    sengsizemin,
    sengsizemax,
    senginetype,
    spic
  ) => {
    const url = `${apiBaseURL}/Listing/AdvSearchListing?SellerTypeID=${sellertypeid}&VehicleTypeID=${vehicletypeid}&CityID=${cityid}&NumberofOwners=${numberofowners}&Price_min=${pricemin}&Price_max=${pricemax}&MakeID=${makeid}&ModelID=${modelid}&VersionID=${versionid}&VersionYearFrom=${versionyearfrom}&VersionYearTo=${versionyearto}&Assembly=${assembly}&BodyTypeID=${bodytypeid}&TransmissionID=${transmissionid}&Mileage_min=${mileagemin}&Mileage_max=${mileagemax}&ExtColorID=${extcolorid}&Doors_min=${sdoormin}&Doors_max=${sdoormax}&DriveSide=${sdriveside}&MeterReading_min=${smetermin}&MeterReading_max=${smetermax}&EngineSize_min=${sengsizemin}&EngineSize_max=${sengsizemax}&EngineType=${senginetype}&Image1=${spic}&VehicleCategoryID=${1}`;
    //console.log(url, "url");
    axios
      .get(url)
      .then(res => {
        console.log(res.data);
        if (res.data.length > 0) {
          this.setState({
            totalResults: res.data.length,
            toggleAlertMsg: false
          });

          const data = res.data;

          if (
            this.state.offset + 1 + this.state.perPage >
            this.state.totalResults
          ) {
            this.setState({
              end: this.state.totalResults
            });
          } else {
            this.setState({
              end: this.state.offset + this.state.perPage
            });
          }

          const slice = data.slice(
            this.state.offset,
            this.state.offset + this.state.perPage
          );
          const listingData = slice.map((toplist, index) => (
            <div key={index} className="col-12 line-o my-3 px-0 bg-shadow">
              {/*Listing start*/}
              <div className="row">
                <div className="col-lg-8 px-0">
                  <div className="row">
                    <div className="col-12 px-0 pl-3 mt-2 mb-3">
                      <div className="d-flex align-items-center Ad-top-sec">
                        <i className="fas fa-star mr-2" />
                        <span className="mr-3">Featured ad</span>
                        <i className="fas fa-bolt mr-2" />
                        <span className="mr-3">Super hot</span>
                        <button
                          type="submit"
                          className="btn btn-sm liketoggle like Ad-like-btn-style"
                          name="like"
                        >
                          <i className="fas fa-heart" /> <span>Save</span>
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-md-5">
                      <div
                        className="b-goods-f__listing mb-2"
                        style={{ height: "134px" }}
                      >
                        <a href="Detailed-Ad.html">
                          <img
                            className="watermark-listing"
                            src="./assets/media/watermark.png"
                            style={{ height: "134px" }}
                          />
                          <img
                            className="b-goods-f__img img-scale"
                            src={toplist.ImageUrl}
                            style={{ height: "134px" }}
                            alt="foto"
                          />
                        </a>
                      </div>

                      <span
                        className="Ad-location"
                        style={{ display: "flex", alignItems: "center" }}
                      >
                        <i className="fas fa-map-marker-alt" />
                        <span style={{ margin: "0 0 10px 8px" }}>
                          {toplist.Area}
                        </span>
                      </span>
                    </div>
                    <div className="col-12 col-md-7">
                      <div className="b-goods-f__main d-flex">
                        <div className="b-goods-f__descrip">
                          <div className="b-goods-f__title_myad">
                            <Link
                              to={{
                                pathname: "/postad-detail",
                                state: {
                                  AdPostID: toplist.AdPostID
                                }
                              }}
                            >
                              {toplist.Vehicle}{" "}
                            </Link>
                          </div>
                          <div className="b-goods-f__info Ad-posted-date">
                            {toplist.CreateDate}
                          </div>
                          <ul className="b-goods-f__list list-unstyled mt-1 fsize-car-descr">
                            <li className="b-goods-f__list-item">
                              <span className="b-goods-f__list-title">
                                Mileage :
                              </span>
                              <span className="b-goods-f__list-info Ad-mileage">
                                {toplist.Mileage}
                              </span>
                            </li>
                            <li className="b-goods-f__list-item">
                              <span className="b-goods-f__list-title">
                                Model :
                              </span>
                              <span className="b-goods-f__list-info Ad-Model">
                                {toplist.VersionYear}
                              </span>
                            </li>
                            <li className="b-goods-f__list-item">
                              <span className="b-goods-f__list-title">
                                Transmission :
                              </span>
                              <span className="b-goods-f__list-info Ad-Transmission">
                                {toplist.TransmissionName}
                              </span>
                            </li>
                            <li className="b-goods-f__list-item">
                              <span className="b-goods-f__list-title">
                                Color :
                              </span>
                              <span className="b-goods-f__list-info Ad-fuel">
                                {toplist.ColorName}
                              </span>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="Ad-email-listing">
                        <a style={{ color: "grey" }} href="#">
                          <span className="email-font-size mr-3">
                            <i className="far fa-envelope mr-1" /> Email this
                            listing
                          </span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4 col-sm-12 px-0 list-sec-r">
                  {" "}
                  {/* price section */}
                  <div className="Ad-price-sec">
                    <span className="Ad-price-style">
                      {toplist.CurCode} {toplist.Price}
                    </span>
                  </div>
                  <div>
                    {" "}
                    {/* buttons */}
                    <div className="b-goods-f__sidebar px-3">
                      <span className="b-goods-f__price-group">
                        <button
                          type="button"
                          className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center Ad-btn-grey"
                        >
                          {" "}
                          <i className="fas fa-phone prefix justify-content-between" />
                          <span className="Ad-btn-grey-s">
                            Show Phone Number
                          </span>
                        </button>
                        <button
                          type="button"
                          className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center Ad-btn-grey"
                        >
                          {" "}
                          <i className="fas fa-envelope prefix justify-content-start" />
                          <span className="Ad-btn-grey-s">Show Email</span>
                        </button>
                        <button
                          type="button"
                          className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center Ad-btn-grey"
                        >
                          {" "}
                          <i className="far fa-comment-dots prefix justify-content-center" />
                          <span className="Ad-btn-grey-s">
                            Chat with seller
                          </span>
                        </button>
                      </span>
                      <span className="b-goods-f__compare">
                        <div className="form-check d-flex align-self-center">
                          <input
                            className="form-check-input Atocompare-checkbox "
                            type="checkbox"
                            id="inlineCheckbox1"
                            defaultValue={1}
                          />
                          <label className="form-check-label Atocompare-text">
                            Add to compare
                          </label>
                        </div>
                      </span>
                    </div>
                  </div>
                </div>
              </div>{" "}
              {/* end row */}
            </div>
          ));

          this.setState({
            pageCount: Math.ceil(data.length / this.state.perPage),
            start: this.state.offset + 1,
            listingData
          });
        } else {
          this.setState({
            start: 0,
            end: 0,
            offset: 0,
            perPage: 7,
            currentPage: 0,
            totalResults: 0,
            toggleAlertMsg: true,
            listingData: []
          });
        }
      })
      .catch(error => {
        this.setState({
          start: 0,
          end: 0,
          offset: 0,
          perPage: 7,
          currentPage: 0,
          totalResults: 0,
          toggleAlertMsg: true,
          listingData: []
        });
        console.log(error, "from AdvSearchListing api");
      });
  };

  handlePageClick = e => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.perPage;

    this.setState(
      {
        currentPage: selectedPage,
        offset: offset
      },
      () => {
        let sellertypeid = this.context.data.ssellertype;
        let vehicletypeid = this.context.data.vehicleTypeID;
        let cityid = this.context.data.scity;
        let numberofowners = this.context.data.sowner;
        let pricemin = this.context.data.minVal;
        let pricemax = this.context.data.maxVal;
        let makeid = this.context.data.smake;
        let modelid = this.context.data.smodel;
        let versionid = this.context.data.sversion;
        let versionyearfrom = this.context.data.versionyearfrom;
        let versionyearto = this.context.data.versionyearto;
        let assembly = this.context.data.sassembly;
        let bodytypeid = this.context.data.bodytypeid;
        let transmissionid = this.context.data.transmissionid;
        let mileagemin = this.context.data.mileagemin;
        let mileagemax = this.context.data.mileagemax;
        let extcolorid = this.context.data.extcolorid;
        let sdoormin = this.context.data.sdoormin;
        let sdoormax = this.context.data.sdoormax;
        let sdriveside = this.context.data.sdriveside;
        let smetermin = this.context.data.smetermin;
        let smetermax = this.context.data.smetermax;
        let sengsizemin = this.context.data.sengsizemin;
        let sengsizemax = this.context.data.sengsizemax;
        let senginetype = this.context.data.senginetype;
        let spic = this.context.data.spic;

        this.receivedData(
          sellertypeid,
          vehicletypeid,
          cityid,
          numberofowners,
          pricemin,
          pricemax,
          makeid,
          modelid,
          versionid,
          versionyearfrom,
          versionyearto,
          assembly,
          bodytypeid,
          transmissionid,
          mileagemin,
          mileagemax,
          extcolorid,
          sdoormin,
          sdoormax,
          sdriveside,
          smetermin,
          smetermax,
          sengsizemin,
          sengsizemax,
          senginetype,
          spic
        );
      }
    );
  };

  render() {
    return (
      <div>
        <div className="b-filter-goods pb-0">
          <div className="row justify-content-between align-items-center">
            <div className="b-filter-goods__info col-auto">
              Showing
              <strong>
                {" "}
                {this.state.start} - {this.state.end}
              </strong>
              &nbsp; out of <strong> {this.state.totalResults}</strong> listings
            </div>
          </div>
        </div>
        <Listinglatestadstop />

        {this.state.toggleAlertMsg === true ? (
          <Alert variant="primary" show={this.state.toggleAlertMsg}>
            <p style={{ color: "#721c24", fontWeight: 600 }}>
              Data not found...
            </p>
          </Alert>
        ) : (
          <div>
            {this.state.listingData}

            <Listingadvertisementmiddle />

            <ReactPaginate
              previousLabel={"prev"}
              nextLabel={"next"}
              breakLabel={"..."}
              breakClassName={"break-me"}
              pageCount={this.state.pageCount}
              marginPagesDisplayed={2}
              pageRangeDisplayed={5}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"}
              forcePage={this.state.currentPage}
            />
          </div>
        )}
      </div>
    );
  }
}
listinglistbackup.contextType = MyContext;
export default listinglistbackup;