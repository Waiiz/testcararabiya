import React, { Component } from "react";

class Listinglatestadsleft extends Component {
  render() {
    return (
      <div>
        <div
          className="d-flex mb-3 pb-3"
          style={{ borderBottom: "1px solid #cacaca" }}
        >
          <div className="mr-2">
            <img
              src="./assets/media/content/b-goods/300x220/2.jpg"
              alt=""
              width="80px"
              height="100%"
            />
          </div>
          <div
            className="b-goods-f__title_myad"
            style={{ display: "inline-block" }}
          >
            <span style={{ fontSize: "11px", fontWeight: 600, color: "#000" }}>
              <a
                style={{ fontSize: "11px", fontWeight: 600 }}
                href="Detailed-Ad.html"
              >
                Honda Vezel for Sale
              </a>
            </span>
            <br />
            <span
              style={{ fontSize: "11px", fontWeight: 600, color: "#D3E428" }}
            >
              SAR 95,000
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default Listinglatestadsleft;
