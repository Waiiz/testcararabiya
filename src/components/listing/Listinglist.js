import React, { Component, Fragment } from "react";
import Listinglatestadstop from "./Listinglatestadstop";
import Listingadvertisementmiddle from "./Listingadvertisementmiddle";
import ReactPaginate from "react-paginate";
import axios from "axios";
import { apiBaseURL } from "../../ApiBaseURL";
import "./paginate.css";
import { MyContext } from "../../Context";
import _ from "lodash";
import { Link } from "react-router-dom";
import { Button, Modal } from "react-bootstrap";
import moment from "moment";
import Loader from "../../pages/Loader";
import ErrorAlert from "../../pages/ErrorAlert";
import HashMap from "hashmap";

var UserSaveAds = new HashMap();
var CompareVehicle = new HashMap();
var StoreCompareVehicleIDs = new HashMap();

class Listinglist extends Component {
  static contextType = MyContext;
  constructor(props) {
    super(props);
    this.simpleWatermarkImg = "./assets/media/watermark.png";
    this.authdealer = "./assets/media/authrized.svg";

    this.previousContext = "";
    this.state = {
      userId: "",
      isLoadingForListing: false,
      ListingAdvSearch: [],
      GlobalListingAdvSearch: [],
      //pagination
      offset: 0,
      perPage: 7,
      currentPage: 0,
      listingData: [],
      toggleAlertMsg: false,

      start: "",
      end: "",
      totalResults: "",

      //sortby
      SortListing: [
        { id: 1, value: "Newest to Oldest", method: "new" },
        { id: 2, value: "Oldest to Newest", method: "old" },
        { id: 3, value: "Price Highest to Lowest", method: "high" },
        { id: 4, value: "Price Lowest to Highest", method: "low" },
      ],
      selSortValue: "",
      //modals
      showModalPhone: false,
      showModalEmail: false,
      showcompareheader: false,

      //modal
      showModalEmailListing: false,
      showModalSuccess: false,
      SuccessMessage: "",
      txtSubject: "",
      taMessage: "Hi, I've found this Ad on #Cararabiya. What do you think? ",
      txtEmail: "",
      vcid: "",
    };
  }

  componentDidMount() {
    console.log("componentDidMount");

    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      this.setState({
        userId: parseUserData[0].UserID,
      });
    } else {
      console.log("localstorage null");
    }
    this.previousContext = this.context.data;
    let sellertypeid = this.previousContext.ssellertype;
    let vehicletypeid = this.previousContext.vehicleTypeID;
    let cityid = this.previousContext.scity;
    let numberofowners = this.previousContext.sowner;
    let pricemin = this.previousContext.minVal;
    let pricemax = this.previousContext.maxVal;
    let makeid = this.previousContext.smake;
    let modelid = this.previousContext.smodel;
    let versionid = this.previousContext.sversion;
    let versionyearfrom = this.previousContext.versionyearfrom;
    let versionyearto = this.previousContext.versionyearto;
    let assembly = this.previousContext.sassembly;
    let bodytypeid = this.previousContext.bodytypeid;
    let transmissionid = this.previousContext.transmissionid;
    let mileagemin = this.previousContext.mileagemin;
    let mileagemax = this.previousContext.mileagemax;
    let extcolorid = this.previousContext.extcolorid;
    let sdoormin = this.previousContext.sdoormin;
    let sdoormax = this.previousContext.sdoormax;
    let sdriveside = this.previousContext.sdriveside;
    let smetermin = this.previousContext.smetermin;
    let smetermax = this.previousContext.smetermax;
    let sengsizemin = this.previousContext.sengsizemin;
    let sengsizemax = this.previousContext.sengsizemax;
    let senginetype = this.previousContext.senginetype;
    let spic = this.previousContext.spic;
    let vehicleCategoryID = this.previousContext.vehicleCategoryID;
    this.setState({ vcid: vehicleCategoryID, isLoadingForListing: true });
    const url = `${apiBaseURL}/Listing/AdvSearchListing?SellerTypeID=${sellertypeid}&VehicleTypeID=${vehicletypeid}&CityID=${cityid}&NumberofOwners=${numberofowners}&Price_min=${pricemin}&Price_max=${pricemax}&MakeID=${makeid}&ModelID=${modelid}&VersionID=${versionid}&VersionYearFrom=${versionyearfrom}&VersionYearTo=${versionyearto}&Assembly=${assembly}&BodyTypeID=${bodytypeid}&TransmissionID=${transmissionid}&Mileage_min=${mileagemin}&Mileage_max=${mileagemax}&ExtColorID=${extcolorid}&Doors_min=${sdoormin}&Doors_max=${sdoormax}&DriveSide=${sdriveside}&MeterReading_min=${smetermin}&MeterReading_max=${smetermax}&EngineSize_min=${sengsizemin}&EngineSize_max=${sengsizemax}&EngineType=${senginetype}&Image1=${spic}&VehicleCategoryID=${vehicleCategoryID}`;
    axios
      .get(url)
      .then((res) => {
        console.log(res.data, "res.data");
        this.setState({
          GlobalListingAdvSearch: res.data,
        });
        //saveads Loading
        res.data.forEach((item) => {
          UserSaveAds.set(item.AdPostID, false);
        });
        if (this.state.userId !== "") {
          this.loadUserSavedAdListing(this.state.userId);
        }
        //compare
        res.data.forEach((item) => {
          CompareVehicle.set(item.AdPostID, false);
        });

        if (StoreCompareVehicleIDs.size > 0) {
          for (let pair of StoreCompareVehicleIDs) {
            CompareVehicle.set(pair.key, true);
            this.setState({
              showcompareheader: true,
            });
          }
        }
        this.setState({ isLoadingForListing: false });
        this.receivedData(res.data);
      })
      .catch((error) => {
        this.setState({
          start: 0,
          end: 0,
          offset: 0,
          perPage: 7,
          currentPage: 0,
          totalResults: 0,
          toggleAlertMsg: true,
          listingData: [],
          isLoadingForListing: false,
        });
        console.log(error, "from AdvSearchListing api");
      });
  }

  loadUserSavedAdListing = (userid) => {
    const url = `${apiBaseURL}/UserSavedAd?UserID=${userid}`;
    axios
      .get(url)
      .then((res) => {
        const UserSavedAdRes = res.data;
        //saveads Loading
        UserSavedAdRes.forEach((item) => {
          UserSaveAds.set(item.AdPostID, true);
        });
        this.receivedData(this.state.GlobalListingAdvSearch);
      })
      .catch((error) => {
        console.log(error, "from UserSavedAd api");
      });
  };

  getUserData = () => {
    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      this.setState({
        userId: parseUserData[0].UserID,
      });
      this.loadUserSavedAdListing(parseUserData[0].UserID);
    } else {
      console.log("localstorage null");
    }
    this.setState({
      start: 0,
      end: 0,
      offset: 0,
      currentPage: 0,
      perPage: 7,
    });
  }
  componentDidUpdate() {
    if (
      !_.isEqual(
        this.previousContext.searchkeyword,
        this.context.data.searchkeyword
      )
    ) {
      console.log("componentDidUpdate", this.context.data);
      this.getUserData();

      let searchkeyword = this.context.data.searchkeyword;
      console.log(searchkeyword);
      this.previousContext = this.context.data;
      this.receivedDataBySearchkeyword(searchkeyword);
    }

    if (!_.isEqual(this.previousContext, this.context.data)) {
      //console.log("componentDidUpdate");
      this.getUserData();
      let sellertypeid = this.context.data.ssellertype;
      let vehicletypeid = this.context.data.vehicleTypeID;
      let cityid = this.context.data.scity;
      let numberofowners = this.context.data.sowner;
      let pricemin = this.context.data.minVal;
      let pricemax = this.context.data.maxVal;
      let makeid = this.context.data.smake;
      let modelid = this.context.data.smodel;
      let versionid = this.context.data.sversion;
      let versionyearfrom = this.context.data.versionyearfrom;
      let versionyearto = this.context.data.versionyearto;
      let assembly = this.context.data.sassembly;
      let bodytypeid = this.context.data.bodytypeid;
      let transmissionid = this.context.data.transmissionid;
      let mileagemin = this.context.data.mileagemin;
      let mileagemax = this.context.data.mileagemax;
      let extcolorid = this.context.data.extcolorid;
      let sdoormin = this.context.data.sdoormin;
      let sdoormax = this.context.data.sdoormax;
      let sdriveside = this.context.data.sdriveside;
      let smetermin = this.context.data.smetermin;
      let smetermax = this.context.data.smetermax;
      let sengsizemin = this.context.data.sengsizemin;
      let sengsizemax = this.context.data.sengsizemax;
      let senginetype = this.context.data.senginetype;
      let spic = this.context.data.spic;
      let vehicleCategoryID = this.context.data.vehicleCategoryID;

      this.previousContext = this.context.data;

      this.setState({ vcid: vehicleCategoryID, isLoadingForListing: true });
      const url = `${apiBaseURL}/Listing/AdvSearchListing?SellerTypeID=${sellertypeid}&VehicleTypeID=${vehicletypeid}&CityID=${cityid}&NumberofOwners=${numberofowners}&Price_min=${pricemin}&Price_max=${pricemax}&MakeID=${makeid}&ModelID=${modelid}&VersionID=${versionid}&VersionYearFrom=${versionyearfrom}&VersionYearTo=${versionyearto}&Assembly=${assembly}&BodyTypeID=${bodytypeid}&TransmissionID=${transmissionid}&Mileage_min=${mileagemin}&Mileage_max=${mileagemax}&ExtColorID=${extcolorid}&Doors_min=${sdoormin}&Doors_max=${sdoormax}&DriveSide=${sdriveside}&MeterReading_min=${smetermin}&MeterReading_max=${smetermax}&EngineSize_min=${sengsizemin}&EngineSize_max=${sengsizemax}&EngineType=${senginetype}&Image1=${spic}&VehicleCategoryID=${vehicleCategoryID}`;
      axios
        .get(url)
        .then((res) => {
          console.log(res.data, "res.data");
          this.setState({
            GlobalListingAdvSearch: res.data,
          });
          //saveads Loading
          res.data.forEach((item) => {
            UserSaveAds.set(item.AdPostID, false);
          });
          if (this.state.userId !== "") {
            this.loadUserSavedAdListing(this.state.userId);
          }
          //compare
          res.data.forEach((item) => {
            CompareVehicle.set(item.AdPostID, false);
          });

          this.setState({ isLoadingForListing: false });

          this.receivedData(res.data);
        })
        .catch((error) => {
          this.setState({
            start: 0,
            end: 0,
            offset: 0,
            perPage: 7,
            currentPage: 0,
            totalResults: 0,
            toggleAlertMsg: true,
            listingData: [],
            isLoadingForListing: false,
          });
          console.log(error, "from AdvSearchListing api");
        });
    }
  }

  receivedDataBySearchkeyword = (searchkeyword) => {
    this.setState({ isLoadingForListing: true });
    const url = `${apiBaseURL}/Listing/ListingByKeyWord?KeyWord=${searchkeyword}`;
    axios
      .get(url)
      .then(res => {
        this.setState({
          GlobalListingAdvSearch: res.data
        });
        //saveads Loading
        res.data.forEach((item) => {
          UserSaveAds.set(item.AdPostID, false);
        });
        if (this.state.userId !== "") {
          this.loadUserSavedAdListing(this.state.userId);
        }
        //compare
        res.data.forEach((item) => {
          CompareVehicle.set(item.AdPostID, false);
        });

        this.setState({ isLoadingForListing: false });

        this.receivedData(res.data);
      })
      .catch(error => {
        this.setState({
          start: 0,
          end: 0,
          offset: 0,
          perPage: 7,
          currentPage: 0,
          totalResults: 0,
          toggleAlertMsg: true,
          listingData: [],
          isLoadingForListing: false,
        });
        console.log(error, "from AdvSearchPromo api");
      });
  };

  numberWithCommas = (price) => {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  receivedData = (listingadvsearch) => {
    if (listingadvsearch.length > 0) {
      this.setState({
        ListingAdvSearch: listingadvsearch,
        totalResults: listingadvsearch.length,
        toggleAlertMsg: false,
      });

      const data = listingadvsearch;

      if (
        this.state.offset + 1 + this.state.perPage >
        listingadvsearch.length
      ) {
        this.setState({
          end: listingadvsearch.length,
        });
      } else {
        this.setState({
          end: this.state.offset + this.state.perPage,
        });
      }

      const slice = data.slice(
        this.state.offset,
        this.state.offset + this.state.perPage
      );

      const listingData = slice.map((listingadvsearch, index) =>
        this.showAdvSearchListingData(listingadvsearch, index)
      );

      this.setState({
        pageCount: Math.ceil(data.length / this.state.perPage),
        start: this.state.offset + 1,
        listingData,
      });
    } else {
      this.setState({
        start: 0,
        end: 0,
        offset: 0,
        perPage: 7,
        currentPage: 0,
        totalResults: 0,
        toggleAlertMsg: true,
        listingData: [],
      });
    }
  };

  formattedDate = (datetime) => {
    let splitTimeDateWithT = datetime.split("T");
    let date = splitTimeDateWithT[0]; //T00:00:00
    let formatteddate = moment(date).format("D MMMM YYYY");
    return formatteddate;
  };

  handleUserSaveAd = (adpostid, userid) => {
    let userData = localStorage.getItem("user");
    if (userData === null) {
      this.context.handleChildOpenPopover(true);
    } else {
      if (UserSaveAds.get(adpostid) === true) {
        this.handleAdDeleteByAdPostIDUserID(adpostid, userid);
      } else if (UserSaveAds.get(adpostid) === false) {
        this.handleAdSaveByAdPostIDUserID(adpostid, userid);
      }
    }
  };

  handleAdDeleteByAdPostIDUserID = (adpostid, userid) => {
    const url = `${apiBaseURL}/UserSavedAd/AdDeleteByAdPostIDUserID`;
    let ApiParamForAdDelete = {
      UserID: userid,
      AdpostID: adpostid,
    };
    axios
      .post(url, null, { params: ApiParamForAdDelete })
      .then((res) => {
        console.log(res, "res from ApiParamForAdDelete api");

        if (res.status === 200) {
          UserSaveAds.set(adpostid, false);
          this.receivedData(this.state.ListingAdvSearch);
          console.log(res.status, "ApiParamForAdDelete");
        } else {
          console.log(
            res.status,
            res.statusText,
            "statuscode from ApiParamForAdDelete api"
          );
        }
      })
      .catch((error) => {
        console.log(error, "from ApiParamForAdDelete api");
      });
  };

  handleAdSaveByAdPostIDUserID = (adpostid, userid) => {
    const url = `${apiBaseURL}/UserSavedAd`;
    let ApiParamForAdSave = {
      UserID: userid,
      AdpostID: adpostid,
    };
    axios
      .post(url, ApiParamForAdSave)
      .then((res) => {
        console.log(res, "res from ApiParamForAdSave api");

        if (res.status === 200) {
          UserSaveAds.set(adpostid, true);
          this.receivedData(this.state.ListingAdvSearch);
          console.log(res.status, "ApiParamForAdSave");
        } else {
          console.log(
            res.status,
            res.statusText,
            "statuscode from ApiParamForAdSave api"
          );
        }
      })
      .catch((error) => {
        console.log(error, "from ApiParamForAdSave api");
      });
  };

  showAdvSearchListingData = (listingadvsearch, index) => {
    return (
      <Fragment key={index}>
        <div className="col-12 line-o my-3 px-0 bg-shadow">
          {/*Listing start*/}
          <div className="row">
            <div className="col-lg-8 px-0">
              <div className="row">
                <div className="col-12 px-0 pl-3 mt-2 mb-3">
                  <div className="d-flex align-items-center Ad-top-sec">
                    <i className="fas fa-star mr-2" />
                    <span className="mr-3">Featured ad</span>
                    {listingadvsearch.Counter > 1000 ? (
                      <>
                        <i className="fas fa-bolt mr-2" />
                        <span className="mr-3">Hot</span>
                      </>
                    ) : (
                        <>
                          <i
                            className="fas fa-bolt mr-2"
                            style={{ color: "#848484" }}
                          />
                        </>
                      )}


                    {listingadvsearch.SellerTypeID === 2 ?
                      true === true ? (
                        <span style={{ position: 'absolute', top: '2px', left: '45%' }}>
                          <img
                            style={{ height: '25px' }}
                            src={this.authdealer}
                          />
                        </span>
                      ) : (
                          <span>
                            <img
                              className=""
                              src={this.simpleWatermarkImg}
                              style={{ height: "134px" }}
                            />
                          </span>
                        ) : <></>}


                    <button
                      onClick={() =>
                        this.handleUserSaveAd(
                          listingadvsearch.AdPostID,
                          this.state.userId
                        )
                      }
                      type="button"
                      className="btn btn-sm liketoggle like Ad-like-btn-style"
                      name="like"
                    >
                      <i
                        className="fas fa-heart"
                        style={{
                          color:
                            UserSaveAds.get(listingadvsearch.AdPostID) === true
                              ? "#D3E428"
                              : "",
                        }}
                      />{" "}
                      <span>Save</span>
                    </button>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12 col-md-5">
                  <div
                    className="b-goods-f__listing mb-2"
                    style={{ height: "134px" }}
                  >
                    <>
                      <img
                        className="watermark-listing"
                        src={this.simpleWatermarkImg}
                        style={{ height: "134px" }}
                      />
                      <img
                        className="b-goods-f__img img-scale"
                        src={listingadvsearch.ImageUrl}
                        style={{ height: "134px" }}
                        alt="foto"
                      />
                    </>
                  </div>

                  <span className="Ad-location" style={{ display: "flex" }}>
                    <i
                      className="fas fa-map-marker-alt"
                      style={{ color: "#D3E428", marginTop: "5px" }}
                    />
                    <span style={{ margin: "0 0 10px 8px" }}>
                      {listingadvsearch.Area}
                    </span>
                  </span>
                </div>
                <div className="col-12 col-md-7">
                  <div className="b-goods-f__main d-flex">
                    <div className="b-goods-f__descrip">
                      <div className="b-goods-f__title_myad">
                        {this.state.vcid === 1 ? (
                          <Link
                            to={{
                              pathname: "/postad-detail",
                              state: {
                                AdPostID: listingadvsearch.AdPostID,
                              },
                            }}
                          >
                            {listingadvsearch.Vehicle}
                          </Link>
                        ) : (
                            <Link
                              to={{
                                pathname: "/postad-detailbike",
                                state: {
                                  AdPostID: listingadvsearch.AdPostID,
                                },
                              }}
                            >
                              {listingadvsearch.Vehicle}
                            </Link>
                          )}
                      </div>
                      <div className="b-goods-f__info Ad-posted-date">
                        {this.formattedDate(listingadvsearch.CreateDate)}
                      </div>
                      <ul className="b-goods-f__list list-unstyled mt-1 fsize-car-descr">
                        <li className="b-goods-f__list-item">
                          <span className="b-goods-f__list-title">
                            Mileage :
                          </span>
                          <span className="b-goods-f__list-info Ad-mileage">
                            {listingadvsearch.Mileage === ""
                              ? 0
                              : listingadvsearch.Mileage}
                          </span>
                        </li>
                        <li className="b-goods-f__list-item">
                          <span className="b-goods-f__list-title">Model :</span>
                          <span className="b-goods-f__list-info Ad-Model">
                            {listingadvsearch.ModelName}
                          </span>
                        </li>
                        <li className="b-goods-f__list-item">
                          <span className="b-goods-f__list-title">
                            Transmission :
                          </span>
                          <span className="b-goods-f__list-info Ad-Transmission">
                            {listingadvsearch.TransmissionName}
                          </span>
                        </li>
                        <li className="b-goods-f__list-item">
                          <span className="b-goods-f__list-title">Color :</span>
                          <span className="b-goods-f__list-info Ad-Color">
                            {listingadvsearch.ColorName}
                          </span>
                        </li>
                      </ul>
                    </div>
                  </div>

                  <div className="Ad-email-listing">
                    <Link
                      onClick={() =>
                        this.handleShowEmailListing(
                          listingadvsearch.Vehicle,
                          listingadvsearch.AdPostID
                        )
                      }
                      style={{ color: "grey" }}
                    >
                      <span className="email-font-size mr-3">
                        <i className="far fa-envelope mr-1" /> Email this Ad
                      </span>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-sm-12 px-0 list-sec-r">
              {" "}
              {/* price section */}
              <div className="Ad-price-sec">
                <span className="Ad-price-style">
                  {listingadvsearch.Price === 0
                    ? "Call For Price"
                    : listingadvsearch.CurCode +
                    " " +
                    this.numberWithCommas(listingadvsearch.Price)}
                </span>
              </div>
              <div>
                {" "}
                {/* buttons */}
                <div className="b-goods-f__sidebar px-3">
                  <span className="b-goods-f__price-group">
                    <button
                      onClick={() =>
                        this.handleShowPhone(listingadvsearch.UserID)
                      }
                      type="button"
                      className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center Ad-btn-grey"
                    >
                      {" "}
                      <i className="fas fa-phone prefix justify-content-between" />
                      <span className="Ad-btn-grey-s">Show Phone Number</span>
                    </button>
                    <button
                      onClick={() =>
                        this.handleShowEmail(listingadvsearch.UserID)
                      }
                      type="button"
                      className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center Ad-btn-grey"
                    >
                      {" "}
                      <i className="fas fa-envelope prefix justify-content-start" />
                      <span className="Ad-btn-grey-s">Show Email</span>
                    </button>
                    <button
                      type="button"
                      className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center Ad-btn-grey"
                    >
                      {" "}
                      <i className="far fa-comment-dots prefix justify-content-center" />
                      <span className="Ad-btn-grey-s">Chat with seller</span>
                    </button>
                  </span>

                  <span className="b-goods-f__compare">
                    <div className="form-check d-flex align-self-center">
                      <input
                        className="form-check-input Atocompare-checkbox "
                        type="checkbox"
                        checked={
                          CompareVehicle.get(listingadvsearch.AdPostID) === true
                            ? true
                            : false
                        }
                        onClick={() =>
                          this.handleCBCompare(
                            listingadvsearch.AdPostID,
                            listingadvsearch.ImageUrl
                          )
                        }
                      />
                      <label className="form-check-label Atocompare-text">
                        Add to compare
                      </label>
                    </div>
                  </span>
                </div>
              </div>
            </div>
          </div>{" "}
          {/* end row */}
        </div>
      </Fragment>
    );
  };

  handlePageClick = (e) => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.perPage;

    this.setState(
      {
        currentPage: selectedPage,
        offset: offset,
      },
      () => {
        this.receivedData(this.state.ListingAdvSearch);
      }
    );
  };

  //sortby
  handleSortByAdvSearchListing = (event) => {
    this.setState({ selSortValue: event.target.value });
    let adpostData = [];
    adpostData = this.state.GlobalListingAdvSearch;

    if (event.target.value === "new") {
      adpostData = _.orderBy(adpostData, ["CreateDate"], ["desc"]);
    } else if (event.target.value === "old") {
      adpostData = _.orderBy(adpostData, ["CreateDate"], ["asc"]);
    } else if (event.target.value === "high") {
      adpostData = _.orderBy(adpostData, ["Price"], ["desc"]);
    } else if (event.target.value === "low") {
      adpostData = _.orderBy(adpostData, ["Price"], ["asc"]);
    } else {
      console.log("filter error");
    }

    this.setState({ ListingAdvSearch: adpostData });
    this.receivedData(adpostData);
  };

  showMyListHeader = () => {
    return (
      <div className="row">
        <div className="col-6 px-0">
          <div className="list-heading" style={{ paddingLeft: 0 }}>
            {this.context.data.vehicleTypeID == 1
              ? "New"
              : this.context.data.vehicleTypeID == 2
                ? "Used"
                : "Certified"}{" "}
            Vehicle for Sale
          </div>
          <div
            className="b-filter-goods__info col-auto"
            style={{ paddingLeft: 0 }}
          >
            Showing
            <strong>
              {" "}
              {this.state.start} - {this.state.end}{" "}
            </strong>{" "}
            out of <strong> {this.state.totalResults}</strong> listings
          </div>
        </div>

        <div className="col-6" style={{ padding: 0 }}>
          <div className="b-filter-goods__wrap" style={{ float: "right" }}>
            <div className="b-filter-goods__select d-inline-flex">
              <select
                style={{ inlineSize: "auto" }}
                className="custom-select-adpost"
                onChange={this.handleSortByAdvSearchListing}
                value={this.state.selSortValue}
              >
                <option selected>Sort By</option>
                {this.state.SortListing.map((sortlist) => (
                  <option key={sortlist.id} value={sortlist.method}>
                    {sortlist.value}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
      </div>
    );
  };

  handleShowPhone = (userid) => {
    const urlUserPhone = `${apiBaseURL}/User/UserMobile?UserID=${userid}`;
    axios
      .get(urlUserPhone)
      .then((res) => {
        if (res.data.length > 0) {
          const data = res.data;
          console.log(data, "urlUserPhone");
          this.setState({
            userPhone: data,
            showModalPhone: true,
          });
        } else {
          this.setState({});
        }
      })
      .catch((error) => {
        this.setState({});
        console.log(error, "from urlUserPhone api");
      });
  };

  //phone modal
  handleClosePhone = () => {
    this.setState({
      showModalPhone: false,
    });
  };

  modalPhone = () => {
    return (
      <Modal show={this.state.showModalPhone}>
        <Modal.Header>
          <Modal.Title>User Phone Number</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h6>{this.state.userPhone}</h6>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={this.handleClosePhone}
            style={{ backgroundColor: "#D3E428" }}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    );
  };

  handleShowEmail = (userid) => {
    const urlUserEmail = `${apiBaseURL}/User/UserEmail?UserID=${userid}`;
    axios
      .get(urlUserEmail)
      .then((res) => {
        if (res.data.length > 0) {
          const data = res.data;
          console.log(data, "urlUserEmail");
          this.setState({
            userEmail: data,
            showModalEmail: true,
          });
        } else {
          this.setState({});
        }
      })
      .catch((error) => {
        this.setState({});
        console.log(error, "from urlUserEmail api");
      });
  };

  //phone modal
  handleCloseEmail = () => {
    this.setState({
      showModalEmail: false,
    });
  };

  modalEmail = () => {
    return (
      <Modal show={this.state.showModalEmail}>
        <Modal.Header>
          <Modal.Title>User Email</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h6>{this.state.userEmail}</h6>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={this.handleCloseEmail}
            style={{ backgroundColor: "#D3E428" }}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    );
  };

  //compare work
  handleCBCompare = (adpostid, imageurl) => {
    if (CompareVehicle.get(adpostid) === true) {
      this.handleDisableCompareCB(adpostid);
    } else if (CompareVehicle.get(adpostid) === false) {
      this.handleEnableCompareCB(adpostid, imageurl);
    }
  };

  handleDisableCompareCB = (adpostid) => {
    StoreCompareVehicleIDs.delete(adpostid);
    CompareVehicle.set(adpostid, false);
    if (StoreCompareVehicleIDs.size === 0) {
      this.setState({
        showcompareheader: false,
      });
    }

    this.receivedData(this.state.ListingAdvSearch);
  };
  handleEnableCompareCB = (adpostid, imageurl) => {
    if (StoreCompareVehicleIDs.size < 4) {
      StoreCompareVehicleIDs.set(adpostid, imageurl);
      CompareVehicle.set(adpostid, true);
      this.setState({
        showcompareheader: true,
      });
      this.receivedData(this.state.ListingAdvSearch);
    } else {
      alert("limit exceed...");
    }
  };
  compareListHeader = () => {
    return (
      <Fragment>
        <div
          className="container-fluid bg-shadow"
          style={{
            position: "fixed",
            zIndex: "1000",
            backgroundColor: "white",
            top: "74px",
            left: 0,
          }}
        >
          <div className="container py-3">
            <div className="row">
              {[...StoreCompareVehicleIDs.keys()].map((adpostid) => (
                <div key={adpostid}>
                  <button
                    className="compare-cross"
                    onClick={() => this.handleDisableCompareCB(adpostid)}
                  >
                    ×
                  </button>
                  <img
                    className=""
                    width="120px"
                    height="88px"
                    src={StoreCompareVehicleIDs.get(adpostid)}
                    alt=""
                  />
                </div>
              ))}
              <div style={{ margin: "auto", marginRight: 0 }}>
                <Link
                  to={{
                    pathname: "/compare-vehicle",
                    state: {
                      comparedata: StoreCompareVehicleIDs,
                      vehiclecategoryid: this.state.vcid
                    },
                  }}
                  className="btn"
                  style={{
                    padding: "10px 80px",
                    border: "none",
                    backgroundColor: "#D3E428",
                    color: "white",
                    borderRadius: "0px",
                  }}
                >
                  Compare
                </Link>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  };

  //Email Listing
  handleCloseEmailListing = () => {
    this.setState({
      txtEmail: "",
      txtSubject: "",
      taMessage: "Hi, I've found this Ad on #Cararabiya. What do you think? ",
      showModalEmailListing: false,
    });
  };
  handleShowEmailListing = (vehicle, adpostid) => {
    let userData = localStorage.getItem("user");
    if (userData === null) {
      this.context.handleChildOpenPopover(true);
    } else {
      this.setState({
        adpostid,
        vehicle,
        showModalEmailListing: true,
      });
    }
  };

  handleCloseSuccess = () => {
    this.setState({
      showModalSuccess: false,
    });
  };

  handletxtEmailChange = (event) => {
    this.setState({ txtEmail: event.target.value });
  };

  handletxtSubjectChange = (event) => {
    this.setState({ txtSubject: event.target.value });
  };

  handletaMessageChange = (event) => {
    this.setState({ taMessage: event.target.value });
  };

  handleEmailListing = (event) => {
    event.preventDefault();
    const {
      userId,
      txtSubject,
      taMessage,
      adpostid,
      vcid,
      txtEmail,
    } = this.state;
    var adposturl = "";
    if (vcid === 1) {
      adposturl = `https://cararabiya.netlify.app/postad-detail?AdPostId=${adpostid}`;
    } else {
      adposturl = `https://cararabiya.netlify.app/postad-detailbike?AdPostId=${adpostid}`;
    }

    this.setState({
      txtEmail: "",
      txtSubject: "",
      taMessage: "Hi, I've found this Ad on #Cararabiya. What do you think? ",
    });

    const url = `${apiBaseURL}/Email/EmailThisAd`;
    let ApiParamForEmailThisAd = {
      UID: userId,
      ToEmail: txtEmail,
      subLine: txtSubject,
      mailMessage: taMessage,
      AdPostURL: adposturl,
    };
    axios
      .post(url, null, { params: ApiParamForEmailThisAd })
      .then((res) => {
        console.log(res.data, "EmailThisAd api");

        if (res.status === 200) {
          this.setState({
            SuccessMessage: res.data,
            showModalEmailListing: false,
            showModalSuccess: true,
          });
        } else {
          console.log(res.status, "statuscode from EmailThisAd api");
        }
      })
      .catch((error) => {
        console.log(error, "EmailThisAd api");
      });
  };

  modalListingEmail = () => {
    return (
      <Modal
        show={this.state.showModalEmailListing}
        onHide={this.handleCloseEmailListing}
      >
        <Modal.Body>
          <div className="my-3 px-3">
            <div className="d-flex justify-content-center my-3">
              <h4>
                <strong>Email this Ad</strong>
              </h4>
            </div>
            <form onSubmit={this.handleEmailListing}>
              <div>
                <div className="form-group">
                  <label style={{ fontSize: "16px" }}>Title: </label>
                  <div>
                    <span style={{ fontSize: "16px" }}>
                      {this.state.vehicle}
                    </span>
                  </div>
                </div>

                <input
                  name="email"
                  required
                  onChange={this.handletxtEmailChange}
                  value={this.state.txtEmail}
                  type="email"
                  className="form-control"
                  placeholder="Email *"
                />
                <br />
                <input
                  name="subject"
                  required
                  onChange={this.handletxtSubjectChange}
                  value={this.state.txtSubject}
                  type="text"
                  className="form-control"
                  placeholder="Subject *"
                />
              </div>
              <div className="mt-3">
                <textarea
                  name="message"
                  required
                  cols={30}
                  rows={5}
                  className="form-control"
                  placeholder="Message *"
                  onChange={this.handletaMessageChange}
                  value={this.state.taMessage}
                />
              </div>
              <div className="d-flex justify-content-center mb-2 mt-3">
                <button
                  type="submit"
                  className="btn"
                  style={{
                    border: "none",
                    padding: "10px 80px",
                    backgroundColor: "#D3E428",
                    color: "white",
                  }}
                >
                  SEND
                </button>
              </div>
            </form>
          </div>
        </Modal.Body>
      </Modal>
    );
  };

  modalSuccess = () => {
    return (
      <Modal show={this.state.showModalSuccess}>
        <Modal.Body>
          <div className="my-3 px-3">
            <div className="d-flex justify-content-center my-3">
              <h4>
                <strong>Success</strong>
              </h4>
            </div>

            <div style={{ textAlign: "center" }}>
              {this.state.SuccessMessage}
            </div>

            <div className="d-flex justify-content-center mb-2 mt-3">
              <button
                onClick={this.handleCloseSuccess}
                className="btn"
                style={{
                  border: "none",
                  padding: "10px 80px",
                  backgroundColor: "#D3E428",
                  color: "white",
                }}
              >
                OK
              </button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    );
  };

  render() {
    return (
      <Fragment>
        {this.state.showcompareheader === true ? this.compareListHeader() : ""}
        {/* modal phone */}
        {this.modalPhone()}
        {/* modal email */}
        {this.modalEmail()}
        {/* modal emailad */}
        {this.modalListingEmail()}
        {/* modal success */}
        {this.modalSuccess()}

        <div>
          <Listinglatestadstop />

          {this.showMyListHeader()}
          <br />

          {this.state.toggleAlertMsg === true ? (
            <ErrorAlert show={this.state.toggleAlertMsg} />
          ) : (
              <Fragment>
                {this.state.isLoadingForListing ? (
                  <Loader />
                ) : (
                    <span>
                      {this.state.listingData}
                      <Listingadvertisementmiddle />
                      <ReactPaginate
                        previousLabel={"prev"}
                        nextLabel={"next"}
                        breakLabel={"..."}
                        breakClassName={"break-me"}
                        pageCount={this.state.pageCount}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={5}
                        onPageChange={this.handlePageClick}
                        containerClassName={"pagination"}
                        subContainerClassName={"pages pagination"}
                        activeClassName={"active"}
                        forcePage={this.state.currentPage}
                      />
                    </span>
                  )}
              </Fragment>
            )}
        </div>
      </Fragment>
    );
  }
}
Listinglist.contextType = MyContext;
export default Listinglist;