import React, { Component } from "react";

class Listingadvertismenttop extends Component {
  render() {
    const image1 =
      "./assets/media/banner-horizontal.png";
    return (
      <div>
        <div className="row" style={{ textAlign: "center" }}>
          <div className="col-12 mb-5 banner">
            <img src={image1} className="img-fluid" alt="" />
          </div>
        </div>
      </div>
    );
  }
}

export default Listingadvertismenttop;