import React, { Component, Fragment } from "react";
import { Consumer } from "../../Context";

class ListingSearchkeyword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchkeyword: ""
    };
  }
  //searchbykeyword
  handlesearchkeyChange = event => {
    this.setState({
      searchkeyword: event.target.value
    });
  };
  render() {
    return (
      <Fragment>
        <div className="widget widget-search section-sidebar ">
          <div className="b-filter-slider__title" style={{ fontSize: "14px" }}>
            Search by keyword
          </div>
          <Consumer>
            {({ handleChildSearchKeyword }) => (
              <form className="form-sidebar-adv row" id="search-global-form">
                <input
                  className="form-sidebar-adv__input form-control"
                  value={this.state.searchkeyword}
                  onChange={this.handlesearchkeyChange}
                  placeholder="Enter Keyword"
                />
                <button
                  type="button"
                  className="form-sidebar-adv__btn"
                  onClick={() =>
                    handleChildSearchKeyword(this.state.searchkeyword)
                  }
                >
                  <i className="ic icon-magnifier"></i>
                </button>
              </form>
            )}
          </Consumer>
        </div>
      </Fragment>
    );
  }
}
export default ListingSearchkeyword;