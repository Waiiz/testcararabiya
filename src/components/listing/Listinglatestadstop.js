import React, { Component } from "react";

class Listinglatestadstop extends Component {
  constructor(props) {
    super(props);
    this.timer = null;
    this.state = {
      llisting: [],
      toplist: [],
      error: null,
      AdsListHeight: ""
    };
  }

  componentDidMount() {
    const apiUrlListing =
      "https://cararabiya.ahalfa.com/api/Listing/AllListing";

    fetch(apiUrlListing)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
          this.setState({
            llisting: result
          });
          const reverseDataForLatestAds = result;
          if (result.length > 3) {
            var newlist = [];
            for (var i = 0; i < 3; i++) {
              newlist.push(reverseDataForLatestAds[i]);
            }
            this.setState({
              toplist: newlist
            });
          } else {
            this.setState({
              toplist: reverseDataForLatestAds
            });
          }
        } else {
          console.log("Listing");
        }
        },
        error => {
          this.setState({ error });
        }
      );

    this.timer = setTimeout(
      function() {
        //Start the timer
        try {
          const getAdsListHeight = this["listItem_" + 0].offsetHeight;
          this.setState({
            AdsListHeight: getAdsListHeight + 12
          });
        } catch (e) {
          console.log(e);
        }
      }.bind(this),
      1000
    );
  }

  numberWithCommas = (price) => {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  render() {
    return (
      <div>
        <main className="b-goods-group row feature-toggle-top">
          {this.state.toplist.map((toplist, index) => (
            <div key={index} className="b-goods-f col-lg-4 col-md-4">
              <div
                className="b-goods-f__media"
                style={{ height: "200px", backgroundColor: 'rgb(70, 78, 96)' }}
              >
                <img
                  className="watermark-recent"
                  src="./assets/media/watermark.png"
                  style={{ height: "200px" }}
                />
                <img
                  className="b-goods-f__img img-scale"
                  src={toplist.ImageUrl}
                  style={{ height: "200px", backgroundColor: 'rgb(70, 78, 96)' }}
                  alt="foto"
                />
              </div>
              <div className="b-goods-f__main bg-shadow">
                <div className="b-goods-f__descrip">
                  <div
                    className="b-goods-f__title b-goods-f__title_myad"
                    style={{ marginBottom: "0px" }}
                  >
                    <a style={{ fontSize: "18px", fontWeight: 600 }} to="/">
                      {toplist.Vehicle}
                    </a>
                  </div>

                  <ul
                    ref={listItem => {
                      this["listItem_" + index] = listItem;
                    }}
                    className="b-goods-f__list list-unstyled row"
                    style={{
                      height: this.state.AdsListHeight + "px",
                      fontSize: "11px",
                      padding: "5px 10px",
                      marginTop: "0px",
                      justifyContent: "space-evenly"
                    }}
                  >
                    <div class="row">
                      <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                        <div>
                          <i
                            className="fas fa-calendar-alt"
                            style={{ color: "#D3E428", fontSize: "12px" }}
                          />
                        </div>
                        <div>
                          <span
                            className="b-goods-f__list-info"
                            style={{ color: "black" }}
                          >
                            {toplist.VersionYear}
                          </span>
                        </div>
                      </div>

                      <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                        <div>
                          <i
                            className="fas fa-tachometer-alt"
                            style={{ color: "#D3E428", fontSize: "12px" }}
                          />
                        </div>
                        <div>
                          <span
                            className="b-goods-f__list-info"
                            style={{ color: "black" }}
                          >
                            {toplist.Mileage === '' ? 0 : toplist.Mileage}
                          </span>
                        </div>
                      </div>

                      <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                        <div>
                          <i
                            className="fas fa-car-alt"
                            style={{ color: "#D3E428", fontSize: "12px" }}
                          />
                        </div>
                        <div>
                          <span
                            className="b-goods-f__list-info"
                            style={{ color: "black" }}
                          >
                            {toplist.VehicleTypeName}
                          </span>
                        </div>
                      </div>

                      <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                        <div>
                          <i
                            className="fas fa-gas-pump"
                            style={{ color: "#D3E428", fontSize: "12px" }}
                          />
                        </div>
                        <div>
                          <span
                            className="b-goods-f__list-info"
                            style={{ color: "black" }}
                          >
                            {toplist.FuelTypeName}
                          </span>
                        </div>
                      </div>
                    </div>
                  </ul>
                </div>
                <div
                  className="b-goods-f__sidebar"
                  style={{ paddingTop: "10px", borderTop: "1px solid #ddd" }}
                >
                  <a className="b-goods-f__bnr" href="#">
                    <img
                      src="./assets/media/content/b-goods/auto-check.png"
                      alt="auto check"
                    />
                  </a>
                  <span className="b-goods-f__price-group">
                    <span className="b-goods-f__pricee">
                      <span className="b-goods-f__price_col">PRICE:&nbsp;</span>
                      <span className="b-goods-f__price-numbb">
                        {toplist.Price === 0 ? 'Call For Price' : toplist.CurCode +' '+ this.numberWithCommas(toplist.Price)}
                      </span>
                    </span>
                  </span>
                </div>
              </div>
            </div>
          ))}
        </main>
      </div>
    );
  }
}

export default Listinglatestadstop;