import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./listingsearch.css";
import { Consumer } from "../../Context";
import { Range } from "rc-slider";
import "rc-slider/assets/index.css";
import {
  apiUrlcity,
  apiUrlmake,
  apiUrlvehicletype,
  apiUrlBodytype
} from "../../ApiBaseURL";

class Listingsearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //car type 27params total
      lvehicletype: [],
      //city
      lcity: [],
      scityname: "",
      //make
      lmake: [],
      lyear: [],
      //range
      min: 15000,
      max: 15000000,
      //versionyear
      versionyearmin: 1990,
      versionyearmax: 2020,
      //bodytype
      sbodytype: [],
      lbodytype: [],
      //params
      vehiclecategoryid: 1,
      ssellertype: 0,
      svtype: 2,
      scity: 0,
      sowner: 0,
      minVal: "NA",
      maxVal: "NA",
      smake: 0,
      smodel: 0,
      sversion: 0,
      versionyearfrom: "NA",
      versionyearto: "NA",
      sassembly: "NA",
      bodytypeid: 0,
      transmissionid: 0,
      mileagemin: "NA",
      mileagemax: "NA",
      extcolorid: 0,
      sdoormin: "NA",
      sdoormax: "NA",
      sdriveside: "NA",
      smetermin: "NA",
      smetermax: "NA",
      sengsizemin: "NA",
      sengsizemax: "NA",
      senginetype: "NA",
      spic: true
    };
  }

  componentDidMount() {
    var now = new Date();
    var year = now.getFullYear();
    var toyear = year - 50;
    var daysOfYear = [];
    for (var d = new Date(2012, 0, 1); d <= now; d.setDate(d.getDate() + 1)) {
      daysOfYear.push(new Date(d));
    }
    this.setState({
      lyear: daysOfYear
    });

    //vehicle type
    fetch(apiUrlvehicletype)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lvehicletype: result
            });
          } else {
            console.log("lvehicletype");
          }
        },
        error => {
          this.setState({ error });
        }
      );

    //make
    fetch(apiUrlmake)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lmake: result
            });
          } else {
            console.log("lmake");
          }
        },
        error => {
          this.setState({ error });
        }
      );

    //city
    fetch(apiUrlcity)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lcity: result
            });
          } else {
            console.log("lcity");
          }
        },
        error => {
          this.setState({ error });
        }
      );

    fetch(apiUrlBodytype)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lbodytype: result
            });
          } else {
            console.log("lbodytype");
          }
        },
        error => {
          this.setState({ error });
        }
      );
  }

  //methods
  //make
  handleMakeChange = event => {
    this.setState({ smake: event.target.value });

    let filteredArray = this.state.lmake.filter(
      item => event.target.value == item.MakeID
    );
    //this.setState({ smakename: filteredArray[0].MakeName });
  };
  //city
  handleCityChange = event => {
    this.setState({ scity: event.target.value });
  };
  //bodytype
  handleBodytypeChange = event => {
    this.setState({ bodytypeid: event.target.value });
  };
  //price
  onSliderChange = value => {
    this.setState({
      minVal: value[0],
      maxVal: value[1]
    });
    //console.log(this.state.minVal, this.state.maxVal);
  };
  //year
  onVersionYearSliderChange = value => {
    this.setState({
      versionyearfrom: value[0],
      versionyearto: value[1]
    });
  };

  render() {
    const {
      ssellertype,
      svtype,
      scity,
      sowner,
      minVal,
      maxVal,
      smake,
      smodel,
      sversion,
      versionyearfrom,
      versionyearto,
      sassembly,
      bodytypeid,
      transmissionid,
      mileagemin,
      mileagemax,
      extcolorid,
      sdoormin,
      sdoormax,
      sdriveside,
      smetermin,
      smetermax,
      sengsizemin,
      sengsizemax,
      senginetype,
      spic,
      vehiclecategoryid
    } = this.state;
    return (
      <div>
        <div className="card">
          <div className="card-header px-0 py-0">
            <h3
              className="widget-title"
              style={{
                backgroundColor: "#e8e8e8",
                color: "#253241",
                textTransform: "none",
                fontSize: "15px",
                fontWeight: 600
              }}
            >
              Quick Search{" "}
              <Consumer>
                {({ handleChildQuickSrchAdPost }) => (
                  <button
                    onClick={() =>
                      handleChildQuickSrchAdPost(
                        0,
                        2,
                        0,
                        0,
                        "NA",
                        "NA",
                        0,
                        0,
                        0,
                        "NA",
                        "NA",
                        "NA",
                        0,
                        0,
                        "NA",
                        "NA",
                        0,
                        "NA",
                        "NA",
                        "NA",
                        "NA",
                        "NA",
                        "NA",
                        "NA",
                        "NA",
                        true,
                        1,
                        'reset'
                      )
                    }
                  >
                    <i class="fas fa-redo-alt"></i>
                  </button>
                )}
              </Consumer>
            </h3>
          </div>

          <div>
            <div className="card-body">
              <div className="widget-inner" style={{ paddingTop: "20px" }}>
                <Consumer>
                  {({ data, handleChildVehicletypeId }) => (
                    <div className="label-group mb-3">
                      <div className="b-filter-slider__title">Condition</div>
                      {this.state.lvehicletype.map((vtype, index) => (
                        <div key={index} style={{ display: "inline" }}>
                          <input
                            style={{ opacity: 1, margin: "5px 10px 0 0" }}
                            type="radio"
                            value={vtype.VehicleTypeID}
                            checked={vtype.VehicleTypeID == data.vehicleTypeID}
                            onChange={() =>
                              handleChildVehicletypeId(vtype.VehicleTypeID)
                            }
                          />

                          <label
                            className="form-check-label"
                            style={{ marginLeft: "18px", marginRight: "12px" }}
                          >
                            {vtype.VehicleTypeName}
                          </label>
                        </div>
                      ))}
                    </div>
                  )}
                </Consumer>

                <div className="form-group">
                  <div className="row">
                    <div className="col-12" style={{ padding: 0 }}>
                      <select
                        className="selectClass"
                        onChange={this.handleCityChange}
                        value={scity}
                      >
                        <option value="" selected>
                          City
                        </option>
                        {this.state.lcity.map(city => (
                          <option key={city.CityID} value={city.CityID}>
                            {city.CityName}
                          </option>
                        ))}
                      </select>
                    </div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-12" style={{ padding: 0 }}>
                      <select
                        className="selectClass"
                        onChange={this.handleMakeChange}
                        value={smake}
                      >
                        <option value="" selected>
                          Brand
                        </option>
                        {this.state.lmake.map(make => (
                          <option key={make.MakeID} value={make.MakeID}>
                            {make.MakeName}
                          </option>
                        ))}
                      </select>
                    </div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="row">
                    <div className="col-12" style={{ padding: 0 }}>
                      <select
                        className="selectClass"
                        onChange={this.handleBodytypeChange}
                        value={bodytypeid}
                      >
                        <option value="" selected>
                          Body Type
                        </option>
                        {this.state.lbodytype.map(bodytype => (
                          <option
                            key={bodytype.BodyTypeID}
                            value={bodytype.BodyTypeID}
                          >
                            {bodytype.BodyTypeName}
                          </option>
                        ))}
                      </select>
                    </div>
                  </div>
                </div>
                <br />

                <div className="b-filter-slider__title">Price</div>
                <div>
                  <Range
                    defaultValue={[this.state.min, this.state.max]}
                    min={this.state.min}
                    max={this.state.max}
                    onChange={this.onSliderChange}
                  />

                  <div className="b-filter__row row">
                    <div
                      className="b-filter__item col-md-6 col-lg-12 col-xl-6"
                      style={{ padding: 0 }}
                    >
                      <input
                        placeholder="15000"
                        className="quick-select text-box single-line"
                        style={{ paddingRight: 1 }}
                        type="number"
                        value={minVal}
                      />
                    </div>
                    <div
                      className="b-filter__item col-md-6 col-lg-12 col-xl-6"
                      style={{ padding: 0 }}
                    >
                      <input
                        placeholder="15000000"
                        className="quick-select text-box single-line"
                        style={{ paddingRight: 1 }}
                        type="number"
                        value={maxVal}
                      />
                    </div>
                  </div>
                </div>
                <br />

                <div className="b-filter-slider__title">Year</div>
                <div>
                  <Range
                    defaultValue={[
                      this.state.versionyearmin,
                      this.state.versionyearmax
                    ]}
                    min={this.state.versionyearmin}
                    max={this.state.versionyearmax}
                    onChange={this.onVersionYearSliderChange}
                  />

                  <div className="b-filter__row row">
                    <div
                      className="b-filter__item col-md-6 col-lg-12 col-xl-6"
                      style={{ padding: 0 }}
                    >
                      <input
                        placeholder="1990"
                        className="quick-select text-box single-line"
                        style={{ paddingRight: 1 }}
                        type="number"
                        value={versionyearfrom}
                      />
                    </div>
                    <div
                      className="b-filter__item col-md-6 col-lg-12 col-xl-6"
                      style={{ padding: 0 }}
                    >
                      <input
                        placeholder="2020"
                        className="quick-select text-box single-line"
                        style={{ paddingRight: 1 }}
                        type="number"
                        value={versionyearto}
                      />
                    </div>
                  </div>
                </div>
                <br />

                <Consumer>
                  {({ handleChildQuickSrchAdPost }) => (
                    <button
                      className="btn btn-primary btn-block"
                      style={{ backgroundColor: "#D3E428" }}
                      onClick={() =>
                        handleChildQuickSrchAdPost(
                          ssellertype,
                          svtype,
                          scity,
                          sowner,
                          minVal,
                          maxVal,
                          smake,
                          smodel,
                          sversion,
                          versionyearfrom,
                          versionyearto,
                          sassembly,
                          bodytypeid,
                          transmissionid,
                          mileagemin,
                          mileagemax,
                          extcolorid,
                          sdoormin,
                          sdoormax,
                          sdriveside,
                          smetermin,
                          smetermax,
                          sengsizemin,
                          sengsizemax,
                          senginetype,
                          spic,
                          vehiclecategoryid
                        )
                      }
                    >
                      Search
                    </button>
                  )}
                </Consumer>
              </div>
              <div className="widget-content">
                {/* quick search widget inner close */}
                <div className="widget-advance advance-search">
                  {" "}
                  {/* advance search widget inner start */}
                  <div className="accordion" id="accordion-1">
                    <div className="card">
                      <div className="card-header" id="headingOne">
                        <Link
                          className="accordion-trigger ui-subtitle justify-content-center collapsed ml-3"
                          type="button"
                          to="/advance-search"
                        >
                          Advance search
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Listingsearch;
