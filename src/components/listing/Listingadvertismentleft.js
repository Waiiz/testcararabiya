import React, { Component } from "react";

class Listingadvertismentleft extends Component {
  render() {
    const image1 =
      "./assets/media/banner-ad-vertical.jpg";
    return (
      <div>
        <div
          className="container py-3 d-flex justify-content-center"
          style={{ backgroundColor: "#dddddd" }}
        >
          <img src={image1} alt="" />
          <br />
          <br />
        </div>
      </div>
    );
  }
}

export default Listingadvertismentleft;