import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Consumer } from "../../Context";
import { apiUrlcity, apiUrldealer } from "../../ApiBaseURL";
import { Button, Modal } from "react-bootstrap";

class Promosearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchkeyword: "",

      ldealer: [],
      sdealer: [],
      dealerString: "0",

      lcity: [],
      scity: [],
      cityString: "0",

      error: null,

      //images
      images: [
        { id: true, value: "Promotion with images" },
        { id: false, value: "Promotion without images" }
      ],
      selpic: true,
      //modals
      showModalCity: false,
      showModalDealer: false
    };
  }

  componentDidMount() {
    fetch(apiUrlcity)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
          this.setState({
            lcity: result
          });
        } else {
          console.log("city api");
        }
        },
        error => {
          this.setState({ error });
        }
      );

    fetch(apiUrldealer)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              ldealer: result
            });
          }
          else {
            console.log("dealer api");
          }
        },
        error => {
          this.setState({ error });
        }
      );
  }

  //searchbykeyword
  handlesearchkeyChange = event => {
    this.setState({
      searchkeyword: event.target.value
    });
  };

  //searchbycity,dealer,image
  handlecityChange = event => {
    const searchkeyid = event.target.id;
    const cheked = event.target.checked;
    if (cheked == true) {
      var joined = this.state.scity;
      joined.push(searchkeyid);
      let joinedString = joined.toString();
      this.setState({
        cityString: joinedString,
        scity: joined
      });
    } else {
      let filteredArray = this.state.scity.filter(item => item !== searchkeyid);
      let filteredArrayString = filteredArray.toString();
      this.setState({
        cityString: filteredArrayString,
        scity: filteredArray
      });
    }
  };

  handledealerChange = event => {
    const searchkeyid = event.target.id;
    const cheked = event.target.checked;

    if (cheked == true) {
      var joined = this.state.sdealer;
      joined.push(searchkeyid);
      let joinedString = joined.toString();
      this.setState({
        dealerString: joinedString,
        sdealer: joined
      });
    } else {
      let filteredArray = this.state.sdealer.filter(
        item => item !== searchkeyid
      );
      let filteredArrayString = filteredArray.toString();
      this.setState({
        dealerString: filteredArrayString,
        sdealer: filteredArray
      });
    }
  };

  handleImageChange = image => {
    this.setState({
      selpic: image
    });
  };

  //city modal
  handleCloseCity = () => {
    this.setState({
      showModalCity: false
    });
  };
  handleShowCity = () => {
    this.setState({
      showModalCity: true
    });
  };
  //dealer modal
  handleCloseDealer = () => {
    this.setState({
      showModalDealer: false
    });
  };
  handleShowDealer = () => {
    this.setState({
      showModalDealer: true
    });
  };

  modalCity = () => {
    return (
      <Modal show={this.state.showModalCity} onHide={this.handleCloseCity}>
        <Modal.Header closeButton>
          <Modal.Title>Select Cities</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="modal-body checkbox-list">
            <ul
              id="make_list_popular"
              name="make_list"
              className="list-unstyled inline list-links clearfix"
            >
              {this.state.lcity.map((city, index) => (
                <li key={index}>
                  <label className="checkbox">
                    <input
                      style={{ marginRight: 5 }}
                      id={city.CityID}
                      value={city.CityName}
                      onChange={this.handlecityChange}
                      type="checkbox"
                    />
                    {city.CityName}{" "}
                    <span className="badge badge-gray-light pull-right" />
                  </label>
                </li>
              ))}
            </ul>
          </div>
        </Modal.Body>
      </Modal>
    );
  };

  modalDealer = () => {
    return (
      <Modal show={this.state.showModalDealer} onHide={this.handleCloseDealer}>
        <Modal.Header closeButton>
          <Modal.Title>Select Dealers</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="modal-body checkbox-list">
            <ul
              id="make_list_popular"
              name="make_list"
              className="list-unstyled inline list-links clearfix"
            >
              {this.state.ldealer.map((dealer, index) => (
                <li key={index}>
                  <label className="checkbox">
                    <input
                      style={{ marginRight: 5 }}
                      id={dealer.dealerID}
                          value={dealer.dealerName}
                          onChange={this.handledealerChange}
                      type="checkbox"
                    />
                    &nbsp;{dealer.dealerName}
                    <span className="badge badge-gray-light pull-right" />
                  </label>
                </li>
              ))}
            </ul>
          </div>
        </Modal.Body>
      </Modal>
    );
  };

  render() {
    const { cityString, dealerString, selpic } = this.state;
    console.log(cityString);
    return (
      <aside className="l-sidebar">
        <div className="widget widget-search section-sidebar ">
          <div className="b-filter-slider__title" style={{ fontSize: "14px" }}>
            Search by keyword
          </div>
          <Consumer>
            {({ handleChildSearchKeyword }) => (
              <form className="form-sidebar-adv row" id="search-global-form">
                <input
                  className="form-sidebar-adv__input form-control"
                  value={this.state.searchkeyword}
                  onChange={this.handlesearchkeyChange}
                  placeholder="Enter Keyword"
                />
                <button
                  type="button"
                  className="form-sidebar-adv__btn"
                  onClick={() =>
                    handleChildSearchKeyword(this.state.searchkeyword)
                  }
                >
                  <i className="ic icon-magnifier"></i>
                </button>
              </form>
            )}
          </Consumer>
        </div>

        <div className="accordion" id="accordion-2">
          <div className="card">
            <div
              className="card-header px-0 py-0"
              id="headingTwo"
              data-toggle="collapse"
              data-target="#collapseTwo"
              aria-expanded="false"
              aria-controls="collapseTwo"
            >
              <h3
                className="widget-title"
                style={{
                  backgroundColor: "#e8e8e8",
                  color: "#253241",
                  textTransform: "none",
                  fontSize: "16px",
                  fontWeight: 600
                }}
              >
                Quick Search
                <Consumer>
                  {({ handleChildAdvSearchPromo }) => (
                    <button
                      onClick={() =>
                        handleChildAdvSearchPromo("0", "0", "true")
                      }
                    >
                      <i class="fas fa-redo-alt"></i>
                    </button>
                  )}
                </Consumer>
              </h3>
            </div>
            <div
              className="accordion-body"
              id="collapseTwo"
              aria-labelledby="headingTwo"
              data-parent="#accordion-2"
            >
              <div className="card-body">
                <div className="widget-inner">
                  <div className="widget widget-search section-sidebar">
                    <div className="b-filter-slider__title">City</div>

                    {this.state.lcity.map((city, index) =>
                      index < 4 ? (
                        <div
                          key={index}
                          className="form-check col-12"
                          style={{ padding: 0 }}
                        >
                          <input
                            type="checkbox"
                            id={city.CityID}
                            value={city.CityName}
                            onChange={this.handlecityChange}
                          />
                          &nbsp;{city.CityName}
                        </div>
                      ) : (
                        ""
                      )
                    )}

                    <div style={{ marginLeft: "-22px" }} className="savesearch">
                      <Button
                        style={{
                          fontSize: "12px",
                          textDecoration: "underline",
                          color: "#D3E428",
                          border: 0
                        }}
                        variant="light"
                        onClick={this.handleShowCity}
                      >
                        more choices
                      </Button>
                    </div>

                    <br />
                    <div className="b-filter-slider__title">Dealers</div>

                    {this.state.ldealer.map((dealer, index) => 
                      index < 4 ? (
                      <div key={index} className="form-check col-12" style={{ padding: 0 }}>
                        <input
                          type="checkbox"
                          id={dealer.dealerID}
                          value={dealer.dealerName}
                          onChange={this.handledealerChange}
                        />
                        &nbsp;{dealer.dealerName}
                      </div>
                      ) : (
                        ""
                      )
                    )}

                    <div style={{ marginLeft: "-22px" }} className="savesearch">
                      <Button
                        style={{
                          fontSize: "12px",
                          textDecoration: "underline",
                          color: "#D3E428",
                          border: 0
                        }}
                        variant="light"
                        onClick={this.handleShowDealer}
                      >
                        more choices
                      </Button>
                      </div>

                    <br />
                    <div className="b-filter-slider__title">Images</div>

                    <div id="forms" className="form-group">
                      <div className="row">
                        {this.state.images.map((image, index) => (
                          <div
                            key={index}
                            className="col-12 pt-1"
                            style={{ padding: 0 }}
                          >
                            <div className="label-group mb-3">
                              <div>
                                <input
                                  style={{
                                    opacity: 1,
                                    margin: "9px 10px 0 0"
                                  }}
                                  type="radio"
                                  value={image.id}
                                  checked={this.state.selpic === image.id}
                                  onChange={() =>
                                    this.handleImageChange(image.id)
                                  }
                                />

                                <label
                                  className="form-check-label"
                                  style={{
                                    marginLeft: "18px",
                                    marginRight: "30px"
                                  }}
                                >
                                  {image.value}
                                </label>
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>

                    <hr />
                  </div>
                  <div id="special">
                    <Consumer>
                      {({ handleChildAdvSearchPromo }) => (
                        <Link
                          type="button"
                          className="btn btn-primary btn-block"
                          onClick={() =>
                            handleChildAdvSearchPromo(
                              cityString,
                              dealerString,
                              selpic
                            )
                          }
                        >
                          Search
                        </Link>
                      )}
                    </Consumer>
                  </div>
                  {/* modal city */}
                  {this.modalCity()}
                  {/* modal city */}
                  {this.modalDealer()}
                </div>
              </div>
            </div>
          </div>
        </div>
      </aside>
    );
  }
}

export default Promosearch;