import React, { Component, Fragment } from "react";
import ReactPaginate from "react-paginate";
import axios from "axios";
import { apiBaseURL } from "../../ApiBaseURL";
import "../listing/paginate.css";
import { Link } from "react-router-dom";
import { Modal } from "react-bootstrap";
import { MyContext } from "../../Context";
import Loader from "../../pages/Loader";
import ErrorAlert from "../../pages/ErrorAlert";
import _ from "lodash";
import HashMap from "hashmap";

var PromoLikeDislike = new HashMap();
class Promolist extends Component {
  static contextType = MyContext;
  constructor(props) {
    super(props);
    this.previousContext = "";
    this.usertypeid = "";
    this.branchid = "";
    this.userid = "";
    //like-dislike
    this.isFetchingDataforPromoLikeDisLike = false;
    this.state = {
      //promolisting
      isLoadingForPromoListing: false,
      PromoListingData: [],
      GlobalPromolistingData: [],
      //pagination
      offset: 0,
      perPage: 7,
      currentPage: 0,
      listingData: [],
      toggleAlertMsg: false,

      //modal
      showModalRedeem: false,
      showModalSuccess: false,
      SuccessMessage: "",
      txtMsgDealer: "",
      dealerDescription: "",

      //sortby
      SortPromoListing: [
        { id: 1, value: "Newest to Oldest", method: "new" },
        { id: 2, value: "Oldest to Newest", method: "old" }
      ],
      selSortPromoValue: ""
    };
  }

  componentDidMount() {
    this.previousContext = this.context.data;

    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      this.userid = parseUserData[0].UserID;
      this.usertypeid = parseUserData[0].UserTypeID;
      this.branchid = parseUserData[0].BranchID;
      this.loadPromoLikeDislikeData(this.userid);
      this.loadUserData(this.userid);
    } else {
      PromoLikeDislike.clear();
    }
    this.loadPromoListing();
  }

  loadUserData = userid => {
    const urluser = `${apiBaseURL}/user?UserID=${userid}`;
    axios
      .get(urluser)
      .then(res => {
        this.setState({
          userData: res.data,
          Firstname: res.data[0].FirstName,
          Lastname: res.data[0].LastName,
          Email: res.data[0].Email
        });
      })
      .catch(error => {
        console.log(error, "userdata api");
      });
  };

  loadPromoLikeDislikeData = userid => {
    const urlgetpromolikedisLike = `${apiBaseURL}/PromoAndOffer/GetPromoLikeDisLikeByUserID?userID=${userid}`;
    axios
      .get(urlgetpromolikedisLike)
      .then(res => {
        const promolikedislike = res.data;
        //promoID Loading
        promolikedislike.forEach(item => {
          PromoLikeDislike.set(item.promoID, item.like);
        });

        this.setState({
          GetPromoLikeDisLikeData: res.data
        });
      })
      .catch(error => {
        console.log(error, "userGetPromoLikeDisLikeByUserIDdata api");
      });
  };

  loadPromoListing = () => {
    this.setState({ isLoadingForPromoListing: true });
    const url = `${apiBaseURL}/promoandoffer`;
    axios
      .get(url)
      .then(res => {
        this.setState({
          GlobalPromolistingData: res.data
        });
        const reverseListing = res.data;
        this.setState({ isLoadingForPromoListing: false });
        this.receivedData(reverseListing);
      })
      .catch(error => {
        this.setState({
          offset: 0,
          perPage: 7,
          currentPage: 0,
          toggleAlertMsg: true,
          listingData: [],
          isLoadingForPromoListing: false
        });
        console.log(error, "from promoandoffer api");
      });
  };

  componentDidUpdate() {
    if (
      !_.isEqual(
        this.previousContext.searchkeyword,
        this.context.data.searchkeyword
      )
    ) {
      console.log("componentDidUpdate", this.context.data);
      //pagination
      this.setState({
        offset: 0,
        perPage: 7,
        currentPage: 0,
        PromoListingData: []
      });
      let searchkeyword = this.context.data.searchkeyword;
      this.previousContext = this.context.data;
      this.receivedDataBySearchkeyword(searchkeyword);
    }

    if (!_.isEqual(this.previousContext, this.context.data)) {
      console.log("receivedDataByAdvSearchPromo", this.context.data);
      //pagination
      this.setState({
        offset: 0,
        perPage: 7,
        currentPage: 0,
        PromoListingData: []
      });
      this.context.data.searchkeyword = "";
      let cityString = this.context.data.cityString;
      let dealerString = this.context.data.dealerString;
      let selpic = this.context.data.selpic;

      this.previousContext = this.context.data;
      this.receivedDataByAdvSearchPromo(cityString, dealerString, selpic);
    }
  }

  receivedDataByAdvSearchPromo = (cityString, dealerString, selpic) => {
    const url = `${apiBaseURL}/Search/AdvSearchPromo?CityID=${cityString}&DealerID=${dealerString}&Image=${selpic}`;
    axios
      .get(url)
      .then(res => {
        this.setState({
          GlobalPromolistingData: res.data
        });
        const reverseListing = res.data;
        this.setState({ isLoadingForPromoListing: false });
        this.receivedData(reverseListing);
      })
      .catch(error => {
        this.setState({
          offset: 0,
          perPage: 7,
          currentPage: 0,
          toggleAlertMsg: true,
          listingData: [],
          isLoadingForPromoListing: false
        });
        console.log(error, "from AdvSearchPromo api");
      });
  };

  receivedDataBySearchkeyword = searchkeyword => {
    const url = `${apiBaseURL}/Search/AdvSearchPromoKeyword?Keyword=${searchkeyword}`;
    axios
      .get(url)
      .then(res => {
        this.setState({
          GlobalPromolistingData: res.data
        });
        const reverseListing = res.data;
        this.setState({ isLoadingForPromoListing: false });
        this.receivedData(reverseListing);
      })
      .catch(error => {
        this.setState({
          offset: 0,
          perPage: 7,
          currentPage: 0,
          toggleAlertMsg: true,
          listingData: [],
          isLoadingForPromoListing: false
        });
        console.log(error, "from AdvSearchPromoKeyword api");
      });
  };

  //redeem
  handleCloseRedeem = () => {
    this.setState({
      showModalRedeem: false
    });
  };
  handleShowRedeem = (promotionID, branchID, bUserEmail) => {
    let userData = localStorage.getItem("user");
    if (userData === null) {
      this.context.handleChildOpenPopover(true);
    } else {
      this.setState({
        promotionID,
        branchID,
        bUserEmail,
        showModalRedeem: true
      });
    }
  };
  //redeem
  handleCloseSuccess = () => {
    this.setState({
      showModalSuccess: false
    });
  };
  handletxtMsgDealerChange = event => {
    this.setState({ txtMsgDealer: event.target.value });
  };

  handleDealerDescChange = event => {
    this.setState({ dealerDescription: event.target.value });
  };

  handleSendMsgToDealer = () => {
    this.setState({
      txtMsgDealer: "",
      dealerDescription: ""
    });

    const url = `${apiBaseURL}/RedeemPromo`;
    let ApiParamForRedeemPromo = {
      promoID: this.state.promotionID,
      branchID: this.state.branchID,
      promoURL: "cararabiya.ahalfa.com/promoandoffer?id=1",
      fromUserID: this.userid,
      toUserID: 0,
      fromUserEmail: this.state.Email,
      toUserEmail: this.state.bUserEmail,
      userName: this.state.Firstname + " " + this.state.Lastname,
      subject: this.state.txtMsgDealer,
      message: this.state.dealerDescription
    };
    axios
      .post(url, null, { params: ApiParamForRedeemPromo })
      .then(res => {
        console.log(res.data, "RedeemPromo api");

        if (res.status === 200) {
          this.setState({
            SuccessMessage: res.data,
            showModalRedeem: false,
            showModalSuccess: true
          });
        } else {
          console.log(res.status, "statuscode from RedeemPromo api");
        }
      })
      .catch(error => {
        console.log(error, "RedeemPromo api");
      });
  };

  modalRedeem = () => {
    return (
      <Modal show={this.state.showModalRedeem} onHide={this.handleCloseRedeem}>
        <Modal.Body>
          <div className="my-3 px-3">
            <div className="d-flex justify-content-center my-3">
              <h4>
                <strong>Send Message to Dealer</strong>
              </h4>
            </div>
            <div>
              <input
                onChange={this.handletxtMsgDealerChange}
                value={this.state.txtMsgDealer}
                type="text"
                className="form-control"
                placeholder="Subject"
              />
            </div>
            <div className="mt-3">
              <textarea
                cols={30}
                rows={5}
                className="form-control"
                placeholder="Message"
                onChange={this.handleDealerDescChange}
                value={this.state.dealerDescription}
              />
            </div>
            <div className="d-flex justify-content-center mb-2 mt-3">
              <button
                onClick={this.handleSendMsgToDealer}
                className="btn"
                style={{
                  border: "none",
                  padding: "10px 80px",
                  backgroundColor: "#D3E428",
                  color: "white"
                }}
              >
                SEND
              </button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    );
  };

  modalSuccess = () => {
    return (
      <Modal show={this.state.showModalSuccess}>
        <Modal.Body>
          <div className="my-3 px-3">
            <div className="d-flex justify-content-center my-3">
              <h4>
                <strong>Success</strong>
              </h4>
            </div>

            <div style={{ textAlign: "center" }}>
              {this.state.SuccessMessage}
            </div>

            <div className="d-flex justify-content-center mb-2 mt-3">
              <button
                onClick={this.handleCloseSuccess}
                className="btn"
                style={{
                  border: "none",
                  padding: "10px 80px",
                  backgroundColor: "#D3E428",
                  color: "white"
                }}
              >
                OK
              </button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    );
  };

  extractDate = datetime => {
    let splitTimeDateWithT = datetime.split("T");
    var date = splitTimeDateWithT[0];
    return date;
  };

  receivedData = promodata => {
    if (promodata.length > 0) {
      this.setState({
        PromolistingData: promodata,
        toggleAlertMsg: false
      });
      const data = promodata;
      const slice = data.slice(
        this.state.offset,
        this.state.offset + this.state.perPage
      );

      const listingData = slice.map((promolist, index) =>
        this.showPromoListing(promolist, index)
      );

      this.setState({
        pageCount: Math.ceil(data.length / this.state.perPage),
        listingData
      });
    } else {
      this.setState({
        offset: 0,
        perPage: 7,
        currentPage: 0,
        toggleAlertMsg: true,
        listingData: []
      });
    }
  };

  handleLikeClick = promoid => {
    let userData = localStorage.getItem("user");
    if (userData === null) {
      this.context.handleChildOpenPopover(true);
    } else {

      if (this.isFetchingDataforPromoLikeDisLike) {
        return;
      }
      this.isFetchingDataforPromoLikeDisLike = true;

      const url = `${apiBaseURL}/PromoAndOffer/PromoLikeDisLike`;
      let ApiParamForPromoLikeDisLike = {
        userID: this.userid,
        promoID: promoid,
        like: true,
        disklike: false
      };
      axios
        .post(url, null, { params: ApiParamForPromoLikeDisLike })
        .then(res => {
          this.isFetchingDataforPromoLikeDisLike = false;
          console.log(res, "PromoLikeDisLike api");

          if (res.status === 200) {
            PromoLikeDislike.set(promoid, true);
            this.receivedData(this.state.PromolistingData);
            this.setState({
              SuccessMessage: "You like this Promotion",
              showModalSuccess: true
            });
          } else {
            this.isFetchingDataforPromoLikeDisLike = false;
            console.log(res.status, "statuscode from PromoLikeDisLike api");
          }
        })
        .catch(error => {
          this.isFetchingDataforPromoLikeDisLike = false;
          console.log(error, "PromoLikeDisLike api");
        });
    }
  };

  handleDislikeClick = promoid => {
    let userData = localStorage.getItem("user");
    if (userData === null) {
      this.context.handleChildOpenPopover(true);
    } else {

      if (this.isFetchingDataforPromoLikeDisLike) {
        return;
      }
      this.isFetchingDataforPromoLikeDisLike = true;

      const url = `${apiBaseURL}/PromoAndOffer/PromoLikeDisLike`;
      let ApiParamForPromoLikeDisLike = {
        userID: this.userid,
        promoID: promoid,
        like: false,
        disklike: true
      };
      axios
        .post(url, null, { params: ApiParamForPromoLikeDisLike })
        .then(res => {
          this.isFetchingDataforPromoLikeDisLike = false;
          console.log(res, "PromoLikeDisLike api");

          if (res.status === 200) {
            PromoLikeDislike.set(promoid, false);
            this.receivedData(this.state.PromolistingData);
            this.setState({
              SuccessMessage: "You Dislike this Promotion",
              showModalSuccess: true
            });
          } else {
            this.isFetchingDataforPromoLikeDisLike = false;
            console.log(res.status, "statuscode from PromoLikeDisLike api");
          }
        })
        .catch(error => {
          this.isFetchingDataforPromoLikeDisLike = false;
          console.log(error, "PromoLikeDisLike api");
        });
    }
  };

  showPromoListing = (promolist, index) => {
    return (
      <div key={index} className="col-12 line-o my-4 px-0">
        {/*Listing start*/}
        <div className="row">
          <div className="col-12 px-0">
            <div className="row">
              <div className="col-12 col-md-4">
                <div
                  className="mb-2 pb-3"
                  style={{ height: "246px", backgroundColor: "whitesmoke" }}
                >
                  <Link to="/">
                    <img
                      style={{
                        height: "246px",
                        backgroundColor: "whitesmoke",
                        border: "1px solid #ddd",
                        borderRadius: "4px",
                        padding: "5px"
                      }}
                      className="b-goods-f__img img-scale"
                      src={promolist.ImageUrl}
                      alt={promolist.PromotionID}
                    />
                  </Link>
                </div>
              </div>
              <div className="col-12 col-md-8">
                <div className="b-goods-f__main">
                  <div className="b-goods-f__descrip">
                    <div className="offers-f__title_myad d-flex justify-content-between">
                      <Link to="/">{promolist.dealerName}</Link>
                      <div>
                        <Link
                          to={promolist.Ext_URL}
                          className="special"
                          style={{
                            fontSize: "0.7em",
                            fontWeight: 400,
                            marginRight: "10px"
                          }}
                        >
                          Visit Website
                        </Link>

                        <i
                          onClick={() =>
                            this.handleLikeClick(promolist.PromotionID)
                          }
                          style={{
                            cursor: "pointer",
                            color:
                              PromoLikeDislike.get(promolist.PromotionID) ===
                                true
                                ? "#D3E428"
                                : ""
                          }}
                          className="far fa-thumbs-up px-1"
                        />
                        <i
                          onClick={() =>
                            this.handleDislikeClick(promolist.PromotionID)
                          }
                          style={{
                            cursor: "pointer",
                            color:
                              PromoLikeDislike.get(promolist.PromotionID) ===
                                false
                                ? "#D3E428"
                                : ""
                          }}
                          className="far fa-thumbs-down"
                        />
                      </div>
                    </div>
                    <div
                      className="b-goods-f__info mt-2"
                      style={{
                        fontSize: "1.1em",
                        fontWeight: 600,
                        position: "relative",
                        top: "-2px",
                        backgroundColor: "#f7f3db",
                        padding: "5px"
                      }}
                    >
                      {promolist.PromotionName}
                    </div>
                    <div
                      className="b-goods-f__info mt-2"
                      style={{
                        fontSize: "0.9em",
                        position: "relative",
                        top: "-2px",
                        backgroundColor: "white",
                        padding: "5px"
                      }}
                    >
                      {promolist.Description}
                    </div>
                    <div
                      className="d-flex justify-content-between"
                      style={{ color: "grey", fontSize: "0.9em" }}
                    >
                      <div className="align-self-center">
                        <span className="ml-2">
                          <b>Date posted:</b>{" "}
                          {this.extractDate(promolist.CreateDate)}
                        </span>
                        <span className="ml-2">
                          <b>Valid till:</b>{" "}
                          {this.extractDate(promolist.Valid_Till)}
                        </span>
                      </div>
                      <div>
                        <button
                          className="btn btn-primary cararabiya-color py-2"
                          onClick={() =>
                            this.handleShowRedeem(
                              promolist.PromotionID,
                              promolist.BranchID,
                              promolist.bUserEmail
                            )
                          }
                        >
                          Redeem
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  handlePageClick = e => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.perPage;
    let searchkeyword = this.context.data.searchkeyword;
    let cityString = this.context.data.cityString;
    let dealerString = this.context.data.dealerString;
    let selpic = this.context.data.selpic;

    this.setState(
      {
        currentPage: selectedPage,
        offset: offset
      },
      () => {
        if (searchkeyword !== "") {
          this.receivedDataBySearchkeyword(searchkeyword);
        } else if (
          cityString !== undefined ||
          dealerString !== undefined ||
          selpic !== undefined
        ) {
          this.receivedDataByAdvSearchPromo(cityString, dealerString, selpic);
        } else {
          this.receivedData(this.state.PromolistingData);
        }
      }
    );
  };

  //sortby
  handleSortByPromoListing = event => {
    this.setState({ selSortPromoValue: event.target.value });
    let PromoData = [];
    PromoData = this.state.GlobalPromolistingData;

    if (event.target.value === "new") {
      PromoData = _.orderBy(PromoData, ["CreateDate"], ["desc"]);
    } else if (event.target.value === "old") {
      PromoData = _.orderBy(PromoData, ["CreateDate"], ["asc"]);
    } else {
      console.log("sort error");
    }

    this.setState({ PromolistingData: PromoData });
    this.receivedData(PromoData);
  };

  dropdownSortBy = () => {
    return (
      <div className="b-filter-goods pt-3">
        <div className="row justify-content-between">
          <div className="list-heading">Offers &amp; Promotions</div>
          <div className="b-filter-goods__wrap col-auto">
            <div className="b-filter-goods__select">
              <select
                className="custom-select-adpost"
                onChange={this.handleSortByPromoListing}
                value={this.state.selSortPromoValue}
              >
                <option selected>Sort by</option>
                {this.state.SortPromoListing.map(sortpromo => (
                  <option key={sortpromo.id} value={sortpromo.method}>
                    {sortpromo.value}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
      </div>
    );
  };

  render() {
    return (
      <Fragment>
        {this.dropdownSortBy()}
        {this.modalRedeem()}
        {this.modalSuccess()}
        {this.state.toggleAlertMsg === true ? (
          <ErrorAlert show={this.state.toggleAlertMsg} />
        ) : (
            <Fragment>
              {this.state.isLoadingForPromoListing ? (
                <Loader />
              ) : (
                  <span>
                    {this.state.listingData}
                    <div style={{ padding: "40px 0px 20px" }}>
                      <ReactPaginate
                        previousLabel={"prev"}
                        nextLabel={"next"}
                        breakLabel={"..."}
                        breakClassName={"break-me"}
                        pageCount={this.state.pageCount}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={5}
                        onPageChange={this.handlePageClick}
                        containerClassName={"pagination"}
                        subContainerClassName={"pages pagination"}
                        activeClassName={"active"}
                        forcePage={this.state.currentPage}
                      />
                    </div>
                  </span>
                )}
            </Fragment>
          )}
      </Fragment>
    );
  }
}
Promolist.contextType = MyContext;
export default Promolist;