import React, { Component, Fragment } from "react";
import { apiBaseURL } from "../../ApiBaseURL";
import axios from "axios";
import ReactPaginate from "react-paginate";
import "../listing/paginate.css";
import _ from "lodash";
import { Button, Modal } from "react-bootstrap";
import { Link } from "react-router-dom";
import moment from "moment";
import Loader from "../../pages/Loader";
import ErrorAlert from "../../pages/ErrorAlert";
import HashMap from 'hashmap';

var UserSaveAds = new HashMap();
var CompareVehicle = new HashMap();
var StoreCompareVehicleIDs = new HashMap();

class SavedAdsDetail extends Component {
  constructor(props) {
    super(props);
    this.isTotalMySaveAdsCountLoading = false;

    this.simpleWatermarkImg = "./assets/media/watermark.png";
    this.soldWatermarkImg = "./assets/media/watermark-sold.png";
    this.disabledWatermarkImg = "./assets/media/watermark-disabled.png";

    this.state = {
      userId: "",
      isLoadingForUserSavedAdListing: false,
      TotalMySavedAdsCountData: [],
      ListingUserSavedAd: [],
      GlobalUserSavedAdListing: [],
      //pagination
      offset: 0,
      perPage: 7,
      currentPage: 0,
      listingData: [],
      toggleAlertMsg: false,

      start: "",
      end: "",
      totalResults: "",

      //filterby
      FilterAdPostListing: [
        { id: 1, value: "Active Ads", method: "active" },
        { id: 2, value: "InActive Ads", method: "inactive" },
        { id: 3, value: "Sold Ads", method: "sold" },
        { id: 4, value: "UnSold Ads", method: "unsold" },
      ],
      selFilterAdPostValue: "",

      //sortby
      SortAdPostListing: [
        { id: 1, value: "Newest to Oldest", method: "new" },
        { id: 2, value: "Oldest to Newest", method: "old" },
        { id: 3, value: "Price Highest to Lowest", method: "high" },
        { id: 4, value: "Price Lowest to Highest", method: "low" },
      ],
      selSortAdPostValue: "",
      //modals
      vcid: '',
      showcompareheader: false,
      showModalPhone: false,
      showModalEmail: false,
      showModalEmailListing: false,
      showModalSuccess: false,
      SuccessMessage: "",
      txtSubject: "",
      taMessage: "Hi, I've found this Ad on #Cararabiya. What do you think? ",
      txtEmail: "",
    };
  }
  componentDidMount() {
    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      let userid = parseUserData[0].UserID;
      this.setState({
        userId: parseUserData[0].UserID,
      });

      this.setState({ isLoadingForUserSavedAdListing: true });
      const url = `${apiBaseURL}/UserSavedAd?UserID=${userid}`;
      axios
        .get(url)
        .then((res) => {
          console.log(res.data);
          this.setState({
            GlobalUserSavedAdListing: res.data,
          });
          const reverseListingUserSavedAd = res.data;
          //AdPostID Loading
          reverseListingUserSavedAd.forEach(item => {
            UserSaveAds.set(item.AdPostID, true);
          });
           //compare
        res.data.forEach((item) => {
          CompareVehicle.set(item.AdPostID, false);
        });

        if (StoreCompareVehicleIDs.size > 0) {
          for (let pair of StoreCompareVehicleIDs) {
            CompareVehicle.set(pair.key, true);
            this.setState({
              showcompareheader: true,
            });
          }
        }
          this.setState({ isLoadingForUserSavedAdListing: false });

          this.receivedData(reverseListingUserSavedAd);
        })
        .catch((error) => {
          this.setState({
            start: 0,
            end: 0,
            offset: 0,
            perPage: 7,
            currentPage: 0,
            totalResults: 0,
            toggleAlertMsg: true,
            listingData: [],
            isLoadingForUserSavedAdListing: false,
          });
          console.log(error, "from UserSavedAd api");
        });

      this.getMySaveAdsSummary(userid);
    }
  }

  getMySaveAdsSummary = (userid) => {
    if (this.isTotalMySaveAdsCountLoading) {
      return;
    }
    this.isTotalMySaveAdsCountLoading = true;

    const url = `${apiBaseURL}/TotalMySaveAdsCount/MySaveAdsSummary?UserID=${userid}`;
    axios
      .get(url)
      .then((res) => {
        if (res.data.length > 0) {
          this.isTotalMySaveAdsCountLoading = false;
          this.setState({
            TotalMySavedAdsCountData: res.data,
          });
        } else {
          console.log(res.data, "res.data");
        }
      })
      .catch((error) => {
        this.isTotalMySaveAdsCountLoading = false;
        console.log(error, "TotalMySavedAdsCountData api");
      });
  };

  numberWithCommas = (price) => {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  receivedData = (listingUserSavedAd) => {
    console.log(listingUserSavedAd, 'receivedData');
    if (listingUserSavedAd.length > 0) {
      this.setState({
        ListingUserSavedAd: listingUserSavedAd,
        totalResults: listingUserSavedAd.length,
        toggleAlertMsg: false,
      });

      const data = listingUserSavedAd;

      if (
        this.state.offset + 1 + this.state.perPage >
        listingUserSavedAd.length
      ) {
        this.setState({
          end: listingUserSavedAd.length,
        });
      } else {
        this.setState({
          end: this.state.offset + this.state.perPage,
        });
      }

      const slice = data.slice(
        this.state.offset,
        this.state.offset + this.state.perPage
      );

      const listingData = slice.map((listusersavedad, index) =>
        this.showUserSavedAdListingData(listusersavedad, index)
      );

      this.setState({
        pageCount: Math.ceil(data.length / this.state.perPage),
        start: this.state.offset + 1,
        listingData,
      });
    } else {
      this.setState({
        start: 0,
        end: 0,
        offset: 0,
        perPage: 7,
        currentPage: 0,
        totalResults: 0,
        toggleAlertMsg: true,
        listingData: [],
      });
    }
  };

  showWatermark = (listadpostisactive, listadpostissold) => {
    if (listadpostissold) {
      return this.soldWatermarkImg;
    } else if (listadpostisactive) {
      return this.simpleWatermarkImg;
    } else {
      return this.disabledWatermarkImg;
    }
  };

  formattedDate = (datetime) => {
    let splitTimeDateWithT = datetime.split("T");
    let date = splitTimeDateWithT[0]; //T00:00:00
    let formatteddate = moment(date).format("D MMMM YYYY");
    return formatteddate;
  };

  handleUserSaveAd = (adpostid, userid) => {
    if (UserSaveAds.get(adpostid) === true) {
      this.handleAdDeleteByAdPostIDUserID(adpostid, userid);
    } else if (UserSaveAds.get(adpostid) === false) {
      this.handleAdSaveByAdPostIDUserID(adpostid, userid);
    }
  }

  handleAdDeleteByAdPostIDUserID = (adpostid, userid) => {
    const url = `${apiBaseURL}/UserSavedAd/AdDeleteByAdPostIDUserID`;
    let ApiParamForAdDelete = {
      UserID: userid,
      AdpostID: adpostid,
    };
    axios
      .post(url, null, {params: ApiParamForAdDelete} )
      .then((res) => {
        console.log(res, "res from ApiParamForAdDelete api");

        if (res.status === 200) {
          UserSaveAds.set(adpostid, false);
          this.receivedData(this.state.ListingUserSavedAd);
          this.getMySaveAdsSummary(userid);
          console.log(res.status, "ApiParamForAdDelete");
        } else {
          console.log(
            res.status,
            res.statusText,
            "statuscode from ApiParamForAdDelete api"
          );
        }
      })
      .catch((error) => {
        console.log(error, "from ApiParamForAdDelete api");
      });
  };

  handleAdSaveByAdPostIDUserID = (adpostid, userid) => {
    const url = `${apiBaseURL}/UserSavedAd`;
    let ApiParamForAdSave = {
      UserID: userid,
      AdpostID: adpostid,
    };
    axios
      .post(url, ApiParamForAdSave)
      .then((res) => {
        console.log(res, "res from ApiParamForAdSave api");

        if (res.status === 200) {
          UserSaveAds.set(adpostid, true);
          this.receivedData(this.state.ListingUserSavedAd);
          this.getMySaveAdsSummary(userid);
          console.log(res.status, "ApiParamForAdSave");
        } else {
          console.log(
            res.status,
            res.statusText,
            "statuscode from ApiParamForAdSave api"
          );
        }
      })
      .catch((error) => {
        console.log(error, "from ApiParamForAdSave api");
      });
  };

  //Email Listing
  handleCloseEmailListing = () => {
    this.setState({
      txtEmail: "",
      txtSubject: "",
      taMessage: "Hi, I've found this Ad on #Cararabiya. What do you think? ",
      showModalEmailListing: false,
    });
  };
  handleShowEmailListing = (vehicle, adpostid, vcid) => {
    this.setState({
      adpostid,
      vehicle,
      vcid,
      showModalEmailListing: true,
    });
  };

  handleCloseSuccess = () => {
    this.setState({
      showModalSuccess: false,
    });
  };

  handletxtEmailChange = (event) => {
    this.setState({ txtEmail: event.target.value });
  };

  handletxtSubjectChange = (event) => {
    this.setState({ txtSubject: event.target.value });
  };

  handletaMessageChange = (event) => {
    this.setState({ taMessage: event.target.value });
  };

  handleEmailListing = (event) => {
    event.preventDefault();
    const {
      userId,
      txtSubject,
      taMessage,
      adpostid,
      vcid,
      txtEmail,
    } = this.state;
    var adposturl = "";
    if (vcid === 1) {
      adposturl = `https://cararabiya.netlify.app/postad-detail?AdPostId=${adpostid}`;
    } else {
      adposturl = `https://cararabiya.netlify.app/postad-detailbike?AdPostId=${adpostid}`;
    }

    this.setState({
      txtEmail: "",
      txtSubject: "",
      taMessage: "Hi, I've found this Ad on #Cararabiya. What do you think? ",
    });

    const url = `${apiBaseURL}/Email/EmailThisAd`;
    let ApiParamForEmailThisAd = {
      UID: userId,
      ToEmail: txtEmail,
      subLine: txtSubject,
      mailMessage: taMessage,
      AdPostURL: adposturl,
    };
    axios
      .post(url, null, { params: ApiParamForEmailThisAd })
      .then((res) => {
        console.log(res.data, "EmailThisAd api");

        if (res.status === 200) {
          this.setState({
            SuccessMessage: res.data,
            showModalEmailListing: false,
            showModalSuccess: true,
          });
        } else {
          console.log(res.status, "statuscode from EmailThisAd api");
        }
      })
      .catch((error) => {
        console.log(error, "EmailThisAd api");
      });
  };

  modalListingEmail = () => {
    return (
      <Modal
        show={this.state.showModalEmailListing}
        onHide={this.handleCloseEmailListing}
      >
        <Modal.Body>
          <div className="my-3 px-3">
            <div className="d-flex justify-content-center my-3">
              <h4>
                <strong>Email this Ad</strong>
              </h4>
            </div>
            <form onSubmit={this.handleEmailListing}>
              <div>
                <div className="form-group">
                  <label style={{ fontSize: "16px" }}>Title: </label>
                  <div>
                    <span style={{ fontSize: "16px" }}>
                      {this.state.vehicle}
                    </span>
                  </div>
                </div>

                <input
                  name="email"
                  required
                  onChange={this.handletxtEmailChange}
                  value={this.state.txtEmail}
                  type="email"
                  className="form-control"
                  placeholder="Email *"
                />
                <br />
                <input
                  name="subject"
                  required
                  onChange={this.handletxtSubjectChange}
                  value={this.state.txtSubject}
                  type="text"
                  className="form-control"
                  placeholder="Subject *"
                />
              </div>
              <div className="mt-3">
                <textarea
                  name="message"
                  required
                  cols={30}
                  rows={5}
                  className="form-control"
                  placeholder="Message *"
                  onChange={this.handletaMessageChange}
                  value={this.state.taMessage}
                />
              </div>
              <div className="d-flex justify-content-center mb-2 mt-3">
                <button
                  type="submit"
                  className="btn"
                  style={{
                    border: "none",
                    padding: "10px 80px",
                    backgroundColor: "#D3E428",
                    color: "white",
                  }}
                >
                  SEND
                </button>
              </div>
            </form>
          </div>
        </Modal.Body>
      </Modal>
    );
  };

  modalSuccess = () => {
    return (
      <Modal show={this.state.showModalSuccess}>
        <Modal.Body>
          <div className="my-3 px-3">
            <div className="d-flex justify-content-center my-3">
              <h4>
                <strong>Success</strong>
              </h4>
            </div>

            <div style={{ textAlign: "center" }}>
              {this.state.SuccessMessage}
            </div>

            <div className="d-flex justify-content-center mb-2 mt-3">
              <button
                onClick={this.handleCloseSuccess}
                className="btn"
                style={{
                  border: "none",
                  padding: "10px 80px",
                  backgroundColor: "#D3E428",
                  color: "white",
                }}
              >
                OK
              </button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    );
  };

  showUserSavedAdListingData = (listusersavedad, index) => {
    return (
      <Fragment key={index}>
        <div className="col-12 line-o my-3 px-0 bg-shadow">
          {/*Listing start*/}
          <div className="row">
            <div className="col-lg-8 px-0">
              <div className="row">
                <div className="col-12 px-0 pl-3 mt-2 mb-3">
                  <div className="d-flex align-items-center Ad-top-sec">
                    <i className="fas fa-star mr-2" />
                    <span className="mr-3">Featured ad</span>
                    {listusersavedad.Counter > 1000 ? (
                      <>
                      <i className="fas fa-bolt mr-2" />
                      <span className="mr-3">Super hot</span>
                      </>
                    ) : 
                    <>
                    <i className="fas fa-bolt mr-2" style={{color: '#848484'}} />
                    </>
  }
                    <button
                      onClick={() => this.handleUserSaveAd(listusersavedad.AdPostID, this.state.userId)}
                      type="button"
                      className="btn btn-sm liketoggle like Ad-like-btn-style"
                      name="like"
                    >
                      <i className="fas fa-heart" style={{ color: UserSaveAds.get(listusersavedad.AdPostID) === true ? '#D3E428' : ''}}/> <span>Save</span>
                    </button>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12 col-md-5">
                  <div
                    className="b-goods-f__listing mb-2"
                    style={{ height: "134px" }}
                  >
                    <Link to="">
                      <img
                        className="watermark-listing"
                        src={this.showWatermark(
                          listusersavedad.IsActive,
                          listusersavedad.IsSold
                        )}
                        style={{ height: "134px" }}
                      />
                      <img
                        className="b-goods-f__img img-scale"
                        src={listusersavedad.ImageUrl}
                        style={{ height: "134px" }}
                        alt="foto"
                      />
                    </Link>
                  </div>

                  <span
                    className="Ad-location"
                    style={{ display: "flex"}}
                  >
                    <i className="fas fa-map-marker-alt" style={{color: '#D3E428', marginTop: '5px'}} />
                    <span style={{ margin: "0 0 10px 8px" }}>
                      {listusersavedad.Area}
                    </span>
                  </span>
                </div>
                <div className="col-12 col-md-7">
                  <div className="b-goods-f__main d-flex">
                    <div className="b-goods-f__descrip">
                      <div className="b-goods-f__title_myad">
                        {listusersavedad.VersionID === 1 ? (
                            <Link
                              to={{
                                pathname: "/postad-detail",
                                state: {
                                  AdPostID: listusersavedad.AdPostID,
                                },
                              }}
                            >
                              {listusersavedad.Vehicle}
                            </Link>
                          ) : (
                            <Link
                              to={{
                                pathname: "/postad-detailbike",
                                state: {
                                  AdPostID: listusersavedad.AdPostID,
                                },
                              }}
                            >
                              {listusersavedad.Vehicle}
                            </Link>
                          )}
                      </div>
                      <div className="b-goods-f__info Ad-posted-date">
                        {this.formattedDate(listusersavedad.CreateDate)}
                      </div>
                      <ul className="b-goods-f__list list-unstyled mt-1 fsize-car-descr">
                        <li className="b-goods-f__list-item">
                          <span className="b-goods-f__list-title">
                          Version Year :
                          </span>
                          <span className="b-goods-f__list-info Ad-Transmission">
                            {listusersavedad.VersionYear}
                          </span>
                        </li>
                        <li className="b-goods-f__list-item">
                          <span className="b-goods-f__list-title">Model :</span>
                          <span className="b-goods-f__list-info Ad-Model">
                            {listusersavedad.ModelName}
                          </span>
                        </li>
                        <li className="b-goods-f__list-item">
                          <span className="b-goods-f__list-title">
                            Transmission :
                          </span>
                          <span className="b-goods-f__list-info Ad-Transmission">
                            {listusersavedad.TransmissionName}
                          </span>
                        </li>
                        <li className="b-goods-f__list-item">
                          <span className="b-goods-f__list-title">Color :</span>
                          <span className="b-goods-f__list-info Ad-Color">
                            {listusersavedad.ColorName}
                          </span>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="Ad-email-listing">
                    <Link style={{ color: "grey" }} 
                     onClick={() =>
                      this.handleShowEmailListing(
                        listusersavedad.Vehicle,
                        listusersavedad.AdPostID,
                        listusersavedad.VersionID 
                      )
                    }
                    >
                      <span className="email-font-size mr-3">
                        <i className="far fa-envelope mr-1" /> Email this
                        Ad
                      </span>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-sm-12 px-0 list-sec-r">
              {" "}
              {/* price section */}
              <div className="Ad-price-sec">
                <span className="Ad-price-style">
                  {listusersavedad.Price === 0 ? 'Call For Price' : listusersavedad.CurCode +' '+ this.numberWithCommas(listusersavedad.Price)}
                </span>
              </div>
              <div>
                {" "}
                {/* buttons */}
                <div className="b-goods-f__sidebar px-3">
                  <span className="b-goods-f__price-group">
                    <button
                      onClick={() => this.handleShowPhone(listusersavedad.UserID)}
                      type="button"
                      className="btn btn-secondaryCuz btn-sm btn-block mt-2 d-flex align-items-center Ad-btn-grey"
                    >
                      {" "}
                      <i className="fas fa-phone prefix justify-content-between" />
                      <span className="Ad-btn-grey-s">Show Phone Number</span>
                    </button>
                    <button
                      onClick={() => this.handleShowEmail(listusersavedad.UserID)}
                      type="button"
                      className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center Ad-btn-grey"
                    >
                      {" "}
                      <i className="fas fa-envelope prefix justify-content-start" />
                      <span className="Ad-btn-grey-s">Show Email</span>
                    </button>
                    <button
                      type="button"
                      className="btn btn-secondaryCuz btn-sm btn-block mt-1 d-flex align-items-center Ad-btn-grey"
                    >
                      {" "}
                      <i className="far fa-comment-dots prefix justify-content-center" />
                      <span className="Ad-btn-grey-s">Chat with seller</span>
                    </button>
                  </span>
                  <span className="b-goods-f__compare">
                    <div className="form-check d-flex align-self-center">
                      <input
                        className="form-check-input Atocompare-checkbox "
                        type="checkbox"
                        checked={
                          CompareVehicle.get(listusersavedad.AdPostID) === true
                            ? true
                            : false
                        }
                        onClick={() =>
                          this.handleCBCompare(
                            listusersavedad.AdPostID,
                            listusersavedad.ImageUrl
                          )
                        }
                      />
                      <label className="form-check-label Atocompare-text">
                        Add to compare
                      </label>
                    </div>
                  </span>
                </div>
              </div>
            </div>
          </div>{" "}
          {/* end row */}
        </div>
      </Fragment>
    );
  };

  handlePageClick = (e) => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.perPage;

    this.setState(
      {
        currentPage: selectedPage,
        offset: offset,
      },
      () => {
        this.receivedData(this.state.ListingUserSavedAd);
      }
    );
  };

  //filterby
  handleFilterByAdPostListing = (event) => {
    this.setState({ selFilterAdPostValue: event.target.value });
    let adpostData = [];
    adpostData = this.state.GlobalUserSavedAdListing;

    if (event.target.value === "active") {
      adpostData = _.filter(
        adpostData,
        (adpostlisting) => adpostlisting.IsActive === true
      );
    } else if (event.target.value === "inactive") {
      adpostData = _.filter(
        adpostData,
        (adpostlisting) => adpostlisting.IsActive === false
      );
    } else if (event.target.value === "sold") {
      adpostData = _.filter(
        adpostData,
        (adpostlisting) => adpostlisting.IsSold === true
      );
    } else if (event.target.value === "unsold") {
      adpostData = _.filter(
        adpostData,
        (adpostlisting) => adpostlisting.IsSold === false
      );
    } else {
      console.log("filter error");
    }

    this.setState({ ListingUserSavedAd: adpostData });
    this.receivedData(adpostData);
  };

  //sortby
  handleSortByAdPostListing = (event) => {
    this.setState({ selSortAdPostValue: event.target.value });
    let adpostData = [];
    adpostData = this.state.GlobalUserSavedAdListing;

    if (event.target.value === "new") {
      adpostData = _.orderBy(adpostData, ["CreateDate"], ["desc"]);
    } else if (event.target.value === "old") {
      adpostData = _.orderBy(adpostData, ["CreateDate"], ["asc"]);
    } else if (event.target.value === "high") {
      adpostData = _.orderBy(adpostData, ["Price"], ["desc"]);
    } else if (event.target.value === "low") {
      adpostData = _.orderBy(adpostData, ["Price"], ["asc"]);
    } else {
      console.log("filter error");
    }

    this.setState({ ListingUserSavedAd: adpostData });
    this.receivedData(adpostData);
  };

  showMySavedAdsHeader = () => {
    return (
      <div className="row">
        <div className="col-6 px-0">
          <div className="list-heading">Saved Ads</div>
          <div className="b-filter-goods__info col-auto">
            Showing
            <strong>
              {" "}
              {this.state.start} - {this.state.end}{" "}
            </strong>{" "}
            out of <strong> {this.state.totalResults}</strong> listings
          </div>
        </div>

        <div className="col-6 px-5">
          <div className="b-filter-goods__wrap col-auto">
            <div className="b-filter-goods__select d-inline-flex">
              <select
                style={{ inlineSize: "auto" }}
                className="custom-select-adpost"
                onChange={this.handleFilterByAdPostListing}
                value={this.state.selFilterAdPostValue}
              >
                <option selected>Filter By</option>
                {this.state.FilterAdPostListing.map((filteradpost) => (
                  <option key={filteradpost.id} value={filteradpost.method}>
                    {filteradpost.value}
                  </option>
                ))}
              </select>

              <select
                style={{ inlineSize: "auto" }}
                className="custom-select-adpost"
                onChange={this.handleSortByAdPostListing}
                value={this.state.selSortAdPostValue}
              >
                <option selected>Sort By</option>
                {this.state.SortAdPostListing.map((sortadpost) => (
                  <option key={sortadpost.id} value={sortadpost.method}>
                    {sortadpost.value}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
      </div>
    );
  };

  showMySavedAdsSummary = () => {
    return (
      <Fragment>
        {this.state.TotalMySavedAdsCountData.map((totalmysavedads, index) => (
          <aside key={index} className="l-sidebar">
            <div className="widget section-sidebar">
              <div className="widget-content">
                <div className="d-flex justify-content-between px-3">
                  <strong>Available Ads</strong>
                  <span>{totalmysavedads.TotalMySaveAdsAvailable}</span>
                </div>
                <hr />
                <div className="d-flex justify-content-between px-3">
                  <strong>Unavailable Ads</strong>
                  <span>{totalmysavedads.TotalMySaveAdsUnavailable}</span>
                </div>
                <hr />
              </div>
            </div>
          </aside>
        ))}
      </Fragment>
    );
  };

  handleShowPhone = (userid) => {
    const urlUserPhone = `${apiBaseURL}/User/UserMobile?UserID=${userid}`;
    axios
      .get(urlUserPhone)
      .then((res) => {
        if (res.data.length > 0) {
          const data = res.data;
          console.log(data, "urlUserPhone");
          this.setState({
            userPhone: data,
            showModalPhone: true,
          });
        } else {
          this.setState({});
        }
      })
      .catch((error) => {
        this.setState({});
        console.log(error, "from urlUserPhone api");
      });
  };

  //phone modal
  handleClosePhone = () => {
    this.setState({
      showModalPhone: false,
    });
  };

  modalPhone = () => {
    return (
      <Modal show={this.state.showModalPhone}>
        <Modal.Header>
          <Modal.Title>User Phone Number</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h6>{this.state.userPhone}</h6>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={this.handleClosePhone}
            style={{ backgroundColor: "#D3E428" }}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    );
  };

  handleShowEmail = (userid) => {
    const urlUserEmail = `${apiBaseURL}/User/UserEmail?UserID=${userid}`;
    axios
      .get(urlUserEmail)
      .then((res) => {
        if (res.data.length > 0) {
          const data = res.data;
          console.log(data, "urlUserEmail");
          this.setState({
            userEmail: data,
            showModalEmail: true,
          });
        } else {
          this.setState({});
        }
      })
      .catch((error) => {
        this.setState({});
        console.log(error, "from urlUserEmail api");
      });
  };

  //phone modal
  handleCloseEmail = () => {
    this.setState({
      showModalEmail: false,
    });
  };

  modalEmail = () => {
    return (
      <Modal show={this.state.showModalEmail}>
        <Modal.Header>
          <Modal.Title>User Email</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h6>{this.state.userEmail}</h6>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={this.handleCloseEmail}
            style={{ backgroundColor: "#D3E428" }}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    );
  };

  //compare work
  handleCBCompare = (adpostid, imageurl) => {
    if (CompareVehicle.get(adpostid) === true) {
      this.handleDisableCompareCB(adpostid);
    } else if (CompareVehicle.get(adpostid) === false) {
      this.handleEnableCompareCB(adpostid, imageurl);
    }
  };

  handleDisableCompareCB = (adpostid) => {
    StoreCompareVehicleIDs.delete(adpostid);
    CompareVehicle.set(adpostid, false);
    if (StoreCompareVehicleIDs.size === 0) {
      this.setState({
        showcompareheader: false,
      });
    }

    this.receivedData(this.state.ListingUserSavedAd);
  };
  handleEnableCompareCB = (adpostid, imageurl) => {
    if (StoreCompareVehicleIDs.size < 4) {
      StoreCompareVehicleIDs.set(adpostid, imageurl);
      CompareVehicle.set(adpostid, true);
      this.setState({
        showcompareheader: true,
      });
      this.receivedData(this.state.ListingUserSavedAd);
    } else {
      alert("limit exceed...");
    }
  };
  compareListHeader = () => {
    return (
      <Fragment>
        <div
          className="container-fluid bg-shadow"
          style={{
            position: "fixed",
            zIndex: "1000",
            backgroundColor: "white",
            top: "74px",
            left: 0,
          }}
        >
          <div className="container py-3">
            <div className="row">
              {[...StoreCompareVehicleIDs.keys()].map((adpostid) => (
                <div key={adpostid}>
                  <button
                    className="compare-cross"
                    onClick={() => this.handleDisableCompareCB(adpostid)}
                  >
                    ×
                  </button>
                  <img
                    className=""
                    width="120px"
                    height="88px"
                    src={StoreCompareVehicleIDs.get(adpostid)}
                    alt=""
                  />
                </div>
              ))}
              <div style={{ margin: "auto", marginRight: 0 }}>
                <Link
                  to={{
                    pathname: "/compare-vehicle",
                    state: { comparedata: StoreCompareVehicleIDs },
                  }}
                  className="btn"
                  style={{
                    padding: "10px 80px",
                    border: "none",
                    backgroundColor: "#D3E428",
                    color: "white",
                    borderRadius: "0px",
                  }}
                >
                  Compare
                </Link>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  };

  render() {
    return (
      <Fragment>
        {this.state.showcompareheader === true ? this.compareListHeader() : ""}
        {/* modal phone */}
        {this.modalPhone()}
        {/* modal email */}
        {this.modalEmail()}
        {/* modal emailad */}
        {this.modalListingEmail()}
        {/* modal success */}
        {this.modalSuccess()}
        <div className="row">
          <div className="col-lg-3">{this.showMySavedAdsSummary()}</div>

          <div className="col-lg-9">
            {this.showMySavedAdsHeader()}
            <br />

            {this.state.toggleAlertMsg === true ? (
              <ErrorAlert show={this.state.toggleAlertMsg} />
            ) : (
              <Fragment>
                {this.state.isLoadingForUserSavedAdListing ? (
                  <Loader />
                ) : (
                  <span>
                    {this.state.listingData}
                    <br />
                    <ReactPaginate
                      previousLabel={"prev"}
                      nextLabel={"next"}
                      breakLabel={"..."}
                      breakClassName={"break-me"}
                      pageCount={this.state.pageCount}
                      marginPagesDisplayed={2}
                      pageRangeDisplayed={5}
                      onPageChange={this.handlePageClick}
                      containerClassName={"pagination"}
                      subContainerClassName={"pages pagination"}
                      activeClassName={"active"}
                      forcePage={this.state.currentPage}
                    />
                  </span>
                )}
              </Fragment>
            )}
          </div>
        </div>
      </Fragment>
    );
  }
}
export default SavedAdsDetail;