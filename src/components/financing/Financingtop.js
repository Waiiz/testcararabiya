import React, { Component } from 'react';

class Financingtop extends Component {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-12">
            <h2 style={{ color: '#D3E428' }} className="mb-4"><strong>Finance Calculator</strong></h2>
            <h6>Let Cararabiya help you with car finance whether you are looking for a new car or used car. Calculate installments, compare banks and get connected to the best one with fastest processing times.</h6>
          </div>
        </div>
      </div>
    );
  }
}

export default Financingtop;