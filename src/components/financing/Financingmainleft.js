import React, { Component } from "react";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";

class Financingmainleft extends Component {
  constructor(props) {
    super(props);
    this.state = {
      carprice: "",
      downpayment: "",
      tenure: "",
      interestrate: "",
      emi: "",
      emidownpayment: "",
      emitenure: "",
      emiinterestrate: "",
      emiP: "0",
      emiPA: "0",
      emiTIA: "0",
      emiTA: "0",
      priceMI: "0",
      priceTLA: "0",
      priceTIA: "0",
      priceTA: "0",
      error: null,
      validationError: "",
      showResultPrice: false,
      showResultEmi: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handlepriceonClick = this.handlepriceonClick.bind(this);
    this.handleemionClick = this.handleemionClick.bind(this);
    this.handleisNumberKey = this.handleisNumberKey.bind(this);
  }

  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value,
    });
  }

  handlepriceonClick = (event) => {
    this.setState({
      showResultPrice: !this.state.showResultPrice,
    });
    event.preventDefault();
    var txt_ID_Price = this.state.carprice;
    var txt_ID_DPay = this.state.downpayment;
    var txt_ID_Tenure = this.state.tenure;
    var txt_ID_IRate = this.state.interestrate;

     //tenure
     var Tm = this.state.tenure * 12;

     // interest rate
     var Im = this.state.interestrate / 100 / 12;
 
     // downpayment
     var DownpaymentPER = this.state.downpayment / 100;
     var DownpaymentAMT = this.state.carprice * DownpaymentPER;
 
     //Lamt
     var Lamt = this.state.carprice - DownpaymentAMT;
 
     var Y = Im * Lamt;
 
     var a = Math.pow(Im + 1, Tm);
     var b = 1/a;
 
     var Z= 1-b;

     var totalloanamount = this.state.carprice - DownpaymentPER;
     var actualemi = Math.round(Y/Z);
     var TotalAmountto = Math.round(actualemi * Tm);

    this.setState({
      priceTLA: totalloanamount,
      priceMI: actualemi,
      priceTA: TotalAmountto,
      priceTIA: Y,
    });
  };

  handleemionClick = (event) => {
    this.setState({
      showResultEmi: !this.state.showResultEmi,
    });
    event.preventDefault();
    var txt_ID_Price = this.state.emi;
    var txt_ID_DPay = this.state.emidownpayment;
    var txt_ID_Tenure = this.state.emitenure;
    var txt_ID_IRate = this.state.emiinterestrate;

  //tenure
  let Tm = this.state.emitenure * 12;

  // interest
  let Im = this.state.emiinterestrate / 100 / 12;

  //downpaymentAMT
  var DownpaymentAMT = this.state.emidownpayment;

  var R = Math.pow((Im + 1), Tm);

  var Lamt = this.state.emi/[(Im * R) / (R -1)];
  var CarPrice = Math.round(Lamt + DownpaymentAMT);
 
    this.setState({
      emiP: txt_ID_Price,
      emiPA: CarPrice,
    //   emiTIA: TotalInterestAmount,
    //   emiTA: TotalAmountto,
    });
  };

  handleisNumberKey(event) {
    var charCode = event.which ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }
  numberWithCommas = (price) => {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };
  render() {
    return (
      <Tabs
        defaultActiveKey="price"
        id="noanim-tab-example"
        variant="tabs"
        style={{ width: 600 }}
      >
        <Tab eventKey="price" title="CALCULATE BY PRICE">
          <hr
            style={{
              borderColor: "#D3E428",
              marginTop: 0,
              width: 549,
              height: 10,
              borderWidth: 1,
            }}
          ></hr>
          <form onSubmit={this.handlepriceonClick}>
            <div className="form-group">
              <div className="row my-3">
                <div
                  className="col-6 align-self-center px-0"
                  style={{ marginTop: 10 }}
                >
                  <label
                    className="control-label"
                    style={{ color: "black", fontWeight: "550", fontSize: 14 }}
                  >
                    CAR PRICE* <span style={{ color: "#D3E428" }}>(SAR)</span>
                  </label>
                </div>
                <div className="col-6" style={{ marginTop: 7 }}>
                  <input
                    className="form-control"
                    name="carprice"
                    value={this.state.carprice}
                    onChange={this.handleChange}
                    onKeyPress={this.handleisNumberKey}
                    placeholder="Enter"
                    size="5"
                    maxlength="9"
                    minlength="9"
                    required
                    type="number"
                  />
                </div>
              </div>
              <div className="row my-3">
                <div className="col-6 align-self-center px-0">
                  <label
                    className="control-label"
                    style={{ color: "black", fontWeight: "550", fontSize: 14 }}
                  >
                    DOWN PAYMENT* <span style={{ color: "#D3E428" }}>(%)</span>
                  </label>
                </div>
                <div className="col-6">
                  <input
                    className="form-control"
                    name="downpayment"
                    value={this.state.downpayment}
                    onChange={this.handleChange}
                    onKeyPress={this.handleisNumberKey}
                    placeholder="Enter"
                    size="5"
                    maxlength="4"
                    minlength="4"
                    required
                    type="number"
                  />
                </div>
              </div>
              <div className="row my-3">
                <div className="col-6 align-self-center px-0">
                  <label
                    className="control-label"
                    style={{ color: "black", fontWeight: "550", fontSize: 14 }}
                  >
                    TENURE* <span style={{ color: "#D3E428" }}>(YEAR)</span>
                  </label>
                </div>
                <div className="col-6">
                  <input
                    className="form-control"
                    name="tenure"
                    value={this.state.tenure}
                    onChange={this.handleChange}
                    onKeyPress={this.handleisNumberKey}
                    placeholder="Enter"
                    size="5"
                    maxlength="3"
                    minlength="3"
                    required
                    type="number"
                  />
                </div>
              </div>
              <div className="row my-3">
                <div className="col-6 align-self-center px-0">
                  <label
                    className="control-label"
                    style={{ color: "black", fontWeight: "550", fontSize: 14 }}
                  >
                    INTEREST RATE* <span style={{ color: "#D3E428" }}>(%)</span>
                  </label>
                </div>
                <div className="col-6">
                  <input
                    className="form-control"
                    name="interestrate"
                    value={this.state.interestrate}
                    onChange={this.handleChange}
                    onKeyPress={this.handleisNumberKey}
                    placeholder="Enter"
                    type="text"
                    size="5"
                    maxlength="4"
                    minlength="4"
                    required
                    type="number"
                  />
                </div>
              </div>
              <div className="pt-3">
                <input
                  className="btn btn-block"
                  style={{ backgroundColor: "#D3E428", color: "#FFF" }}
                  type="submit"
                  value="Calculate"
                />
              </div>
              {this.state.showResultPrice && (
                <div style={{ marginTop: 20 }}>
                  <div
                    style={{
                      padding: 20,
                      backgroundColor: "#495057",
                      color: "white",
                      fontSize: 15,
                      fontWeight: "700",
                      borderRadius: 8,
                      boxShadow: 20,
                    }}
                  >
                    <div class="row mb-3">
                      <div class="col-6">
                        {" "}
                        <label>Monthly Installment</label>{" "}
                      </div>
                      <div class="col-6 align-self-center">
                        <label style={{ textAlign: "left", fontSize: 17 }}>
                          {" "}
                          <span>SAR</span> {this.numberWithCommas(this.state.priceMI)}{" "}
                        </label>
                      </div>
                    </div>
                    <hr style={{ borderColor: "#FFF" }}></hr>
                    <div class="row mb-3">
                      <div class="col-6" st>
                        {" "}
                        <label>Total Loan Amount</label>{" "}
                      </div>
                      <div class="col-6 align-self-center">
                        <label style={{ textAlign: "left", fontSize: 17 }}>
                          {" "}
                          <span>SAR</span> {this.numberWithCommas(this.state.priceTLA)}{" "}
                        </label>
                      </div>
                    </div>
                    <hr style={{ borderColor: "#FFF" }}></hr>
                    <div class="row">
                      <div class="col-6">
                        {" "}
                        <label>Total Interest Amount</label>{" "}
                      </div>
                      <div class="col-6 align-self-center">
                        <label style={{ textAlign: "left", fontSize: 17 }}>
                          {" "}
                          <span>SAR</span> {this.numberWithCommas(this.state.priceTIA)}{" "}
                        </label>
                      </div>
                    </div>
                    <hr style={{ borderColor: "#FFF" }}></hr>
                    <div class="row">
                      <div class="col-6">
                        {" "}
                        <label>Total Amount</label>{" "}
                      </div>
                      <div class="col-6 align-self-center">
                        <label style={{ textAlign: "left", fontSize: 17 }}>
                          {" "}
                          <span>SAR</span> {this.numberWithCommas(this.state.priceTA)}{" "}
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </form>
        </Tab>
        <Tab eventKey="profile" title="CALCULATE BY EMI">
          <form onSubmit={this.handleemionClick}>
            <hr
              style={{
                borderColor: "#D3E428",
                marginTop: 0,
                width: 549,
                height: 10,
                borderWidth: 1,
              }}
            ></hr>
            <div className="form-group">
              <div className="row my-3">
                <div
                  className="col-6 align-self-center px-0"
                  style={{ marginTop: 10 }}
                >
                  <label
                    className="control-label"
                    style={{ color: "black", fontWeight: "550", fontSize: 14 }}
                  >
                    EMI* <span style={{ color: "#D3E428" }}>(SAR)</span>
                  </label>
                </div> 
                <div className="col-6" style={{ marginTop: 7 }}>
                  <input
                    className="form-control"
                    name="emi"
                    value={this.state.emi}
                    onChange={this.handleChange}
                    onChange={this.handleChange}
                    placeholder="Enter"
                    size="5"
                    maxlength="9"
                    minlength="9"
                    required
                    type="number"
                  />
                </div>
              </div>
              <div className="row my-3">
                <div className="col-6 align-self-center px-0">
                  <label
                    className="control-label"
                    style={{ color: "black", fontWeight: "550", fontSize: 14 }}
                  >
                    DOWN PAYMENT* <span style={{ color: "#D3E428" }}>(SAR)</span>
                  </label>
                </div>
                <div className="col-6">
                  <input
                    className="form-control"
                    name="emidownpayment"
                    value={this.state.emidownpayment}
                    onChange={this.handleChange}
                    placeholder="Enter"
                    size="5"
                    maxlength="4"
                    minlength="4"
                    required
                    type="number"
                  />
                </div>
              </div>
              <div className="row my-3">
                <div className="col-6 align-self-center px-0">
                  <label
                    className="control-label"
                    style={{ color: "black", fontWeight: "550", fontSize: 14 }}
                  >
                    TENURE* <span style={{ color: "#D3E428" }}>(YEAR)</span>
                  </label>
                </div>
                <div className="col-6">
                  <input
                    className="form-control"
                    name="emitenure"
                    value={this.state.emitenure}
                    onChange={this.handleChange}
                    onChange={this.handleChange}
                    placeholder="Enter"
                    size="5"
                    maxlength="3"
                    minlength="3"
                    required
                    type="number"
                  />
                </div>
              </div>
              <div className="row my-3">
                <div className="col-6 align-self-center px-0">
                  <label
                    className="control-label"
                    style={{ color: "black", fontWeight: "550", fontSize: 14 }}
                  >
                    INTEREST RATE* <span style={{ color: "#D3E428" }}>(%)</span>
                  </label>
                </div>
                <div className="col-6">
                  <input
                    className="form-control"
                    name="emiinterestrate"
                    value={this.state.emiinterestrate}
                    onChange={this.handleChange}
                    onChange={this.handleChange}
                    placeholder="Enter"
                    size="5"
                    maxlength="4"
                    minlength="4"
                    required
                    type="number"
                  />
                </div>
              </div>
              <div className="pt-3">
                <input
                  className="btn btn-block"
                  style={{ backgroundColor: "#D3E428", color: "#FFF" }}
                  type="submit"
                  value="Calculate"
                />
              </div>
              {this.state.showResultEmi && (
                <div style={{ marginTop: 20 }}>
                  <div
                    style={{
                      padding: 20,
                      backgroundColor: "#495057",
                      color: "white",
                      fontSize: 15,
                      fontWeight: "700",
                      borderRadius: 8,
                      boxShadow: 20,
                    }}
                  >
                    <div class="row mb-3">
                      <div class="col-6">
                        {" "}
                        <label>Monthly Installment</label>{" "}
                      </div>
                      <div class="col-6 align-self-center">
                        <label style={{ textAlign: "left", fontSize: 17 }}>
                          {" "}
                          <span>SAR</span> {this.numberWithCommas(this.state.emiP)}{" "}
                        </label>
                      </div>
                    </div>
                    <hr style={{ borderColor: "#FFF" }}></hr>
                    <div class="row mb-3">
                      <div class="col-6" st>
                        {" "}
                        <label>Car Amount</label>{" "}
                      </div>
                      <div class="col-6 align-self-center">
                        <label style={{ textAlign: "left", fontSize: 17 }}>
                         
                          <span>SAR</span> {this.numberWithCommas(this.state.emiPA)}
                        </label>
                      </div>
                    </div>
                    <hr style={{ borderColor: "#FFF" }}></hr>

                    <div class="row mb-3">
                      <div class="col-6" st>
                        {" "}
                        <label>Total Amount</label>{" "}
                      </div>
                      <div class="col-6 align-self-center">
                        <label style={{ textAlign: "left", fontSize: 17 }}>
                          {" "}
                          <span>SAR</span> {this.numberWithCommas(this.state.emiTA)}{" "}
                        </label>
                      </div>
                    </div>
                    <hr style={{ borderColor: "#FFF" }}></hr>
                    <div class="row">
                      <div class="col-6">
                        {" "}
                        <label>Total Interest Amount</label>{" "}
                      </div>
                      <div class="col-6 align-self-center">
                        <label style={{ textAlign: "left", fontSize: 17 }}>
                          {" "}
                          <span>SAR</span> {this.numberWithCommas(this.state.emiTIA)}{" "}
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </form>
        </Tab>
      </Tabs>
    );
  }
}

export default Financingmainleft;