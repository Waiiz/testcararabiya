import React, { Component, Fragment } from "react";
import { apiBaseURL } from "../../ApiBaseURL";
import axios from "axios";
import ReactPaginate from "react-paginate";
import "../listing/paginate.css";
import _ from "lodash";
import { Link } from "react-router-dom";
import Loader from "../../pages/Loader";
import ErrorAlert from "../../pages/ErrorAlert";
import Switch from "react-switch";
import Dealeradvertisement from "../../pages/Dealeradvertisement";

class BranchDetails extends Component {
  constructor(props) {
    super(props);
    this.isFetchingDataforEnableBranchUser = false;

    this.branchid = "";
    this.userid = "";
    this.state = {
      //userlisting
      isLoadingForBranchListing: false,
      BranchlistingData: [],
      GlobalBranchlistingData: [],
      //pagination
      offset: 0,
      perPage: 7,
      currentPage: 0,
      listingData: [],
      toggleAlertMsg: false,

      start: "",
      end: "",
      totalResults: "",

      //sortby
      SortBranchListing: [
        { id: 1, value: "Newest to Oldest", method: "new" },
        { id: 2, value: "Oldest to Newest", method: "old" },
      ],
      selSortBranchValue: "",
    };
  }

  componentDidMount() {
    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      this.userid = parseUserData[0].UserID;
      this.branchid = parseUserData[0].BranchID;
      this.getDealerID(this.userid);
    }
  }

  getDealerID = (userid) => {
    this.setState({ isLoadingForBranchListing: true });
    const url = `${apiBaseURL}/user?UserID=${userid}`;
    axios
      .get(url)
      .then((res) => {
        this.setState({
          userData: res.data,
          dealerid: res.data[0].CARegistrationID,
          createdby: res.data[0].createdBy,
        });
        this.getDBranchbyDealerID(res.data[0].CARegistrationID);
      })
      .catch((error) => {
        console.log(error, "userdata api");
      });
  }

  getDBranchbyDealerID = (dealerid) => {
    const url = `${apiBaseURL}/DBranch/GetDBranchbyDealerID?DealerID=${dealerid}`;
    axios
      .get(url)
      .then((res) => {
        console.log(res, "GetDBranchbyDealerID");
        this.setState({
          GlobalBranchlistingData: res.data,
        });
        this.setState({ isLoadingForBranchListing: false });
        this.receivedData(res.data);
      })
      .catch((error) => {
        this.setState({
          start: 0,
          end: 0,
          offset: 0,
          perPage: 7,
          currentPage: 0,
          totalResults: 0,
          toggleAlertMsg: true,
          listingData: [],
          isLoadingForBranchListing: false,
        });
        console.log(error, "from GetDBranchbyDealerID api");
      });
  }

  receivedData = (branchlisting) => {
    if (branchlisting.length > 0) {
      this.setState({
        BranchlistingData: branchlisting,
        totalResults: branchlisting.length,
        toggleAlertMsg: false,
      });

      const data = branchlisting;

      if (this.state.offset + 1 + this.state.perPage > branchlisting.length) {
        this.setState({
          end: branchlisting.length,
        });
      } else {
        this.setState({
          end: this.state.offset + this.state.perPage,
        });
      }

      const slice = data.slice(
        this.state.offset,
        this.state.offset + this.state.perPage
      );

      const listingData = slice.map((branchlisting, index) =>
        this.showBranchListing(branchlisting, index)
      );

      this.setState({
        pageCount: Math.ceil(data.length / this.state.perPage),
        start: this.state.offset + 1,
        listingData,
      });
    } else {
      this.setState({
        start: 0,
        end: 0,
        offset: 0,
        perPage: 7,
        currentPage: 0,
        totalResults: 0,
        toggleAlertMsg: true,
        listingData: [],
      });
    }
  };

  extractDate = (datetime) => {
    let splitTimeDateWithT = datetime.split("T");
    var date = splitTimeDateWithT[0];
    return date;
  };

  //handle Change Enable IsActive 
  handleEn_Ds_Branch = (branchlist, index) => {
    var userchecked = branchlist.status === "t" ? "f" : "t";
    branchlist.status = userchecked;

    if (!this.isFetchingDataforEnableBranchUser) {
      this.UpdateIsActivebyUserID(branchlist, index);
    }
  };

  UpdateIsActivebyUserID = (branchlist, index) => {
    this.isFetchingDataforEnableBranchUser = true;

    const url = `${apiBaseURL}/DBranch/En_Ds_Branch`;
    let ApiParamForIsActiveBranch = {
      dBranchID: branchlist.dBranchID,
      status: branchlist.status,
    };
    axios
      .post(url, null, { params: ApiParamForIsActiveBranch })
      .then((res) => {
        this.isFetchingDataforEnableBranchUser = false;
        console.log(res, "En_Ds_Branch api");

        if (res.status === 200) {
          var mylistingbranchdata = this.state.BranchlistingData;

          var getIndex = mylistingbranchdata.indexOf(mylistingbranchdata[index]);

          if (getIndex > -1) {
            mylistingbranchdata.splice(getIndex, 1, branchlist);
          }

          this.setState({
            BranchlistingData: mylistingbranchdata,
          });

          this.receivedData(this.state.BranchlistingData);
        } else {
          this.isFetchingDataforEnableBranchUser = false;
          console.log(
            res.status,
            "statuscode from En_Ds_Branch api"
          );
        }
      })
      .catch((error) => {
        this.isFetchingDataforEnableBranchUser = false;
        console.log(error, "En_Ds_Branch api");
      });
  };

  showBranchListing = (branchlist, index) => {
    return (
      <Fragment key={index}>
        <div className="bg-shadow b-goods-f col-12 b-box row mx-0 mt-4">
          <div className="col-3 col-lg-2 px-0 mb-3 pr-2">
            <div className="user-title">
              <div className="d-flex justify-content-between px-3">
                <span>Branch Name</span>
              </div>
            </div>
            <div className="user-title">
              <div className="d-flex justify-content-between px-3">
                <span>Contact Person</span>
              </div>
            </div>
            <div className="user-title">
              <div className="d-flex justify-content-between px-3">
                <span>Email</span>
              </div>
            </div>
            <div className="user-title">
              <div className="d-flex justify-content-between px-3">
                <span>Mobile no.</span>
              </div>
            </div>
            <div className="user-title">
              <div className="d-flex justify-content-between px-3">
                <span>Designation</span>
              </div>
            </div>
          </div>
          <div className="col-9 col-lg-8 px-0 mb-3">
            <div className="b-table">
              <div className="d-flex justify-content-between px-3">
                <span>{branchlist.dBranchName}</span>
              </div>
            </div>
            <div className="b-table">
              <div className="d-flex justify-content-between px-3">
                <span>{branchlist.contactPerson}</span>
              </div>
            </div>
            <div className="b-table">
              <div className="d-flex justify-content-between px-3">
                <span>{branchlist.email}</span>
              </div>
            </div>
            <div className="b-table">
              <div className="d-flex justify-content-between px-3">
                <span>{branchlist.contactNumber}</span>
              </div>
            </div>
            <div className="b-table">
              <div className="d-flex justify-content-between px-3">
                <span>{branchlist.designation_Dept}</span>
              </div>
            </div>
          </div>
          <div
            className="col-12 col-lg-2 justify-content-end align-items-center align-self-center"
            style={{ marginBottom: '20px' }}
          >
            <label
            >
              <Switch
                value={branchlist.status === "t" ? true : false}
                onChange={() => this.handleEn_Ds_Branch(branchlist, index)}
                checked={branchlist.status === "t" ? true : false}
                onColor="#eee"
                onHandleColor="#D3E428"
                handleDiameter={20}
                uncheckedIcon={false}
                checkedIcon={false}
                boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                height={20}
                width={40}
                className="react-switch"
                id="material-switch"
              />

              <span
                style={{
                  position: "relative",
                  bottom: "5px",
                  left: "5px"
                }}
              >
                {branchlist.status === "t" ? "Enable" : "Disable"}
              </span>
            </label>
          </div>
          <div
            className="col-12 px-0 mt-2"
            style={{ borderTop: '1px solid #D3E428', paddingTop: '5px' }}
          >
            <div className="ml-2" style={{ color: 'grey', fontSize: '0.9em' }}>
              <span className="ml-2">
                <b>Date created:</b> {this.extractDate(branchlist.createDate)}
              </span>

              <span className="ml-2">
                <b>Status:</b> {branchlist.status === "t" ? 'Active' : 'Inactive'}
              </span>

              <span className="ml-2">
                <b>Views:</b> {branchlist.viewCount}
              </span>
            </div>
          </div>
        </div>
      </Fragment>
    );
  };

  handlePageClick = (e) => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.perPage;

    this.setState(
      {
        currentPage: selectedPage,
        offset: offset,
      },
      () => {
        this.receivedData(this.state.BranchlistingData);
      }
    );
  };

  showLeftPanel = () => {
    return (
      <Fragment>
        <aside className="l-sidebar">
          <div className="widget section-sidebar">
            <div className="widget-content">
              <div className="d-flex justify-content-between px-3">
                <Link
                  style={{ color: "#D3E428", fontWeight: "700" }}
                  to={{
                    pathname: "/create-branch",
                    state: {
                      dealerid: this.state.dealerid,
                      createdby: this.state.createdby
                    },
                  }}
                >
                  Create Branch
                </Link>
              </div>
              <hr />
            </div>
            <div>
              <Dealeradvertisement />
            </div>
          </div>
        </aside>
      </Fragment>
    );
  };

  showBranchListHeader = () => {
    return (
      <div className="row">
        <div className="col-8 px-0">
          <div className="list-heading">Branch List</div>
          <div className="b-filter-goods__info col-auto">
            Showing
            <strong>
              {" "}
              {this.state.start} - {this.state.end}{" "}
            </strong>{" "}
            out of <strong> {this.state.totalResults}</strong> listings
          </div>
        </div>

        <div className="col-4">
          <div
            className="b-filter-goods__wrap col-auto"
            style={{ float: "right", padding: 0 }}
          >
            <div className="b-filter-goods__select d-inline-flex">
              <select
                style={{ inlineSize: "auto" }}
                className="custom-select-adpost"
                onChange={this.handleSortByBranchListing}
                value={this.state.selSortBranchValue}
              >
                <option selected>Sort By</option>
                {this.state.SortBranchListing.map((sortuser) => (
                  <option key={sortuser.id} value={sortuser.method}>
                    {sortuser.value}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
      </div>
    );
  };

  //sortby
  handleSortByBranchListing = (event) => {
    this.setState({ selSortBranchValue: event.target.value });
    let PromoData = [];
    PromoData = this.state.GlobalBranchlistingData;

    if (event.target.value === "new") {
      PromoData = _.orderBy(PromoData, ["CreateDate"], ["desc"]);
    } else if (event.target.value === "old") {
      PromoData = _.orderBy(PromoData, ["CreateDate"], ["asc"]);
    } else {
      console.log("sort error");
    }

    this.setState({ BranchlistingData: PromoData });
    this.receivedData(PromoData);
  };

  render() {
    return (
      <Fragment>
        <div className="row">
          <div className="col-lg-3">{this.showLeftPanel()}</div>

          <div className="col-lg-9">
            {this.showBranchListHeader()}
            <br />

            {this.state.toggleAlertMsg === true ? (
              <ErrorAlert show={this.state.toggleAlertMsg} />
            ) : (
                <Fragment>
                  {this.state.isLoadingForBranchListing ? (
                    <Loader />
                  ) : (
                      <span>
                        {this.state.listingData}
                        <br />
                        <ReactPaginate
                          previousLabel={"prev"}
                          nextLabel={"next"}
                          breakLabel={"..."}
                          breakClassName={"break-me"}
                          pageCount={this.state.pageCount}
                          marginPagesDisplayed={2}
                          pageRangeDisplayed={5}
                          onPageChange={this.handlePageClick}
                          containerClassName={"pagination"}
                          subContainerClassName={"pages pagination"}
                          activeClassName={"active"}
                          forcePage={this.state.currentPage}
                        />
                      </span>
                    )}
                </Fragment>
              )}
          </div>
        </div>
      </Fragment>
    );
  }
}
export default BranchDetails;