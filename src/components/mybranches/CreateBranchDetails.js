import React, { Component, Fragment } from "react";
import { apiBaseURL, apiUrlcity } from "../../ApiBaseURL";
import axios from "axios";
import { Link } from "react-router-dom";
import "react-datepicker/dist/react-datepicker.css";
import Dealeradvertisement from "../../pages/Dealeradvertisement";
import { Modal } from "react-bootstrap";

class CreateBranchDetails extends Component {
  constructor(props) {
    super(props);
    this.branchid = "";
    this.userid = "";
    this.state = {
      userData: [],
      branchname: "",
      personname: "",
      email: "",
      contact: "",
      designation: '',
      //city
      lcity: [],
      scity: "",
      officeno: "",
      address1: '',
      address2: '',
      showMessage: '',
      showModal: false,
      dealerid: ''
    };
  }

  componentWillMount() {
    if (this.props.location.state !== undefined) {
      const dealerid = this.props.location.state.dealerid;
      this.setState({
        dealerid
      });
    } else {
      this.props.history.push('my-branches');
    }
  }

  componentDidMount() {
    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      this.userid = parseUserData[0].UserID;
      this.branchid = parseUserData[0].BranchID;
    }

    //city
    fetch(apiUrlcity)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lcity: result
            });
          } else {
            console.log("city api");
          }
        },
        error => {
          this.setState({ error });
        }
      );
  }

  handleBranchNameChange = event => {
    this.setState({ branchname: event.target.value });
  };

  handlePersonNameChange = event => {
    this.setState({ personname: event.target.value });
  };

  handlEmailChange = event => {
    this.setState({ email: event.target.value });
  };

  handleContactChange = event => {
    this.setState({ contact: event.target.value });
  };

  handleDesignationChange = event => {
    this.setState({ designation: event.target.value });
  };

  handleCityChange = event => {
    if (event.target.value === "") {
      return;
    }

    this.setState({ scity: event.target.value });
  };

  handleOfficeNumberChange = event => {
    this.setState({ officeno: event.target.value });
  };

  handleAddress1Change = event => {
    this.setState({ address1: event.target.value });
  };

  handleAddress2Change = event => {
    this.setState({ address2: event.target.value });
  };

  handleCreateBranch = event => {
    event.preventDefault();

    const { dealerid, createdby, branchname, personname, email, contact, designation, scity, officeno, address1, address2 } = this.state;
    console.log(dealerid, branchname, createdby, personname, email, contact, designation, scity, officeno, address1, address2);

    const url = `${apiBaseURL}/DBranch`;
    let ApiParamForCreateBranch = {
      dealerID: dealerid,
      cityID: scity,
      viewCount: 0,
      createdBy: createdby,
      dBranchName: branchname,
      contactPerson: personname,
      contactNumber: contact,
      designation_Dept: designation,
      email: email,
      officeNumber: officeno,
      addressLine1: address1,
      addressLine2: address2,
      status: "t"
    };
    axios
      .post(url, ApiParamForCreateBranch)
      .then(res => {
        console.log(res, "DBranch api");

        if (res.status === 200) {
          this.setState({
            status: res.status,
            showModal: true,
            showMessage: 'Branch Created Successfully...',
          })
        } else {
          console.log(res.status, "statuscode from DBranch api");
        }
      })
      .catch(error => {
        this.setState({
          showModal: true,
          showMessage: 'Something Went Wrong...',
        });
        console.log(error, "DBranch api");
      });
  };

  showCreateBranch = () => {
    return (
      <div className="row">
        <div className="col-12 px-0">
          <div className="list-heading">Create Branch</div>
        </div>

        <form onSubmit={this.handleCreateBranch} style={{ width: "100%" }}>
          <div className="col-12">
            <div className="mt-0">
              <div className="">&nbsp;</div>

              <div className="row">
                <div className="col-6">
                  <ul id="signup" className="logmod__tabs">
                    <li>
                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>Branch Name</label>
                          <input
                            placeholder="Enter..."
                            required
                            className="form-control address ml-3"
                            type="text"
                            onChange={this.handleBranchNameChange}
                            value={this.state.branchname}
                          />
                        </div>
                      </div>

                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>Person Name</label>
                          <input
                            placeholder="Enter..."
                            required
                            className="form-control address ml-3"
                            type="text"
                            onChange={this.handlePersonNameChange}
                            value={this.state.personname}
                          />
                        </div>
                      </div>
                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>Email</label>
                          <input
                            placeholder="Enter..."
                            required
                            className="form-control address ml-3"
                            type="email"
                            onChange={this.handlEmailChange}
                            value={this.state.email}
                          />
                        </div>
                      </div>

                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>Contact Number</label>
                          <input
                            placeholder="Enter..."
                            required
                            className="form-control address ml-3"
                            type="text"
                            onChange={this.handleContactChange}
                            value={this.state.contact}
                          />
                        </div>
                      </div>

                      <div className="sminputs">
                        <div className="input" style={{ width: "500px" }}>
                          <label>Designation Dept</label>
                          <input
                            placeholder="Enter..."
                            required
                            className="form-control address ml-3"
                            type="text"
                            onChange={this.handleDesignationChange}
                            value={this.state.designation}
                          />
                        </div>
                      </div>

                    </li>
                  </ul>
                </div>
                <div className="col-6">
                  <ul className="logmod__tabs">
                    <li>

                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>Office Number</label>
                          <input
                            placeholder="Enter..."
                            required
                            className="form-control address ml-3"
                            type="number"
                            onChange={this.handleOfficeNumberChange}
                            value={this.state.officeno}
                          />
                        </div>
                      </div>

                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>City</label>
                          <select
                            required
                            style={{
                              width: "100%",
                              background: "none",
                              border: "none"
                            }}
                            onChange={this.handleCityChange}
                            value={this.state.scity}
                          >
                            <option value="" selected>
                              Select City
                            </option>
                            {this.state.lcity.map(city => (
                              <option key={city.CityID} value={city.CityID}>
                                {city.CityName}
                              </option>
                            ))}
                          </select>
                        </div>
                      </div>

                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>Address Line 1</label>
                          <input
                            placeholder="Enter..."
                            required
                            className="form-control address ml-3"
                            type="text"
                            onChange={this.handleAddress1Change}
                            value={this.state.address1}
                          />
                        </div>
                      </div>

                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>Address Line 2</label>
                          <input
                            placeholder="Enter..."
                            required
                            className="form-control address ml-3"
                            type="text"
                            onChange={this.handleAddress2Change}
                            value={this.state.address2}
                          />
                        </div>
                      </div>

                    </li>
                  </ul>
                </div>
              </div>
              <ul className="mb-5">
                <li>
                  <div className="simform__actions">
                    <input
                      className="sumbit btn btn-block mt-3"
                      type="submit"
                      value="Create Branch"
                    />
                  </div>
                </li>
              </ul>
            </div>
          </div>
          ;
        </form>
      </div>
    );
  };

  showLeftPanel = () => {
    return (
      <Fragment>
        <aside className="l-sidebar">
          <div className="widget section-sidebar">
            <div className="widget-content">
              <div className="d-flex justify-content-between px-3">
                <Link
                  style={{ color: "#D3E428", fontWeight: "700" }}
                  to="/my-branches"
                >
                  View Branch List
                </Link>
              </div>
              <hr />
            </div>
            <div>
              <Dealeradvertisement />
            </div>
          </div>
        </aside>
      </Fragment>
    );
  };

  handleCloseModal = () => {
    if (this.state.status === 200) {
      this.setState({
        showModal: false
      });
      this.props.history.push("/my-branches");
    } else {
      this.setState({
        showModal: false
      });
    }
  }
  showModal = () => {
    return (
      <Modal show={this.state.showModal} onHide={this.handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Cararabiya</Modal.Title>
        </Modal.Header>
        <Modal.Body>{this.state.showMessage}</Modal.Body>
      </Modal>
    );
  }
  render() {
    return (
      <Fragment>
        {this.showModal()}
        <div className="row">
          <div className="col-lg-3">{this.showLeftPanel()}</div>

          <div className="col-lg-9">{this.showCreateBranch()}</div>
        </div>
      </Fragment>
    );
  }
}
export default CreateBranchDetails;