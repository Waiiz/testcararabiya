import BranchDetails from './BranchDetails';
import CreateBranchDetails from './CreateBranchDetails';

export {
    BranchDetails, CreateBranchDetails
};