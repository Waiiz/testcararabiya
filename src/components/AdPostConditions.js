import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { MyContext } from "../Context";
import axios from "axios";
import { apiBaseURL } from "../ApiBaseURL";
import { Modal } from "react-bootstrap";

class AdPostConditions extends Component {
  static contextType = MyContext;
  constructor(props) {
    super(props);
    this.ispackageloading = false;
    this.state = {
      userid: '',
      isLoadingGetPackageAdPostbyUserID: false,
      //package
      PackagesData: [],
      showModal: false,
      userpackage: "",

      UserPackageAdPostData: [],
    }
  }

  handlePlaceAnAd = () => {
    let userdata = localStorage.getItem("user")
    if (userdata === null) {
      this.context.handleChildOpenPopover(true);
    } else {
      this.loadGetPackageAdPostbyUserID(userdata);
    }
  };

  loadGetPackageAdPostbyUserID = (userdata) => {
    let parseuserdata = JSON.parse(userdata);
    this.setState({ isLoadingGetPackageAdPostbyUserID: true, userid: parseuserdata[0].UserID });
    const urlgetpackagebyuserid = `${apiBaseURL}/UserPackageAdPost/GetPackageAdPostbyUserID?UserID=${parseuserdata[0].UserID}`;
    axios
      .get(urlgetpackagebyuserid)
      .then(res => {
        this.setState({ isLoadingGetPackageAdPostbyUserID: false });
        if (res.data.length > 0) {
          //remaining condition check
          if (res.data[0]['RemainingAds'] === 0) {
            this.handleOpenModal();
          } else {
            this.props.props.history.push("/vehicle-category");
          }
        } else {
          //open popup
          this.handleOpenModal();
        }
      })
      .catch(error => {
        this.setState({
          isLoadingGetPackageAdPostbyUserID: false
        });
        console.log(error, "from GetPackageAdPostbyUserID api");
      });
  };

  // package work modal
  handleOpenModal = () => {
    if (this.ispackageloading) {
      return;
    }
    this.ispackageloading = true;
    const url = `${apiBaseURL}/Package`;
    axios
      .get(url)
      .then((res) => {
        if (res.data.length > 0) {
          this.ispackageloading = false;
          this.setState({
            PackagesData: res.data,
            showModal: true,
          });
        } else {
          this.ispackageloading = false;
          console.log(res.data, "Packages api");
        }
      })
      .catch((error) => {
        this.ispackageloading = false;
        console.log(error, "Packages api");
      });
  };

  handleCloseModal = () => {
    this.setState({
      showModal: false,
    });
  };

  showModal = () => {
    return (
      <Modal
        show={this.state.showModal}
        size="lg"
        centered
        onHide={this.handleCloseModal}
      >
        <div className="modal-body">
          <section className="pricing-area pb-50" id="pricing">
            <div className="container">
              <div className="row">
                <div className="col-xl-8 mx-auto text-center">
                  <div className="section-title">
                    <h2>select package</h2>
                  </div>
                </div>
              </div>

              <div className="row justify-content-center">
                {this.state.PackagesData.map((packages, index) => (
                  <div key={index} className="col-xl-4">
                    <div className="single-price bg-shadow">
                      <div className="price-title">
                        <h4>{packages.PackageName}</h4>
                      </div>
                      <div className="price-tag">
                        <h2>
                          {packages.Cost === "0" ? (
                            "FREE"
                          ) : (
                              <Fragment>
                                <span
                                  style={{ fontSize: "20px", fontWeight: "600" }}
                                >
                                  SAR
                              </span>{" "}
                                {packages.Cost} <span>/ AD</span>
                              </Fragment>
                            )}
                        </h2>
                      </div>
                      <div className="price-item">
                        <ul>
                          <li>
                            <i
                              className="fas fa-check pr-1"
                              style={{ color: "#47c77e" }}
                            ></i>
                            Upto{" "}
                            {packages.NumOfImages === -1
                              ? "Unlimited"
                              : packages.NumOfImages}{" "}
                            photos
                          </li>
                          <li>
                            <i
                              className="fas fa-check pr-1"
                              style={{ color: "#47c77e" }}
                            ></i>
                            {packages.NumOfDays} Days of Run time
                          </li>
                          <li>
                            <i
                              className="fas fa-check pr-1"
                              style={{ color: "#47c77e" }}
                            ></i>
                            Allow{" "}
                            {packages.NumOfAds === 1 ? (
                              <Fragment>
                                {packages.NumOfAds} Ad to
                                <br />
                                posting
                              </Fragment>
                            ) : (
                                <Fragment>
                                  {packages.NumOfAds} Ads to
                                <br />
                                  posting
                              </Fragment>
                              )}
                          </li>
                          <li>
                            <i
                              className={
                                packages.Featured === false
                                  ? "fas fa-times pr-1"
                                  : "fas fa-check pr-1"
                              }
                              style={{
                                color:
                                  packages.Featured === false
                                    ? "red"
                                    : "#47c77e",
                              }}
                            ></i>
                            Featured Listing
                          </li>
                          <li>
                            <i
                              className={
                                packages.NumOfVideos === 0
                                  ? "fas fa-times pr-1"
                                  : "fas fa-check pr-1"
                              }
                              style={{
                                color:
                                  packages.NumOfVideos === 0
                                    ? "red"
                                    : "#47c77e",
                              }}
                            ></i>
                            Allow{" "}
                            {packages.NumOfVideos === 0
                              ? ""
                              : packages.NumOfVideos === -1
                                ? "Unlimited"
                                : packages.NumOfVideos}{" "}
                            Videos
                          </li>
                        </ul>
                      </div>
                      {this.state.userpackage >= packages.Cost ? (
                        <Link
                          style={{ cursor: "auto", color: "black" }}
                          className="box-btn"
                        >
                          SELECTED
                        </Link>
                      ) : (
                          <Link
                            onClick={() =>
                              this.handleUpdatePackage(packages.PackageID)
                            }
                            style={{ cursor: "pointer", color: "white" }}
                            className="box-btn"
                          >
                            SELECT PLAN
                        </Link>
                        )}
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </section>
        </div>
      </Modal>
    );
  };

  handleUpdatePackage = (packageid) => {
    const url = `${apiBaseURL}/UserPackageAdpost/GetUpdatedPackagebyUserIDPackageID?UserID=${this.state.userid}&PackageID=${packageid}`;
    axios
      .get(url)
      .then((res) => {
        console.log(res, "res from ApiParamForUpdatePackage api");
        if (res.status === 200) {
          this.setState({
            UserPackageAdPostData: res.data,
            userpackage: res.data[0].Cost,
            showModal: false,
          });
          this.context.handleChildOpenModal(true, 'Congratulation! YOUR PACKAGE HAS BEEN UPGRADED');
          this.props.props.history.push("/vehicle-category");
          console.log(res.status, "ApiParamForUpdatePackage");
        } else {
          this.setState({ showModal: false });
          this.context.handleChildOpenModal(true, 'Something Went Wrong...');
        }
      })
      .catch((error) => {
        this.setState({ showModal: false });
        this.context.handleChildOpenModal(true, 'Something Went Wrong...');
        console.log(error, "from ApiParamForUpdatePackage api");
      });
  };

  render() {
    return (
      <Fragment>
        {this.showModal()}
        <div id={this.props.classname} onClick={this.handlePlaceAnAd}>
          <Link className={this.props.classname}>Place an Ad</Link>
        </div>
      </Fragment>
    );
  }
}
AdPostConditions.contextType = MyContext;
export default AdPostConditions;