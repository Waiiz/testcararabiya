import React, { Component, Fragment } from "react";
import { apiBaseURL } from "../../ApiBaseURL";
import axios from "axios";
import ReactPaginate from "react-paginate";
import "../listing/paginate.css";
import _ from "lodash";
import { Link } from "react-router-dom";
import Switch from "react-switch";
import moment from "moment";
import Loader from "../../pages/Loader";
import ErrorAlert from "../../pages/ErrorAlert";

class AdsDetail extends Component {
  constructor(props) {
    super(props);
    this.isTotalMyAdsCountLoading = false;
    this.isFetchingDataforEnableAdPost = false;
    this.isFetchingDataforIsSoldAdPost = false;

    this.simpleWatermarkImg = "./assets/media/watermark.png";
    this.soldWatermarkImg = "./assets/media/watermark-sold.png";
    this.disabledWatermarkImg = "./assets/media/watermark-disabled.png";

    this.state = {
      isLoadingForListingAdPostData: false,
      totalMyAdsCountData: [],
      ListingAdPostData: [],
      GlobalListingAdPostData: [],
      //pagination
      offset: 0,
      perPage: 7,
      currentPage: 0,
      listingData: [],
      toggleAlertMsg: false,

      start: "",
      end: "",
      totalResults: "",

      //filterby
      FilterAdPostListing: [
        { id: 1, value: "Active Ads", method: "active" },
        { id: 2, value: "InActive Ads", method: "inactive" },
        { id: 3, value: "Sold Ads", method: "sold" },
        { id: 4, value: "UnSold Ads", method: "unsold" },
      ],
      selFilterAdPostValue: "",

      //sortby
      SortAdPostListing: [
        { id: 1, value: "Newest to Oldest", method: "new" },
        { id: 2, value: "Oldest to Newest", method: "old" },
        { id: 3, value: "Price Highest to Lowest", method: "high" },
        { id: 4, value: "Price Lowest to Highest", method: "low" },
      ],
      selSortAdPostValue: "",
    };
  }
  componentDidMount() {
    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      let userid = parseUserData[0].UserID;
      this.setState({
        userId: parseUserData[0].UserID,
      });

      this.setState({ isLoadingForListingAdPostData: true });
      const url = `${apiBaseURL}/Listing/ListingAdPostByUserID?UserID=${userid}`;
      axios
        .get(url)
        .then((res) => {
          this.setState({
            GlobalListingAdPostData: res.data,
          });
          const reverseListingAdPost = res.data;
          this.setState({ isLoadingForListingAdPostData: false });
          this.receivedData(reverseListingAdPost);
        })
        .catch((error) => {
          this.setState({
            start: 0,
            end: 0,
            offset: 0,
            perPage: 7,
            currentPage: 0,
            totalResults: 0,
            toggleAlertMsg: true,
            listingData: [],
            isLoadingForListingAdPostData: false,
          });
          console.log(error, "from ListingAdPostByUserID api");
        });

      this.getTotalMyAdsCountById(userid);
    }
  }

  getTotalMyAdsCountById = (userid) => {
    if (this.isTotalMyAdsCountLoading) {
      return;
    }
    this.isTotalMyAdsCountLoading = true;

    const url = `${apiBaseURL}/TotalMyAdsCount/TotalMyAdsCount?UserID=${userid}`;
    axios
      .get(url)
      .then((res) => {
        if (res.data.length > 0) {
          this.isTotalMyAdsCountLoading = false;
          this.setState({
            totalMyAdsCountData: res.data,
          });
        } else {
          console.log(res.data, "res.data");
        }
      })
      .catch((error) => {
        this.isTotalMyAdsCountLoading = false;
        console.log(error, "TotalMyAdsCount api");
      });
  };

  numberWithCommas = (price) => {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  formattedDate = (datetime) => {
    let splitTimeDateWithT = datetime.split("T");
    let date = splitTimeDateWithT[0]; //T00:00:00
    let formatteddate = moment(date).format("D MMMM YYYY");
    return formatteddate;
  };

  //handle Change Enable IsActive  listadpost.IsActive,listadpost.AdPostID
  handleUpdateIsActivebyAdpostID = (listadpost, index) => {
    var listingCheckedEnabled = !listadpost.IsActive;
    listadpost.IsActive = listingCheckedEnabled;

    if (!this.isFetchingDataforEnableAdPost) {
      this.UpdateIsActivebyAdpostID(listadpost, index);
    }
  };

  UpdateIsActivebyAdpostID = (listadpost, index) => {
    this.isFetchingDataforEnableAdPost = true;

    const url = `${apiBaseURL}/Adpost/UpdateIsActivebyAdpostID`;
    let ApiParamForIsActiveAdPost = {
      AdpostID: listadpost.AdPostID,
      IsActive: listadpost.IsActive,
    };
    axios
      .post(url, null, { params: ApiParamForIsActiveAdPost })
      .then((res) => {
        this.isFetchingDataforEnableAdPost = false;
        console.log(res.data, "UpdateIsActivebyAdpostID api");

        if (res.status === 200) {
          var myListingAdPostData = this.state.ListingAdPostData;

          var getIndex = myListingAdPostData.indexOf(
            myListingAdPostData[index]
          );
          if (getIndex > -1) {
            myListingAdPostData.splice(getIndex, 1, listadpost);
          }

          this.setState({
            ListingAdPostData: myListingAdPostData,
          });

          this.receivedData(this.state.ListingAdPostData);
          this.getTotalMyAdsCountById(this.state.userId);
        } else {
          this.isFetchingDataforEnableAdPost = false;
          console.log(
            res.status,
            "statuscode from UpdateIsActivebyAdpostID api"
          );
        }
      })
      .catch((error) => {
        this.isFetchingDataforEnableAdPost = false;
        console.log(error, "UpdateIsActivebyAdpostID api");
      });
  };

  //handle Change Enable IsSold
  handleUpdateIsSoldbyAdpostID = (listadpost, index) => {
    var listingCheckedIsSold = !listadpost.IsSold;
    listadpost.IsSold = listingCheckedIsSold;

    if (!this.isFetchingDataforIsSoldAdPost) {
      this.UpdateIsSoldbyAdpostID(listadpost, index);
    }
  };

  UpdateIsSoldbyAdpostID = (listadpost, index) => {
    this.isFetchingDataforIsSoldAdPost = true;

    const url = `${apiBaseURL}/Adpost/UpdateIsSoldbyAdpostID`;
    let ApiParamForIsSoldAdPost = {
      AdpostID: listadpost.AdPostID,
      IsSold: listadpost.IsSold,
    };
    axios
      .post(url, null, { params: ApiParamForIsSoldAdPost })
      .then((res) => {
        this.isFetchingDataforIsSoldAdPost = false;
        console.log(res.data, "UpdateIsSoldbyAdpostID api");

        if (res.status === 200) {
          var myListingAdPostData = this.state.ListingAdPostData;

          var getIndex = myListingAdPostData.indexOf(
            myListingAdPostData[index]
          );
          if (getIndex > -1) {
            myListingAdPostData.splice(getIndex, 1, listadpost);
          }

          this.setState({
            ListingAdPostData: myListingAdPostData,
          });

          this.receivedData(this.state.ListingAdPostData);
        } else {
          this.isFetchingDataforIsSoldAdPost = false;
          console.log(res.status, "statuscode from UpdateIsSoldbyAdpostID api");
        }
      })
      .catch((error) => {
        this.isFetchingDataforIsSoldAdPost = false;
        console.log(error, "UpdateIsSoldbyAdpostID api");
      });
  };

  showWatermark = (listadpostisactive, listadpostissold) => {
    if (listadpostissold) {
      return this.soldWatermarkImg;
    } else if (listadpostisactive) {
      return this.simpleWatermarkImg;
    } else {
      return this.disabledWatermarkImg;
    }
  };

  showStatus = (listadpostisactive, listadpostissold) => {
    if (listadpostissold) {
      return "Sold";
    } else if (listadpostisactive) {
      return "Active";
    } else {
      return "Inactive";
    }
  };

  receivedData = (listingAdPostData) => {
    if (listingAdPostData.length > 0) {
      this.setState({
        ListingAdPostData: listingAdPostData,
        totalResults: listingAdPostData.length,
        toggleAlertMsg: false,
      });
      const data = listingAdPostData;

      if (
        this.state.offset + 1 + this.state.perPage >
        this.state.totalResults
      ) {
        this.setState({
          end: this.state.totalResults,
        });
      } else {
        this.setState({
          end: this.state.offset + this.state.perPage,
        });
      }

      const slice = data.slice(
        this.state.offset,
        this.state.offset + this.state.perPage
      );

      const listingData = slice.map((listadpost, index) =>
        this.showAdPostListingData(listadpost, index)
      );

      this.setState({
        pageCount: Math.ceil(data.length / this.state.perPage),
        start: this.state.offset + 1,
        listingData,
      });
    } else {
      this.setState({
        start: 0,
        end: 0,
        offset: 0,
        perPage: 7,
        currentPage: 0,
        totalResults: 0,
        toggleAlertMsg: true,
        listingData: [],
      });
    }
  };

  showAdPostListingData = (listadpost, index) => {
    return (
      <div
        key={index}
        className="b-goods-f col-12 b-goods-f_row row mx-0 mb-4 bg-shadow"
      >
        <div className="col-lg-3 col-md-3 col-sm-12">
          <div
            className="b-goods-f__listing mb-2"
            style={{ width: "164px", height: "121px" }}
          >
            <Link to="">
              <span className="b-goods-f__sold-inner pb-3">
                <img
                  src={this.showWatermark(
                    listadpost.IsActive,
                    listadpost.IsSold
                  )}
                  alt=""
                  width="183"
                  height="123"
                />
              </span>

              <img
                className="b-goods-f__img img-scale"
                src={listadpost.ImageUrl}
                alt=""
                width="164"
                height="121"
              />
            </Link>
          </div>
          <span style={{ fontSize: "0.9em" }} className="pt-5">
            <i className="fas fa-map-marker-alt"></i> {listadpost.Area}
          </span>
        </div>

        <div className="col-lg-5 col-md-5 col-sm-12">
          <div className="b-goods-f__main">
            <div className="b-goods-f__descrip">
              <div className="b-goods-f__title_myad">
                <Link to="">{listadpost.Vehicle} for Sale</Link>
              </div>
              <div className="b-goods-f__info_myad">
                <strong>
                  {listadpost.Price === 0 ? 'Call For Price' : listadpost.CurCode +' '+ this.numberWithCommas(listadpost.Price)}
                </strong>
              </div>
              <ul className="b-goods-f__list list-unstyled">
                <li className="b-goods-f__list-item">
                  <span className="b-goods-f__list-title">Mileage </span>
                  <span
                    className="b-goods-f__list-info"
                    style={{ paddingLeft: "45px" }}
                  >
                    : {listadpost.Mileage} km
                  </span>
                </li>
                <li className="b-goods-f__list-item">
                  <span className="b-goods-f__list-title">Model </span>
                  <span
                    className="b-goods-f__list-info"
                    style={{ paddingLeft: "55px" }}
                  >
                    : {listadpost.ModelName}
                  </span>
                </li>
                <li className="b-goods-f__list-item">
                  <span className="b-goods-f__list-title">Transmission </span>
                  <span
                    className="b-goods-f__list-info"
                    style={{ paddingLeft: "11px" }}
                  >
                    : {listadpost.TransmissionName}
                  </span>
                </li>
                <li className="b-goods-f__list-item">
                  <span className="b-goods-f__list-title">Fuel Type </span>
                  <span
                    className="b-goods-f__list-info"
                    style={{ paddingLeft: "34px" }}
                  >
                    : {listadpost.FuelTypeName}
                  </span>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-4">
          <div className="b-goods-f__sidebar">
            <span className="b-goods-f__price-group">
              <span className="b-goods-f__price d-flex px-4">
                <div className="mr-2" style={{ textAlign: "center" }}>
                  <i
                    style={{ color: "#D3E428" }}
                    className="fas fa-eye px-4"
                  ></i>
                  <strong>
                    <span style={{ fontSize: "12px" }}>
                      {listadpost.Counter}
                    </span>{" "}
                    <span style={{ fontSize: "11px" }}> views</span>
                  </strong>
                </div>
                <div className="mr-2" style={{ textAlign: "center" }}>
                  <i
                    style={{ color: "#D3E428" }}
                    className="fas fa-envelope px-4"
                  ></i>
                  <strong>
                    <span style={{ fontSize: "12px" }}>
                      {listadpost.EmailCounter}
                    </span>{" "}
                    <span style={{ fontSize: "11px" }}>chats</span>
                  </strong>
                </div>
                <div style={{ textAlign: "center" }}>
                  <i
                    style={{ color: "#D3E428" }}
                    className="fas fa-phone px-4"
                  ></i>
                  <strong>
                    <span style={{ fontSize: "12px" }}>
                      {listadpost.CallCounter}
                    </span>{" "}
                    <span style={{ fontSize: "11px" }}>phones</span>
                  </strong>
                </div>
              </span>
              <br />
              <div className="d-flex" style={{ paddingLeft: "2rem" }}>
                <div>
               
                  <label>
                    <Switch
                      value={listadpost.IsActive}
                      onChange={() =>
                        this.handleUpdateIsActivebyAdpostID(listadpost, index)
                      }
                      checked={listadpost.IsActive}
                      onColor="#eee"
                      onHandleColor="#D3E428"
                      handleDiameter={20}
                      uncheckedIcon={false}
                      checkedIcon={false}
                      boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                      activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                      height={20}
                      width={40}
                      className="react-switch"
                      id="material-switch"
                    />

                    <span
                      style={{
                        position: "relative",
                        bottom: "5px",
                        left: "5px",
                      }}
                    >
                      Enable
                    </span>
                  </label>
                </div>

                <div style={{ paddingLeft: "2rem" }}>
                  <label>
                    <Switch
                      value={listadpost.IsSold}
                      onChange={() =>
                        this.handleUpdateIsSoldbyAdpostID(listadpost, index)
                      }
                      checked={listadpost.IsSold}
                      onColor="#eee"
                      onHandleColor="#D3E428"
                      handleDiameter={20}
                      uncheckedIcon={false}
                      checkedIcon={false}
                      boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                      activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                      height={20}
                      width={40}
                      className="react-switch"
                      id="material-switch"
                    />

                    <span
                      style={{
                        position: "relative",
                        bottom: "5px",
                        left: "5px",
                      }}
                    >
                      Sold
                    </span>
                  </label>
                </div>
              </div>
            </span>
          </div>
        </div>

        <div className="col-12 my-2">
          <div className="row">
            <div className="savesearch">
              <span className="mr-1"></span>
              <span>
                {listadpost.IsSold ? (
                  ""
                ) : (
                  <Link
                    to={{
                      pathname: "/editpost-ad",
                      state: { AdPostID: listadpost.AdPostID },
                    }}
                    className="notDisabled"
                  >
                    Edit Listing
                  </Link>
                )}
              </span>
            </div>

            <div
              style={{
                position: "absolute",
                right: "0px",
                color: "grey",
                fontSize: "0.9em",
              }}
              className="mr-2"
            >
              <span className="ml-2">
                <b>Ad placed:</b> {this.formattedDate(listadpost.CreateDate)}
              </span>
              <span className="ml-2">
                <b>Ad expiry:</b> {listadpost.DateExpiry}
              </span>
              <span className="ml-2">
                <b>Package:</b> {listadpost.PackageName}
              </span>
              <span className="ml-2">
                <b>Status:</b>{" "}
                {this.showStatus(listadpost.IsActive, listadpost.IsSold)}
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  };

  handlePageClick = (e) => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.perPage;

    this.setState(
      {
        currentPage: selectedPage,
        offset: offset,
      },
      () => {
        this.receivedData(this.state.ListingAdPostData);
      }
    );
  };

  //filterby
  handleFilterByAdPostListing = (event) => {
    this.setState({ selFilterAdPostValue: event.target.value });
    let adpostData = [];
    adpostData = this.state.GlobalListingAdPostData;

    if (event.target.value === "active") {
      adpostData = _.filter(
        adpostData,
        (adpostlisting) => adpostlisting.IsActive === true
      );
    } else if (event.target.value === "inactive") {
      adpostData = _.filter(
        adpostData,
        (adpostlisting) => adpostlisting.IsActive === false
      );
    } else if (event.target.value === "sold") {
      adpostData = _.filter(
        adpostData,
        (adpostlisting) => adpostlisting.IsSold === true
      );
    } else if (event.target.value === "unsold") {
      adpostData = _.filter(
        adpostData,
        (adpostlisting) => adpostlisting.IsSold === false
      );
    } else {
      console.log("filter error");
    }

    this.setState({ ListingAdPostData: adpostData });
    this.receivedData(adpostData);
  };

  //sortby
  handleSortByAdPostListing = (event) => {
    this.setState({ selSortAdPostValue: event.target.value });
    let adpostData = [];
    adpostData = this.state.GlobalListingAdPostData;

    if (event.target.value === "new") {
      adpostData = _.orderBy(adpostData, ["CreateDate"], ["desc"]);
    } else if (event.target.value === "old") {
      adpostData = _.orderBy(adpostData, ["CreateDate"], ["asc"]);
    } else if (event.target.value === "high") {
      adpostData = _.orderBy(adpostData, ["Price"], ["desc"]);
    } else if (event.target.value === "low") {
      adpostData = _.orderBy(adpostData, ["Price"], ["asc"]);
    } else {
      console.log("filter error");
    }

    this.setState({ ListingAdPostData: adpostData });
    this.receivedData(adpostData);
  };

  showMyAdsHeader = () => {
    return (
      <div className="row">
        <div className="col-6 px-0">
          <div className="list-heading">My Ads</div>
          <div className="b-filter-goods__info col-auto">
            Showing
            <strong>
              {" "}
              {this.state.start} - {this.state.end}{" "}
            </strong>{" "}
            out of <strong> {this.state.totalResults}</strong> listings
          </div>
        </div>

        <div className="col-6 px-5">
          <div className="b-filter-goods__wrap col-auto">
            <div className="b-filter-goods__select d-inline-flex">
              <select
                style={{ inlineSize: "auto" }}
                className="custom-select-adpost"
                onChange={this.handleFilterByAdPostListing}
                value={this.state.selFilterAdPostValue}
              >
                <option selected>Filter By</option>
                {this.state.FilterAdPostListing.map((filteradpost) => (
                  <option key={filteradpost.id} value={filteradpost.method}>
                    {filteradpost.value}
                  </option>
                ))}
              </select>

              <select
                style={{ inlineSize: "auto" }}
                className="custom-select-adpost"
                onChange={this.handleSortByAdPostListing}
                value={this.state.selSortAdPostValue}
              >
                <option selected>Sort By</option>
                {this.state.SortAdPostListing.map((sortadpost) => (
                  <option key={sortadpost.id} value={sortadpost.method}>
                    {sortadpost.value}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
      </div>
    );
  };

  handleDraftListingRoute = () => {
    this.props.history.push({
      pathname: "/draft-listing",
      //state: { editprofile: 'editprofile' }
    });
  };
  showMyAdsSummary = () => {
    return (
      <Fragment>
        {this.state.totalMyAdsCountData.map((totalmyads, index) => (
          <aside key={index} className="l-sidebar">
            <div className="widget section-sidebar">
              <div className="widget-content">
                <div className="d-flex justify-content-between px-3">
                  <strong>Active</strong>
                  <span>{totalmyads.TotalMyAdsActive}</span>
                </div>
                <hr />
                <div
                  onClick={this.handleDraftListingRoute}
                  style={{ cursor: "pointer" }}
                  className="d-flex justify-content-between  px-3"
                >
                  <strong className="dealer-event">Draft</strong>
                  <i className="fa fa-caret-right pt-1"></i>
                  <span>{totalmyads.TotalMyAdsDraft}</span>
                </div>
                <hr />
                <div className="d-flex justify-content-between px-3">
                  <strong>Inactive</strong>
                  <span>{totalmyads.TotalMyAdsInactive}</span>
                </div>
                <hr />
                <div className="d-flex justify-content-between px-3">
                  <strong>Expired</strong>
                  <span>{totalmyads.TotalMyAdsExpired}</span>
                </div>
                <hr />
                <div className="d-flex justify-content-between px-3">
                  <strong>Expiring in 15 days</strong>
                  <span>{totalmyads.TotalMyAdsExpiring15}</span>
                </div>
                <hr />{" "}
                <div className="d-flex justify-content-between px-3">
                  <strong>Expiring in 30 days</strong>
                  <span>{totalmyads.TotalMyAdsExpiring30}</span>
                </div>
                <hr />
              </div>
            </div>
          </aside>
        ))}
      </Fragment>
    );
  };

  render() {
    return (
      <Fragment>
        <div className="row">
          <div className="col-lg-3">{this.showMyAdsSummary()}</div>

          <div className="col-lg-9">
            {this.showMyAdsHeader()}
            <br />
            {this.state.toggleAlertMsg === true ? (
              <ErrorAlert show={this.state.toggleAlertMsg} />
            ) : (
              <Fragment>
                {this.state.isLoadingForListingAdPostData ? (
                  <Loader />
                ) : (
                  <span>
                    {this.state.listingData}

                    <ReactPaginate
                      previousLabel={"prev"}
                      nextLabel={"next"}
                      breakLabel={"..."}
                      breakClassName={"break-me"}
                      pageCount={this.state.pageCount}
                      marginPagesDisplayed={2}
                      pageRangeDisplayed={5}
                      onPageChange={this.handlePageClick}
                      containerClassName={"pagination"}
                      subContainerClassName={"pages pagination"}
                      activeClassName={"active"}
                      forcePage={this.state.currentPage}
                    />
                  </span>
                )}
              </Fragment>
            )}
          </div>
        </div>
      </Fragment>
    );
  }
}
export default AdsDetail;