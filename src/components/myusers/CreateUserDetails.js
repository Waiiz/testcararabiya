import React, { Component, Fragment } from "react";
import { apiBaseURL, apiUrlcity } from "../../ApiBaseURL";
import axios from "axios";
import { Link } from "react-router-dom";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Dealeradvertisement from "../../pages/Dealeradvertisement";
import { Modal } from "react-bootstrap";

class CreateUserDetails extends Component {
  constructor(props) {
    super(props);
    this.isTotalAdminSummaryLoading = false;
    this.branchid = "";
    this.userid = "";
    this.isUserDataLoading = false;
    this.state = {
      TotalAdminSummaryData: [],
      userData: [],
      //gender
      Gender: [
        { id: 1, value: "Male", icon: "fas fa-male fa-2x" },
        { id: 2, value: "Female", icon: "fas fa-female fa-2x" }
      ],
      selGender: "Male",
      //userrole
      UserRole: [
        { id: 1, value: "Branch Admin" },
        { id: 3, value: "Data Entry Operator" }
      ],
      selRole: 1,
      firstname: "",
      lastname: "",
      email: "",
      cnic: "",
      firstStartDate: null,
      startDate: null,
      //city
      lcity: [],
      scity: "",
      mobile: "",
      registerid: '',
      isuserregester: false,
      showMessage: '',
      showModal: false,
    };
  }

  componentDidMount() {
    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      this.userid = parseUserData[0].UserID;
      this.branchid = parseUserData[0].BranchID;
      this.getDealerID(this.userid);
    }

    //city
    fetch(apiUrlcity)
      .then(res => res.json())
      .then(
        result => {
          if (result.length > 0) {
            this.setState({
              lcity: result
            });
          } else {
            console.log("city api");
          }
        },
        error => {
          this.setState({ error });
        }
      );

    this.getTotalAdminSummary(this.branchid);
  }

  getDealerID = (userid) => {
    const url = `${apiBaseURL}/user?UserID=${userid}`;
    axios
      .get(url)
      .then((res) => {
        this.setState({
          userData: res.data,
          rolename: res.data[0].RoleName,
          dealerid: res.data[0].CARegistrationID
        });
      })
      .catch((error) => {
        console.log(error, "userdata api");
      });
  }


  getTotalAdminSummary = branchid => {
    if (this.isTotalAdminSummaryLoading) {
      return;
    }
    this.isTotalAdminSummaryLoading = true;

    const url = `${apiBaseURL}/TotalAdminSummary/totaluseradminbranch?BranchID=${branchid}`;
    axios
      .get(url)
      .then(res => {
        if (res.data.length > 0) {
          this.isTotalAdminSummaryLoading = false;
          this.setState({
            TotalAdminSummaryData: res.data
          });
        } else {
          console.log(res.data, "res.data");
        }
      })
      .catch(error => {
        this.isTotalAdminSummaryLoading = false;
        console.log(error, "TotalAdminSummaryData api");
      });
  };

  handleGenderChange = gender => {
    this.setState({
      selGender: gender
    });
  };

  handleRoleChange = roleid => {
    this.setState({
      selRole: roleid
    });
  };

  handleFirstNameChange = event => {
    this.setState({ firstname: event.target.value });
  };

  handleLastNameChange = event => {
    this.setState({ lastname: event.target.value });
  };

  handlEmailChange = event => {
    this.setState({ email: event.target.value });
  };

  handleCNICChange = event => {
    this.setState({ cnic: event.target.value });
  };

  handleDOBChange = date => {
    var newdate = this.dateMaker(date);
    this.setState({
      firstStartDate: newdate,
      startDate: date
    });
  };

  //Date Maker Function
  dateMaker = date => {
    var dateInString = date.toString();
    var dateInSplit = dateInString.split("GMT");
    //var dateParts = dateInSplit[1].split(' ');
    var day = ("0" + date.getDate()).slice(-2);
    var monthIndex = ("0" + (date.getMonth() + 1)).slice(-2);
    var year = date.getFullYear();
    var seconds = (date.getSeconds() < 10 ? "0" : "") + date.getSeconds();
    var minutes = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
    var hour = ("0" + date.getHours()).slice(-2);
    var newDate = year + "-" + monthIndex + "-" + day;
    this.setState({
      startDate: newDate
    });
    return newDate;
  };

  handleCityChange = event => {
    if (event.target.value === "") {
      return;
    }

    this.setState({ scity: event.target.value });
  };

  handleMobileChange = event => {
    this.setState({ mobile: event.target.value });
  };

  handleCreateUser = event => {
    event.preventDefault();
    console.log(
      this.state.selGender,
      this.state.firstname,
      this.state.lastname,
      this.state.email,
      this.state.cnic,
      this.state.firstStartDate,
      this.state.scity,
      this.state.mobile,
    );
    const url = `${apiBaseURL}/user`;
    let ApiParamForCreateUser = {
      FirstName: this.state.firstname,
      MiddleName: 'NA',
      LastName: this.state.lastname,
      Email: this.state.email,
      Mobile: this.state.mobile,
      aqamaNo_CNIC: this.state.cnic,
      Phone: 'NA',
      Password: '',
      Address1: 'NA',
      Address2: 'NA',
      UserTypeID: 2,
      CountryID: 184,
      CityID: this.state.scity,
      Area: 0,
      IsActive: 'false',
      CreatedBy: this.userid,
      CreatedDate: '1900-01-01T00:00:00',
      DOB: this.state.firstStartDate,
      Registeredat: "ca",
      ImageUrl: "NA",
      Gender: this.state.selGender,
      BranchID: 2,
      CARegistrationID: this.state.rolename === "Owner" ? this.state.dealerid : 0,
      RoleID: this.state.rolename === "Owner" ? this.state.selRole : 5
    };
    axios
      .post(url, ApiParamForCreateUser)
      .then(res => {
        console.log(res.data, "CreateUser api");

        if (res.status === 200) {
          this.setState({
            registerid: res.data
          });
          this.handleUserResetPasswordApi(res.data, this.state.email);
        } else {
          console.log(res.status, "statuscode from CreateUser api");
        }
      })
      .catch(error => {
        this.setState({
          showModal: true,
          showMessage: 'User Already Register',
        });
        console.log(error, "CreateUser api");
      });
  };

  handleUserResetPasswordApi = (userid, useremail) => {
    const url = `${apiBaseURL}/UserResetPassword/forgetpassword?UserID=${userid}&Email=${useremail}`;
    axios
      .post(url)
      .then((res) => {
        console.log(res.data, "UserResetPassword");

        if (res.status === 200) {
          this.setState({
            status: res.status,
            showModal: true,
            showMessage: "Please check your email and click on the provided link to reset your password.",
          });
        } else {
          console.log(res.status, "statuscode from UserResetPassword api");
        }
      })
      .catch((error) => {
        this.setState({
          showModal: true,
          showMessage: "Something Went Wrong...",
        });
        console.log(error, "from UserResetPassword api");
      });
  };

  //check user already register or not
  handleBlurRegEmail = () => {
    const url = `${apiBaseURL}/userlogin?Email=${this.state.email}`;
    axios
      .get(url)
      .then((res) => {
        if (res.status === 200) {
          console.log(res, "check user register");
          this.setState({
            showMessage: 'User Already Register',
            isuserregester: true,
          });
        } else {
          this.setState({
            showMessage: '',
            isuserregester: false,
          });
          console.log(res.status, "statuscode from userlogin api");
        }
      })
      .catch((error) => {
        this.setState({
          showMessage: '',
          isuserregester: false,
        });
        console.log(error, "from userlogin api");
      });
  };

  showCreateUser = () => {
    return (
      <div className="row">
        <div className="col-12 px-0">
          <div className="list-heading">Create User</div>
        </div>

        <form onSubmit={this.handleCreateUser} style={{ width: "100%" }}>
          <div className="col-12">
            <div className="mt-0">
              <div className="">&nbsp;</div>

              {this.state.rolename === "Owner" ? (
                <>
                  <div className="container mt-3">
                    <div style={{ textAlign: "left" }}>
                      <span
                        style={{
                          fontSize: "16px",
                          fontWeight: "600",
                          paddingRight: "15px"
                        }}
                      >
                        Select User Role
                  </span>
                      {this.state.UserRole.map((role, index) => (
                        <label
                          key={index}
                          className="material-icons"
                          style={{ marginRight: "5%" }}
                        >
                          <input
                            value={role.id}
                            checked={this.state.selRole === role.id}
                            onChange={() => this.handleRoleChange(role.id)}
                            type="radio"
                          />
                          <span
                            className="px-4 rounded-0"
                            style={{ padding: "10px" }}
                          >
                            {role.value}
                          </span>
                        </label>
                      ))}
                    </div>
                  </div>
                  <hr />
                </>)
                :
                ''
              }

              <div className="container mt-3">
                <div style={{ textAlign: "left" }}>
                  <span
                    style={{
                      fontSize: "16px",
                      fontWeight: "600",
                      paddingRight: "15px"
                    }}
                  >
                    Select Gender
                  </span>
                  {this.state.Gender.map((gender, index) => (
                    <label
                      key={index}
                      className="material-icons"
                      style={{ marginRight: "5%" }}
                    >
                      <input
                        value={gender.value}
                        checked={this.state.selGender === gender.value}
                        onChange={() => this.handleGenderChange(gender.value)}
                        type="radio"
                      />
                      <span style={{ padding: "20px 18px 10px 18px" }}>
                        <i className={gender.icon}></i>
                      </span>
                    </label>
                  ))}
                </div>
              </div>
              <hr />
              <div className="row">
                <div className="col-6">
                  <ul id="signup" className="logmod__tabs">
                    <li>
                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>First Name *</label>
                          <input
                            placeholder="First Name"
                            required
                            className="form-control address ml-3"
                            type="text"
                            onChange={this.handleFirstNameChange}
                            value={this.state.firstname}
                          />
                        </div>
                      </div>

                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>Last Name *</label>
                          <input
                            placeholder="Last Name"
                            required
                            className="form-control address ml-3"
                            type="text"
                            onChange={this.handleLastNameChange}
                            value={this.state.lastname}
                          />
                        </div>
                      </div>
                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>Email *</label>
                          <input
                            placeholder="Email"
                            required
                            className="form-control address ml-3"
                            type="email"
                            onChange={this.handlEmailChange}
                            value={this.state.email}
                            onBlur={this.handleBlurRegEmail}
                          />
                        </div>
                      </div>
                      <div
                        style={{ display: this.state.isuserregester ? "block" : "none" }}
                        className="email-verify-status"
                        id="email-verify-status"
                      >
                        {this.state.showMessage}
                      </div>

                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>CNIC Number</label>
                          <input
                            placeholder="CNIC"
                            required
                            className="form-control address ml-3"
                            type="text"
                            onChange={this.handleCNICChange}
                            value={this.state.cnic}
                          />
                        </div>
                      </div>

                    </li>
                  </ul>
                </div>
                <div className="col-6">
                  <ul className="logmod__tabs">
                    <li>
                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>Date of Birth *</label>
                          <div className="d-flex">
                            <div className="form-group">
                              <div className="string optional">
                                <DatePicker
                                  placeholderText="yyyy/mm/dd"
                                  value={this.state.firstStartDate}
                                  selected={this.state.startDate}
                                  onChange={this.handleDOBChange}
                                  peekNextMonth
                                  showMonthDropdown
                                  showYearDropdown
                                  dropdownMode="select"
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>Mobile Phone Number</label>
                          <input
                            placeholder="Phone"
                            required
                            className="form-control address ml-3"
                            type="number"
                            onChange={this.handleMobileChange}
                            value={this.state.mobile}
                          />
                        </div>
                      </div>

                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>City</label>
                          <select
                            required
                            style={{
                              width: "100%",
                              background: "none",
                              border: "none"
                            }}
                            onChange={this.handleCityChange}
                            value={this.state.scity}
                          >
                            <option value="" selected>
                              Select City
                      </option>                            {this.state.lcity.map(city => (
                                <option key={city.CityID} value={city.CityID}>
                                  {city.CityName}
                                </option>
                              ))}
                          </select>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <ul className="mb-5">
                <li>
                  <div className="simform__actions">
                    <input
                      className="sumbit btn btn-block mt-3"
                      type="submit"
                      value="Create User"
                    />
                  </div>
                </li>
              </ul>
            </div>
          </div>
          ;
        </form>
      </div>
    );
  };

  showTotalAdminSummary = () => {
    return (
      <Fragment>
        <aside className="l-sidebar">
          <div className="widget section-sidebar">
            <div className="widget-content">
              <div className="d-flex justify-content-between px-3">
                <Link
                  style={{ color: "#D3E428", fontWeight: "700" }}
                  to="/my-users"
                >
                  View Users
                </Link>
              </div>
              <hr />
              {this.state.TotalAdminSummaryData.map(
                (totaladminsummary, index) => (
                  <Fragment key={index}>
                    <div className="d-flex justify-content-between px-3">
                      <strong>Active Users</strong>
                      <span>{totaladminsummary.Active}</span>
                    </div>
                    <hr />
                    <div className="d-flex justify-content-between px-3">
                      <strong>Disabled Users</strong>
                      <span>{totaladminsummary.Inactive}</span>
                    </div>
                    <hr />
                  </Fragment>
                )
              )}
            </div>
            <div>
              <Dealeradvertisement />
            </div>
          </div>
        </aside>
      </Fragment>
    );
  };

  handleCloseModal = () => {
    if (this.state.status === 200) {
      this.setState({
        showModal: false
      });
      this.props.history.push("/my-users");
    } else {
      this.setState({
        showModal: false
      });
    }
  }
  showModal = () => {
    return (
      <Modal show={this.state.showModal} onHide={this.handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Cararabiya</Modal.Title>
        </Modal.Header>
        <Modal.Body>{this.state.showMessage}</Modal.Body>
      </Modal>
    );
  }
  render() {
    return (
      <Fragment>
        {this.showModal()}
        <div className="row">
          <div className="col-lg-3">{this.showTotalAdminSummary()}</div>

          <div className="col-lg-9">{this.showCreateUser()}</div>
        </div>
      </Fragment>
    );
  }
}
export default CreateUserDetails;