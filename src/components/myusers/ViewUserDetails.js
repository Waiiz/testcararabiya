import React, { Component, Fragment } from "react";
import { apiBaseURL, apiUrlcity } from "../../ApiBaseURL";
import axios from "axios";
import { Link } from "react-router-dom";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Dealeradvertisement from "../../pages/Dealeradvertisement";

class ViewUserDetails extends Component {
  constructor(props) {
    super(props);
    this.isTotalAdminSummaryLoading = false;
    this.usertypeid = "";
    this.branchid = "";
    this.userid = "";
    this.isUserDataLoading = false;
    this.state = {
      TotalAdminSummaryData: [],
      userData: [],
      //gender
      Gender: [
        { id: 1, value: "Male", icon: "fas fa-male fa-2x" },
        { id: 2, value: "Female", icon: "fas fa-female fa-2x" }
      ],
      selGender: "",
      firstname: "",
      lastname: "",
      address: "",
      firstStartDate: null,
      startDate: null,
      //city
      lcity: [],
      scity: ""
    };
  }

  componentDidMount() {
    if (this.props.history.location.state !== undefined) {
      this.setState({
        lsuserid: this.props.history.location.state.userid
      });
      this.getUserDataById(this.props.history.location.state.userid);
    } else {
      this.props.history.push("./my-users");
    }

    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      this.userid = parseUserData[0].UserID;
      this.branchid = parseUserData[0].BranchID;

      let getfirstusertypechar = parseUserData[0].UserTypeID.toString();
      this.usertypeid = getfirstusertypechar.substring(0, 1);
    }

     //city
     fetch(apiUrlcity)
     .then((res) => res.json())
     .then(
       (result) => {
         if (result.length > 0) {
           this.setState({
             lcity: result,
           });
         } else {
           console.log("city api");
         }
       },
       (error) => {
         this.setState({ error });
       }
     );

    this.getTotalAdminSummary(this.branchid);
  }

  getTotalAdminSummary = branchid => {
    if (this.isTotalAdminSummaryLoading) {
      return;
    }
    this.isTotalAdminSummaryLoading = true;

    const url = `${apiBaseURL}/TotalAdminSummary/totaluseradminbranch?BranchID=${branchid}`;
    axios
      .get(url)
      .then(res => {
        if (res.data.length > 0) {
          this.isTotalAdminSummaryLoading = false;
          this.setState({
            TotalAdminSummaryData: res.data
          });
        } else {
          console.log(res.data, "res.data");
        }
      })
      .catch(error => {
        this.isTotalAdminSummaryLoading = false;
        console.log(error, "TotalAdminSummaryData api");
      });
  };

  getUserDataById = userid => {
    if (this.isUserDataLoading) {
      return;
    }
    this.isUserDataLoading = true;

    const url = `${apiBaseURL}/user?UserID=${userid}`;
    axios
      .get(url)
      .then(res => {
        this.isUserDataLoading = false;
        var splitTimeDateWithT = res.data[0].DOB.split("T");
        var date = splitTimeDateWithT[0]; //T00:00:00
        this.setState({
          userData: res.data,
          UserID: res.data[0].UserID,

          firstname: res.data[0].FirstName,
          lastname: res.data[0].LastName,
          firstStartDate: date,
          selGender: res.data[0].Gender,
          scity: res.data[0].CityID,
          address: res.data[0].Address1,
          ImageUrl: res.data[0].ImageUrl
        });
      })
      .catch(error => {
        this.isUserDataLoading = false;
        console.log(error, "userdata api");
      });
  };

  handleGenderChange = gender => {
    console.log(gender, "gender");
    this.setState({
      selGender: gender
    });
  };

  handleFirstNameChange = event => {
    this.setState({ firstname: event.target.value });
  };

  handleLastNameChange = event => {
    this.setState({ lastname: event.target.value });
  };

  handleAddressChange = event => {
    this.setState({ address: event.target.value });
  };

  handleDOBChange = date => {
    var newdate = this.dateMaker(date);
    this.setState({
      firstStartDate: newdate,
      startDate: date
    });
  };

  //Date Maker Function
  dateMaker = date => {
    var dateInString = date.toString();
    var dateInSplit = dateInString.split("GMT");
    //var dateParts = dateInSplit[1].split(' ');
    var day = ("0" + date.getDate()).slice(-2);
    var monthIndex = ("0" + (date.getMonth() + 1)).slice(-2);
    var year = date.getFullYear();
    var seconds = (date.getSeconds() < 10 ? "0" : "") + date.getSeconds();
    var minutes = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
    var hour = ("0" + date.getHours()).slice(-2);
    var newDate = year + "-" + monthIndex + "-" + day;
    this.setState({
      startDate: newDate
    });
    return newDate;
  };

  handleCityChange = event => {
    this.setState({ scity: event.target.value });
  };

  handleEditProfile = event => {
    console.log(
      this.state.selGender,
      this.state.firstname,
      this.state.lastname,
      this.state.address,
      this.state.firstStartDate,
      this.state.scity
    );
    event.preventDefault();
    const url = `${apiBaseURL}/user`;
    let ApiParamForEditProfile = {
      UserID: this.state.UserID,
      FirstName: this.state.firstname,
      LastName: this.state.lastname,
      DOB: this.state.firstStartDate,
      CountryID: 184,
      CityID: this.state.scity,
      Address1: this.state.address,
      ImageUrl: this.state.ImageUrl,
      Gender: this.state.selGender,
      DealerID: 0,
    };
    axios
      .post(url, null, { params: ApiParamForEditProfile })
      .then(res => {
        console.log(res.data, "editprofile api");

        if (res.status === 200) {
          this.setState({
            editProfileSuccess: res.data
          });
          this.props.history.push({
            pathname: "/my-users",
          });
        } else {
          console.log(res.status, "statuscode from editprofile api");
        }
      })
      .catch(error => {
        console.log(error, "editprofile api");
      });
  };

  showEditUser = () => {
    return (
      <div className="row">
        <div className="col-12 px-0">
          <div className="list-heading">View Profile</div>
        </div>

        <form onSubmit={this.handleEditProfile} style={{ width: "100%" }}>
          <div className="col-12">
            <div className="mt-0">
              <div className="">&nbsp;</div>
              <div className="container mt-3">
                <div style={{ textAlign: "left" }}>
                  <span
                    style={{
                      fontSize: "16px",
                      fontWeight: "600",
                      paddingRight: "15px"
                    }}
                  >
                    Gender
                  </span>
                  {this.state.Gender.map((gender, index) => (
                    <label
                      key={index}
                      className="material-icons"
                      style={{ marginRight: "5%" }}
                    >
                      <input
                        disabled
                        value={gender.value}
                        checked={this.state.selGender === gender.value}
                        //onChange={() => this.handleGenderChange(gender.value)}
                        type="radio"
                      />
                      <span style={{ padding: "20px 18px 10px 18px" }}>
                        <i className={gender.icon}></i>
                      </span>
                    </label>
                  ))}
                </div>
              </div>
              <hr />
              <div className="row">
                <div className="col-6">
                  <ul id="signup" className="logmod__tabs">
                    <li>
                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>First Name</label>
                          <input
                            required
                            className="form-control address ml-3"
                            type="text"
                            //onChange={this.handleFirstNameChange}
                            disabled
                            value={this.state.firstname}
                          />
                        </div>
                      </div>

                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>Last Name</label>
                          <input
                            required
                            disabled
                            className="form-control address ml-3"
                            type="text"
                            onChange={this.handleLastNameChange}
                            value={this.state.lastname}
                          />
                        </div>
                      </div>
                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>Address</label>
                          <input
                            disabled
                            required
                            className="form-control address ml-3"
                            type="text"
                            onChange={this.handleAddressChange}
                            value={this.state.address}
                          />
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
                <div className="col-6">
                  <ul className="logmod__tabs">
                    <li>
                      <div className="sminputs">
                        <div className="input" style={{ width: "100%" }}>
                          <label>Date of Birth *</label>
                          <div className="d-flex">
                            <div className="form-group">
                              <div className="string optional">
                                <DatePicker
                                  disabled
                                  value={this.state.firstStartDate}
                                  selected={this.state.startDate}
                                  onChange={this.handleDOBChange}
                                  peekNextMonth
                                  showMonthDropdown
                                  showYearDropdown
                                  dropdownMode="select"
                                  //dateFormat="YY-MM-DD"
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="sminputs">
                        <div className="input" style={{ width: "100% !important" }}>
                          <label>City</label>
                          <select
                            disabled
                            required
                            style={{
                              width: "100% !important",
                              background: "none",
                              border: "none"
                            }}
                            onChange={this.handleCityChange}
                            value={this.state.scity}
                          >
                            {this.state.lcity.map(city => (
                              <option key={city.CityID} value={city.CityID}>
                                {city.CityName}
                              </option>
                            ))}
                          </select>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <ul className="mb-5">
                <li>
                  <div className="simform__actions">
                  <Link
                to={{
                  pathname: "/my-users",
                }}
                className="sumbit btn btn-block mt-3"
                style={{ width: "100%" }}
              >
                View Users
              </Link>    

                  </div>
                </li>
              </ul>
            </div>
          </div>
          ;
        </form>
      </div>
    );
  };

  showTotalAdminSummary = () => {
    return (
      <Fragment>
        <aside className="l-sidebar">
          <div className="widget section-sidebar">
            <div className="widget-content">
              <div className="d-flex justify-content-between px-3">
                <Link
                  style={{ color: "#D3E428", fontWeight: "700" }}
                  to="/my-users"
                >
                  View Users
                </Link>
              </div>
              <hr />
              {this.state.TotalAdminSummaryData.map(
                (totaladminsummary, index) => (
                  <Fragment key={index}>
                    <div className="d-flex justify-content-between px-3">
                      <strong>Active Users</strong>
                      <span>{totaladminsummary.Active}</span>
                    </div>
                    <hr />
                    <div className="d-flex justify-content-between px-3">
                      <strong>Disabled Users</strong>
                      <span>{totaladminsummary.Inactive}</span>
                    </div>
                    <hr />
                  </Fragment>
                )
              )}
            </div>
            <div>
              <Dealeradvertisement />
            </div>
          </div>
        </aside>
      </Fragment>
    );
  };

  render() {
    return (
      <Fragment>
        <div className="row">
          <div className="col-lg-3">{this.showTotalAdminSummary()}</div>

          <div className="col-lg-9">{this.showEditUser()}</div>
        </div>
      </Fragment>
    );
  }
}
export default ViewUserDetails;