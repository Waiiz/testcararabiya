import React, { Component, Fragment } from "react";
import { apiBaseURL } from "../../ApiBaseURL";
import axios from "axios";
import ReactPaginate from "react-paginate";
import "../listing/paginate.css";
import _ from "lodash";
import { Link } from "react-router-dom";
import Loader from "../../pages/Loader";
import ErrorAlert from "../../pages/ErrorAlert";
import Switch from "react-switch";
import Dealeradvertisement from "../../pages/Dealeradvertisement";

class UserDetails extends Component {
  constructor(props) {
    super(props);
    this.isTotalAdminSummaryLoading = false;
    this.isFetchingDataforEnableBranchUser = false;

    this.usertypeid = "";
    this.branchid = "";
    this.userid = "";
    this.state = {
      TotalAdminSummaryData: [],
      //userlisting
      isLoadingForUserListing: false,
      UserlistingData: [],
      GlobalUserlistingData: [],
      //pagination
      offset: 0,
      perPage: 7,
      currentPage: 0,
      listingData: [],
      toggleAlertMsg: false,

      start: "",
      end: "",
      totalResults: "",

      //sortby
      SortUserListing: [
        { id: 1, value: "Newest to Oldest", method: "new" },
        { id: 2, value: "Oldest to Newest", method: "old" },
      ],
      selSortUserValue: "",
    };
  }



  componentDidMount() {
    let userData = localStorage.getItem("user");
    let parseUserData = JSON.parse(userData);
    if (parseUserData !== null) {
      this.userid = parseUserData[0].UserID;
      this.usertypeid = parseUserData[0].UserTypeID;
      this.branchid = parseUserData[0].BranchID;
      this.getDealerID(this.userid, this.branchid, this.usertypeid);
    }
    this.getTotalAdminSummary(this.branchid);
  }

  getDealerID = (userid, branchid, usertypeid) => {
    this.setState({ isLoadingForUserListing: true });
    const url = `${apiBaseURL}/user?UserID=${userid}`;
    axios
      .get(url)
      .then((res) => {
        console.log(res.data);
        this.setState({
          userData: res.data,
        });
        this.getUserByRegIDORBrnchID(res.data[0].RoleName, branchid, res.data[0].CARegistrationID, usertypeid);
      })
      .catch((error) => {
        console.log(error, "userdata api");
      });
  }

  getUserByRegIDORBrnchID = (rolename, branchid, dealerid, usertypeid) => {
    let url = '';
    if (rolename === "Owner") {
      url = `${apiBaseURL}/User/GetUserByRegistrationID?RegistrationID=${dealerid}&UTypeID=${usertypeid}`;
    }
    else {
      url = `${apiBaseURL}/User/UserbyBranchID?BranchID=${branchid}`;
    }
    axios
      .get(url)
      .then((res) => {
        console.log(res, "UserbyBranchID");
        this.setState({
          GlobalUserlistingData: res.data,
        });
        this.setState({ isLoadingForUserListing: false });
        this.receivedData(res.data);
      })
      .catch((error) => {
        this.setState({
          start: 0,
          end: 0,
          offset: 0,
          perPage: 7,
          currentPage: 0,
          totalResults: 0,
          toggleAlertMsg: true,
          listingData: [],
          isLoadingForUserListing: false,
        });
        console.log(error, "from UserbyBranchID api");
      });

  }

  getTotalAdminSummary = (branchid) => {
    if (this.isTotalAdminSummaryLoading) {
      return;
    }
    this.isTotalAdminSummaryLoading = true;

    const url = `${apiBaseURL}/TotalAdminSummary/totaluseradminbranch?BranchID=${branchid}`;
    axios
      .get(url)
      .then((res) => {
        if (res.data.length > 0) {
          this.isTotalAdminSummaryLoading = false;
          this.setState({
            TotalAdminSummaryData: res.data,
          });
        } else {
          console.log(res.data, "res.data");
        }
      })
      .catch((error) => {
        this.isTotalAdminSummaryLoading = false;
        console.log(error, "TotalAdminSummaryData api");
      });
  };

  receivedData = (userlisting) => {
    if (userlisting.length > 0) {
      this.setState({
        UserlistingData: userlisting,
        totalResults: userlisting.length,
        toggleAlertMsg: false,
      });

      const data = userlisting;

      if (this.state.offset + 1 + this.state.perPage > userlisting.length) {
        this.setState({
          end: userlisting.length,
        });
      } else {
        this.setState({
          end: this.state.offset + this.state.perPage,
        });
      }

      const slice = data.slice(
        this.state.offset,
        this.state.offset + this.state.perPage
      );

      const listingData = slice.map((userlist, index) =>
        this.showUserListing(userlist, index)
      );

      this.setState({
        pageCount: Math.ceil(data.length / this.state.perPage),
        start: this.state.offset + 1,
        listingData,
      });
    } else {
      this.setState({
        start: 0,
        end: 0,
        offset: 0,
        perPage: 7,
        currentPage: 0,
        totalResults: 0,
        toggleAlertMsg: true,
        listingData: [],
      });
    }
  };

  extractDate = (datetime) => {
    let splitTimeDateWithT = datetime.split("T");
    var date = splitTimeDateWithT[0];
    return date;
  };

  //handle Change Enable IsActive 
  handleEn_Ds_User = (userlist, index) => {
    var userchecked = !userlist.IsActive;
    userlist.IsActive = userchecked;

    if (!this.isFetchingDataforEnableBranchUser) {
      this.UpdateIsActivebyUserID(userlist, index);
    }
  };

  UpdateIsActivebyUserID = (userlist, index) => {
    this.isFetchingDataforEnableBranchUser = true;

    const url = `${apiBaseURL}/User/En_Ds_BranchUser`;
    let ApiParamForIsActiveUser = {
      dBUserID: userlist.UserID,
      isActive: userlist.IsActive,
    };
    axios
      .post(url, null, { params: ApiParamForIsActiveUser })
      .then((res) => {
        this.isFetchingDataforEnableBranchUser = false;
        console.log(res, "En_Ds_BranchUser api");

        if (res.status === 200) {
          var mylistinguserdata = this.state.UserlistingData;

          var getIndex = mylistinguserdata.indexOf(mylistinguserdata[index]);

          if (getIndex > -1) {
            mylistinguserdata.splice(getIndex, 1, userlist);
          }

          this.setState({
            UserlistingData: mylistinguserdata,
          });

          this.receivedData(this.state.UserlistingData);
          this.getTotalAdminSummary(this.branchid);
        } else {
          this.isFetchingDataforEnableBranchUser = false;
          console.log(
            res.status,
            "statuscode from En_Ds_BranchUser api"
          );
        }
      })
      .catch((error) => {
        this.isFetchingDataforEnableBranchUser = false;
        console.log(error, "En_Ds_BranchUser api");
      });
  };

  showUserListing = (userlist, index) => {
    return (
      <Fragment key={index}>
        <div className="bg-shadow b-goods-f col-12 b-box row mx-0 mt-4">
          <div className="col-3 col-lg-2 px-0 mb-3 pr-2">
            <div className="user-title">
              <div className="d-flex justify-content-between px-3">
                <span>Name</span>
              </div>
            </div>
            <div className="user-title">
              <div className="d-flex justify-content-between px-3">
                <span>Email</span>
              </div>
            </div>
            <div className="user-title">
              <div className="d-flex justify-content-between px-3">
                <span>Mobile no.</span>
              </div>
            </div>
          </div>
          <div className="col-9 col-lg-8 px-0 mb-3">
            <div className="b-table">
              <div className="d-flex justify-content-between px-3">
                <span>{userlist.FirstName} {userlist.LastName}</span>
              </div>
            </div>
            <div className="b-table">
              <div className="d-flex justify-content-between px-3">
                <span>{userlist.Email}</span>
              </div>
            </div>
            <div className="b-table">
              <div className="d-flex justify-content-between px-3">
                <span>{userlist.Mobile}</span>
              </div>
            </div>
          </div>
          <div
            className="col-12 col-lg-2 justify-content-end align-items-center align-self-center"
            style={{ marginBottom: '20px' }}
          >
            <label
            >
              <Switch
                value={userlist.IsActive}
                onChange={() => this.handleEn_Ds_User(userlist, index)}
                checked={userlist.IsActive}
                onColor="#eee"
                onHandleColor="#D3E428"
                handleDiameter={20}
                uncheckedIcon={false}
                checkedIcon={false}
                boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                height={20}
                width={40}
                className="react-switch"
                id="material-switch"
              />

              <span
                style={{
                  position: "relative",
                  bottom: "5px",
                  left: "5px"
                }}
              >
                {userlist.IsActive === true ? "Enable" : "Disable"}
              </span>
            </label>
            <div className="mt-3">
              {userlist.IsActive === true ?
                <Link
                  to={{
                    pathname: "/edit-user",
                    state: { userid: userlist.UserID },
                  }}
                  className="btn btn-primary btn-sm btn-block color-arabiya"
                  style={{ backgroundColor: "#D3E428" }}
                >
                  Edit
              </Link>
                :
                <Link
                  to={{
                    pathname: "/view-user",
                    state: { userid: userlist.UserID },
                  }}
                  className="btn btn-primary btn-sm btn-block color-arabiya"
                  style={{ backgroundColor: "#D3E428" }}
                >
                  View
              </Link>
              }
            </div>
          </div>
          <div
            className="col-12 px-0 mt-2"
            style={{ borderTop: '1px solid #D3E428', paddingTop: '5px' }}
          >
            <div className="ml-2" style={{ color: 'grey', fontSize: '0.9em' }}>
              <span className="ml-2">
                <b>Date created:</b> {this.extractDate(userlist.CreatedDate)}
              </span>

              <span className="ml-2">
                <b>Status:</b> {userlist.IsActive === true ? 'Active' : 'Inactive'}
              </span>
            </div>
          </div>
        </div>
      </Fragment>
    );
  };

  handlePageClick = (e) => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.perPage;

    this.setState(
      {
        currentPage: selectedPage,
        offset: offset,
      },
      () => {
        this.receivedData(this.state.UserlistingData);
      }
    );
  };

  showTotalAdminSummary = () => {
    return (
      <Fragment>
        <aside className="l-sidebar">
          <div className="widget section-sidebar">
            <div className="widget-content">
              <div className="d-flex justify-content-between px-3">
                <Link
                  style={{ color: "#D3E428", fontWeight: "700" }}
                  to="/create-user"
                >
                  Create User
                </Link>
              </div>
              <hr />
              {this.state.TotalAdminSummaryData.map(
                (totaladminsummary, index) => (
                  <Fragment key={index}>
                    <div className="d-flex justify-content-between px-3">
                      <strong>Active Users</strong>
                      <span>{totaladminsummary.Active}</span>
                    </div>
                    <hr />
                    <div className="d-flex justify-content-between px-3">
                      <strong>Disabled Users</strong>
                      <span>{totaladminsummary.Inactive}</span>
                    </div>
                    <hr />
                  </Fragment>
                )
              )}
            </div>
            <div>
              <Dealeradvertisement />
            </div>
          </div>
        </aside>
      </Fragment>
    );
  };

  showUserlistHeader = () => {
    return (
      <div className="row">
        <div className="col-8 px-0">
          <div className="list-heading">User List</div>
          <div className="b-filter-goods__info col-auto">
            Showing
            <strong>
              {" "}
              {this.state.start} - {this.state.end}{" "}
            </strong>{" "}
            out of <strong> {this.state.totalResults}</strong> listings
          </div>
        </div>

        <div className="col-4">
          <div
            className="b-filter-goods__wrap col-auto"
            style={{ float: "right", padding: 0 }}
          >
            <div className="b-filter-goods__select d-inline-flex">
              <select
                style={{ inlineSize: "auto" }}
                className="custom-select-adpost"
                onChange={this.handleSortByUserListing}
                value={this.state.selSortUserValue}
              >
                <option selected>Sort By</option>
                {this.state.SortUserListing.map((sortuser) => (
                  <option key={sortuser.id} value={sortuser.method}>
                    {sortuser.value}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
      </div>
    );
  };

  //sortby
  handleSortByUserListing = (event) => {
    this.setState({ selSortUserValue: event.target.value });
    let PromoData = [];
    PromoData = this.state.GlobalUserlistingData;

    if (event.target.value === "new") {
      PromoData = _.orderBy(PromoData, ["CreatedDate"], ["desc"]);
    } else if (event.target.value === "old") {
      PromoData = _.orderBy(PromoData, ["CreatedDate"], ["asc"]);
    } else {
      console.log("sort error");
    }

    this.setState({ UserlistingData: PromoData });
    this.receivedData(PromoData);
  };

  render() {
    return (
      <Fragment>
        <div className="row">
          <div className="col-lg-3">{this.showTotalAdminSummary()}</div>

          <div className="col-lg-9">
            {this.showUserlistHeader()}
            <br />

            {this.state.toggleAlertMsg === true ? (
              <ErrorAlert show={this.state.toggleAlertMsg} />
            ) : (
                <Fragment>
                  {this.state.isLoadingForUserListing ? (
                    <Loader />
                  ) : (
                      <span>
                        {this.state.listingData}
                        <br />
                        <ReactPaginate
                          previousLabel={"prev"}
                          nextLabel={"next"}
                          breakLabel={"..."}
                          breakClassName={"break-me"}
                          pageCount={this.state.pageCount}
                          marginPagesDisplayed={2}
                          pageRangeDisplayed={5}
                          onPageChange={this.handlePageClick}
                          containerClassName={"pagination"}
                          subContainerClassName={"pages pagination"}
                          activeClassName={"active"}
                          forcePage={this.state.currentPage}
                        />
                      </span>
                    )}
                </Fragment>
              )}
          </div>
        </div>
      </Fragment>
    );
  }
}
export default UserDetails;