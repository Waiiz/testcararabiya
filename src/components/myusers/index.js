import UserDetails from './UserDetails';
import CreateUserDetails from './CreateUserDetails';
import EditUserDetails from './EditUserDetails';
import ViewUserDetails from './ViewUserDetails';

export {
    UserDetails, CreateUserDetails, EditUserDetails, ViewUserDetails
};