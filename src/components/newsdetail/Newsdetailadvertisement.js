import React, { Component } from 'react';

class Newsdetailadvertisement extends Component {
    render() {
        const image1="./assets/media/content/ad/ad1.jpg";
        return (
            <div>
                 <div className="container pt-3">
              <div className="col-12"><h4 className="ui-title-ad">Advertisement</h4></div>
              <div className="b-gallery js-slider" data-slick="{&quot;slidesToShow&quot;: 1, &quot;arrows&quot;: false, &quot;autoplay&quot;: true,  &quot;slidesToScroll&quot;: 2, &quot;rows&quot;: 1, &quot;responsive&quot;: [{&quot;breakpoint&quot;: 1400, &quot;settings&quot;: {&quot;slidesToShow&quot;: 1, &quot;slidesToScroll&quot;: 3}}, {&quot;breakpoint&quot;: 768, &quot;settings&quot;: {&quot;slidesToShow&quot;: 1, &quot;slidesToScroll&quot;: 1}}]}">
                <div className="ad"><img src="./assets/media/content/ad/ad1.jpg" /></div>
                <div className="ad"><img src="./assets/media/content/ad/ad2.jpg" /></div>
                <div className="ad"><img src="./assets/media/content/ad/ad3.jpg" /></div>
                <div className="ad"><img src="./assets/media/content/ad/ad4.jpg" /></div>
                <div className="ad"><img src="./assets/media/content/ad/ad1.jpg" /></div>
                <div className="ad"><img src="./assets/media/content/ad/ad2.jpg" /></div>
                <div className="ad"><img src="./assets/media/content/ad/ad3.jpg" /></div>
                <div className="ad"><img src="./assets/media/content/ad/ad4.jpg" /></div>
              </div>
              <br /><br />
            </div>
            </div>
        );
    }
}

export default Newsdetailadvertisement;