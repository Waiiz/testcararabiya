import axios from "axios";
import {
  apiBaseURL,
} from "../ApiBaseURL.js";

export const getModalData = async (searchkeyid) => {
  try {
    const urlModal = `${apiBaseURL}/model?vehiclecategoryid=${1}&makeid=${searchkeyid}`;
    const res = await axios.get(urlModal);
    return await res.data;
  }
  catch (err) {
    return console.log('Failed to parse: ', JSON.stringify(err));
  }
}

export const getVersionData = async (searchkeyid) => {
  try {
    const urlVersion = `${apiBaseURL}/vehicleversion?vehiclecategoryid=${1}&modelid=${searchkeyid}`;
    const res = await axios.get(urlVersion);
    return await res.data;
  }
  catch (err) {
    return console.log('Failed to parse: ', JSON.stringify(err));
  }
}